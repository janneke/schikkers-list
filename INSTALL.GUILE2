#! /bin/bash -ex

set -ex
/usr/bin/guile --version

export V=2.0
PATH=~/vc/lilypond/out/bin:$PATH
PATH=~/vc/schikkers-list/scripts:$PATH
mkdir -p $HOME/guile-$V
eval $(PKG_PREFIX=$HOME/guile-$V sh-pkg)

cd $HOME/vc

sudo apt-get -y remove guile-2.2 guile-library g-wrap guile-1.8
sudo apt-get -y build-dep guile-$V guile-gnome2-canvas
sudo apt-get -y install libunistring-dev libgc-dev libffi-dev
sudo apt-get -y install help2man xz-utils
sudo apt-get -y install librsvg2-common

#git config --global url.git+ssh://git.sv.gnu.org/srv/git/.insteadof gnu:
git config --global url.git://git.sv.gnu.org/.insteadof gnu:
git config --global url.git://git.savannah.gnu.org/.insteadof gnu-anon:

if [ ! -d lilypond ]; then
    git clone gnu:lilypond
(
    cd lilypond
    git am $HOME/vc/schikkers-list/patches/0001-Start-OTF-font-from-E800-avoids-hardcoded-linux-unic.patch
    git am $HOME/vc/schikkers-list/patches/0002-output-socket-Implement-path.patch
)
fi

(lilypond --version || echo none) | grep 2.19.25 || (

echo NOT: sudo apt-get -y build-dep lilypond
sudo apt-get -y install dblatex guile-1.8 guile-1.8-dev tex-common texlive-binaries texlive-latex-base texlive-fonts-recommended texlive-lang-cyrillic

cd lilypond
git pull -r
./autogen.sh --noconfigure
./configure --prefix=$HOME/vc/lilypond/out --infodir=$HOME/vc/lilypond/out/share/info --disable-optimising --enable-gui --enable-relocation
make
)

which autoconf
[ ! -d autoconf ] && git clone gnu-anon:autoconf
(((autoconf --version || echo none) | grep -E '2[.](69|70)') \
    && ((automake --version || echo none) | grep -E '1[.](1[4-9][.]|99)')) || (
cd autoconf
autoreconf -i
rm -rf build-$V
mkdir build-$V && cd build-$V
../configure --prefix=$HOME/guile-$V
make
make install
)

eval $(PKG_PREFIX=$HOME/guile-$V sh-pkg)

[ ! -d automake ] && git clone gnu-anon:automake
(automake --version || echo none) | grep -E '1[.](1[4-9][.]|99)' || (
cd automake
./bootstrap.sh --noconfigure
rm -rf build-$V
mkdir build-$V && cd build-$V
../configure --prefix=$HOME/guile-$V
make
make install
)

[ ! -d libtool ] && git clone gnu-anon:libtool
(libtoolize --version || echo none) | grep '2.4.[2-6]' || (
cd libtool
./bootstrap
rm -rf build-$V
mkdir build-$V && cd build-$V
../configure --prefix=$HOME/guile-$V
make
make install
)

# FIXME: shortcut gettext and pkg-config
if [ -d ~/guile-$V/share/aclocal ]; then
    cp /usr/share/aclocal/{gettext,intl,intlmacosx,lib-*,nls,pkg,po,progtest}.m4 ~/guile-$V/share/aclocal/
fi

[ ! -d guile ] && git clone gnu-anon:guile
(guile --version || echo none) | grep "^guile (GNU Guile) 2[.][01][.]" || (
cd guile
git branch | grep stable-$V || git checkout -b stable-$V origin/stable-$V
git branch | grep -e "^* stable-$V" || git checkout stable-$V
./autogen.sh --noconfigure
git reset --hard HEAD
rm -rf build-$V
mkdir build-$V && cd build-$V
../configure --prefix=$HOME/guile-$V
make
make install
)

eval $(PKG_PREFIX=$HOME/guile-$V sh-pkg)

[ ! -d g-wrap ] && git clone gnu-anon:g-wrap.git
($HOME/guile-$V/bin/g-wrap-config --version || echo none) | grep 1.9.15 || (
cd g-wrap
./autogen.sh --noconfigure
rm -rf build-$V
mkdir build-$V && cd build-$V
../configure --prefix=$HOME/guile-$V --disable-Werror
make -k ||:
make -k install ||:
)

[ ! -d guile-cairo ] && git clone gnu:guile-cairo.git
(
cd guile-cairo
./autogen.sh --help
rm -rf build-$V
mkdir build-$V && cd build-$V
../configure --prefix=$HOME/guile-$V
make
make install
)

[ ! -d guile-gnome ] && git clone gnu:guile-gnome.git
(
cd guile-gnome

git log --pretty=oneline | head -10 | grep 'vector->gdk-event' || git am $HOME/vc/schikkers-list/patches/0001-Creation-of-GdkEvent-using-new-function-vector-gdk-e.patch
git log --pretty=oneline | head -10 | grep 'GdkScroll' || git am $HOME/vc/schikkers-list/patches/0002-Implement-GdkScroll.patch
git log --pretty=oneline | head -10 | grep 'integer types of keypress' || git am $HOME/vc/schikkers-list/patches/0003-gdk-support-get-integer-types-of-keypress-right.-Fix.patch
git log --pretty=oneline | head -10 | grep 'Add model-view-control example' || git am $HOME/vc/schikkers-list/patches/0004-Add-model-view-control-example-showing-synthesised-k.patch
git log --pretty=oneline | head -10 | grep 'Comment-out g_main_loop' || git am $HOME/vc/schikkers-list/patches/0005-Comment-out-g_main_loop_new.-Fixes-build.patch

./autogen.sh --noconfigure
rm -rf build-$V
mkdir build-$V && cd build-$V
../configure --prefix=$HOME/guile-$V
make
make install
)


patch < $HOME/vc/schikkers-list/patches/guile-gnome-2.patch $HOME/guile-$V/bin/guile-gnome-2

mkdir -p ~/.fonts
ln -fs ~/vc/lilypond/mf/out/emmentaler-20.otf ~/.fonts
#ln -s ~/vc/lilypond/mf/out/emmentaler-brace.otf ~/.fonts

cat <<EOF
Now, set path and Guile environment, do

    PATH=\$HOME/vc/lilypond/out/bin:\$PATH
    PATH=\$HOME/vc/schikkers-list/scripts:\$PATH
    . gg2.0 env
    ./schikkers.scm
EOF
