\version "2.13.36"
\context Staff = "foo" { \context Voice = "one" << {

       \key c \major

       { \set Staff . clefGlyph = #"clefs.G"

         \set Staff . clefPosition = #-2

         \set Staff . middleCPosition = #-6

         } \time 3/4

       c' e' e' e' f' e' d' g' g' e' c' e' f' e' d' c'' b' a' g' f' e' d' c' \bar "|."

       

     } \context Lyrics = "#<<simultaneous-music> b3a3f250>" \lyricsto "one" {

       Schik- kers List, you won't be missed; Frogs love E- macs to ra- ther type: GUIs are the E- vil Em- pire's hype!  

     } >> }
