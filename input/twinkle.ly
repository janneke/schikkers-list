\version "2.13.36"

\header{
  title = "Twinkle, twinkle little star"
  filename = "twinkle.ly"
  composer = "traditional"
  enteredby = "HWN, JCN, chords by Johan Vromans"
  copyright = "public domain"
}

acc = \chords {
  c2 c f c
  f c g:7 c
  g f c  g:7
  g f c  g:7
  %% copy 1-8
  c2 c f c
  f c g:7 c
}


<<  
%  \acc
  \relative c' {
    | c4 c g' g
    | a a g2
    | f4 f e e
    | d d c2
    | g'4 g f f
    | e e d2
    | g4 g f f
    | e e d2
    | c4 c g' g
    | a a g2
    | f4 f e e
    | d d c2
    \bar "|."
  }
  \addlyrics {
    \override LyricText  #'font-shape = #'italic
    % FIXME: ikli uses lyric's durations, even when using addlyrics
    Twin-4 kle, twin- kle, lit- tle star,2
    How4 I won- der what you are!2
    Up4 a- bove the world so high,2
    Like4 a dia- mond in the sky.2

    %% copy 1-8
    Twin-4 kle, twin- kle, lit- tle star,2
    How4 I won- der what you are!2
  }
>>
