#! /bin/bash

set -x

slots="\
cursor\
line-width\
"

slots="$(grep -hE '[(][a-z-]+ #:|^  [(][a-z-]+[)]$' *scm | grep -v gtk- | pytt '\s[(]([a-z-]+).*' '\1' | sort -u | tac)"

for slot in $slots; do
    pytt "^  [(]$slot[)]$" "  ($slot #:accessor x:$slot)" *scm
    pytt "[(]$slot #:" "($slot #:accessor x:$slot #:" *scm
    pytt "[(]slot-set! ([a-z-]+|[(][^()]+[)]) '$slot" "(set! (x:$slot \1)" *scm
    pytt "[(]slot-ref ([a-z-]+|[(][^()]+[)]) '$slot" "(x:$slot \1" *scm
done
