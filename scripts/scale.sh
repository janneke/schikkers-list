#! /bin/sh

rm -rf midi
mkdir -p midi
oct=0
alt=0
log=2
for patch in piano; do
    for oct in $(seq -4 3); do
	for step in $(seq 0 6); do
	    for alt in -2 -1 0 1 2; do
		lilypond -o midi/$patch-$oct-$step-$alt-$log -e '(define-public oct '$oct') (define-public step '$step') (define-public alt '$alt'/2) (define-public log '$log') (define-public patch "'$patch'")' ly/midi.ly
	    done
	done
    done
done
timidity -Ow2S midi/*.midi
oggenc -q 3 midi/*wav
cp -p midi/*ogg schikkers/web/root/audio
for i in midi/*wav; do lame -h $i $(dirname $i)/$(basename $i .wav).mp3; done
cp -p midi/*mp3 schikkers/web/root/audio/legacy
