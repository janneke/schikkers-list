#! /bin/bash

cat >/dev/null <<EOF
# Usage:
./schikkers.scm --debug x.ly 2> 1 # keep only the music >>> <<< bit
# insert \include "ly/display-as-scheme.ly" in x.ly
# run
lilypond x.ly 2> 2
scripts/compare.sh 1
scripts/compare.sh 2
diff -u 1 2
EOF

file=$1
pytt '\(' '\n(' $file
pytt '\(quote (.*)\)' "'\1" $file
pytt "'\n\(" "\n'(" $file
pytt "'input-tag.*\n" '' $file
pytt "\n\n*" '\n' $file
pytt "\n\n*" '\n' $file
pytt "\)\)" ')\n)' $file
pytt "\)\)" ')\n)' $file
pytt '^ *' '' $file
pytt ' *$' '' $file
pytt ' (.)' '\n\1' $file
