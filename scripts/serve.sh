#! /bin/bash

SCRIPTS=$(dirname $0)
SCHIKKERS=$(dirname $SCRIPTS)
LANG=C.UTF-8
HOST=${HOST-$(hostname -I | cut -f1 -d' ')}
GG2_HOME=${GG2_HOME-$HOME}
export GG2_PREFIX=$GG2_HOME/guile-2

PATH=$SCRIPTS:$PATH
PATH=$HOME/vc/lilypond/out/bin:$PATH
PATH=$GG2_HOME/vc/lilypond/out/bin:$PATH

cd $SCHIKKERS

gg2=$GG2_HOME/bin/guile-gnome-2
grep ' = "env"' $gg2 > /dev/null || patch < patches/guile-gnome-2.patch $gg2
. scripts/gg2 env

scripts/kill-schikkers

while true; do
    killall -9 lilypond
    # FIXME: emacs23: "Back to top level"
    # emacsclient --eval "t" || (emacs --daemon &)
    ./schikkers.scm --web --host=$HOST --size=line "$@"
done
