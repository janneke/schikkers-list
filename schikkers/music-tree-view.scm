;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; it under the terms of the GNU Affero General Public License as

;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers music-tree-view)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 receive)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gobject)
  :use-module (gnome glib)
  :use-module (gnome gtk)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-model)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers music-tree-model)
  ;;
  :export (<music-tree-view>
	    set-selection
	    .view)
  :re-export (set-model
	      .model))

(define-class <music-tree-view> (<gobject>)
  (model :accessor .model :init-value #f :init-keyword :model)
  (view :accessor .view :init-value #f :init-keyword :view)

  (tree-model :accessor .tree-model :init-value #f)

  :gsignal (list 'selection-changed #f <gmusic>))

(define-method (initialize (o <music-tree-view>) . initargs)
  (debugf 'tree "initialize music-tree-view\n")
  (next-method)
  (debugf 'tree "next-method music-tree-view\n")
  (let* ((tree-model (make <music-tree-model> :model (.model o)))
	 (view (.view o))
	 (selection (get-selection view)))
    (set! (.tree-model o) tree-model)
    (set-model view tree-model)
    (connect selection 'changed (lambda (x) (selection-changed-handler o x))))
  o)

(define-method (set-model (o <music-tree-view>) (model <music-model>))
  (let ((view (.view o)))
    (row-deleted (get-model view) '(0))
    (let ((tree-model (make <music-tree-model> :model model)))
      (set! (.tree-model o) tree-model)
      (set-model o tree-model))))

(define-method (selection-changed-handler (o <music-tree-view>) selection)
  (receive (model iter) (get-selected selection)
    (and-let* ((path (get-path model iter))
	       (music (on-get-iter model path)))
	      (emit o 'selection-changed (g music)))))

(define-method (set-selection (o <music-tree-view>) music)
  (and-let* ((view (.view o))
	     (path (on-get-path (get-model view) music)))
	    (expand-to-path view path)
	    (scroll-to-cell view path)
	    (set-cursor view path)))
