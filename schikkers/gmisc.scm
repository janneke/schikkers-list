;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers gmisc)
  ;; base
  :use-module (srfi srfi-1)
  :use-module (ice-9 optargs)
  :use-module (ice-9 receive)
  :use-module (os process)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome glib)
  :use-module (gnome gobject)
  :use-module (gnome gtk)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers point)
  ;;
  :export (<garticulation-type>
	   <gclef-type>
	   <gdynamic-type>
	   <gkey-signature>
	   <gmusic>
	   <gpoint>
	   <grect>
	   <grest-type>
	   <gspan-type>
	   LILYPOND-COMMAND
	   allocation-rect
	   fork-lilypond-server
	   g
	   char->utf-8-string
	   integer->utf-8-string
	   make-button-release-event
	   make-button-press-event
	   make-expose-event
	   make-key-press-event
	   make-scroll-event
	   string->utf-8-string
	   .point
	   .rect)
  :re-export (height
	      .music
	      width))

(define (integer->utf-8-string integer)
  (g-unichar-to-utf8 integer))

(define (char->utf-8-string char)
  (g-unichar-to-utf8 (char->integer char)))

(define (string->utf-8-string string)
  (apply
   string-append
   (map (lambda (x) (char->utf-8-string x)) (string->list string))))

(define-method (allocation-rect (o <gtk-widget>))
  (let ((rect (get-allocation o)))
    (if (equal? rect #(-1 -1 1 1)) #(0 0 0 0) rect)))

(define-method (height (o <gtk-widget>)) (height (allocation-rect o)))
(define-method (width (o <gtk-widget>)) (width (allocation-rect o)))

(if (not (defined? 'primitive-fork))
    (toplevel-define!
     'run-with-pipe
     (lambda (foo command . args)
       (load-extension "libguile-glib-spawn" "scm_init_glib_spawn_module")
       (toplevel-define!
	'lily-pointer-finalize 
	(apply (@ (glib spawn) me:spawn-async) (cons command args)))
       '(#f . #f)))
    (re-export run-with-pipe))

(define LILYPOND-COMMAND "lilypond")
(define (fork-lilypond-server)
  (system "killall -9 lilypond 2> /dev/null|| :")
  (let ((e (current-error-port)))
    (if (not (null-is-#f (debug?)))
	(set-current-error-port (open-file "lily.log" "w")))
    (let* ((fork (run-with-pipe "r" LILYPOND-COMMAND "-dbackend=socket" (string-append (get-data-dir) "/ly/server.ly"))))
      (if (not (null-is-#f (debug?)))
	  (set-current-error-port e))
      fork)))

(define-class <gmusic> (<gobject>)
  (music :accessor .music :init-value #f :init-keyword :music))

(define-method (g (o <music>)) (make <gmusic> :music o))

(define-class <gpoint> (<gobject>)
  (point :accessor .point :init-value #f :init-keyword :point))

(define-method (g (o <point>)) (make <gpoint> :point o))

(define-class <grect> (<gobject>)
  (rect :accessor .rect :init-value #f :init-keyword :rect))

(define-method (g (o <rect>)) (make <grect> :rect o))

(define (genum-vector lst)
  (list->vector
   (reverse (cdr (fold
		  (lambda (x r)
		    (cons (1+ (car r))
			  (cons (list x (symbol->string x) (car r)) (cdr r))))
		  '(1 . ()) lst)))))

(genum-register-static
 "garticulation-type"
 (genum-vector '(articulation.clear
		 articulation.digit-0
		 articulation.digit-1
		 articulation.digit-2
		 articulation.digit-3
		 articulation.digit-4
		 articulation.digit-5
		 articulation.accent
		 articulation.coda
		 articulation.downbow
		 articulation.downmordent
		 articulation.downprall
		 articulation.espressivo
		 articulation.fermata
		 articulation.flageolet
		 articulation.halfopen
		 articulation.lheel
		 articulation.lineprall
		 articulation.longfermata
		 articulation.ltoe
		 articulation.marcato
		 articulation.mordent
		 articulation.open
		 articulation.portato
		 articulation.prall
		 articulation.pralldown
		 articulation.prallmordent
		 articulation.prallprall
		 articulation.prallup
		 articulation.reverseturn
		 articulation.rheel
		 articulation.rtoe
		 articulation.segno
		 articulation.shortfermata
		 articulation.signumcongruentiae
		 articulation.snappizzicato
		 articulation.staccatissimo
		 articulation.staccato
		 articulation.stopped
		 articulation.tenuto
		 articulation.thumb
		 articulation.trill
		 articulation.turn
		 articulation.upbow
		 articulation.upmordent
		 articulation.upprall
		 articulation.varcoda
		 articulation.verylongfermata)))

(define <garticulation-type> (gtype-name->class "garticulation-type"))

(genum-register-static
 "gdynamic-type"
 (genum-vector '(dynamic.mp
		 dynamic.p
		 dynamic.pp
		 dynamic.ppp
		 dynamic.pppp
		 dynamic.ppppp
		 dynamic.mf
		 dynamic.f
		 dynamic.ff
		 dynamic.fff
		 dynamic.ffff
		 dynamic.fffff
		 dynamic.clear
		 dynamic.sp
		 dynamic.spp
		 dynamic.sf
		 dynamic.sff
		 dynamic.fp
		 dynamic.sfz
		 dynamic.rfz)))
(define <gdynamic-type> (gtype-name->class "gdynamic-type"))

(genum-register-static "gclef-type" #((clefs.G "clefs.G" 0)
				      (clefs.F "clefs.F" 1)
				      (clefs.C "clefs.C" 2)
				      (clefs.C.2 "clefs.C" 3)))
(define <gclef-type> (gtype-name->class "gclef-type"))

(genum-register-static
 "gkey-signature"
 (genum-vector '(C
		 G D A E B FIS CIS
		 F BES EES AES DES GES CES)))
(define <gkey-signature> (gtype-name->class "gkey-signature"))

(genum-register-static "grest-type" (genum-vector '(rest skip)))
(define <grest-type> (gtype-name->class "grest-type"))

(genum-register-static "gspan-type" (genum-vector '(slur)))
(define <gspan-type> (gtype-name->class "gspan-type"))

(define (genum-values genum-class)
  (map car (vector->list (genum-class->value-table genum-class))))

(define-method (g (o <symbol>))
  (cond
   ((member o (genum-values <gclef-type>))
    (make <gclef-type> :value o))
   ((member o (genum-values <grest-type>))
    (make <grest-type> :value o))
   ((member o (genum-values <gspan-type>))
    (make <gspan-type> :value o))
   (else o)))

(define* (make-expose-event :key
			    (window #f)
			    (send #f)
			    (area #(0 0 0 0))
			    (region 0)
			    (count 0))
  (let ((type (make <gdk-event-type> :value 'expose)))
    (list->vector (list type window send area region count))))

(define* (make-button-press-event :key
				  (window #f)
				  (send #f)
				  (time 0)
				  (x 0)
				  (y 0)
				  (state 0)
				  (button 0)
				  (device 0)
				  (x-root 0)
				  (y-root 0))
  (let ((type (make <gdk-event-type> :value 'button-press)))
    (list->vector (list type window send time x y state button device x-root y-root))))

(define* (make-button-release-event :key
				  (window #f)
				  (send #f)
				  (time 0)
				  (x 0)
				  (y 0)
				  (state 0)
				  (button 0)
				  (device 0)
				  (x-root 0)
				  (y-root 0))
  (let ((type (make <gdk-event-type> :value 'button-release)))
    (list->vector (list type window send time x y state button device x-root y-root))))

(define* (make-key-press-event :key
			       (window #f)
			       (send #f)
			       (time 0)
			       (state 0)
			       (key 0)
			       (hwkey 0)
			       (group 0))
  (let ((type (make <gdk-event-type> :value 'key-press)))
    (list->vector (list type window send time state key hwkey group))))

(define* (make-scroll-event :key
			    (window #f)
			    (send #f)
			    (time 0)
			    (x 0)
			    (y 0)
			    (state 0)
			    (dir 'up)
			    (device 0)
			    (x-root 0)
			    (y-root 0))
  (let ((type (make <gdk-event-type> :value 'scroll))
	(direction (make <gdk-scroll-direction> :value dir)))
    (list->vector (list type window send time x y state direction device x-root y-root))))
