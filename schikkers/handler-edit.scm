;; JUNKME

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers handler-edit)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gtk gdk-event)
  ;; user
  :use-module (schikkers handler)
  :use-module (schikkers ly)
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-canvas)
  :use-module (schikkers music-layout)
  :use-module (schikkers music-model)
  :use-module (schikkers music-modify)
  :use-module (schikkers notation)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  ;; user-gtk
  :use-module (schikkers gmisc)
  ;; user-gnome
  :use-module (schikkers gnome music-edit-dialogs)
  :export (<handler-edit>
	   edit-paper
	   render-pdf
	   .canvas))
	   
(define-class <handler-edit> ()
  (canvas :accessor .canvas :init-value #f :init-keyword :canvas)
  (handler :accessor .handler :init-value #f :init-keyword :handler)

  (model :accessor .model :init-value #f :init-keyword :model)
  (key :accessor .key :accessor .key)
  (duration-input :accessor .duration-input :init-value #f)
  (page :accessor .page :init-value 0)
  (system :accessor .system :init-value 0)
  (simultaneous-mode :accessor .simultaneous-mode :init-value #f))

(define-method (get-cursor (o <handler-edit>)) (get-cursor (.model o)))
(define-method (notify-modified (o <handler-edit>) cursor modified))
(define-method (notify-dirty (o <handler-edit>) system modified))
(define-method (modify-at-cursor (o <handler-edit>) method . args))

(define-method (add-character (o <handler-edit>) c)
  (debugf 'edit "MUSIC-EDIT-ADD-CHARACTER\n")
  (modify-at-cursor o add-character c))

(define-method (add-note (o <handler-edit>) duration)
  (debugf 'edit "MUSIC-EDIT-ADD-NOTE: ~a\n" duration)
  (set! (get-cursor o) (find-last (.model o) is-a-note-or-rest? (inf)))
  (if (is-a? (get-cursor o) <note-event>)
      (modify-at-cursor o add-rhythmic-event duration)
      (modify-at-cursor o add-note duration 3 (.key o))))

(define-method (add-pitch (o <handler-edit>) step)
  (debugf 'edit "MUSIC-EDIT-ADD-PITCH:~a\n" step)
  (modify-at-cursor o add-pitch step (.key o)))

(define-method (add-rest (o <handler-edit>) duration)
  (debugf 'edit "MUSIC-EDIT-ADD-RHYTHMIC-EVENT: ~a\n" duration)
  (set! (get-cursor o) (find-last (.model o) is-a-note-or-rest? (inf)))
  (if (is-a? (get-cursor o) <rest-event>)
      (modify-at-cursor o add-rhythmic-event duration)
      (modify-at-cursor o add-rest duration)))

(define-method (add-rhythmic-event (o <handler-edit>) duration)
  (debugf 'edit "MUSIC-EDIT-ADD-RHYTHMIC-EVENT: ~a\n" duration)
  (modify-at-cursor o add-rhythmic-event duration))

(define-method (simultaneous-mode (o <handler-edit>) bool)
  (stderr "SIMULTANEOUS: ~a\n" bool)
  (set! (.simultaneous-mode o) bool))


;;;; Edit music dialogs

(define-method (edit-paper (o <handler-edit>))
  (let ((paper (.paper o)))
    (when (edit paper)
      (set! (.paper (.canvas o)) paper))
      (notify-dirty o #t (get-cursor o))))

(define LILYPOND-COMMAND "lilypond")
(define VIEW-COMMAND (if (eq? PLATFORM 'windows) "acrord32" "evince"))
(define-method (render-pdf (o <handler-edit>)) #f)
(define-method (view-pdf (o <handler-edit>))
  (and-let* ((pdf-name (render-pdf o)))
	    (if (eq? PLATFORM 'windows)
		(run-with-pipe "r" VIEW-COMMAND pdf-name)
		(system (string-append "ld_library_path= " VIEW-COMMAND " " pdf-name "&")))))


;;;; Cursor and paging
(define-method (get-notation-item (o <handler-edit>) (music <music>))
  #f)

(define-method (system-first-element (o <handler-edit>) system)
  #f)

(define-method (set-cursor (o <handler-edit>) (x <boolean>)))
(define-method (set-cursor (o <handler-edit>) (music <event-chord>)))

(define-method (chord-move-cursor (o <handler-edit>) steps)
  (and-let* ((cursor (get-cursor o))
	     (e (.parent cursor))
	     (new (get-neighbor e cursor steps))
	     (is-new (not (eq? new cursor))))
	    (set-cursor o new)))


;;;; keyboard handler-edit control bindings
;;;; ~/pkg/guile-config/share/guile-gnome-2/gnome/gtk/gdk-event.scm

(define-method (key-press-handler (o <handler-edit>) widget event)
  (let* ((keyval (gdk-event-key:keyval event))
	 (mods (gdk-event-key:modifiers event))
	 (canvas (.canvas o))
	 (handler (.handler o))
	 (cursor (get-cursor o)))
    (debugf 'key "~a urg-key-press-handler: ~a ~a\n" (class-name (class-of o)) mods keyval)

    ;; FIXME: mod2-mask
    (set! mods (filter (lambda (x) (not (equal? x 'mod2-mask))) mods))
    (cond ((and #f (dirty? o))
	   (if (equal? mods '())
	       (stderr "MUSIC-EDIT DIRTY, ignoring key: ~a\n" keyval)
	       #f))
	  ((and (equal? mods '(control-mask))
		(eq? keyval gdk:s))
	   (save-sy handler #f)
	   #t)
	  ((and (equal? mods '(control-mask))
		(eq? keyval gdk:y))
	   (save-ly handler)
	   #t)
	  ((and (equal? mods '(control-mask))
		(eq? keyval gdk:p))
	   (render-pdf o)
	   #t)
	  ((or (is-a? cursor <note-event>)
	       (is-a? cursor <rest-event>))
	   (debugf 'edit "CURSOR IS NOTE\n")
	   (cond
	    ((and (equal? mods '())
		  (eq? keyval gdk:Up))
	     (chord-move-cursor o 1)
	     #t)
	    ((and (equal? mods '())
		  (eq? keyval gdk:Down))
	     (chord-move-cursor o -1)
	     #t)
	    ((and (or (equal? mods '())
		      (equal? mods '(shift-mask)))
		  (eq? keyval gdk:less))
	     (simultaneous-mode o #t)
	     #t)
	    ((and (or (equal? mods '())
		      (equal? mods '(shift-mask)))
		  (eq? keyval gdk:greater))
	     (simultaneous-mode o #f)
	     #t)
	    ((and (equal? mods '(control-mask))
		  (eq? keyval gdk:r))
	     (force-update o)
	     #t)
	    (else
	     (set! (.duration-input o) #f)
	     #f)))
	  ((or (is-a? cursor <lyric-event>)
	       (is-a? cursor <extender-event>))
	   (debugf 'edit "CURSOR IS LYRIC\n")
	   (cond
	    ((and (or (equal? mods '())
		      (equal? mods '(shift-mask)))
		  (> keyval gdk:space)
		  (< keyval #x0ff))
	     (add-character o keyval)
	     #t)
	    ;;;(else #f)))
	    (else (debugf "NO LYRIC MATS\n")
		  #f)))
	  (else #f))))

(define (test . args)
  (test-set-pitch))

(define (test-set-pitch)
  (let ((handler-edit (make <handler-edit>)))
    (stderr "handler-edit:~a\n" handler-edit)
    (stderr "cursor:~a\n" (get-cursor handler-edit))
    (set-pitch handler-edit 1)))
