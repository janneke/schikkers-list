;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers web ly-editor)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gtk)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers point)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers ly-editor)
  :use-module (schikkers music)
  :use-module (schikkers music-model)
  ;; user-web
  :use-module (schikkers web misc)
  :use-module (schikkers web scrolled-window)
  :use-module (schikkers web text-view)
  :export (<web-ly-editor>)
  :re-export (script))

(define-class <web-ly-editor> (<ly-editor>)
  (widget :accessor .widget :init-form (make <web-scrolled-window>)))


;;; Gtk-wrappers
(define-method (pack-start (w <gtk-container>) (o <web-ly-editor>) . rest) (apply pack-start (cons* w (.widget o) rest)))

(define-method (boolean->js (o <top>))
  (if o "true" "false"))

(define-method (script (o <web-ly-editor>))
   (let* ((buffer (get-buffer (.view o)))
	  (start (get-start-iter buffer))
	  (end (get-end-iter buffer))
	  (text (get-text buffer start end))
	  (pos (get-offset o))
	  ;;(cursor (get-previous-music o (get-cursor o))))
	  (cursor (or (get-cursor (.model o))
		      (get-previous-music o (get-cursor o)))))
     (string-append
      ;; FIXME: .html () set once, but sometimes does not update
      ;;(format #f "$ ('#editor').html (\"~a\");\n" (string->xml-string text))
      (format #f "$ ('#editor').val (\"~a\");\n"
	      (string-regexp-substitute
	       (string-regexp-substitute
		(string-regexp-substitute text "\\\\" "\\\\")
		"\"" "\\\"")
	       "\n" "\\n"))
      "cursor = new "
      (cond ((is-a-note? cursor)
	     (let* ((pitch (.pitch cursor))
		    (duration (.duration cursor)))
	       (format #f "Music_note (new Music_pitch (~a, ~a, ~a), new Music_duration (~a, ~a, ~a));\n"
		       (.step pitch) (.alteration pitch) (.octave pitch)
		       (.log duration) (.dots duration) (.factor duration))))
	    ((is-a-rest? cursor)
	     (let ((duration (.duration cursor)))
	       (format #f "Music_rest (new Music_duration (~a, ~a, ~a));\n"
		       (.log duration) (.dots duration) (.factor duration))))
	    (else "Music_note ();\n"))
      (format #f "free_ahead = ~a;\n" (let ((n (free-ahead (.model o)))) (if (equal? n (inf)) 1e9 n)))
      (format #f "tie_active_p = ~a;\n" (boolean->js (tie-active? cursor)))
      (format #f "tie_possible_p = ~a;\n" (boolean->js (tie-possible? cursor)))
      "$ ('#editor').on ('focus', set_cursor);\n" 
      "if ($ ('#editor').is (':focus')) focus_div = $ ('#editor'); else focus_div = $ ('#score');\n"
      "update_focus ();\n"
      "toolbar_update ();\n"
      "audio_play_music (cursor);\n"
      "selection_buttons ();\n"
      (format #f "var cursor_pos = ~a;\n" pos)
      "function set_cursor () { $ ('#editor').set_caret (cursor_pos); return false; }\n"
      )))
