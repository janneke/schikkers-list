;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers web app)
  ;; base
  :use-module (ice-9 and-let-star)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gobject)
  :use-module (gnome gtk)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers point)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers ly-editor)
  :use-module (schikkers music)
  :use-module (schikkers music-canvas)
  :use-module (schikkers music-edit)
  :use-module (schikkers music-layout)
  :use-module (schikkers music-model)
  :use-module (schikkers music-view)
  ;; user-web
  :use-module (schikkers web ly-editor)
  :use-module (schikkers web misc)
  :use-module (schikkers web music-canvas)
  :use-module (schikkers web music-edit)
  :use-module (schikkers web music-view)
  :export (<web-app>
	   ly-source
	   .birth)
  :re-export (handle
	      render
	      script))

(define-class <web-app> (<gobject>)
  (model :accessor .model :init-value #f :init-keyword :model)
  (edit :accessor .edit :init-value #f :init-keyword :edit)
  (editor :accessor .editor :init-value #f :init-keyword :editor)
  (birth :accessor .birth :init-form (current-time))

  :gsignal (list 'scale #f <gint>))

(define-method (initialize (o <web-app>) . initargs)
  (next-method)
  ;; FIXME: c&p from <gnome-app>
  (connect (.editor o) 'insert-mark-changed
	   (lambda (x m) (insert-mark-changed-handler o m)))
  (connect (.edit o) 'set-cursor
	   (lambda (x m) (set-cursor-handler o m))))

;; FIXME: c&p from <gnome-app>
(define-method (insert-mark-changed-handler (o <web-app>) (cursor <gmusic>))
  (debugf 'cursor "~a: insert-mark-changed-handler: ~a\n" (class-name (class-of o)) cursor)
  (insert-mark-changed-handler o (.music cursor)))

(define-method (insert-mark-changed-handler (o <web-app>) (cursor <music>))
  (debugf 'cursor "~a: insert-mark-changed-handler: ~a\n" (class-name (class-of o)) cursor)
  (set-cursor (.edit o) cursor))

;; FIXME: c&p from <gnome-app>
(define-method (set-cursor-handler (o <web-app>) (cursor <gmusic>))
  (debugf 'cursor "~a: set-cursor-handler: ~a\n" (class-name (class-of o)) cursor)
  (set-cursor-handler o (.music cursor)))

(define-method (set-cursor-handler (o <web-app>) (cursor <music>))
  (debugf 'cursor "~a: set-cursor-handler: ~a\n" (class-name (class-of o)) cursor)
  (move-insert-mark (.editor o) cursor))

(define-method (flush-events (o <web-app>))
  (while (gtk-events-pending)
    (gtk-main-iteration)))

(define-method (handle (o <web-app>) alteration articulation-type clef-type clef-details cursor debug dot? drag-details duration dynamic-type edit-command edit-details exposed key-details key-signature key-val mouse ottava rest? scale-val scroll slur-rect tie? time-details time-signature)
  (let* ((layout (.layout (.edit o)))
	 (canvas (.canvas layout))
	 (scale-step (case (integer->char key-val)
			  ((#\+) 1)
			  ((#\-) -1)
			  (else scale-val))))
    (when (and key-val (!=0 key-val))
      (emit canvas 'key-press-event (key->key-press-event key-val))
      (flush-events o))
    (when (and mouse (not (equal? mouse '(0 0))))
      (let ((b (button-press-event mouse)))
	(debugf 'mouse "the event: ~a\n" b)
	(emit canvas 'button-press-event b))
      (flush-events o))
    (when (and scroll (not (equal? scroll '(0 0 0))))
      (let ((s (scroll-event (car scroll) (cdr scroll))))
	(debugf 'scroll "the event: ~a\n" s)
	(emit canvas 'scroll-event s))
      (flush-events o))
    (when (and alteration (!=0 alteration))
      (emit (.edit o) 'alteration alteration))
    (when (and articulation-type (not (equal? articulation-type 'none)))
      (emit (.edit o) 'articulation (g articulation-type))
      (flush-events o))
    (when (and clef-type (not (equal? clef-type 'none)))
      (emit (.edit o) 'clef (g clef-type))
      (flush-events o))
    (when (and clef-details (not (equal? clef-details '("none"))))
      (let ((glyph (string->symbol (car clef-details)))
	    (position (string->integer (cadr clef-details)))
	    (middle-c  (string->integer (caddr clef-details))))
      (clef (.edit o) glyph position middle-c)
      (flush-events o)))
    (when (and cursor (>=0 cursor))
      (set-cursor (.editor o) cursor)
      (flush-events o))
    (when (eq? dot? '.)
      (dot (.edit o)))
    (when (and drag-details (not (equal? drag-details '("none"))))
      (and-let* ((input-tag (string->number (car drag-details)))
		 (x (string->number (cadr drag-details)))
		 (y (string->number (caddr drag-details)))
		 (music (get-music (.model o) input-tag)))
		(emit (.edit o) 'drag (g music) x y)
		(flush-events o)))
    (when (and duration (>=0 duration))
      (debugf 'app "setting duration-input: ~a\n" (expt 2 duration))
      (duration-log (.edit o) duration))
    (when (and dynamic-type (not (equal? dynamic-type 'none)))
      (emit (.edit o) 'dynamic (g dynamic-type))
      (flush-events o))
    (when (member edit-command '(cut))
	  (emit (.edit o) edit-command)
      (flush-events o))
    (when (and edit-details (not (equal? edit-details '("none"))))
      (let ((pos (string->number (car edit-details)))
	    (key (string->symbol (caddr edit-details)))
	    (value (cadr edit-details)))
	(debugf 'parse "app <top> <top> ~a ~a\n" (class-of key) (class-of value))
	(handle (.editor o) pos key value)
	(flush-events o)))
    (when exposed
      (let ((e (make-expose-event :area exposed)))
	(emit canvas 'expose-event e)
	;;(emit canvas 'headless-expose-event exposed)
	(flush-events o)))
    (when (and key-details (not (equal? key-details '("none"))))
      (let ((tonic (string->integer (car key-details)))
	    (alteration (string->integer (cadr key-details)))
	    (scale (map string->number (string-split (string-regexp-substitute (caddr key-details) "%20" " ") #\ ))))
	(key (.edit o) tonic alteration scale)
	(flush-events o)))
    (when (and key-signature (not (equal? key-signature 'none)))
      (emit (.edit o) 'key (g key-signature))
      (flush-events o))
    (when (and ottava (!=0 ottava))
      (emit (.edit o) 'octave ottava))
    (when (not (eq? rest? 'none))
      (rest (.edit o) (eq? rest? 'true)))
    (when scale-step
      (scale canvas scale-step)
      (emit o 'scale (make <gint> :value scale-step))
      (flush-events o))
    (when (and slur-rect (not (equal? slur-rect #(0 0 0 0))))
	  (emit (.edit o) 'slur (g slur-rect))
      (flush-events o))
    (when (not (eq? tie? 'none))
      (tie (.edit o) (eq? tie? 'true)))
    (when (and time-details (not (equal? time-details '("none"))))
      (let ((numerator (string->integer (car time-details)))
	    (denominator (string->integer (cadr time-details)))
	    (style (string->symbol (caddr time-details))))
	(time (.edit o) numerator denominator style)
	(flush-events o)))
    (when (and time-signature (not (equal? time-signature 'none)))
      (emit (.edit o) 'time (object->string time-signature))
      (flush-events o))
  (script o debug)))

(define-method (ly-source (o <web-app>))
  (and-let* ((file (save-ly (.edit o) #f)))
    (gulp-text-file file)))

(define-method (render (o <web-app>) (format <symbol>))
  (and-let* ((file (render (.edit o) format #f)))
    (gulp-file file)))

(define-method (script (o <web-app>) debug)
  (string-append
   (string-join
    (map script (list
		 (.layout (.edit o))
		 (.edit o)
		 (.editor o)
		 (find (.model o) is-a-clef-glyph?)
		 (find (.model o) is-a-key-change-event?)
		 (find (.model o) is-a-time-signature?))))
   (format #f "debug = '~a';\n" debug)
   "if (debug != 'none') $ ('#debug').show (); else  $ ('#debug').hide ();\n"

   ;; Disable editor input & parsing unless in debug mode
   ;; Too experimental to show by default on lilypond.org
   "function disable_keys (e) { return false; }\n"
   "if (debug == 'none') { $ ('#editor-window').attr ('readonly', 'readonly'); $ ('#editor').on ('keypress', disable_keys); $ ('#editor').on ('keydown', disable_keys); }\n"
   "\n"))

(define-method (script (o <boolean>))
  "")

(define-method (script (o <music-layout>))
  (script (.canvas o)))

(define-method (script (o <web-music-edit>))
  (string-append
   (format #f "$ ('#score-window').scrollLeft (~a);\n" (x (offset o)))
   (format #f "$ ('#score-window').scrollTop (~a);\n" (y (offset o)))))

(define-method (script (o <property-set>))
  (cond
   ((is-a-clef-glyph? o) (clef-script o))
   (else "")))

(define-method (clef-script (o <property-set>))
  ;; FIXME: c&p from music-modify.scm
  (let* ((context (.parent o))
	 (music (.parent context))
	 (pos-property
	  (find music (lambda (x) (is-a-clef-position? x))))
	 (middle-c-property
	  (find music (lambda (x) (is-a-clef-middle-c? x)))))
    (format #f "var clef_music = {'glyph': '~a', 'position': ~a, 'middle-c': ~a, };\n"
	    (.value o)
	    (if pos-property (.value pos-property) -2)
	    (if middle-c-property (.value middle-c-property) -6))))

(define-method (script (o <key-change-event>))
  (format #f "var key_music = {'tonic': ~a, 'alteration': '~a', 'scale': '~a', };\n" (.step (.tonic o)) (.alteration (.tonic o)) (string-join (map number->string (.scale o)) " ")))

(define-method (script (o <time-signature-music>))
  (format #f "var time_music = {'numerator': ~a, 'denominator': '~a', 'style': '~a', };\n" (.numerator o) (.denominator o) (or (.style o) 'default)))
