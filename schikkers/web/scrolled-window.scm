;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers web scrolled-window)
  ;; base
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gobject)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers point)
  ;; user-gtk
  ;; user-web
  :use-module (schikkers web window)
  :export (<web-scrolled-window>)
  :re-export (offset
	      move-viewport
	      set-adjustments
	      update-adjustments))

(define-class <web-scrolled-window> (<web-window>)
  (offset :accessor .offset :init-form (list 0 0)))

(define-method (offset (o <web-scrolled-window>))
  (.offset o))

(define-method (move-viewport (o <web-scrolled-window>) (step <symbol>) count)
  (debugf 'scroll "sw: move-viewport: ~a ~a\n" step count))

(define-method (set-adjustments (o <web-scrolled-window>) (offset <point>))
  (debugf 'scroll "sw: set-adjustments: ~a\n" offset)
  (set! (.offset o) offset))

(define-method (update-adjustments (o <web-scrolled-window>))
  (debugf 'scroll "sw: update-adjustments\n"))
