#! /bin/sh
# -*- scheme -*-
exec guile -L $(dirname $(dirname $0)) -e test -s $0 "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

;; http://raphaeljs.com/reference.html

(define (test . args)
  (eval '(test (command-line)) (resolve-module '(schikkers web music-canvas))))

(define-module (schikkers web music-canvas)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 receive)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gobject)
  :use-module (gnome gtk)
  ;; user
  :use-module (schikkers font)
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-canvas)
  :use-module (schikkers notation)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  :use-module (schikkers system)
  ;; user-gtk
  :use-module (schikkers gmisc)
  ;; web
  :use-module (schikkers web window)
  ;;
  :export (<web-music-canvas>)
  :re-export (activate
	      add
	      connect
	      draw
	      draw-
	      freeze-updates
	      glyph
	      line
	      path
	      polygon
	      rectangle
	      remove
	      scale
	      script
	      set-cursor
	      set-scroll-region
	      text
	      thaw-updates
	      translate))


;;;

(define-class <raphael-item> ()
  (paper :accessor .paper :init-value #f :init-keyword :paper)
  (script :accessor .script :init-value "" :init-keyword :script))
(define-class <raphael-line> (<raphael-item>))
(define-class <raphael-path> (<raphael-item>))
(define-class <raphael-rect> (<raphael-item>))
(define-class <raphael-text> (<raphael-item>))

(define-class <web-music-canvas> (<music-canvas>)

  (page-count :accessor .page-count :init-value 0)
  (scroll-pages :accessor .scroll-pages :init-value 0)
  (background :accessor .background :init-value #f)
  (resolution :accessor .resolution :init-value 96)
  (widget :accessor .widget :init-form (make <web-window>)))

(define-method (initialize (o <web-music-canvas>) . initargs)
  (next-method)
  ;; (scale o 14.5139) ;; 25.5 ppu = 1600% evince scaling
  ;; (scale o 12.25)
  (scale o 10)
  (set! (.when-cursor o) (apply create-cursor (cons* o 'cwhen (cursor-color 'when))))
  (set! (.nitem-cursor o) (apply create-cursor (cons* o 'cnitem (cursor-color 'nitem))))
  o)


;;; gtk-widget wrappers

(define-method (add (w <gtk-widget>) (o <web-music-canvas>)))
(define-method (connect (o <web-music-canvas>) . rest) (apply connect (cons* (.widget o) rest)))
(define-method (emit (o <web-music-canvas>) . rest) (apply emit (cons* (.widget o) rest)))

(define (cursor-color cursor)
  (if (member "--stealth" (command-line))
      (list #xf0f0f0 0)
      (case cursor
	((when) (list "#ebeef7" "#0036ff"))
	((nitem) (list "#0036ff" "#001872")))))

(define-method (create-cursor (o <web-music-canvas>) var fill outline)
  (string-append
   (if (eq? var 'cnitem)
       (format #f "var ~a = paper.rect (~a,~a,~a,~a,~a);\n" var 0 0 1 1 0)
       (format #f "var ~a = paper.rect (~a,~a,~a,~a,~a);\n" var 1 1 2 2 0))
   (format #f "~a.attr ({stroke: ~s, fill: ~s, 'stroke-width': ~a, 'stroke-linejoin': 'round'});\n" var outline fill (1/ (.pixels-per-unit o)))))

(define-method (draw-background (o <web-music-canvas>))
  (let* ((width (paper-width o))
	 (height (paper-height o))
	 (thick (/ 1 (.pixels-per-unit o))))
    (string-join (map (lambda (x)
			 (draw-line o "'black'" thick
				    0 (* (1+ x) height)
				    width (* (1+ x) height)))
		      (iota (.page-count o))) "\n")))
  
(define-method (draw-line (o <web-music-canvas>) color thick x1 y1 x2 y2)
  (string-append
   (format #f "paper.path ('m~a,~aL~a,~a')" x1 y1 x2 y2)
   (format #f ".attr ({stroke: ~s, fill: ~s, 'stroke-width': ~a})"
	   color color thick)))

;;; Stencil primitives
(define-method (line (o <notation-item-line>) (canvas <web-music-canvas>) color thick x1 y1 x2 y2)
  (make <raphael-line> :paper canvas
	:script (draw-line canvas color thick x1 y2 x2 y2)))
  
(define-method (path (o <notation-item-path>) (canvas <web-music-canvas>) color path thick filled? cap join)
  (define (args->string args)
    (string-join
     (map number->string
	  (points->list (map flip-y (list->points args))))))
  (debugf 'path "path: ~a\n" path)
  (let* ((svg-alist '((rmoveto . m)
		      (rcurveto . c)
		      (curveto . C)
		      (moveto . M)
		      (lineto . L)
		      (rlineto . l)
		      (closepath . z)))
	 (path (let loop ((path path))
		 (if (null? path)
		     '()
		     (let* ((head (car path))
			    (rest (cdr path))
			    (arity (cond ((memq head '(rmoveto rlineto lineto moveto)) 2)
					 ((memq head '(rcurveto curveto)) 6)
					 ((eq? head 'closepath) 0)
					 (else 1)))
			    (args (take rest arity))
			    (svg-head (assoc-get-default svg-alist head '#{}#)))
		       (cons (string-append (symbol->string svg-head) (args->string args))
			     (loop (drop rest arity))))))))
    (make <raphael-path>
      :paper canvas
      :script
      (string-append
       "paper.path ('" (string-join path) "')"
       (format #f ".attr ({stroke: ~s, fill: ~s, 'stroke-width': ~a, 'stroke-linejoin': '~a', 'stroke-linecap': '~a'})"
	       color (if filled? color "white") thick join cap)))))

(define-method (polygon (o <notation-item-polygon>) (canvas <web-music-canvas>) color coords blot filled?)
  (let* ((points (map flip-y (list->points coords)))
	 (last-point (car (last-pair points))))
    (make <raphael-path> :paper canvas
	  :script
	  (string-append
	   (apply format (cons* #f "paper.path ('M0,0m~a,~a" last-point))
	   (string-join
	    (map (lambda (x) (apply format (cons* #f "L~a,~a" x))) points) "")
	   "')"
	   (format #f ".attr ({stroke: ~s, fill: ~s, 'stroke-width': ~a, 'stroke-linejoin': 'round'})" color (if filled? color "white") blot)))))

(define-method (rectangle (o <notation-item-rectangle>) (canvas <web-music-canvas>) color breapth width depth height blot)
  (let* ((b (/ blot 2))
	 (offset (list (- b breapth) (- b height)))
	 (epsilon (/ blot 10))
	 (area (list (- (+ breapth width) blot) (max (- (+ depth height) blot) epsilon))))
    (make <raphael-rect> :paper canvas
	  :script
	  (string-append
	   (format #f "paper.rect (~a,~a,~a,~a,~a)"
		   (x offset) (y offset) (car area) (cadr area) b)
	   (format #f ".attr ({stroke: ~s, fill: ~s, 'stroke-width': ~a, 'stroke-linejoin': 'round'})" color color blot)))))

(define-method (text (o <notation-item>) (canvas <web-music-canvas>) color font size text)
  (debugf 'canvas "font: ~a, ~a\n" font text)
  (if (equal? font "")
      (set! font "emmentaler-20"))
  (let* ((resolution (.resolution canvas))
	 (magic (* (if (emmentaler? font) (/ 43 3) (/ 94 9)) (/ 1 resolution)))
	 (web-magic 1.328685)
	 (scale (* magic web-magic (design-size font)))
	 (x (if (emmentaler? font) (* -0.09925 (design-size font)) 0))
	 (y (if (emmentaler? font) (/ 28 75) 0)))
    (make <raphael-text> :paper canvas
	  :script
	  (string-append
	   (format #f "paper.print (~a,~a,~s,paper.getFont (~s,0),~a)" x y text font scale)
	   (format #f ".attr ({'text-anchor': 'start', fill: ~s})" color)))))

(define-method (set-cursor (o <web-music-canvas>) (nitem <notation-item>))
  (let* ((notation (.notation o))
	 (paper (.paper notation))
	 (system (get-system notation (.system nitem))))
    (set! (.when-cursor o)
	  (string-append
	   (apply create-cursor (cons* o 'cwhen (cursor-color 'when)))
	   (set-cursor o 'cwhen (paper-when-rect system nitem paper))))
  (set! (.nitem-cursor o)
	(string-append
	 (apply create-cursor (cons* o 'cnitem (cursor-color 'nitem)))
	 (set-cursor o 'cnitem (paper-rect nitem paper))))))

(define-method (set-cursor (o <web-music-canvas>)
			   (var <symbol>) (cursor <rect>))
  (debugf 'canvas "cursor: ~a\n" cursor)
  ;; FIXME
  (let* ((w (width cursor))
	 (h (abs (height cursor)))
	 (x (x cursor))
	 (yc (y cursor))
	 (y (if (<0 (height cursor)) (+ yc (height cursor)) yc)))
    (format #f "~a.attr ({x: ~a, y: ~a, width: ~a, height: ~a});\n"
	    var x y w h)))

(define-method (set-scroll-region (o <web-music-canvas>) (paper <paper>) page-count)
  (debugf 'page "set-scroll-region: ~a\n" page-count)
  (set! (.scroll-pages o) page-count)
  (set! (.page-count o) page-count))

(define-method (freeze-updates (o <web-music-canvas>)))
(define-method (thaw-updates (o <web-music-canvas>)))

(define-method (scale (o <web-music-canvas>) step)
  (let ((ppu (next-method)))
    (debugf 'canvas "scale: ~a --> ~a\n" step ppu)
    ppu))

;;clicked on staff at (422,127): 14.276377952755904 2.8722482440944903
;;31.832447856449956 × 4.1
;; obj.attr("x")-canvasPosition.x,

(define-method (script (o <web-music-canvas>) . rest)
  ;;(next-method)
  (debugf 'page "script: ~a\n" (page-count (.notation o)))
  (debugf 'page "script: ~a\n" (.page-count o))
  (set! (.page-count o) (page-count (.notation o)))
  (let* ((width (paper-width o))
	 (height (* (.page-count o) (paper-height o)))
	 (step (or (and-let* (((pair? rest))
			      (x (car rest))
			      (number? x))
			     x)
		   0))
	 (ppu (scale o step)))
    (string-join
     (list
      (format #f "var paper = new ScaleRaphael ('score', ~a, ~a);" width height)
      "paper.path ('').constructor.prototype.set_control_points = function (x) { this.control_points = x; return this; };"
      (format #f "var ppu = ~a;" ppu)
      "paper.scaleAll (ppu, center=true, clipping=true);"
      (format #f "$ ('#score').width (~a * ppu);" width)
      (format #f "$ ('#score').height (~a * ppu);" height)
      ""
      (draw-background o)
      ""
      "function attr (x) { return function (e) { e.attr (x); }; };"
      ""
      "var background = '#fff';"
      "background = '#fcf6e8'; /* middle */"
      "background = '#fcf7ee'; /* whiter */"
      ""
      "var canvas = paper.rect (0, 0, paper.width, paper.height).attr ({fill: background, stroke: background});"
      ""
      "var select_stroke = '#0036ff';"
      "var select_fill = '#ebeef7';"
      "var selected_fill = select_stroke;"
      ""
      "var select_rect = paper.rect (0, 0, 0, 0).attr ({fill: select_fill, stroke: select_stroke, 'stroke-width': 1/ppu});"
      "select_region = false;"
      ""
      "canvas.drag (selection_move, selection_start, selection_end);"
      ""
      "var control_point = paper.circle (0,0,2/ppu).attr ({'stroke-width': 1/ppu, 'stroke-opacity': 0, stroke: 'red', fill: 'red', opacity: 0});"
      ""
      "function on_item_click (e) { this.attr ({fill: 'orange'}); };"
      "function on_item_dblclick (e) { this.attr ({fill: 'green'}); };"
      ;; "function mouse_click (x, y) { var query = '&mouse=' + x + ',' + y; return calling_lily (query); }"
      ;;"function on_staff_click (e) { mouse_click (e.offsetX, e.offsetY); }"
      "function on_item_hover_in (e) { this.attr ({fill: 'red'}); };"
      "function on_item_hover_out (e) { this.attr ({fill: 'black'}); };"
      ""
      "function on_note_hover_in (e, note) { this.attr ({fill: 'red'}); audio_play (note.join ('-')); };"
      ""
      "function edit_clef (e) { open_clef_dialog (e); };"
      "function edit_key (e) { open_key_dialog (e); };"
      "function edit_time (e) { open_time_dialog (e); };"
      ""
      "function drag (id, x, y) { calling_lily ('&drag=' + id + ',' + x.toPrecision (5) + ',' + y.toPrecision (5)); }"
      "function text_drag_move (dx, dy) { var x = (0 + ctrl) * dx / ppu; var y = dy / ppu; this.translate (x - self.x, y - self.y); self.x = x; self.y = y; dragging = true; return true; }"
      "function text_drag_start (e) { self.x = 0; self.y = 0; return true; }"
      "function text_drag_end (id) { if (ctrl) drag (id, self.x, self.y); return true; }"
      ""
      (.when-cursor o)
      (.nitem-cursor o)
      ""
      "paper.setStart ();"
      ""
      (string-join
       (map (lambda (x) (draw- o x)) (get-systems (.notation o)))
       "\n")
      "var set = paper.setFinish ();"
      "var select_items = paper.set ();"
      ""
      "canvas.toBack ();"
      ""
      )
     "\n")))

(define-method (clear (o <web-music-canvas>) (system <system>))
  ;; TODO
  (debugf 'canvas "~a: clear system:~a\n" (class-name (class-of o)) system))

(define-method (draw- (o <web-music-canvas>) (system <system>))
  (next-method)
  (string-append
   (format #f "// system: ~a\n" (.number system))
   (if (member 'canvas (debug?)) (draw-staff o system) "")
   (string-join
    (map (lambda (x) (.script (.canvas-item x))) (.items system))
    "\n")))

(define-method (draw-staff (o <web-music-canvas>) (system <system>))
  (and-let* ((rect (paper-staff-rect system (.paper (.notation o))))
	     (off (offset rect))
	     (x (x off))
	     (yo (y off))
	     (y (if (<0 (height rect)) (+ yo (height rect)) yo)))
	    (string-append
	     (format #f "paper.rect (~a,~a,~a,~a)" x y (width rect) (- (height rect)))
	     (format #f ".attr ({'fill': '#500000', 'fill-opacity': 0.5, opacity: 0.5, 'stroke-width': ~a})" (1/ (.pixels-per-unit o)))
	     ";\n"
	     ;;look at document
	     ;;(format #f ".click (~a);\n" "on_staff_click")
	     )))
     
(define-method (script-add (o <raphael-item>) . rest)
  (set! (.script o) (apply string-append (cons* (.script o) rest))))

(define-method (activate (o <web-music-canvas>) (nitem <notation-item>))
  (let ((music (.music nitem)))
    (script-add
     (.canvas-item nitem)
     (format #f ".click (~a)" "on_item_click")
     (format #f ".dblclick (~a)"
	     (cond ((is-a-clef-glyph? music) "edit_clef")
		   ((is-a-key-change-event? music) "edit_key")
		   ((is-a-time-signature? music) "edit_time")
		   ((is-a-slur? music) "edit_slur")
		   (else "on_item_dblclick")))
     (cond ((is-a-note? music)
	    (let* ((patch "piano")
		   (pitch (.pitch music))
		   (duration (.duration music))
		   (log 2))
	      (format #f ".hover (function (e) { on_note_hover_in (e, ['~a', ~a, ~a, ~a, ~a]); }, on_item_hover_out)" patch (.octave pitch) (.step pitch) (* (.alteration pitch) 2) log)))
	      (else (format #f ".hover (~a, ~a)" "on_item_hover_in" "on_item_hover_out")))
     (if music
	 (format #f ".drag (text_drag_move, text_drag_start, function () { text_drag_end ('~a'); } )" (.input-tag music))
	 "")
     ";\n"))
  nitem)

(define-method (get-path (o <notation-item-path>))
  (let* ((params (cdr (.fields o))))
    (map string->number (filter string->number (cddddr params)))))

;; See also  pointer-events="boundingBox" 
(define-method (activate (o <web-music-canvas>) (nitem <notation-item-path>))
  (let* ((path (get-path nitem))
	 (origin (+ (.offset o) (flip-y (.offset nitem))))
	 (control (map flip-y (list->points path))))
    (debugf 'slur "path: ~a\n" path)
    (debugf 'slur "nitem-offset: ~a\n" (.offset nitem))
    (debugf 'slur "offset: ~a\n" (.offset o))
    (debugf 'slur "origin: ~a\n" origin)
    (debugf 'slur "control: ~a\n" control)
    (script-add
     (.canvas-item nitem)
     ".clone ()"
     ".toBack ()"
     ".attr ({stroke: select_fill, 'stroke-width': 1, 'stroke-opacity': 0})"
     ".click (function (e) { var bbox = this.getBBox (); var cx = (bbox.x2 - bbox.x) / 3; var cy = (bbox.y2 - bbox.y); this.control_points.forEach (attr ({opacity: 1, 'stroke-opacity': 1})); })"
     ".mouseover (function (e) { this.attr ({'stroke-opacity': 0.8}); this.control_points.forEach (attr ({opacity: 1, 'stroke-opacity': 1})); })"
     ".mouseout (function (e) { e = pointer_compat (e, $ ('#score')); if (!Raphael.isPointInsideBBox (this.getBBox (), e.offsetX / ppu, e.offsetY / ppu)) { this.attr ({'stroke-opacity': 0}); this.control_points.forEach (attr ({opacity: 0, 'stroke-opacity': 0})); } return true; })"
     ".set_control_points (["
     (apply format (cons* #f "control_point.clone ().transform  ('T~a,~at~a,~a').drag (cp_drag_move, cp_drag_start, function () { cp_drag_end ('~a'); } )," (append origin (append (list-ref control 0) origin))))
     (apply format (cons* #f "control_point.clone ().transform  ('T~a,~at~a,~a').drag (cp_drag_move, cp_drag_start, function () { cp_drag_end ('~a'); } )," (append origin (append (list-ref control 1) origin))))
     (apply format (cons* #f "control_point.clone ().transform  ('T~a,~at~a,~a').drag (cp_drag_move, cp_drag_start, function () { cp_drag_end ('~a'); } )," (append origin (append (list-ref control 2) origin))))
     (apply format (cons* #f "control_point.clone ().transform  ('T~a,~at~a,~a').drag (cp_drag_move, cp_drag_start, function () { cp_drag_end ('~a'); } )," (append origin (append (list-ref control 3) origin))))
     "]);"))
  nitem)

(define-method (translate (o <web-music-canvas>) (citem <raphael-item>) origin offset)
  (script-add
   citem
   (apply format (cons* #f ".transform ('t~a,~a')"
			(+ origin (flip-y offset)))))
  citem)


;;; Test

(define (test . args)
  (let* ((music (test-music))
	 (canvas (make <web-music-canvas>))
	 (system ((@ (schikkers test mock-system) mock-system)))
	 (xx (set-paper canvas (make <paper>)))
	 (raphael (draw canvas system))
	 (file (open-file "score.js" "w")))
    (display raphael file)))
