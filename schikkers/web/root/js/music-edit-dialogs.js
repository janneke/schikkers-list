// -*- c-style: gnu -*-

// This file is part of Schikkers List, a GUI platform for LilyPond.
//
// Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
//
// Schikkers List is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Schikkers List is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

function field_info (t)
{
  var tips = $ (".validate-tips");

  tips.text (t).addClass ('ui-state-highlight');
  setTimeout (function () { tips.removeClass ('ui-state-highlight', 1500 ); },
	      500 );
}

function validate_length (o, n, min, max)
{
  if (o.val ().length > max || o.val ().length < min ) {
    o.addClass ('ui-state-error');
    field_info ('Length of ' + n + ' must be between ' + min + ' and ' + max + '.');
    return false;
  }
  return true;
}
 
function validate (o, re, n)
{
  if (! (re.test (o.val ())))
    {
      o.addClass ('ui-state-error' );
      field_info (n);
      return false;
    }
  return true;
}

function edit_clef_dialog ()
{
    var glyph = $ ("#glyph");
    var position = $ ("#position");
    var middle_c = $ ("#middle-c");
    var fields = $ ([])
      .add (glyph)
      .add (position)
      .add (middle_c);

  $ ('#edit-clef-dialog' ).dialog ({
    autoOpen: false,
	height: 410,
	width: 350,
	modal: false,
	buttons:
      {
      'edit clef': function ()
	  {
	    var valid = true;
	    fields.removeClass ('ui-state-error');
	    
	    valid = valid && validate_length (glyph, 'glyph', 7, 9 );
	    valid = valid && validate_length (position, 'position', 1, 2);
	    valid = valid && validate_length (middle_c, 'middle-c', 1, 2);
	    
	    valid = valid && validate (glyph, /^clefs[.](G|F|C(|[.]2))$/,
				       'clefs.G, clefs.F, clefs.C, clefs.C.2');
	    valid = valid && validate (position, /^-?[0-9]*/, 'integer');
	    valid = valid && validate (middle_c, /^-?[0-9]*/, 'integer');
	    
	    if (valid)
	      {
		calling_lily ('&clef-details='
			      + glyph.val ()
			      + ','
			      + position.val ()
			      + ','
			      + middle_c.val ());
		$ (this ).dialog ('close');
	      }
	  },
	  'cancel': function ()
	  {
	    $ (this).dialog ('close');
	  }
      },
	'close': function ()
	{
	  fields.val ('').removeClass ('ui-state-error');
	}
    });
}

function open_clef_dialog (e)
{
  $ ('ul.hover-menu').hide ();
  $ ('#edit-clef-dialog').dialog ('open');
  $ ('#glyph').val (clef_music['glyph']);
  $ ('#position').val (clef_music['position']);
  $ ('#middle-c').val (clef_music['middle-c']);
}

function edit_key_dialog ()
{
    var tonic = $ ("#tonic");
    var alteration = $ ("#alteration");
    var scale = $ ("#scale");
    var fields = $ ([])
      .add (tonic)
      .add (alteration)
      .add (scale);

  $ ('#edit-key-dialog' ).dialog ({
    autoOpen: false,
	height: 410,
	width: 350,
	modal: true,
	buttons:
      {
      'edit key': function ()
	  {
	    var valid = true;
	    fields.removeClass ('ui-state-error');
	    
	    valid = valid && validate_length (tonic, 'tonic', 1, 1 );
	    valid = valid && validate_length (alteration, 'alteration', 1, 4);
	    valid = valid && validate_length (scale, 'scale', 15, 33);
	    
	    valid = valid && validate (tonic, /^[0-6]$/, '0..6');
	    valid = valid && validate (alteration, /^(0|-?1\/2)$/, '0, 1/2, -1/2');
	    valid = valid && validate (scale, /^((0|-?1\/2) ){6}(0|-?1\/2)$/, '0, 1/2, -1/2 *7');
	    
	    if (valid)
	      {
		calling_lily ('&key-details='
			      + tonic.val ()
			      + ','
			      + alteration.val ()
			      + ','
			      + scale.val ());
		$ (this ).dialog ('close');
	      }
	  },
	  'cancel': function ()
	  {
	    $ (this).dialog ('close');
	  }
      },
	'close': function ()
	{
	  fields.val ('').removeClass ('ui-state-error');
	}
    });
}

function open_key_dialog (e)
{
  $ ('ul.hover-menu').hide ();
  $ ('#edit-key-dialog').dialog ('open');
  $ ('#tonic').val (key_music['tonic']);
  $ ('#alteration').val (key_music['alteration']);
  $ ('#scale').val (key_music['scale']);
}

function edit_time_dialog ()
{
    var numerator = $ ("#numerator");
    var denominator = $ ("#denominator");
    var style = $ ("#style");
    var fields = $ ([])
      .add (numerator)
      .add (denominator)
      .add (style);

  $ ('#edit-time-dialog' ).dialog ({
    autoOpen: false,
	height: 410,
	width: 350,
	modal: true,
	buttons:
      {
      'edit time': function ()
	  {
	    var valid = true;
	    fields.removeClass ('ui-state-error');
	    
	    valid = valid && validate_length (numerator, 'numerator', 1, 2 );
	    valid = valid && validate_length (denominator, 'denominator', 1, 3);
	    valid = valid && validate_length (style, 'style', 7, 7);
	    
	    valid = valid && validate (numerator, /^[1-9][0-9]*$/, 'positive integer');
	    valid = valid && validate (denominator, /^(1|2|4|8|16|32|64|128?)$/, 'duration (1, 2, 4, 8 ..)');
	    valid = valid && validate (style, /^(default|numeric)$/, 'default or numeric');
	    
	    if (valid)
	      {
		calling_lily ('&time-details='
			      + numerator.val ()
			      + ','
			      + denominator.val ()
			      + ','
			      + style.val ());
		$ (this ).dialog ('close');
	      }
	  },
	  'cancel': function ()
	  {
	    $ (this).dialog ('close');
	  }
      },
	'close': function ()
	{
	  fields.val ('').removeClass ('ui-state-error');
	}
    });
}

function open_time_dialog (e)
{
  $ ('ul.hover-menu').hide ();
  $ ('#edit-time-dialog').dialog ('open');
  $ ('#numerator').val (time_music['numerator']);
  $ ('#denominator').val (time_music['denominator']);
  $ ('#style').val (time_music['style']);
}

function connect_music_edit_dialogs ()
{
  $ ('#any-clef').click (open_clef_dialog);
  edit_clef_dialog ();

  $ ('#any-key').click (open_key_dialog);
  edit_key_dialog ();

  $ ('#any-time').click (open_time_dialog);
  edit_time_dialog ();
}
