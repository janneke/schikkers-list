// -*- c-style: gnu -*-

// This file is part of Schikkers List, a GUI platform for LilyPond.
//
// Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
//
// Schikkers List is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Schikkers List is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.


function selection_start_ (x, y, e)
{
  var height = cwhen.attr ('height');
  var y = cwhen.attr ('y');
  if (ctrl)
    {
      height = 0;
      y = y/ppu;
    }
  select_rect.attr ({x: x/ppu, y: y, width: 0, height: height});
}

function selection_start (x, y, e)
{
  e = pointer_compat (e, $ ('#score'));
  selection_start_ (e.offsetX, e.offsetY, e);
  return true;
}

function selection_move (dx, dy, x, y, e)
{
  var offset_x = 0;
  var offset_y = 0;
  selecting = dx > 2;
  dx /= ppu;
  dy /= ppu;
  if (dx < 0)
    {
      offset_x = dx;
      dx *= -1;
    }
  if (!ctrl)
    dy = select_rect.attr ('height');
  if (dy < 0)
    {
      offset_y = dy;
      dy *= -1;
    }
  select_rect.transform ('T' + offset_x + ',' + offset_y);
  if (selecting)
    select_rect.attr ({width: dx, height: dy})
}

function intersect_p (a, b)
{
  return intersect_x_p (a, b) && intersect_y_p (a, b);
}

function intersect_x_p (a, b)
{
  return (a.x >= b.x && a.x <= b.x2 || a.x2 >= b.x && a.x2 <= b.x2);
}

function intersect_y_p (a, b)
{
  return (a.y >= b.y && a.y <= b.y2 || a.y2 >= b.y && a.y2 <= b.y2);
}

function selection_end (e)
{
  var bbox = select_rect.getBBox ();
  console.log ('select:', bbox.x + ',' + bbox.y + ',' + bbox.x2 + ',' + bbox.y2);
  selection_clear ();
  selecting = (bbox.x2 - bbox.x) * ppu > 1;
  if (!selecting)
    return true;
  set.forEach (function (x) {
      if (intersect_p (x.getBBox (), bbox))
	select_items.push (x);
    });
  select_items.attr ({fill: selected_fill, opacity: 0.5});
  select_region = bbox;
  selection_buttons ();
}

function selection_clear ()
{
  selecting = false;
  selection_buttons ();
  select_items = paper.set ();
  set.attr ({fill: 'black', opacity: 1});
  select_rect.attr ({width: 0, height: 0});
  select_region = false;//select_rect.getBBox ();
}
