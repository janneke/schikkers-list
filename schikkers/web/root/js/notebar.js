// -*- c-style: gnu -*-

// This file is part of Schikkers List, a GUI platform for LilyPond.
//
// Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
//
// Schikkers List is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Schikkers List is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

$ (function ()
   {
     $ ("#duration").buttonset ();
     $ ('#note-0').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#note-1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#note-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#note-3').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#note-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#note-5').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#note-6').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#note-7').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dot').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#rest').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#alteration-1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#alteration--1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#ottava-1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#ottava--1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#button-clef-menu').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#clefs-G').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#clefs-F').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#clefs-C').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#clefs-C-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#any-clef').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#button-key-menu').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#key-C').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#key-G').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#key-D').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#key-A').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#key-E').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#key-F').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#key-BES').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#key-EES').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#key-AES').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#any-key').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#button-time-menu').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#qtime-4-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#qtime-2-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#qtime-3-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#qtime-3-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-1-1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-2-1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-3-1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-4-1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-5-1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-1-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-C-').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-2-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-3-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-4-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-5-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-6-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-1-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-2-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-3-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-C').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-4-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-5-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-6-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-7-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-8-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-9-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-12-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-1-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-2-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-3-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-4-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-5-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-6-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-7-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-8-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-9-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-12-8').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-1-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-2-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-3-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-4-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-5-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-6-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-7-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-8-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-9-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#time-12-16').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#any-time').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#button-articulation-menu').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-clear').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-digit-0').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-digit-1').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-digit-2').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-digit-3').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-digit-4').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-digit-5').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-accent').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-coda').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-downbow').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-downmordent').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-downprall').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-espressivo').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-fermata').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-flageolet').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-halfopen').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-lheel').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-lineprall').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-longfermata').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-ltoe').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-marcato').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-mordent').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-open').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-portato').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-prall').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-pralldown').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-prallmordent').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-prallprall').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-prallup').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-reverseturn').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-rheel').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-rtoe').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-segno').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-shortfermata').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-signumcongruentiae').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-snappizzicato').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-staccatissimo').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-staccato').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-stopped').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-tenuto').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-thumb').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-trill').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-turn').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-upbow').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-upmordent').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-upprall').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-varcoda').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#articulation-verylongfermata').button ({text: false, icons: {primary: 'ui-icon-custom'}});

     $ ('#button-dynamic-menu').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-mp').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-p').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-pp').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-ppp').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-pppp').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-ppppp').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-mf').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-f').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-ff').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-fff').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-ffff').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-fffff').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-clear').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-sp').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-spp').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-sf').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-sff').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-fp').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-sfz').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#dynamic-rfz').button ({text: false, icons: {primary: 'ui-icon-custom'}});

     $ ('#slur').button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ('#tie').button ({text: false, icons: {primary: 'ui-icon-custom'}});

   });
