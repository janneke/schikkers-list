// -*- c-style: gnu -*-

// This file is part of Schikkers List, a GUI platform for LilyPond.
//
// Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
//
// Schikkers List is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Schikkers List is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

// FIXME: c&p from bezier-bow.cc
function F0_1 (x)
{
  return ((2 / Math.PI) * (Math.atan (PI * x / 2)));
}

function slur_height (w, h_inf, r_0)
{
  return F0_1 (((w * r_0) / h_inf) * h_inf);
}

function make_slur (width, thickness)
{
  var height_limit = 1;
  var ratio = 0.33;
  var angularity = 0.5;
  var height = slur_height (width, height_limit, ratio);
  return make_bow (width, - height, thickness, angularity);
}

function slur_drag (id, x, y)
{
  //TODO
  //calling_lily ('&drag=' + id + ',' + x.toPrecision (5) + ',' + y.toPrecision (5));
  
}

function cp_drag_move (dx, dy)
{
  var x = dx / ppu;
  var y = dy / ppu;
  this.translate (x - self.x, y - self.y);
  self.x = x;
  self.y = y;
  dragging = true;
  return true;
}

function cp_drag_start (e) { self.x = 0; self.y = 0; return true; }
function cp_drag_end (id) { slur_drag (id, self.x, self.y); return true; }
