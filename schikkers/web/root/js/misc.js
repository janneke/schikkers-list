// -*- c-style: gnu -*-

// This file is part of Schikkers List, a GUI platform for LilyPond.
//
// Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
//
// Schikkers List is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Schikkers List is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

; (function ($)
   {
     $.fn.caret_position = function ()
       {
	 var input = this[0];
	 if ('selectionStart' in input)
	   return input.selectionStart;
	 else if ('selection' in document)
	   {
	     input.focus ();
	     var sel = document.selection.createRange ();
	     var len = document.selection.createRange ().text.length;
	     sel.moveStart ('character', -input.value.length);
	     return sel.text.length - len;
	   }
	 return 0;
       };
   })(jQuery);

; (function ($)
    {
      $.fn.select_range = function (start, end)
	{
	  if (!end)
	    end = start; 
	  var input = this[0];
	  if (input.setSelectionRange)
	    input.setSelectionRange (start, end);
	  else if (input.createTextRange)
	    {
	      var range = input.createTextRange ();
	      range.collapse (true);
	      range.moveEnd ('character', end);
	      range.moveStart ('character', start);
	      range.select ();
	    }
	};
    })(jQuery);

; (function ($)
   {
     $.fn.set_caret = function (position)
       {
	 this.select_range (position);
       };
   })(jQuery);

; (function ($)
   {
     $.fn.get_scroll_bar_width = function ()
       {
	 var inner = document.createElement ('p');
	 inner.style.width = '100%';
	 inner.style.height = '200px';
	 
	 var outer = document.createElement ('div');
	 outer.style.position = 'absolute';
	 outer.style.top = '0px';
	 outer.style.left = '0px';
	 outer.style.visibility = 'hidden';
	 outer.style.width = '200px';
	 outer.style.height = '150px';
	 outer.style.overflow = 'hidden';
	 outer.appendChild (inner);
	 
	 document.body.appendChild (outer);
	 var w1 = inner.offsetWidth;
	 outer.style.overflow = 'scroll';
	 var w2 = inner.offsetWidth;
	 if (w1 == w2) w2 = outer.clientWidth;
	 
	 document.body.removeChild (outer);
	 
	 return (w1 - w2);
       };
   })(jQuery);
