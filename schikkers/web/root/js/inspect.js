// -*- c-style: gnu -*-

// This file is part of Schikkers List, a GUI platform for LilyPond.
//
// Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
//
// Schikkers List is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Schikkers List is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

var root = './';
var server = '';
var query = $.getQuery ();
var user = query['user'] || 'nobody';
var cookie = '?user=' + user + '&callback=?';

; (function ($)
   {
     $.fn.require = function (file)
       {
	 $.ajax({
	   url: server + root + file + cookie,
	   dataType: 'script',
	   async: false,
	   /* chrome needs --allow-file-access-from-files */
	   headers : {Accept : 'application/javascript',
		      'Access-Control-Allow-Origin' : '*',
		      'Access-Control-Allow-Origin' : 'null',
		      'Access-Control-Allow-Origin' : 0,
		      'Access-Control-Allow-Origin' : null,
	     },
	   success: function ()
	   {
	   },
	   error: function (request, status, message)
	   {
	     $ ('#result').html ('error: ' + message + ': cannot load: ' + file);
	   }
	   });
       };
   })(jQuery);

var require = $().require;


$ (document).ready (function () {
  $ ('#score').css ({display: 'block'});
  require ('js/dump.js');
})
