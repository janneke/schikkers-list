// -*- c-style: gnu -*-

// This file is part of Schikkers List, a GUI platform for LilyPond.
//
// Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
//
// Schikkers List is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Schikkers List is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

var audio = audio_initialize (true);

function audio_load ()
{
  var dir = 'audio/';
  var legacy = dir + 'legacy/';
  var patches = ['piano'];
  var octaves = [-1, 0, 1];
  var steps = [0, 1, 2, 3, 4, 5, 6];
  var alterations = [-2, -1, 0, 1, 2];
  var durations = [2];
  patches.forEach
    (function (patch) {
      octaves.forEach (function (octave) {
	  steps.forEach (function (step) {
	      alterations.forEach (function (alt) {
		  durations.forEach (function (log) {
		      var note = '' + [patch, octave, step, alt, log].join ('-');
		      var base = dir + note;
		      var lbase = legacy + note;
		      if (isSafari)
			audio.load ([base + '.ogg', lbase + '.mp3'], note);
		      else
			audio.load ([base + '.ogg'], note);
		    });
		});
	    });
	});
    });
}

audio_load ();

