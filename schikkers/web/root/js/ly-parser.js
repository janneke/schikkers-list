// -*- c-style: gnu -*-

// This file is part of Schikkers List, a GUI platform for LilyPond.
//
// Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
//
// Schikkers List is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Schikkers List is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

function Music_duration (log, dots, factor)
{
  this.log = log;
  if (log == undefined)
    this.log = 2;
  this.dots = dots || 0;
  this.factor = factor || 1;
  this.to_number = function ()
    {
      var dots_factor = 1;
      if (this.dots == 1)
	dots_factor = (1 + 0.5) / 1;
      if (this.dots == 2)
	dots_factor = (1 + 0.5 + 0.25) / 1;
      return (1 / Math.pow (2, this.log)) * dots_factor * this.factor;
    };
}

function Music_pitch (step, alteration, octave)
{
  this.step = step || 0;
  this.alteration = alteration || 0;
  this.octave = octave || 0;
}

function Music_note (pitch, duration)
{
  this.pitch = pitch || new Music_pitch ();
  this.duration = duration || new Music_duration ();
}

function Music_rest (duration)
{
  this.duration = duration || new Music_duration ();
}

function is_a_duration_p (x)
{
  return x.constructor == Music_duration;
}

function is_a_note_p (x)
{
  return x.constructor == Music_note;
}

function is_a_rest_p (x)
{
  return x.constructor == Music_rest;
}

function is_a_note_or_rest_p (x)
{
  return is_a_note_p (x) || is_a_rest_p (x);
}

function Cons (car, cdr)
{
  this.car = car;
  this.cdr = cdr;
}

function append (x, y) { return x.concat (y); }
function cons (car, cdr) { return new Cons (car, cdr); }
function car (x) { if (x.car == undefined) return x[0]; else return x.car; }
function cdr (x) { if (x.cdr == undefined) return x.slice (1); else return x.cdr; }
function char_p (x) { return (typeof (x) == typeof ('') && x.length == 1); }
function list_p (x) { return (typeof (x) == typeof ([]) && x.length > 0 && x.car == undefined); }
function member_p (lst, x) { return lst.indexOf (x) != -1; }
function null_p (x) { return x == undefined || x.length == 0; }
function procedure_p (x) { return (typeof (x) == typeof (function () {})); }
function string_p (x) { return typeof (x) == typeof (''); }

var input = '';

var cursor = new Music_note ();
var free_ahead = 0;

function duration_radio_update ()
{
  var log = 2;
  if (is_a_note_or_rest_p (cursor))
    log = cursor.duration.log;
  radio_update ('#duration', log);

  $ ('#note-0').button ({disabled: false});
  $ ('#note-1').button ({disabled: false});
  $ ('#note-2').button ({disabled: false});
  $ ('#note-3').button ({disabled: false});
  $ ('#note-4').button ({disabled: false});
  $ ('#note-5').button ({disabled: false});
  $ ('#note-6').button ({disabled: false});
  $ ('#note-7').button ({disabled: false});
  $ ('#dot').button ({disabled: false});
  if (is_a_note_p (cursor))
    {
      if (free_ahead < 1)
	$ ('#note-0').button ({disabled: true});
      if (free_ahead < 1 / 2)
	$ ('#note-1').button ({disabled: true});
      if (free_ahead < 1 / 4)
	$ ('#note-2').button ({disabled: true});
      if (free_ahead < 1 / 8)
	$ ('#note-3').button ({disabled: true});
      if (free_ahead < 1 / 16)
	$ ('#note-4').button ({disabled: true});
      if (free_ahead < 1 / 32)
	$ ('#note-5').button ({disabled: true});
      if (free_ahead < 1 / 64)
	$ ('#note-6').button ({disabled: true});
      if (free_ahead < 1 / 128)
	$ ('#note-7').button ({disabled: true});
      var n = cursor.duration.to_number ();
      switch (cursor.duration.dots)
	{
	case 0:
	  if (free_ahead < 1.5 * n)
	    $ ('#dot').button ({disabled: true});
	  break;
	case 1:
	  if (free_ahead < (1.75 / 1.5) * n)
	    $ ('#dot').button ({disabled: true});
	  break;
	}
    }

  if ($ ('#rest').prop ('checked'))
    {
      $ ('#duration').children ().addClass ('rest');

      $ ('#label-note-0').children ().addClass ('rest');
      $ ('#label-note-1').children ().addClass ('rest');
      $ ('#label-note-2').children ().addClass ('rest');
      $ ('#label-note-3').children ().addClass ('rest');
      $ ('#label-note-4').children ().addClass ('rest');
      $ ('#label-note-5').children ().addClass ('rest');
      $ ('#label-note-6').children ().addClass ('rest');
      $ ('#label-note-7').children ().addClass ('rest');

      $ ('#label-dot').children ().addClass ('rest');
      $ ('#dot').children ().addClass ('rest');
      $ ('#label-rest').children ().addClass ('rest');
      $ ('#rest').children ().addClass ('rest');
    }
  else
    {
      $ ('#duration').children ().addClass ('rest');

      $ ('#label-note-0').children ().removeClass ('rest');
      $ ('#label-note-1').children ().removeClass ('rest');
      $ ('#label-note-2').children ().removeClass ('rest');
      $ ('#label-note-3').children ().removeClass ('rest');
      $ ('#label-note-4').children ().removeClass ('rest');
      $ ('#label-note-5').children ().removeClass ('rest');
      $ ('#label-note-6').children ().removeClass ('rest');
      $ ('#label-note-7').children ().removeClass ('rest');

      $ ('#label-dot').children ().removeClass ('rest');
      $ ('#dot').children ().removeClass ('rest');
      $ ('#label-rest').children ().removeClass ('rest');
      $ ('#rest').children ().removeClass ('rest');
    }

  $ ('#dot').button ('refresh');
  $ ('#rest').button ('refresh');
  try { $ (id).children ().button ('refresh'); } catch (e) { };
}

function checkbox_update (cb, checked_p)
{
  $ (cb).button ().removeProp ('checked');
  if (checked_p)
    $ (cb).button ().prop ('checked', 'checked');
  $ (cb).button ('refresh');
}

function rest_checkbox_update ()
{
  if (is_a_note_or_rest_p (cursor))
    checkbox_update ($ ('#rest'), is_a_rest_p (cursor));
}

var tie_active_p = false;
var tie_possible_p = false;
function tie_checkbox_update ()
{
  checkbox_update ($ ('#tie'), tie_active_p);
  $ ('#tie').button ({disabled: !tie_possible_p});
}

function toolbar_update ()
{
  rest_checkbox_update ();
  duration_radio_update ();
  tie_checkbox_update ();
}

// Translated from ly-parser.scm
function parse_ly (pos, chr)
{
  var space_p = $ ('#editor').val ()[Math.max (pos - 1, 0)] == ' ';
  var text = input + String.fromCharCode (chr);

  var parse_tree
    =
    [
     [
      function (text) { return space_p; },
      cons (['a', 'b', 'c', 'd', 'e', 'f', 'g'], 'note')],
     [
      function (text) { return !space_p; },
      [
       function (text) { return is_a_note_p (cursor); },
       cons ("'", 'higher'),
       cons (',', 'lower'),
       cons (['1 ', '2 ', '8 '], 'duration-space'),
       cons (['4', '8', '16', '32', '64', '128'], 'duration'),
       cons (['1', '12', '3', '6'], true),
       cons (
	     function (text) { return (cursor.duration.log == 0
				       && member_p (['2', '6'], text)); },
	     cons ('eat', 1)),
       [
	function (text) {
	  return (cursor.pitch.alteration >= 0
		  && cursor.pitch.alteration < 1.5); },
	cons ('i', true),
	cons ('is', 'sharpen')],
       [
	function (text) {
	  return (cursor.pitch.alteration <= 0
		  && cursor.pitch.alteration > -1.5); },
	cons ('e', true),
	cons ('es', 'flatten'),
	cons (function (text) {
	    return ((cursor.pitch.step == 2
		     || cursor.pitch.step == 5)
		    && text == 's'); },
	  'flatten'),
	cons ('-', true),
	cons ([
	       '-+',
	       '--',
	       '-.',
	       '-0',
	       '-1',
	       '-2',
	       '-3',
	       '-4',
	       '-6',
	       '-<',
	       '-^',
	       '-_',
	       '-|',
	       ], 'articulation'),
	cons (' ', 'space')]]]];
  
  var stack = 0;
  function loop (tree, path)
  {
    stack += 1;
    console.log ('');
    console.log ('*************************');
    console.log ('tree:', tree);
    console.log ('null_p-tree:', null_p (tree));
    console.log ('path:', path);
    console.log ('null_p-path:', null_p (path));
    console.log ('type path:', typeof (path));
    if (stack > 30)
      {
	alert ('stack overflow');
	return false;
      }
    if (null_p (tree))
      {
	if (null_p (path))
	  return false;
	else
	  return loop ([path[0]].concat ([tree]), path.slice (1));
      }
    else
      {
	var sub_tree = car (tree);
	console.log ('sub_tree:', sub_tree);
	var head = car (sub_tree);
	console.log ('head:', head);
	var tail = cdr (tree);
	console.log ('tail:', tail);
	if (list_p (head))
	  {
	    console.log ('list_p');
	    console.log ('member', text, head, member_p (head, text));
	  }
	else
	  console.log ('NOT list_p', head);
	if (procedure_p (head))
	  {
	    console.log ('proc -->', head (text));
	    console.log ('==>', cdr (sub_tree));
	    console.log ('==>', cdr (sub_tree));
	  }
	if ((char_p (head)
	     && (text.length == 1)
	     && (text == head))
	    || (string_p (head)
		&& text == head)
	    || (list_p (head)
		&& member_p (head, text))
	    || (procedure_p (head)
		&& head (text)))
	  {
	    if (list_p (sub_tree))
	      {
		console.log ('reloop: ', cdr (sub_tree));
		return loop (cdr (sub_tree), append (tail, path));
	      }
	    else
	      return cdr (sub_tree);
	  }
	else
	  return loop (tail, path);
      }
  }
  
  console.log ('========================');
  console.log ('chr', chr);
  console.log ('input', text);
  console.log ('text', text);

  var m = loop (parse_tree, []);

  console.log ('m:', m, '(' + typeof (m) + ')');
  if (m == true && typeof (m) == typeof (true))
    {
      input += text;
      return true;
    }
  else if (m)
    {
      input = '';
      var query = '&edit-details=' + pos + ',' + text + ',';
      return calling_lily (query + m);
    }
  return false;
}
