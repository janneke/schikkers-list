var paper = new ScaleRaphael ('score', 119.50157480314964, 17.07165354330709);
var ppu = 9.313225746154785;
paper.scaleAll (ppu, center=true, clipping=true);
var dragging = false;

function on_item_click (e) { this.attr ({fill: 'orange'}); };
function on_item_dblclick (e) { this.attr ({fill: 'green'}); };
function on_item_hover_in (e) { this.attr ({fill: 'red'}); };
function on_item_hover_out (e) { this.attr ({fill: 'black'}); };

function edit_clef (e) { open_clef_dialog (e); };
function edit_key (e) { open_key_dialog (e); };
function edit_time (e) { open_time_dialog (e); };

function drag (id, x, y) { calling_lily ('&drag=' + id + ',' + x.toPrecision (5) + ',' + y.toPrecision (5)); dragging = true; }
function text_drag_move (dx, dy) { var x = dx / ppu, y = dy / ppu; this.translate (x - self.x, y - self.y); self.x = x; self.y = y; }
function text_drag_start () { self.x = 0; self.y = 0; }
function text_drag_end (id) { return drag (id, self.x, self.y); }

var cwhen = paper.rect (1,1,2,2,0);
cwhen.attr ({stroke: "#0036ff", fill: "#ebeef7", 'stroke-width': 0.1073741824, 'stroke-linejoin': 'round'});
cwhen.attr ({x: 75.14494953630503, y: 0.6381102362204727, width: 1.8426492000000108, height: 8.8975});

var cnitem = paper.rect (0,0,1,1,0);
cnitem.attr ({stroke: "#001872", fill: "#0036ff", 'stroke-width': 0.1073741824, 'stroke-linejoin': 'round'});
cnitem.attr ({x: 75.40818513630504, y: 4.435549905511813, width: 1.3161780000000078, height: 1.100012});


paper.rect (0.0,-2.0,0.19,4.0,0.0).attr ({stroke: "black", fill: "black", 'stroke-width': 0.0, 'stroke-linejoin': 'round'}).transform ('t113.62102362204725,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('24'); } );

paper.path ('m0.05,0.0L113.191968503937,0.0').attr ({stroke: "black", fill: "black", 'stroke-width': 0.1}).transform ('t0.5690551181102363,7.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('m0.05,0.0L113.191968503937,0.0').attr ({stroke: "black", fill: "black", 'stroke-width': 0.1}).transform ('t0.5690551181102363,6.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('m0.05,0.0L113.191968503937,0.0').attr ({stroke: "black", fill: "black", 'stroke-width': 0.1}).transform ('t0.5690551181102363,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('m0.05,0.0L113.191968503937,0.0').attr ({stroke: "black", fill: "black", 'stroke-width': 0.1}).transform ('t0.5690551181102363,4.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('m0.05,0.0L113.191968503937,0.0').attr ({stroke: "black", fill: "black", 'stroke-width': 0.1}).transform ('t0.5690551181102363,3.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (8.4709495,0.0,1.7742669999999994,0.02,0.1).attr ({stroke: "black", fill: "black", 'stroke-width': 0.2, 'stroke-linejoin': 'round'}).transform ('t0.5690551181102363,8.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (0.0,-2.0,0.19,4.0,0.0).attr ({stroke: "black", fill: "black", 'stroke-width': 0.0, 'stroke-linejoin': 'round'}).transform ('t51.563326475378936,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('24'); } );

paper.rect (0.0,-2.0,0.19,4.0,0.0).attr ({stroke: "black", fill: "black", 'stroke-width': 0.0, 'stroke-linejoin': 'round'}).transform ('t29.059417695963933,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('24'); } );

paper.rect (0.0,-2.0,0.19,4.0,0.0).attr ({stroke: "black", fill: "black", 'stroke-width': 0.0, 'stroke-linejoin': 'round'}).transform ('t74.19340105457734,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('24'); } );

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t30.142892511467934,6.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('13'); } );

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t47.334019357095336,4.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('17'); } );

paper.rect (-0.025,-0.46,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t10.520227118110236,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,-2.96,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t36.282516477359934,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,-0.27218600000000004,0.05,3.042186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t44.89099783771533,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,0.22781400000000002,0.05,3.06551933333333,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t40.002551871823336,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('M0.858088999999993 -1.20341828195028 C1.77125131234561 -2.03949698305434 5.1541740305228 -2.03949698305434 6.06733634286842 -1.20341828195028 C5.1541740305228 -1.91949698305434 1.77125131234561 -1.91949698305434 0.858088999999993 -1.20341828195028 z').attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round', 'stroke-linecap': 'round'}).transform ('t69.79893779343664,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue931",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t49.100193357095336,4.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t9.269049118110237,8.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('9'); } );

paper.print (-1.985,28/75,"\ue973",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t1.3690551181102364,6.485555905511813').click (on_item_click).dblclick (edit_clef).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('4'); } );

paper.rect (-0.025,-0.27218600000000004,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t69.86393779343665,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,-2.46,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t31.394070511467934,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,-0.27218600000000004,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t75.47318513630505,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,-1.46,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t20.297119049894235,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t75.40818513630504,4.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('21'); } );

paper.rect (-0.025,-0.27218600000000004,0.05,3.042186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t47.399019357095334,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,-0.7721859999999999,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t55.351132076486635,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t35.03133847735994,5.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('14'); } );

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t44.825997837715335,4.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('16'); } );

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t64.82129582754465,5.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('20'); } );

paper.print (0,0,"4",paper.getFont ("Century Schoolbook L",0),1.7346720833333331).attr ({'text-anchor': 'start', fill: "black"}).transform ('t73.33238845615215,2.435555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t39.93755187182334,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('15'); } );

paper.print (-1.985,28/75,"\ue979",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t5.569055118110237,5.485555905511813').click (on_item_click).dblclick (edit_time).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('6'); } );

paper.print (-1.985,28/75,"\uea22",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t69.92893779343665,8.445555905511814').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,-1.272186,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t60.23957804237863,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t55.28613207648664,4.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('18'); } );

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t52.778110557106636,4.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('17'); } );

paper.print (-1.985,28/75,"\ue931",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t71.56511179343664,4.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\uea23",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t52.90811055710663,8.445555905511814').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\uea23",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t75.53818513630505,8.445555905511814').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t14.157495084002237,7.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('10'); } );

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t69.79893779343664,4.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('21'); } );

paper.path ('M0.858089000000007 -1.20562274895673 C1.75528755705302 -2.03498597882394 5.00498164295828 -2.03498597882394 5.90218020001129 -1.20562274895673 C5.00498164295828 -1.91498597882394 1.75528755705302 -1.91498597882394 0.858089000000007 -1.20562274895673 z').attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round', 'stroke-linecap': 'round'}).transform ('t47.334019357095336,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (0,0,"3",paper.getFont ("Century Schoolbook L",0),1.7346720833333331).attr ({'text-anchor': 'start', fill: "black"}).transform ('t50.70231387695374,2.4014125984251926').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t23.934387015786236,6.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('12'); } );

paper.rect (-0.025,-0.27218600000000004,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t52.843110557106634,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t19.045941049894235,7.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('11'); } );

paper.print (0,0,"5",paper.getFont ("Century Schoolbook L",0),1.7346720833333331).attr ({'text-anchor': 'start', fill: "black"}).transform ('t112.66501102362224,2.4014125984251926').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue994",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t60.174578042378634,3.985555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('19'); } );

paper.print (0,0,"2",paper.getFont ("Century Schoolbook L",0),1.7346720833333331).attr ({'text-anchor': 'start', fill: "black"}).transform ('t28.198405097538735,2.435555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,-2.96,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t66.07247382754464,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('M0,0m0.04,-0.2L1.125,-0.2L1.125,0.2L0.04,0.2L0.04,-0.2').attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t44.825997837715335,7.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('M0,0m0.04,-0.2L2.59802151938,-0.2L2.59802151938,0.2L0.04,0.2L0.04,-0.2').attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t44.825997837715335,8.295555905511812').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,-1.96,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t25.185565015786235,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (-0.025,-0.96,0.05,3.232186,0.04).attr ({stroke: "black", fill: "black", 'stroke-width': 0.08, 'stroke-linejoin': 'round'}).transform ('t15.408673084002237,5.485555905511813').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.rect (0.0,-2.0,0.6,4.0,0.0).attr ({stroke: "black", fill: "black", 'stroke-width': 0.0, 'stroke-linejoin': 'round'}).transform ('t19.984073118110235,22.5572094488189').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('24'); } );

paper.rect (0.0,-2.0,0.19,4.0,0.0).attr ({stroke: "black", fill: "black", 'stroke-width': 0.0, 'stroke-linejoin': 'round'}).transform ('t19.494073118110236,22.5572094488189').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('24'); } );

paper.path ('m0.05,0.0L19.965018,0.0').attr ({stroke: "black", fill: "black", 'stroke-width': 0.1}).transform ('t0.5690551181102363,24.5572094488189').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('m0.05,0.0L19.965018,0.0').attr ({stroke: "black", fill: "black", 'stroke-width': 0.1}).transform ('t0.5690551181102363,23.5572094488189').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('m0.05,0.0L19.965018,0.0').attr ({stroke: "black", fill: "black", 'stroke-width': 0.1}).transform ('t0.5690551181102363,22.5572094488189').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('m0.05,0.0L19.965018,0.0').attr ({stroke: "black", fill: "black", 'stroke-width': 0.1}).transform ('t0.5690551181102363,21.5572094488189').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.path ('m0.05,0.0L19.965018,0.0').attr ({stroke: "black", fill: "black", 'stroke-width': 0.1}).transform ('t0.5690551181102363,20.5572094488189').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);

paper.print (-1.985,28/75,"\ue973",paper.getFont ("Emmentaler-20",0),3.9676010416666663).attr ({'text-anchor': 'start', fill: "black"}).transform ('t1.3690551181102364,23.5572094488189').click (on_item_click).dblclick (edit_clef).hover (on_item_hover_in, on_item_hover_out).drag (text_drag_move, text_drag_start, function () { text_drag_end ('4'); } );

paper.print (0,0,"5",paper.getFont ("Century Schoolbook L",0),1.7346720833333331).attr ({'text-anchor': 'start', fill: "black"}).transform ('t-0.3869574803149607,19.47306614173228').click (on_item_click).dblclick (on_item_dblclick).hover (on_item_hover_in, on_item_hover_out);
  $ ('#ly').val ("\\relative\n\\context Voice = \"one\" \n{\n  \\clef \"G\"\n  \\key c \\major\n  \\time 4/4\n  c' d e f g a b c16 c4 d e a, c \n  \\bar \"|.\" \n} ");
cursor = new Music_note (new Music_pitch (0, 0, 1), new Music_duration (2, 0, 1));
var focus = $ ('#score');
if ($ ('#ly').is (':focus')) focus = $ ('#ly');
var cursor_pos = 108;
function set_cursor () { $ ('#ly').set_caret (cursor_pos); return false; }
$ ('#ly').on ('focus', set_cursor);
focus.focus ();
 var clef_music = {'glyph': 'clefs.G', 'position': -2, 'middle-c': -6, };
 var key_music = {'tonic': 0, 'alteration': '0', 'scale': '0 0 0 0 0 0 0', };
 var time_music = {'numerator': 4, 'denominator': '4', 'style': 'default', };
debug = 'none';
if (debug != 'none') $ ('#debug').show (); else  $ ('#debug').hide ();
function disable_keys (e) { return false; }
if (debug == 'none') { $ ('#editor').attr ('readonly', 'readonly'); $ ('#ly').on ('keypress', disable_keys); $ ('#ly').on ('keydown', disable_keys); }

