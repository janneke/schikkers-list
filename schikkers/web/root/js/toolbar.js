// -*- c-style: gnu -*-

// This file is part of Schikkers List, a GUI platform for LilyPond.
//
// Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
//
// Schikkers List is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Schikkers List is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

$ (function ()
   {
     $ ("#document-new").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#document-open").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#document-save").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#document-save-as").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#document-export-ly").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#document-print").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#document-svg").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#document-png").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#zoom-in").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#zoom-out").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#view-refresh").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#edit-cut").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#audio-play").button ({text: false, icons: {primary: 'ui-icon-custom'}});
     $ ("#audio-volume-muted").button ({text: false, icons: {primary: 'ui-icon-custom'}});
   });
