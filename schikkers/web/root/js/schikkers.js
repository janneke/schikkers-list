// -*- c-style: gnu -*-

// This file is part of Schikkers List, a GUI platform for LilyPond.
//
// Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
//
// Schikkers List is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Schikkers List is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

var root = '/';
var server = '';
var query = $.getQuery ();
var user = query['user'] || 'nobody';

if (user == 'nobody')
  {
    var root = '/';
    if (document.location.hostname == 'lilypond.org')
      root = '/schikkers';
    window.location = server + root + '?user=' + 'demo';
  }

var debug = query['debug'] || 'none';
var cookie = '?user=' + user;
if (debug != 'none')
  cookie += '&debug=' + debug;

if (document.location.hostname == 'lilypond.org')
  server = 'http://janneke.dyndns.org:8888';

console.log ('server:' + server);

function require (file)
{
  $.ajax({
    url: server + root + file + cookie,
        dataType: 'script',
        async: false,
        success: function ()
	{
        },
	error: function (request, status, message)
	{
	  $ ('#result').html ('error: ' + message + ': cannot load: ' + file);
        }
    });
}

if (0) // FIXME
  {
require ('js/raphael.js');
require ('js/scale-raphael.js');
require ('js/cufon.js');
require ('fonts/Emmentaler-20.font.js');
require ('fonts/Vegur.font.js');
  }

Cufon.replace ('h1', {
  color: '-linear-gradient(0=#fff, 0.48=#caa, 0.50=#800, 0.52=#caa, 1=#fff)',
  fontFamily: 'Vegur'
});

var focus_div = $ ('#score');

function update_focus ()
{
  focus_div.focus ();
}

function calling_lily (query)
{
  update_focus ();
  $.ajax ({
    url: server + root + 'score' + cookie + query,
	type: 'GET',
	success: function (data)
	{
	  if (data != null)
	    {
	      $ ('#result').html ('success');
	      return true;
	    }
	},
	error: function (request, status, message)
	{
	  $ ('#result').html ('error: ' + message);
	}
    });
  return false;
}

function key_handler (key)
{
  console.log (key.toString (16));
  $ ('#key').html (key.toString (16));
  
  var query = '&exposed=' + exposed ();
  
  if (key == 'i'.charCodeAt (0))
    query += '&inspect=#t';
  query += '&key=' + key;
  
  return calling_lily (query);
}

function exposed ()
{
  var w = $ ('#score-window');
  return [w.scrollLeft (), w.scrollTop (), w.width (), w.height ()]
    .map (Math.floor)
    .join (',');
}

function keypress_handler (key)
{
  console.log ('keypress', key.toString (16));
  if (key == '#'.charCodeAt (0)
      || key == '('.charCodeAt (0)
      || key == ')'.charCodeAt (0)
      || key == '+'.charCodeAt (0)
      || key == ','.charCodeAt (0)
      || key == '-'.charCodeAt (0)
      || key == '@'.charCodeAt (0)
      || key == '\''.charCodeAt (0)
      || (key >= '0'.charCodeAt (0) && key <= '9'.charCodeAt (0))
      || (key >= 'a'.charCodeAt (0) && key <= 'g'.charCodeAt (0))
      || key == 'i'.charCodeAt (0)
      || key == 'r'.charCodeAt (0))
    return key_handler (key);
  return true;
}

function keydown_handler (key)
{
  console.log ('keydown', key.toString (16));
  if (key == '\b'.charCodeAt (0)
      || key == ' '.charCodeAt (0)
      || (key >= 0xff25 && key <= 0xff28))
    return key_handler (key);
  return true;
}

function score_keypress (e)
{
  var key = e.which;
  $ ('#trace').append ('' + key);
  return keypress_handler (key);
}

// http://www.javascripter.net/faq/keycodes.htm
function score_keydown (e)
{
  var key = e.which;
  $ ('#key').html ('' + key.toString (16) + ' (d)');
  
  if (key == '\b'.charCodeAt (0)
      || key == ' '.charCodeAt (0)
      || (key >= 0x25 && key <= 0x28))
    e.preventDefault ();

  if (key == '\b'.charCodeAt (0)
      || key == ' '.charCodeAt (0))
    return keydown_handler (key);
  
  if (key >= 0x25 && key <= 0x28)
    return keydown_handler (0xff00 + key);
  
  return true; // continue to keypress
}

function pointer_compat (e, parent)
{
  /* Cough: Firefox compatibility */
  if (typeof e.offsetX === 'undefined' || typeof e.offsetY === 'undefined')
    {
      var offset = parent.offset ();
      e.offsetX = e.pageX - offset.left;
      e.offsetY = e.pageY - offset.top;
    }
  return e;
}

function score_click_ (e)
{
  var x = e.offsetX;
  var y = e.offsetY;
  if (dragging && !ctrl && !shift)
    x = mousedown_x;
  dragging = false;
  ctrl = false;
  shift = false;

  var query = '&mouse=' + x + ',' + y;
  $ ('#mouse').html ('(' + x + ',' + y + ')');

  return calling_lily (query);
}

var dragging = false;
var selecting = false;
var select_region = false;
var ctrl = false;
var shift = false;
var mousedown_x = 0;
var mousedown_y = 0;

function score_click (e)
{
  focus_div = $ ('#score');
  focus_div.focus ();

  if (!selecting && !ctrl && !shift)
    score_click_ (pointer_compat (e, $ ('#score')));
  return true;
}

function register_mousedown (e)
{
  mousedown_x = e.offsetX;
  mousedown_y = e.offsetY;
}

function score_mousedown (e)
{
  register_mousedown (pointer_compat (e, $ ('#score')));
  ctrl = e.ctrlKey;
  shift = e.shiftKey;
  return true;
}

function score_mouseup (e)
{
  if (!ctrl && !shift)
    score_click (e);
  return true;
}

function score_scroll_ (e, d, dx, dy)
{
  var x = e.offsetX;
  var y = e.offsetY;
  dragging = false;

  var query = '&scroll=' + d + ',' + x + ',' + y;
  $ ('#mouse').html ('(' + d + ',' + x + ',' + y + ')');

  calling_lily (query);
  return false;
}

function score_scroll (e, d, dx, dy)
{
  score_scroll_ (pointer_compat (e, $ ('#score')), d, dx, dy);
}
  
function ly_keypress (e)
{
  var key = e.which;
  $ ('#key').html ('ly: ' + key.toString (16) + ' (d)');
  return parse_ly ($ (this).caret_position (), key);
}

function line (editor, pos, dir)
{
  editor = $ ('#editor');
  var text = editor.val ();
  var start = text.lastIndexOf ('\n', pos - 1);
  if (start == -1 && dir == -1)
    return 0;
  var end = text.indexOf ('\n', pos);
  if (end == -1 && dir == 1)
    return text.length;
  var end_p = false;
  if (pos == end)
    end_p = true;
  var line_pos = pos - start;
  var new_start;
  if (dir == -1)
    new_start = text.lastIndexOf ('\n', start - 1);
  else
    new_start = text.indexOf ('\n', pos);
  var new_end = text.indexOf ('\n', new_start + 1);
  if (new_end == -1 && dir == 1)
    new_end = text.length;

  if (end_p)
    return new_end;
  return Math.min (new_start + line_pos, new_end);
}

function ly_keydown (e)
{
  var key = e.which;
  $ ('#key').html ('ly: ' + key.toString (16) + ' (d)');
  if (key >= 0x25 && key <= 0x28)
    {
      var pos = $ (this).caret_position ();
      if (key == 0x25)
	pos -= 1;
      else if (key == 0x27)
	pos += 1;
      else if (key == 0x26)
	pos = line (this, pos, -1);
      else if (key == 0x28)
	pos = line (this, pos, 1);
      calling_lily ('&cursor=' + pos);
    }

  return true; // continue to keypress
}

function ly_click (e)
{
  var x = e.offsetX;
  var y = e.offsetY;
  $ ('#mouse').html ('ly: (' + x + ',' + y + ')');
  this.focus ();
  focus_div = $ ('#editor');

  var pos = $ (this).caret_position ();
  return calling_lily ('&cursor=' + pos);
}

// https://code.google.com/p/chromium/issues/detail?id=228851
function chrome_caret_workaround (e)
{
  if ($ ('#editor').is (':focus'))
    set_cursor ();
}

function connect_handlers ()
{
  $ ('#score').on ('keypress', score_keypress);
  $ ('#score').on ('keydown', score_keydown);
  //$ ('#score').on ('click', score_click);
  $ ('#score').on ('mousedown', score_mousedown);
  $ ('#score').on ('mouseup', score_mouseup);
  $ ('#score').on ('mousewheel', score_scroll);
  $ ('#editor').on ('keypress', ly_keypress);
  $ ('#editor').on ('keydown', ly_keydown);
  $ ('#editor').on ('click', ly_click);

  $ (document).on ('keyup', chrome_caret_workaround);
}

function download (format)
{
  var query = '&download=' + format;
  update_focus ();
  window.location = server + root + 'score.' + format + cookie + query;
  return false;
}

function button_edit (o, e)
{
  console.log ('clicked key: ' + o.value);
  return button ('edit', o.value);
}

function connect_toolbar_buttons ()
{
  $ ('#document-new').click (function (e) {
      var w = $ ('#score-window');
      calling_lily ('&document=' + this.value + '&exposed=0,0,' + w.width () + ',' + w.height ());
      return false;
    });
  $ ('#document-open').click (function (e) {
      alert ('connectme');
      return false;
    });
  $ ('#document-open').hide ();
  $ ('#document-save').click (function (e) {
      return download ('ly');
    });
  $ ('#document-save-as').click (function (e) {
      alert ('connectme');
      return false;
    });
  $ ('#document-save-as').hide ();
  $ ('#document-export-ly').click (function (e) {
      alert ('connectme');
      return false;
    });
  $ ('#document-export-ly').hide ();
  $ ('#document-print').click (function (e) {
      return download ('pdf');
    });
  $ ('#document-svg').click (function (e) {
      return download ('svg');
    });
  $ ('#document-png').click (function (e) {
      return download ('png');
    });
  $ ('#zoom-in').click (function (e) {
      console.log ('clicked zoom-in: ' + this.value);
      calling_lily ('&scale=' + this.value);
      return false;
    });
  $ ('#zoom-out').click (function (e) {
      console.log ('clicked zoom-out: ' + this.value);
      calling_lily ('&scale=' + this.value);
      return false;
    });
  $ ('#edit-cut').click (function (e) {
      return button_edit (this, e);
    });
  $ ('#view-refresh').click (function (e) {
      alert ('connectme');
      return false;
    });
  $ ('#view-refresh').hide ();
  $ ('#audio-play').click (function (e) {
      audio_play_start ($ (this));
      return true;
    });
  $ ('#audio-play').removeProp ('checked');
  $ ('#audio-play').children ().removeClass ('checked');
  $ ('#label-audio-volume-muted').children ().removeClass ('checked');
  $ ('#audio-volume-muted').click (function (e) {
      console.log ('clicked audio-volume-muted: ' + !checkbox_compat (this));
      update_focus ();
      muted = !checkbox_compat (this);
      audio_mute_update ();
      return true;
    });
  $ ('#audio-volume-muted').prop ('checked', 'checked');
  $ ('#audio-volume-muted').children ().addClass ('checked');
  $ ('#label-audio-volume-muted').children ().addClass ('checked');
  audio_mute_update ();
  audio.volume.gain.value = 4;
  $ ('#audio-volume').slider ({
    value: 4,
	min: 0,
	max: 11,
	orientation: 'horizontal',
	range: 'min',
	animate: true,
	disabled: true,

	start: function (e, t) {
	audio.volume.gain.value = t.value;
        },
	change: function (e, t) {
	console.log ('volume:' + t.value);
	audio.volume.gain.value = t.value;
      },
	});
}

function urg_label_checkbox_update (cb, checked_p)
{
  cb.children ().button ().removeProp ('checked');
  cb.children ().removeClass ('checked');
  if (checked_p)
    {
      cb.children ().addClass ('checked');
      cb.children ().button ().prop ('checked', 'checked');
    }
}

function label_checkbox_update (cb, checked_p)
{
  console.log ('checkbox: ' + cb + ': ' + checked_p);
  if (checked_p)
    cb.children ().addClass ('checked');
  else
    cb.children ().removeClass ('checked');
}

function radio_update (id, value)
{
  $ (id).removeProp ('checked');
  $ (id + ' [value=' + value + ']').prop ('checked', 'checked');
  try { $ (id).children ().button ('refresh'); } catch (e) { };
}

/* FIXME: factor-out audio player [html,css,js] */
var muted = true;
var audio_playing_p = false;
var audio_cursor_x = 0;
var note_timer = false;
var wholes_per_minute = 20;
var last = 0;

function audio_play (e)
{
  if (!muted)
    audio.play (e);
}

function audio_mute_update ()
{
  $ ('#audio-volume').slider ({disabled: muted});
  label_checkbox_update ($ ('#label-audio-volume-muted'), muted);
}

function audio_play_music (music)
{
  if (audio_playing_p && audio_cursor_x == cwhen.getBBox ().x)
    return audio_play_start ();
  if (audio_playing_p)
    note_timer = window.setTimeout (play_note, music_duration (cursor) * 1000);
  if (is_a_note_p (music))
    {
      var pitch = music.pitch;
      audio_play (['piano', pitch.octave, pitch.step, pitch.alteration * 2, 2].join ('-'));
    }
}

function music_duration (music)
{
  var partial = 1 / Math.pow (2, cursor.duration.log);
  var wholes_per_second = wholes_per_minute / 60;
  var seconds = partial / wholes_per_second;
  return seconds;
}

function play_note ()
{
  if (!audio_playing_p || !cursor)
    return;
  now = $.now ();
  console.log ('t: ' + now + ' e: ' +  (now - last));
  last = now;
  calling_lily ('&key=' + 0xff27);
  audio_cursor_x = cwhen.getBBox ().x;
}

function audio_play_start ()
{
  audio_playing_p = !audio_playing_p;
  if (!audio_playing_p && note_timer)
    window.clearTimeout (note_timer);
  audio_cursor_x = 0;
  label_checkbox_update ($ ('#label-audio-play'), audio_playing_p);
  $ ('#audio-play').button ('refresh');
  last = $.now ();
  note_timer = false;
  if (audio_playing_p)
    audio_play_music (cursor);
  return true;
}

function button (action, value)
{
  $ ('ul.hover-menu').hide ();
  calling_lily ('&' + action + '=' + value);
  return false;
}

function button_articulation (o, e)
{
  console.log ('clicked articulation: ' + o.value);
  return button ('articulation', o.value);
}

function button_dynamic (o, e)
{
  console.log ('clicked dynamic: ' + o.value);
  return button ('dynamic', o.value);
}

function button_clef (o, e)
{
  console.log ('clicked clef: ' + o.value);
  return button ('clef', o.value);
}

function button_key (o, e)
{
  console.log ('clicked key: ' + o.value);
  return button ('keysig', o.value);
}

function button_time (o, e)
{
  console.log ('clicked time: ' + o.value);
  return button ('timesig', o.value);
}

function bbox_to_string (box)
{
  return [box.x, box.y, box.x2 - box.x, box.y2 - box.y].join (',');
}

function checkbox_compat (c)
{
  // weird..FF20 has .checked == false, .[] == true
  // it seems that in FF20, we get .click () before
  // the checkbox has been updated
  //return $ (c).prop ('checked');
  return (c.checked == 'checked' || c.checked === false);
}

function connect_notebar_buttons ()
{
  $ ('input[name=duration]').click (function (e) {
      console.log ('clicked duration: ' + this.value);
      calling_lily ('&duration=' + this.value);
      return false;
    });
  $ ('#dot').click (function (e) {
      console.log ('clicked dot: ' + this.value);
      calling_lily ('&dot=' + this.value);
      return false;
    });
  $ ('#rest').click (function (e) {
      //alert ($ (this).prop ('checked'));
      console.log ('clicked rest: ' + !checkbox_compat (this));
      calling_lily ('&rest=' + !checkbox_compat (this));
      return true;
    });
  $ ('#alteration-1').click (function (e) {
      console.log ('clicked sharpen: ' + this.value);
      calling_lily ('&alteration=' + this.value);
      return false;
    });
  $ ('#alteration--1').click (function (e) {
      console.log ('clicked flatten: ' + this.value);
      calling_lily ('&alteration=' + this.value);
      return false;
    });
  $ ('#ottava-1').click (function (e) {
      console.log ('clicked 8va: ' + this.value);
      calling_lily ('&ottava=' + this.value);
      return false;
    });
  $ ('#ottava--1').click (function (e) {
      console.log ('clicked 8vb: ' + this.value);
      calling_lily ('&ottava=' + this.value);
      return false;
    });
  [
   '#clefs-G',
   '#clefs-F',
   '#clefs-C',
   '#clefs-C-2',
   ].forEach (function (x) {
       $ (x).click (function (e) { return button_clef (this, e) } );
     });
  [
   '#key-C',
   '#key-G',
   '#key-D',
   '#key-A',
   '#key-E',
   '#key-F',
   '#key-BES',
   '#key-EES',
   '#key-AES',
   ].forEach (function (x) {
       $ (x).click (function (e) { return button_key (this, e) } );
     });
  [
   '#qtime-4-4',
   '#qtime-2-2',
   '#qtime-3-4',
   '#qtime-3-8',
   '#time-1',
   '#time-1-1',
   '#time-2-1',
   '#time-3-1',
   '#time-4-1',
   '#time-5-1',
   '#time-2',
   '#time-1-2',
   '#time-C-',
   '#time-2-2',
   '#time-3-2',
   '#time-4-2',
   '#time-5-2',
   '#time-6-2',
   '#time-4',
   '#time-1-4',
   '#time-2-4',
   '#time-3-4',
   '#time-C',
   '#time-4-4',
   '#time-5-4',
   '#time-6-4',
   '#time-7-4',
   '#time-8-4',
   '#time-9-4',
   '#time-12-4',
   '#time-8',
   '#time-1-8',
   '#time-2-8',
   '#time-3-8',
   '#time-4-8',
   '#time-5-8',
   '#time-6-8',
   '#time-7-8',
   '#time-8-8',
   '#time-9-8',
   '#time-12-8',
   '#time-16',
   '#time-1-16',
   '#time-2-16',
   '#time-3-16',
   '#time-4-16',
   '#time-5-16',
   '#time-6-16',
   '#time-7-16',
   '#time-8-16',
   '#time-9-16',
   '#time-12-16',
   ].forEach (function (x) {
       $ (x).click (function (e) { return button_time (this, e) } );
     });
  [
   '#articulation-clear',
   '#articulation-digit-0',
   '#articulation-digit-1',
   '#articulation-digit-2',
   '#articulation-digit-3',
   '#articulation-digit-4',
   '#articulation-digit-5',
   '#articulation-accent',
   '#articulation-coda',
   '#articulation-downbow',
   '#articulation-downmordent',
   '#articulation-downprall',
   '#articulation-espressivo',
   '#articulation-fermata',
   '#articulation-flageolet',
   '#articulation-halfopen',
   '#articulation-lheel',
   '#articulation-lineprall',
   '#articulation-longfermata',
   '#articulation-ltoe',
   '#articulation-marcato',
   '#articulation-mordent',
   '#articulation-open',
   '#articulation-portato',
   '#articulation-prall',
   '#articulation-pralldown',
   '#articulation-prallmordent',
   '#articulation-prallprall',
   '#articulation-prallup',
   '#articulation-reverseturn',
   '#articulation-rheel',
   '#articulation-rtoe',
   '#articulation-segno',
   '#articulation-shortfermata',
   '#articulation-signumcongruentiae',
   '#articulation-snappizzicato',
   '#articulation-staccatissimo',
   '#articulation-staccato',
   '#articulation-stopped',
   '#articulation-tenuto',
   '#articulation-thumb',
   '#articulation-trill',
   '#articulation-turn',
   '#articulation-upbow',
   '#articulation-upmordent',
   '#articulation-upprall',
   '#articulation-varcoda',
   '#articulation-verylongfermata',
   ].forEach (function (x) {
       $ (x).click (function (e) { return button_articulation (this, e) } );
     });
  [
   '#dynamic-mp',
   '#dynamic-p',
   '#dynamic-pp',
   '#dynamic-ppp',
   '#dynamic-pppp',
   '#dynamic-ppppp',
   '#dynamic-mf',
   '#dynamic-f',
   '#dynamic-ff',
   '#dynamic-fff',
   '#dynamic-ffff',
   '#dynamic-fffff',
   '#dynamic-clear',
   '#dynamic-sp',
   '#dynamic-spp',
   '#dynamic-sf',
   '#dynamic-sff',
   '#dynamic-fp',
   '#dynamic-sfz',
   '#dynamic-rfz',
   ].forEach (function (x) {
       $ (x).click (function (e) { return button_dynamic (this, e) } );
     });
  $ ('#slur').click (function (e) {
      console.log ('clicked slur: ' + this.value);
      calling_lily ('&slur=' + bbox_to_string (select_region));
      return false;
    });
  $ ('#tie').click (function (e) {
      console.log ('clicked tie: ' + this.value);
      calling_lily ('&tie=' + !checkbox_compat (this));
      return false;
    });
  //$ ('#tie').button ({disabled: false});
}

function activate_menus ()
{
  $ ('ul.hover-menu').hide ();
  $ ('.hover-button').hover (function () { $ (this).children ('ul').css ({'display': 'inline'}); },
			     function () { $ ('ul.hover-menu').hide (); });
}

function selection_buttons ()
{
  $ ('#slur').button ({disabled: !select_region});
}

var document_ready_p = false;

$ (document).ready (function () {
    console.log ('document.ready\n');
    $ ('#title').html ("(cons* 'Schikkers (list))");
    $ ('#progress').html ('document ready');
    
    if (debug)
      $ ('#footer').css ({display: 'block'});
      
    var scroll_bar_width = 18;
    var scroll_bar_height = 18;
    var app_width = $ (window).width ()
      - $ ('#left-margin').width () - $ ('#right-margin').width ();
    var app_height = $ (window).height () - 60; //$ ('#app').css ('margin-top');
    $ ('#app').width (app_width);
    $ ('#app').height (app_height);
    var menu_height = $ ('#toolbar').height () + $ ('#notebar').height ();
    var pane_width = app_width / 2 - 2;
    var pane_height = app_height - menu_height - scroll_bar_height;
    
    $ ('#score-window').width (pane_width);
    $ ('#score-window').height (pane_height);
    $ ('#editor-window').width (pane_width);
    $ ('#editor-window').height (pane_height);
    $ ('#score-window').resizable ({handles: 'e',
	  alsoResize: '#editor-window',
	  maxWidth: app_width - scroll_bar_width,
	  horizontalSign: -1});
    
    $ ('#score').width (pane_width - scroll_bar_width);
    $ ('#score').height (pane_height - scroll_bar_height);
    $ ('#editor').width (app_width - 2 * scroll_bar_width);
    $ ('#editor').height (pane_height - scroll_bar_height);

    calling_lily ('&exposed=' + exposed ());

    activate_menus ();
    connect_elevator ();
    connect_handlers ();
    connect_music_edit_dialogs ();
    connect_notebar_buttons ();
    connect_toolbar_buttons ();
    selection_buttons ();

    $ ('#progress').html ('done');
    focus_div = $ ('#score');
    document_ready_p = true;
  });
