;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers web window)
  ;; base
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gobject)
  :use-module (gnome gtk)
  ;; user
  ;; user-gtk
  :use-module (schikkers gmisc)
  ;; user-web
  :export (<web-window>))

(define-class <web-window> (<gdk-window>)
  :gsignal (list 'expose-event #f <gdk-event-expose>)
  :gsignal (list 'headless-expose-event #f <gdk-rectangle>)
  :gsignal (list 'key-press-event #f <gdk-event-key>)
  :gsignal (list 'button-press-event #f <gdk-event-button>)
  :gsignal (list 'button-release-event #f <gdk-event-button>)
  :gsignal (list 'scroll-event #f <gdk-event-scroll>))
