#! /bin/sh
# -*- scheme -*-
exec guile -L $(dirname $(dirname $0)) -e test -s $0 "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers web misc)
  ;; base
  :use-module (sxml simple)
  :use-module (web request)
  :use-module (web response)
  :use-module (web uri)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gtk gdk-event)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers point)
  ;; user-gtk
  :use-module (schikkers gmisc)
  ;;
  :export (button-press-event
	   components->file-name
	   key->key-press-event
	   query->alist
	   query->list
	   query-element->pair
	   request-path-components
	   scroll-event
	   string->xml-string
	   sxml->xml-string
	   web-file
	   web-file-name
	   web-prefix))

(define (request-path-components request)
  (split-and-decode-uri-path (uri-path (request-uri request))))
     
(define (web-prefix) '(schikkers web root))

(define (components->file-name components)
  (string-join (map symbol->string components) "/"))

(define (web-file-name components)
  (components->file-name (append (web-prefix) components)))

(define (web-file components)
  (let ((file-name (web-file-name components)))
    (gulp-file file-name)))

(define (query->elements query) (string-split query #\&))

(define (query->alist query)
  (if query
      (map option->pair (query->elements query))
      '()))

(define gdk-key-alist
  `((#\backspace . ,gdk:BackSpace)
    (8 . ,gdk:BackSpace)

    (#xff21 . ,gdk:Page-Up)
    (#xff22 . ,gdk:Page-Down)

    (#xff25 . ,gdk:Left)
    (#xff26 . ,gdk:Up)
    (#xff27 . ,gdk:Right)
    (#xff28 . ,gdk:Down)))

(define (key->gdk-key key)
  (assoc-get-default gdk-key-alist key key))

(define (key->key-press-event key)
  (make-key-press-event :key (key->gdk-key key)))

(define (button-press-event mouse)
  (debugf 'mouse "bp mouse: ~a\n" mouse)
  (make-button-press-event :x (x mouse) :y (y mouse)))

(define (scroll-event dir mouse)
  (debugf 'mouse "scroll: ~a ~a\n" dir mouse)
  (make-scroll-event :dir (if (=1 dir) 'up 'down) :x (x mouse) :y (y mouse)))

(define-method (sxml->xml-string (o <string>))
  (call-with-output-string (lambda (p) (sxml->xml o p))))

(define-method (string->xml-string (o <string>))
  (sxml->xml-string
   (let loop ((s o) (re '(("\\\\" "\\\\")
			  ("\n" "\\n")
			  ("'" "\\'"))))
     (if (null? re)
	 s
	 (loop (string-regexp-substitute s (caar re) (cadar re)) (cdr re))))))
