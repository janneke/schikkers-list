;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers web music-edit)
  ;; base
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gtk)
  ;; user
  ;; user-gtk
  :use-module (schikkers music-edit)
  :use-module (schikkers music-view)
  :use-module (schikkers point)
  ;; user-web
  :use-module (schikkers web music-view)
  :export (<web-music-edit>)
  :re-export (configure-handler
	      move-viewport
	      set-adjustments
	      update-adjustments))

(define-class <web-music-edit> (<music-edit> <web-music-view>))
