1;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

;; Hello world json server
;;
;; telnet localhost 8080
;; GET /index.htm HTTP/1.1

(define-module (schikkers web app-server)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (srfi srfi-1)
  :use-module (web http)
  :use-module (web server)
  :use-module (web request)
  :use-module (web response)
  :use-module (web uri)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gtk)
  ;; user
  :use-module (schikkers misc)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers engraver)
  :use-module (schikkers handler)
  :use-module (schikkers handler-edit)
  :use-module (schikkers music-edit)
  :use-module (schikkers music-layout)
  :use-module (schikkers music-model)
  :use-module (schikkers music-view)
  :use-module (schikkers paper)
  ;; web
  :use-module (schikkers web app)
  :use-module (schikkers web ly-editor)
  :use-module (schikkers web misc)
  :use-module (schikkers web music-canvas)
  :use-module (schikkers web music-edit)
  :use-module (schikkers web scrolled-window)
  :use-module (schikkers web music-view)
  :use-module (schikkers web text-view)
  :export (main))

(define WIDTH 700)
(define HEIGHT 500)

(define (not-found request)
  ;;(format #t "request not found: ~a\n" request)
  (format #t "not found: ~S\n" (request-path-components request))
  (values (build-response #:code 404)
	  (string-append "Resource not found: "
			 (uri->string (request-uri request)))))

(define-class <web-app-server> ()
  (host :accessor .host :init-value "localhost" :init-keyword :host)
  (port :accessor .port :init-value 8888 :init-keyword :port)
  (accounts :accessor .accounts :init-form (list) :init-keyword :accounts)
  (start :accessor .start :init-value #f)
  (lily-pid :accessor .lily-pid :init-value #f :init-keyword :lily-pid)
  (lily-stdout :accessor .lily-stdout :init-value #f :init-keyword :lily-stdout)
  (starting :accessor .starting :init-value #t))

(define-method (initialize (o <web-app-server>) . initargs)
  (next-method)
  (declare-opaque-header! "Access-Control-Allow-Origin")
  (sigaction SIGHUP (lambda (x) (catch-sighup o)))
  (sigaction SIGINT (lambda (x) (catch-sigint o)))
  (sigaction SIGUSR1 (lambda (x) (catch-sigusr1 o)))
  (if (.start o) (run o))
  o)

(define-method (run (o <web-app-server>))
  (while (.starting o)
    (set! (.starting o) #f)
    (debugf 'web "[re]starting server\n")
    (run-server (lambda (r b) (http-handler o r b))
		'http (list :host (.host o) :port (.port o))))
  (on-exit o))

(define-method (on-exit (o <web-app-server>))
  (format #t "killing lily: ~a\n" (.lily-pid o))
  (kill (.lily-pid o) SIGTERM)
  (format #t "killed with exit: ~a\n" (waitpid (.lily-pid o)))
  (gtk-main-quit)
  #t)

(define (make-web-app)
  (let* ((args '(web))
	 (size (command-line-symbol-opt '--size "line"))
	 (debug00 (debugf 'web "size: ~a\n" size))
	 (paper (make <paper> :size size))
	 (debug01 (debugf 'web "paper: ~a\n" paper))
	 (model (get-music-model args :paper paper))
	 (view (make <web-text-view>))
	 (editor (make <web-ly-editor> :view view :model model))
	 (handler (make <handler> :model model))
	 (canvas (make <web-music-canvas> :width WIDTH :height HEIGHT))
	 (engraver (make <engraver> :model model))
	 (layout (make <music-layout> :canvas canvas :engraver engraver))
	 (handler-edit (make <handler-edit> :canvas canvas :handler handler :model model))
	 (edit (make <web-music-edit> :model model :layout layout)))
    (make <web-app> :model model :edit edit :editor editor)))

(define-method (timed-out? (o <web-app>))
  (> (- (current-time) (.birth o))
     (* 1 3600)))

(define-method (dump-ly (o <top>) (dq <top>))
  (stderr "programming-error: dump-ly: ~a ~a\n" o dq))
(define-method (dump-ly (o <boolean>) (dq <symbol>)) #f)
(define-method (dump-ly (o <list>) (dq <symbol>)) #f)
(define-method (dump-ly (o <web-app>) (dq <symbol>))
  (and-let* ((ly (ly (.model o)))
	     (default-ly "\\relative \\context Voice = \"one\" { \\clef \"G\" \\key c \\major \\time 4/4 c' \\bar \"|.\" }")
	     ((not (equal? ly default-ly)))
	     (file-name (components->file-name (list 'dump (symbol-append dq '.ly))))
	     ((system (string-append "cp --backup=t --force " file-name " " file-name)))
	     (file (open-file file-name "w")))
	    (display ly file)
	    (force-output file)))

(define-method (clean-account (o <web-app-server>) user)
  (debugf 'web "cleaning: ~a\n" (assoc-get (.accounts o) user))
  (and-let* ((account (assoc-get (.accounts o) user)))
	    (for-each (lambda (entry)
			(debugf 'web "looking at entry: ~a\n" entry)
			(debugf 'web "looking at cdr: ~a\n" (cdr entry))
			(dump-ly (cdr entry) (car entry))
			(if (is-a? (cdr entry) <web-app>)
			  (if (and (eq? user 'demo)
				   (timed-out? (cdr entry)))
			      (assoc-set! (.accounts o) user
					  (assoc-remove! account (car entry)))
			      (assoc-set! (.accounts o) user
					  (assoc-set! account (car entry) '())))))
		      account))
  (debugf 'web "cleaned: ~a\n" (assoc-get (.accounts o) user)))

(define-method (new-web-app (o <web-app-server>) user dq)
  (let* ((app (make-web-app))
	 (account (assoc-get-default (.accounts o) user '(dq))))
    (and-let* ((entry (null-is-#f (assoc-get account dq))))
             (dump-ly entry dq))
    (assoc-set! (.accounts o) user (assoc-set! account dq app))
    app))

(define-method (get-app (o <web-app-server>) user dq ip)
  (and-let* ((account (assoc-get (.accounts o) user)))
	    (debugf 'web "account: ~a\n" account)
	    (debugf 'web "entry: ~a\n" (assoc-get account dq))
	    (debugf 'web "dynamic: ~a\n" (assoc-get account 'dynamic))
	    (or (and-let* ((entry (assoc-get account dq)))
			  (if (or (null? entry)
				  (timed-out? entry))
			      (new-web-app o user dq)
			      entry))
		(and-let* ((dynamic (assoc-get account 'dynamic))
			   (fqdn (get-host-name ip))
			   ((string-suffix? (symbol->string dynamic) fqdn)))
			  (debugf 'web "new dynamic dq: ~a\n" dq)
			  (clean-account o user)
			  (new-web-app o user dq))
		  (begin
		    (debugf 'web "no account for: ~a ~a ~a\n" user dq ip)
		    #f))))

(define-method (http-handler (o <web-app-server>) request body)
  (let* ((components (map string->symbol (request-path-components request)))
	 (uri (request-uri request))
	 (query (uri-query uri))
	 (query-alist (query->alist query))
	 (port (request-port request))
	 (address (getpeername port))
	 (ip (sockaddr:addr address))
	 (dq (string->symbol (inet-ntoa ip)))
	 (meta (request-meta request))
	 (user (alist-symbol-opt query-alist 'user 'nobody))
	 (user-agent (request-user-agent request))
	 (app (get-app o user dq ip))
	 (time (strftime "%c" (localtime (current-time))))
	 (download (none-is-#f (alist-symbol-opt query-alist 'download 'none))))

    (debugf 'web "\n~a: ~a\n" time request)
    (stderr "~a:~a:~a:~a:~a\n" time dq components query-alist user-agent)
    (debugf 'web "body: ~a\n" body)
    (debugf 'web "components: ~a\n" components)
    (debugf 'web "uri: ~a\n" uri)
    (debugf 'web "query: ~a\n" query)
    (debugf 'web "meta: ~a\n" meta)
    (debugf 'web "port: ~a\n" port)
    (debugf 'web "ip: ~a\n" ip)
    (debugf 'web "dq: ~a\n" dq)

    (cond

     ;; html page with jquery and schikkers.js
     ((or (equal? components '())
	  (equal? components '(index))
	  (equal? components '(index.html)))
      (debugf 'web "serving index.html\n")
      (values '((content-type . (text/html))) (web-file '(index.html))))

     ((or (equal? components '(test))
	  (equal? components '(test.html)))
      (debugf 'web "serving test.html\n")
      (values '((content-type . (text/html))) (web-file '(test.html))))

     ((or (equal? components '(inspect))
	  (equal? components '(inspect.html)))
      (debugf 'web "serving inspect.html\n")
      (values '((content-type . (text/html))) (web-file '(inspect.html))))

     ;; json test
     ((equal? components '(json))
      (values '((content-type . (application/json)))
	      "{\"message\": \"hello, world!\"}"))

     ;; jquery css images
     ((and (equal? (car components) 'css)
	   (equal? (cadr components) 'themes)
	   (equal? (caddr components) 'smoothness)
	   (equal? (cadddr components) 'images))
      (debugf 'web "serving icon: image png ~a\n" components)
      (values '((content-type . (image/png))) (web-file components)))

     ;; css images
     ((and (equal? (car components) 'css)
	   (equal? (cadr components) 'images)
	   (string-suffix? ".png" (symbol->string (caddr components))))
      (debugf 'web "serving icon: image png ~a\n" components)
      (values '((content-type . (image/png))) (web-file components)))

     ;; css images
     ((and (equal? (car components) 'css)
	   (equal? (cadr components) 'images)
	   (string-suffix? ".svg" (symbol->string (caddr components))))
      (debugf 'web "serving icon: image svg ~a\n" components)
      (values '((content-type . (image/svg+xml))) (web-file components)))

     ;; images
     ((and (equal? (car components) 'images)
	   (string-suffix? ".png" (symbol->string (cadr components))))
      (debugf 'web "serving image: image png ~a\n" components)
      (values '((content-type . (image/png))) (web-file components)))

     ;; css
     ((equal? (car components) 'css)
      (values '((content-type . (text/css))) (web-file components)))

     ;; audio
     ((and (equal? (car components) 'audio)
	   (string-suffix? ".ogg" (symbol->string (cadr components))))
      (debugf 'web "serving audio: ogg ~a\n" components)
      (values '((content-type . (audio/ogg))) (web-file components)))

     ;; audio
     ((and (equal? (car components) 'audio)
	   (string-suffix? ".mp3" (symbol->string (last components))))
      (debugf 'web "serving audio: mp3 ~a\n" components)
      (values '((content-type . (audio/mp3))) (web-file components)))

     ((and (equal? (car components) 'audio)
	   (string-suffix? ".midi" (symbol->string (cadr components))))
      (debugf 'web "serving audio: midi ~a\n" components)
      (values '((content-type . (audio/midi))) (web-file components)))

     ((and (equal? (car components) 'audio)
	   (string-suffix? ".mid" (symbol->string (cadr components))))
      (debugf 'web "serving audio: midi ~a\n" components)
      (values '((content-type . (audio/midi))) (web-file components)))

     ;; javascript
     ((and (equal? (car components) 'js)
	   (member (cadr components)
		   '(audio.js
		     audio-init.js
		     cufon.js
		     elevator.js
		     google-analytics.js
		     dump.js
		     inspect.js
		     jquery.js
		     jquery-mousewheel.js
		     jquery-query-parser.js
		     jquery-ui.js
		     misc.js
		     music-edit-dialogs.js
		     ly-parser.js
		     raphael.js
		     register-cufon.js
		     scale-raphael.js
		     schikkers.js
		     selection.js
		     slur.js
		     notebar.js
		     toolbar.js)))
      (values '((content-type . (text/javascript)))
	      (web-file components)))

     ;; fonts
     ((and (equal? (car components) 'fonts)
	   (member (cadr components)
		   '(emmentaler-11.otf
		     emmentaler-20.otf
		     Emmentaler-11.font.js
		     Emmentaler-20.font.js
		     CenturySchoolbook-L.font.js
		     Vegur.font.js)))
      (values '((content-type . (text/javascript)))
	      (web-file components)))

     ;; favicon
     ((member 'favicon.ico components)
      (values '((content-type . (image/x-ico)))
	      (web-file '(images favicon.ico))))

     ;; css images
     ((and (equal? (car components) 'images)
	   (member (cadr components)
		   '(note-elevator.png)))
      (debugf 'web "serving icon: image ~a\n" components)
      (values '((content-type . (image/png)))
	      (web-file components)))

     ((or (not user) (eq? user 'nobody) (not app)) (not-found request))

     ;; the canvas -- FIXME: input validation!
     ((equal? components '(score))
      (debugf 'web "~a: serving score\n" time)
      (debugf 'web "query-alist: ~a\n" query-alist)
      (let* ((alteration (alist-number-opt query-alist 'alteration 0))
	     (articulation (alist-symbol-opt query-alist 'articulation 'none))
	     (clef (alist-symbol-opt query-alist 'clef 'none))
	     (clef-details (alist-split-opt query-alist 'clef-details #\, "none"))
	     (cursor (alist-number-opt query-alist 'cursor -1))
	     (debug (alist-symbol-opt query-alist 'debug 'none))
	     (document (alist-symbol-opt query-alist 'document 'none))
	     (drag-details (alist-split-opt query-alist 'drag #\, "none"))
	     (duration (alist-number-opt query-alist 'duration -1))
	     (dot (alist-symbol-opt query-alist 'dot 'none))
	     (dynamic (alist-symbol-opt query-alist 'dynamic 'none))
	     (edit-command (alist-symbol-opt query-alist 'edit 'none))
	     (edit-details (alist-split-opt query-alist 'edit-details #\, "none"))
	     (exposed (list->vector
		       (map string->number
			    (alist-split-opt query-alist 'exposed #\, "0,0,0,0"))))
	     (inspect (alist-opt query-alist 'inspect #f))
	     (key (alist-number-opt query-alist 'key 0))
	     (key-details (alist-split-opt query-alist 'key-details #\, "none"))
	     (key-signature (alist-symbol-opt query-alist 'keysig 'none))
	     (mouse (map string->number (alist-split-opt query-alist 'mouse #\, "0,0")))
	     (scroll (map string->number (alist-split-opt query-alist 'scroll #\, "0,0,0")))
	     (ottava (alist-number-opt query-alist 'ottava 0))
	     (rest (alist-symbol-opt query-alist 'rest 'none))
	     (scale (alist-number-opt query-alist 'scale 0))
	     (slur (list->vector (map string->number (alist-split-opt query-alist 'slur #\, "0,0,0,0"))))
	     (tie (alist-symbol-opt query-alist 'tie 'none))
	     (time-details (alist-split-opt query-alist 'time-details #\, "none"))
	     (time-signature (alist-symbol-opt query-alist 'timesig 'none))
	     (debug00 (debugf 'web "alteration: ~a\n" alteration))
	     (debug00 (debugf 'web "articulation: ~a\n" articulation))
	     (debug00 (debugf 'web "clef: ~a\n" clef))
	     (debug00 (debugf 'web "clef-details: ~a\n" clef-details))
	     (debug00 (debugf 'web "cursor: ~a\n" cursor))
	     (debug00 (debugf 'web "debug: ~a\n" debug))
	     (debug00 (debugf 'web "document: ~a\n" document))
	     (debug00 (debugf 'web "dot: ~a\n" dot))
	     (debug00 (debugf 'web "drag-details: ~a\n" drag-details))
	     (debug00 (debugf 'web "duration: ~a\n" duration))
	     (debug00 (debugf 'web "dynamic: ~a\n" dynamic))
	     (debug00 (debugf 'web "edit-command: ~a\n" edit-command))
	     (debug00 (debugf 'web "edit-details: ~a\n" edit-details))
	     (debug00 (debugf 'web "exposed: ~a\n" exposed))
	     (debug00 (debugf 'web "inspect: ~a\n" inspect))
	     (debug00 (debugf 'web "key: ~a\n" key))
	     (debug00 (debugf 'web "key-details: ~a\n" key-details))
	     (debug00 (debugf 'web "key-signature: ~a\n" key-signature))
	     (debug00 (debugf 'web "mouse: ~a\n" mouse))
	     (debug00 (debugf 'web "ottava: ~a\n" ottava))
	     (debug00 (debugf 'web "rest: ~a\n" rest))
	     (debug00 (debugf 'web "scale: ~a\n" scale))
	     (debug00 (debugf 'web "scroll: ~a\n" scroll))
	     (debug00 (debugf 'web "slur: ~a\n" slur))
	     (debug00 (debugf 'web "tie: ~a\n" tie))
	     (debug00 (debugf 'web "time-details: ~a\n" time-details))
	     (debug00 (debugf 'web "time-signature: ~a\n" time-signature))
	     (app (if (eq? document 'new) (new-web-app o user dq) app)))
	    (let ((js (if inspect
			  (web-file '(js dump.js))
			  (apply handle (list app alteration articulation clef clef-details cursor debug dot drag-details duration dynamic edit-command edit-details exposed key-details key-signature key mouse ottava rest scale scroll slur tie time-details time-signature)))))
	      (if (and (not inspect)
		       (not (eq? user 'alive)))
		  (let* ((fancy-file-name (components->file-name (list 'dump (symbol-append dq '.js))))
			 (file-name "dump.js")
			 (file (open-file file-name "w")))
		    (display js file)
		    (force-output file)))
	      (values '((Access-Control-Allow-Origin . "http://lilypond.org")
			(content-type . (application/javascript))) js))))

     ((and download
	   (or (equal? components '(score.ly))
	       (equal? components '(score.pdf))
	       (equal? components '(score.png))
	       (equal? components '(score.svg))))
      (cond ((member download '(pdf png svg))
	     (values '((Access-Control-Allow-Origin . "http://lilypond.org")
		       (content-type . (application/octet-stream)))
		     (render app download)))
	    ((eq? download 'ly)
	     (values '((Access-Control-Allow-Origin . "http://lilypond.org")
		       (content-type . (application/octet-stream)))
		     (ly-source app)))
	    (else (not-found request))))

     (else (not-found request)))))

(define-method (catch-sighup (o <web-app-server>))
  (sigaction SIGHUP SIG_DFL)
  (on-exit o))

(define-method (catch-sigint (o <web-app-server>))
  (unless (.starting o)
    (sigaction SIGINT SIG_DFL)
    (on-exit o)))

(define-method (catch-sigusr1 (o <web-app-server>))
  (debugf 'web "raising refreshing SIGINT\n")
  (set! (.starting o) #t)
  (raise SIGINT))


;;;; Entry point
(define (main . args)
  (putenv "LANG=C.UTF-8") ;; FIXME: LANG=, or LANG=C breaks Emmentaler font
  (let* ((fork (fork-lilypond-server))
	 (host (command-line-opt '--host "127.0.0.1"))
	 (port (command-line-integer-opt '--port "8888"))
	 (accounts '((adwin . ((89.242.212.103) (dynamic . adsl.alicedsl.de)))
		     (alive . ((127.0.0.1) (192.168.1.1) (192.168.1.105) (192.168.1.112) (192.168.1.115)))
		     (david . ((127.0.0.1) (192.168.1.1) (77.11.233.72) (dynamic . pool.mediaWays.net)))
		     (demo . ((dynamic . #{}#)))
		     (eddy . ((127.0.0.1) (192.168.1.1) (dynamic . internode.on.net)))
		     (hanwen . ((127.0.0.1) (192.168.1.1) (0.0.0.0)))
		     (janneke . ((127.0.0.1) (192.168.1.1) (192.168.1.105) (192.168.1.112) (192.168.1.115) (194.109.21.8) (80.255.245.177) (88.159.206.46)))
		     (inge . ((31.151.84.153)))
		     (rutger . ((127.0.0.1) (192.168.1.1) (84.31.191.56)))
		     (test . ((dynamic . #{}#)))
		     (tineke . ((192.168.1.1) (88.159.206.46)))
		     (wilbert . ((127.0.0.1) (192.168.1.1) (82.95.165.247)))
		     (wingo . ((127.0.0.1) (192.168.1.1) (88.160.190.192)))))
	 (app-server (make <web-app-server> :host host :port port
			   :accounts accounts
			   :lily-pid (car fork) :lily-stdout (cdr fork))))
    (debugf 'web "listening on ~a:~a\n" host port)
    (run app-server)))
