;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers web music-view)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 threads)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gtk)
  :use-module (gnome gtk gdk-event)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers point)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers music-layout)
  :use-module (schikkers music-view)
  ;; user-web
  :use-module (schikkers web music-canvas)
  :use-module (schikkers web scrolled-window)
  :export (<web-music-view>)
  :re-export (move-viewport
	      offset
	      set-adjustments
	      update-adjustments))

(define-class <web-music-view> (<music-view>)
  (widget :accessor .widget :init-form (make <web-scrolled-window>)))

(define-method (initialize (o <web-music-view>) . initargs)
  (next-method)
  (let ((canvas (.canvas (.layout o))))
    (connect canvas 'headless-expose-event (lambda (w e) (expose-handler o e))))
  o)

(define-method (offset (o <web-music-view>))
  (offset (.widget o)))

(define-method (expose-handler (o <web-music-view>) (event <vector>))
  (debugf 'scroll "expose: ~a\n" event)
  (next-method o event))


;;; Gtk-wrappers
(define-method (pack-start (w <gtk-container>) (o <web-music-view>) . rest) (apply pack-start (cons* w (.widget o) rest)))

(define-method (move-viewport (o <music-view>) (step <symbol>) count)
  (move-viewport (.widget o) step count))

(define-method (set-adjustments (o <web-music-view>) (offset <point>))
  (set-adjustments (.widget o) offset))

(define-method (update-adjustments (o <web-music-view>))
  (update-adjustments (.widget o)))
