;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; it under the terms of the GNU Affero General Public License as

;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers ly-editor)
  ;; base
  :use-module (srfi srfi-1)
  :use-module (ice-9 and-let-star)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gobject)
  :use-module (gnome gtk)
  :use-module (gnome gw gtk)
  :use-module (gnome pango)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers music)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers ly-parser)
  :use-module (schikkers music-model)
  :export (<ly-editor>
	   get-previous-music
	   get-offset
	   move-insert-mark
	   .view)
  :re-export (get-cursor
	      handle
	      set-model))

(define-class <text-item> ()
  (music :accessor .music :init-value #f :init-keyword :music)
  (text-tag :accessor .text-tag :init-value #f :init-keyword :text-tag)
  (start :accessor .start :init-value #f :init-keyword :start)
  (end :accessor .end :init-value #f :init-keyword :end))

(define-class <ly-editor> (<gobject>)
  (model :accessor .model :init-form #f :init-keyword :model)
  (view :accessor .view :init-value #f :init-keyword :view)

  (indent :accessor .indent :init-value 0)
  (input :accessor .input :init-value "")
  (input-mark :accessor .input-mark :init-value #f)
  (input-tag :accessor .input-tag :init-value #f)
  (insert-text-id :accessor .insert-text-id :init-value #f)
  (buffer :accessor .buffer :init-value #f)
  (parser :accessor .parser :init-value (make <ly-parser>))
  (text-items :accessor .text-items :init-form (list))
  (tree-window :accessor .tree-window :init-value #f)

  :gsignal (list 'insert-mark-changed #f <gmusic>))


(define (noop . rest) #t)

(define-method (get-char (o <gtk-text-iter>))
  (integer->char (gtk-text-iter-get-char o)))

(define-method (get-offset (o <gtk-text-iter>))
  (gtk-text-iter-get-offset o))

(define-method (get-offset (o <gtk-text-mark>))
  (get-offset (get-iter o)))

(define-method (backward-char-get-iter (o <gtk-text-iter>) . count)
  (let ((c (gtk-text-iter-copy o))
	(n (if (pair? count) (car count) 1)))
    (gtk-text-iter-backward-visible-cursor-positions c n)
    c))

(define-method (forward-char-get-iter (o <gtk-text-iter>) . count)
  (let ((c (gtk-text-iter-copy o))
	(n (if (pair? count) (car count) 1)))
    (gtk-text-iter-forward-visible-cursor-positions c n)
    c))

(define-method (get-iter (o <gtk-text-mark>))
  (get-iter-at-mark (gtk-text-mark-get-buffer o) o))

(define-method (get-insert-iter (buffer <gtk-text-buffer>))
  (get-iter (get-insert buffer)))
		
(define-method (initialize (o <ly-editor>) . initargs)
  (next-method)
  (let ((buffer (get-buffer (.view o))))
    (set! (.buffer o) buffer)
    (set-model o (.model o))
    (set! (.input-tag o) (create-tag buffer "input"))
    (set (.input-tag o) 'underline (make <pango-underline> :value 'error))
    (connect buffer 'mark-set (lambda (b i t) (mark-set-handler o b i t)))
    (connect buffer 'delete-range (lambda (b s e) (delete-range-handler o b s e)))
    (set! (.insert-text-id o) (connect-after buffer 'insert-text (lambda (b i t l) (insert-text-handler o b i t l))))
    (debugf 'editor "model: ~a\n" (.model o))
    (connect (.model o) 'modified (lambda (x m c) (modified-handler o m c))))
  o)

(define-method (get-cursor (o <ly-editor>))
  (get-insert-iter (.buffer o)))

(define-method (get-offset (o <ly-editor>))
  (get-offset (get-cursor o)))

(define-method (get-iter (o <ly-editor>) (offset <integer>))
  (get-iter-at-offset (.buffer o) offset))

(define-method (get-start (o <ly-editor>))
  (get-start-iter (.buffer o)))

(define-method (get-end (o <ly-editor>))
  (get-end-iter (.buffer o)))

(define-method (get-length (o <ly-editor>))
  (get-offset (get-end o)))

(define-method (set-cursor (o <ly-editor>) (iter <gtk-text-iter>))
  (if (!= (get-offset o) (get-offset iter))
      (place-cursor (.buffer o) iter)))

(define-method (set-cursor (o <ly-editor>) (offset <integer>))
  (set-cursor o (get-iter o offset)))

(define-method (set-cursor (o <ly-editor>) (cursor <music>))
  (move-insert-mark o cursor))

(define-method (set-model (o <ly-editor>) model)
  (and-let* ((id (.insert-text-id o))) (block (.buffer o) id))
  (let* ((offset (get-offset (get-cursor o)))
	 (length (get-length o))
	 (tail-offset (- length offset)))
    (debugf 'editor "offset: ~a\n" offset)
    (debugf 'editor "length: ~a\n" length o)
    (debugf 'editor "tail-offset: ~a\n" tail-offset)
    (set-model- o model)
    (debugf 'editor "new length: ~a\n" (get-length o))
    (debugf 'editor "new offset: ~a\n" (- (get-length o) tail-offset))
    (if (!=0 length)
	(set-cursor o (- (get-length o) tail-offset))
	(set-cursor o (get-cursor (.model o)))))
  (and-let* ((id (.insert-text-id o))) (unblock (.buffer o) id)))

(define-method (set-model- (o <ly-editor>) model)
  (let ((buffer (.buffer o)))
    (set! (.model o) model)
    (set! (.indent o) 0)
    (set! (.input-mark o) #f)
    (set! (.input o) "")
    (gtk-text-buffer-delete buffer
			    (get-start-iter buffer) (get-end-iter buffer))
    (walk model (lambda (m d s) (add-ly o m d s)) noop)))

(define-method (move-insert-mark (o <ly-editor>) music)
  (and-let* ((text-item (get-text-item o music))
	     (start (.start text-item))
	     (end (.end text-item))
	     (iter (get-iter end)))
	    (set-cursor o iter)))

(define-method (add-text-item (o <ly-editor>) tag music start end)
  (let ((text-item (make <text-item> :text-tag tag :music music :start start :end end)))
    (set! (.text-items o) (cons text-item (.text-items o)))
    text-item))

(define-method (whitespace-before (o <ly-editor>) music ly)
  (if (or (equal? ly "}") (equal? ly ">>"))
      (set! (.indent o) (2- (.indent o))))
  (if (or (equal? ly "}") (equal? ly ">>")
	  (equal? ly "{") (equal? ly "<<")
	  (string-prefix? "\\tweak" ly)
	  (is-a-bar-line? music))
      (let* ((buffer (.buffer o))
	     (indent (.indent o))
	     (whitespace (string-append "\n" (make-string indent #\ ))))
	(insert buffer (get-insert-iter buffer) whitespace))))

(define-method (whitespace-after (o <ly-editor>) music ly)
  (if (or (equal? ly "{") (equal? ly "<<"))
      (set! (.indent o) (2+ (.indent o))))
  (if (and (> (string-length ly) 0)
	   (not (equal? ly "<"))
	   (not (and (is-a? music <note-event>)
		     (is-a? (.parent music) <event-chord>)
		     (debugf 'editor "checking if last in event-chord: ~a : ~a\n" music (get-neighbor music 1))
		     (eq? (get-neighbor (.parent music) music 1) music))))
      (let* ((buffer (.buffer o))
	     (indent (.indent o))
	     (whitespace
	      (if (or (equal? ly "{") (equal? ly "<<")
		      (is-a-clef-glyph? music)
		      (is-a-key-change-event? music)
		      ;;(is-a-relative-octave-music? music)
		      (is-a-time-signature? music))
		  (string-append "\n" (make-string indent #\ ))
		  " ")))
	(insert buffer (get-insert-iter buffer) whitespace))))

(define-method (add-ly (o <ly-editor>) music dir state)
  (let* ((ly (ly-dir music dir state))
	 (buffer (.buffer o))
	 (tag (create-tag buffer #f))
	 (cursor (lambda () (get-insert-iter buffer)))
	 (start (create-mark buffer #f (cursor) #t)))
    (debugf 'editor "add-ly: music: ~a\n" music)
    (debugf 'editor "add-ly: ly: ~a\n" ly)
    (set tag 'editable-set #t)
    (set tag 'editable #f) ;; FIXME: TODO*: allow removing/editing music
    (whitespace-before o music ly)
    (insert-with-tags buffer (cursor) ly (list tag))
    (let ((start (create-mark buffer #f (get-iter start) #f))
	   (end (create-mark buffer #f (cursor) #t)))
      (add-text-item o tag music start end))
    (whitespace-after o music ly)))

(define-method (delete-range-handler (o <ly-editor>) buffer start end)
  (debugf 'editor "delete-range-handler\n")
  (debugf 'editor "delete-range-handler start: ~a\n" end)
  (debugf 'editor "delete-range-handler end: ~a\n" start)
  ;;(remove-input-tag o start end)
  (let ((r-len (abs (- (get-offset start) (get-offset end))))
	(i-len (string-length (.input o))))
    (set! (.input o) (substring (.input o) 0 (min (max (- i-len r-len) 0)
						  i-len))))
  #f)

(define-method (insert-text-handler (o <ly-editor>) buffer iter text length)
  (debugf 'editor "insert-text-handler\n")
  (debugf 'editor "insert-text-handler iter: ~a\n" iter)
  (debugf 'editor "insert-text-handler text: ~a\n" text)
  (debugf 'editor "insert-text-handler length: ~a\n" length)
  (not (insert-text o iter text length)))

(define-method (get-text-item (o <ly-editor>) (x <top>)) #f)

(define-method (get-text-item (o <ly-editor>) key accessor)
  (and-let* ((lst (member
		   key (.text-items o)
		   (lambda (key text-item) (eq? key (accessor text-item))))))
	    (car lst)))

(define-method (get-text-item (o <ly-editor>) (tags <list>))
  (let loop ((tags tags))
    (if (null? tags)
	#f
	(or (get-text-item o (car tags))
	    (loop (cdr tags))))))

(define-method (get-text-item (o <ly-editor>) (tag <gtk-text-tag>))
  (debugf 'tag "get-text-item tag: ~a\n" tag)
  (get-text-item o tag .text-tag))

(define-method (get-text-item (o <ly-editor>) (music <music>))
  (debugf 'tag "get-text-item music: ~a\n" music)
  (get-text-item o music .music))

(define-method (insert-mark-changed (o <ly-editor>) iter)
  (let* ((music (get-music o iter))
	 (offset (get-offset iter)))
    (debugf 'editor "insert-mark-changed\n")
    (debugf 'editor "insert-mark-changed music: ~a\n" music)
    (reset-input o)
    (if music
	(emit o 'insert-mark-changed (g music)))))

(define-method (get-music (o <ly-editor>) (iter <gtk-text-iter>))
  (and-let* ((tags (gtk-text-iter-get-tags iter))
	     (text-item (get-text-item o tags)))
	    (.music text-item)))

(define-method (get-previous-music (o <ly-editor>) (iter <gtk-text-iter>))
  (let loop ((iter iter))
    (if (<0 (get-offset iter))
	#f
	(or (get-music o iter)
	    (loop (backward-char-get-iter iter 1))))))

(define-method (mark-set-handler (o <ly-editor>) buffer iter text-mark)
  (and-let* ((insert-mark (equal? (get-name text-mark) "insert")))
	    (insert-mark-changed o iter)))

(define-method (modified-handler (o <ly-editor>)
				 (cursor <gmusic>) (modified <gmusic>))
  (modified-handler o (.music cursor) (.music modified)))

(define-method (modified-handler (o <ly-editor>)
				 (cursor <music>) (modified <music>))
  (debugf 'modify "modified-handler c:~a --> m: ~a\n" cursor modified)
  (set-model o (.model o)))


;;;; Experimental: .ly input parsing
;;;; 
(define-method (insert-text (o <ly-editor>) iter text length)
  (debugf 'parse "\n\npending: ~a\n" (.input o))
  ;; FIXME: no stop-emission / emit-stop et al. in guile-gnome?
  ;; (gtk-signal-emit-stop-by-name buffer 'insert-text)
  ;; (gtk-signal-stop-emission buffer 'insert-text)
  ;; (stop-emission buffer 'insert-text)
  ;; (gtk-signal-emit-stop-by-name buffer "insert-text")
  ;; (emit-stop-by-name buffer "insert-text")
  (and-let* ((mark (.input-mark o))
	     (cursor (get-iter mark)))
	    (if (not (eq? (get-offset iter)
			  (get-offset cursor)))
		(reset-input o)))
  (set! (.input-mark o) (create-mark (.buffer o) #f iter #f))
  (let* ((buffer (.buffer o))
	 (cursor (backward-char-get-iter iter))
	 (result (parse o cursor text)))
    (debugf 'parse "result: ~a\n" result)
    (cond
     ((eq? result #f)
      (set! (.input o) (string-append (.input o) text))
      (gtk-text-buffer-delete buffer cursor iter)
      #f)
     ((is-a? result <music>)
      (set! (.input o) "")
      #t)
     ((eq? result 'space) (set! (.input o) "") #t)
     ((and (pair? result)
	   (case (car result)
	     ((eat) (set! (.input o)
			  (string-append "1" (.input o))))
	     (else (stderr "programming error\n") #f))))
     (result
      (set! (.input o) (string-append (.input o) text))
      (debugf 'parse "input: ~a\n" (.input o))
      (apply-tag buffer (.input-tag o)
		 cursor
		 (forward-char-get-iter iter (1- length)))
      #t)
     (else (stderr "programming error\n") #f))))

(define-method (parse (o <ly-editor>) (iter <gtk-text-iter>) text)
  (let* ((cursor (get-previous-music o iter))
	 (char (get-char (backward-char-get-iter iter)))
	 (space? (eq? char #\ ))) ;; FIXME: TODO*: looking at
    (debugf 'parse "parse music (~a): ~a\n" char cursor)
    (parse o cursor space? iter (string-append (.input o) text))))

(define-method (parse (o <ly-editor>) (cursor <top>) space? iter text)
  (parse o (make <note-event>) space? iter text))

(define-method (parse (o <ly-editor>) (cursor <music>) space? iter text)
  (parse (.parser o) (.model o) cursor space? iter text))

(define-method (handle (o <ly-editor>) (pos <integer>) (key <symbol>) (value <string>))
  (set-cursor o pos)
  (and-let* ((cursor (get-previous-music o (get-cursor o)))
	     (result (handle o cursor key value))
	     (offset (if (eq? key 'space) (1+ pos) (+ (string-length value) pos))))
	    (set-cursor o offset)))

(define-method (handle (o <ly-editor>) (cursor <top>) (key <symbol>) (value <string>))
  (handle o (make <note-event>) key value))

(define-method (handle (o <ly-editor>) (cursor <music>) (key <symbol>) (value <string>))
  (handle (.parser o) (.model o) cursor key value))

;;;; 
(define-method (remove-input-tag (o <ly-editor>) (start <gtk-text-iter>)
				 (end <gtk-text-iter>))
  (debugf 'editor "removing: ~a..~a\n" (get-offset start) (get-offset end))
  (and-let* ((tag (.input-tag o)))
	     (gtk-text-buffer-remove-tag (.buffer o) tag start end)))

(define-method (remove-input-tag (o <ly-editor>))
  (and-let* ((tag (.input-tag o))
	     (mark (.input-mark o))
	     (start (get-iter mark))
	     (input (.input o))
	     (len (string-length input))
	     ((>0 len))
	     (end (backward-char-get-iter start len)))
	    (remove-input-tag o start end)))

(define-method (reset-input (o <ly-editor>))
  (and-let* ((mark (.input-mark o))
	     (start (get-iter mark))
	     (input (.input o))
	     (len (string-length input))
	     ((>0 len))
	     (end (backward-char-get-iter start len)))
	    ;;(remove-input-tag o start end)
	    (gtk-text-buffer-delete (.buffer o) start end))
  (set! (.input o) "")
  (set! (.input-mark o) #f))
