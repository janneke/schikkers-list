;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2010--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers misc)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 rdelim)
  :use-module (ice-9 regex)
  :use-module (ice-9 threads)
  :use-module (rnrs io ports)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;;
  :export (empty-is-#f
	   none-is-#f
	   null-is-#f
	   !=
	   !=0
	   1/
	   2*
	   2+
	   2-
	   2/
	   <0
	   <=0
	   =0
	   >0
	   >=0
	   <1
	   !=-1
	   =-1
	   =1
	   >1
	   <2
	   =-2
	   =2
	   >2
	   PATHSEP
	   PLATFORM
	   alist-integer-opt
	   alist-number-opt
	   alist-opt
	   alist-split-opt
	   alist-string-opt
	   alist-symbol-opt
	   alist-swap
	   assert
	   assoc-get
	   assoc-get-default
	   caddddr
	   char->string
	   command-line-alist
	   command-line-integer-opt
	   command-line-number-opt
	   command-line-opt
	   command-line-symbol-opt
	   command-line-string-opt
	   debug?
	   debugf
	   directory-files
	   dump-file
	   filter-out
	   first-is-newer?
	   fold-merge
	   get-data-dir
	   get-host-name
	   gulp-file
	   gulp-text-file
	   integer->string
	   is-dir?
	   is-file?
	   lisp->string
	   log2
	   number->integer
	   member-get
	   object->symbol
	   option-list->alist
	   option->pair
	   pure
	   quote-symbol
	   quote-symbol-cons
	   quote-symbol-null
	   read-lines
	   sign
	   smart-string-join
	   split
	   stderr
	   stealth?
	   string->integer
	   string-drop-prefix
	   string-empty?
	   string-regexp-substitute
	   string-unquote
	   thread-handler
	   toplevel-define!
	   xith-mutex))

;; junkme
(define (assoc-get-default alist key default)
  (let ((item (assoc key alist)))
    (if (pair? item) (cdr item) default)))

;; junkme
(define (assoc-get alist key)
  (assoc-get-default alist key #f))

(define (alist-swap alist)
  (define (assoc-swap x)
    (if (list? x)
	(reverse x)
	(cons (cdr x) (car x))))
  (map assoc-swap alist))

(define (member-get x lst)
  (and (member x lst) x))

(define (option->pair option)
  (let ((lst (string-split option #\=)))
    (cons (string->symbol (car lst)) (cdr lst))))
  
(define (option-list->alist lst) (map option->pair lst))

(define (command-line-alist) (option-list->alist (command-line)))
;;GDB debugging
;;(define (command-line-alist) (option-list->alist (list "--web" "--debug=app" "--debug=view" "--debug=web")))
;;(define (command-line-alist) (option-list->alist (list "--size=line" "--debug=mouse")))
;;(define (command-line-alist) (option-list->alist (list "--web" "--debug=account")))
(define (scar x) (if (null? x) x (car x)))
(define (alist-opt alist key . default)
  (scar
   (or (null-is-#f (cdr (or (null-is-#f (assoc key alist))
			    (cons* key default))))
       default)))

(define (alist-split-opt alist key char . default)
  (string-split (apply alist-opt (cons* alist key default)) char))

(define (alist-integer-opt alist key . default)
  (number->integer (apply alist-number-opt (cons* alist key default))))

(define (alist-number-opt alist key . default)
  (object->number (apply alist-opt (cons* alist key default))))

(define (alist-symbol-opt alist key . default)
  (object->symbol (apply alist-opt (cons* alist key default))))

(define alist-string-opt alist-opt)

(define (command-line-opt key . default)
  (apply alist-opt (cons* (command-line-alist) key default)))

(define (command-line-number-opt key . default)
  (apply alist-number-opt (cons* (command-line-alist) key default)))

(define (command-line-integer-opt key . default)
  (apply alist-integer-opt (cons* (command-line-alist) key default)))

(define (command-line-symbol-opt key . default)
  (apply alist-symbol-opt (cons* (command-line-alist) key default)))

(define command-line-string-opt command-line-opt)

(define logf-mutex (make-mutex))

(define (xith-mutex mutex . rest)
  (begin rest))

(define (logf port string . rest)
  (with-mutex logf-mutex
	      (apply format (cons* port string rest))
	      (force-output port))
  #t)
  
(define (stderr string . rest)
  (apply logf (cons* (current-error-port) string rest)))

(define (debug?)
  (filter-map (lambda (x)
		(if (equal? (car x) '--debug)
		    (if (pair? (cdr x))
			(string->symbol (cadr x))
			(car x))
		    #f))
	      (command-line-alist)))

(define (debugf-port-symbol port symbol first . rest)
  (let ((debug (debug?)))
    (if (null-is-#f debug)
	(if (or (eq? first #t)
		(member 'all debug)
		(member symbol debug))
	    (apply logf (cons* port (string-append (if (symbol? symbol) (symbol->string symbol) "") ": " first) rest)))
	#t)))

(define (debugf-port port first . rest)
  (if (symbol? first)
      (apply debugf-port-symbol (cons* port first rest))
      (apply debugf-port-symbol (cons* port #t first rest))))
      
(define (debugf first . rest)
  (if (port? first)
      (apply debugf-port (cons* first rest))
      (apply debugf-port (cons* (current-error-port) first rest))))

(define (stealth?)
  (member "--stealth" (command-line)))

(define (fold-merge lst1 lst2)
  (fold (lambda (x y r) (append r (list x y))) '() lst1 lst2))

(define (split lst)
  (let loop ((lst lst) (odd '()) (even '()))
    (if (not (pair? lst))
	(cons odd even)
	(loop (cddr lst) (append odd (list (car lst))) (append even (list (cadr lst)))))))

(define (number->integer x)
  (inexact->exact (round x)))

(define (string->integer x)
  (number->integer (string->number x)))

(define (assert x)
  (if (not x)
      (throw 'assert "assert failed")))

(define (pure . rest)
  (throw 'assert (string-join (cons* "pure virtual function called" rest))))

(define (string-regexp-substitute str re sub)
  (regexp-substitute/global #f re str 'pre sub 'post))

(define (gulp-text-file name)
  (let* ((file (open-file name "r"))
	 (text (read-delimited "" file)))
    (close file)
    text))

(define (gulp-file file-name)
  (call-with-input-file file-name get-bytevector-all))

(define (dump-file name string)
  (let* ((file (open-output-file name)))
    (display string file)
    (close file)))

(define (quote-symbol x)
  (if (symbol? x) `',x x))

(define (quote-symbol-cons x)
  (if (or (symbol? x)
	  (and (pair? x) (not (list? x)) (not (pair? (cdr x)))))
      `',x
      x))

(define (quote-symbol-null x)
  (if (or (symbol? x)
	  (and (pair? x) (not (list? x)) (not (pair? (cdr x))))
	  (and (not (pair? x)) (list? x)))
      `',x
      x))

(define (sign x)
  (cond ((<0 x) -1)
	((>0 x) 1)
	(else 0)))

(define (filter-out pred? lst)
  (filter (lambda (x) (not (pred? x))) lst))

(define (string-drop-prefix string prefix)
  (if (string-prefix? prefix string)
      (string-drop string (string-length prefix))
      string))
  
(define (string-empty? str)
  (equal? str ""))
  
(define (smart-string-join lst)
  (string-join (filter-out string-empty? lst)))
  
(define (integer->string i)
  (make-string 1 (integer->char i)))

(define (char->string char)
  (make-string 1 char))

(define (utf-8 i)
  (cond
   ((< i #x80) (list (integer->char i)))
   ((< i #x800) (map integer->char
		     (list (+ #xc0 (quotient i #x40))
			   (+ #x80 (modulo i #x40)))))
   ((< i #x10000)
    (let ((x (quotient i #x1000))
	  (y (modulo i #x1000)))
      (map integer->char
	   (list (+ #xe0 x)
		 (+ #x80 (quotient y #x40))
		 (+ #x80 (modulo y #x40))))))
   (else (begin (stderr "programming-error: utf-8 too big:~x\n" i)
		(list (integer->char 32))))))

(define (log2 x) (/ (log x) (log 2)))

(define (string-unquote string)
  (substring string 1 (max 1 (- (string-length string) 1))))

(define (first-is-newer? f1 f2)
  (or (not (file-exists? f2))
      (> (stat:mtime (stat f1))
	 (stat:mtime (stat f2)))))

(define (vector-for-each proc . vecs)
  (apply for-each (cons proc (map vector->list vecs))))
(define (vector-map proc . vecs)
  (list->vector (apply map (cons proc (map vector->list vecs)))))

(define (list-slice lst from to)
  (take (drop lst from) (1+ (- to from))))

(define (vector-slice v from to)
  (list->vector (list-slice (vector->list v) from to)))

(define (get-prefix-dir)
  (let* ((exe (car (command-line)))
	 (bin-dir (dirname exe))
	 (prefix (dirname bin-dir)))
    prefix))

(define (get-data-dir)
  (if (equal? (get-prefix-dir) ".")
      "."
      (string-append (get-prefix-dir) "/share/schikkers")))

(define (is-file? x)
  (and (file-exists? x)
       (eq? 'regular (stat:type (stat x)))))
	     
(define (is-dir? x)
  (and (file-exists? x)
       (eq? 'directory (stat:type (stat x)))))

(define (directory-files dir)
  (let ((p (opendir dir)))
    (let loop ((file (readdir p)) (files '()))
      (if (eof-object? file)
	  (begin (closedir p) (reverse files))
	  (let ((full (string-append dir "/" file)))
	    (loop (readdir p) (if (is-file? full) (cons file files) files)))))))

(define (read-lines f)
  (let ((p (open-input-file f)))
    (let loop ((line (read-line p)) (lines '()))
      (if (eof-object? line)
	  (begin (close p) (reverse lines))
	  (loop (read-line p) (cons line lines))))))

(define PLATFORM
  (string->symbol
   (string-downcase
    (car (string-tokenize (vector-ref (uname) 0) char-set:letter)))))

(define PATHSEP (if (eq? PLATFORM 'windows) ";" ":"))

(if (eq? PLATFORM 'windows)
    (let ((system-rename-file rename-file))
      (set! rename-file
	    (lambda (name new-name)
	      (if (file-exists? new-name)
		  (delete-file new-name))
	      (system-rename-file name new-name)))))

(define-method (!= (o <number>) x) (not (= o x)))
(define-method (!= (o <boolean>) x) (not (eq? o x)))
(define (1/ x) (/ 1 x))
(define (2* x) (* x 2))
(define (2/ x) (/ x 2))
(define (2+ x) (+ x 2))
(define (2+ x) (+ x 2))
(define (2- x) (- x 2))
(define (2- x) (- x 2))

(define (!=-1 x) (!= x -1))
(define (=-1 x) (= x -1))
(define (=-2 x) (= x -2))

(define (<0 x) (< x 0))
(define (>0 x) (> x 0))
(define (<=0 x) (<= x 0))
(define (>=0 x) (>= x 0))
(define (=0 x) (= x 0))
(define (!=0 x) (!= x 0))

(define (<1 x) (< x 1))
(define (>1 x) (> x 1))
(define (=1 x) (= x 1))
(define (!=1 x) (!= x 1))

(define (<2 x) (< x 2))
(define (>2 x) (> x 2))
(define (=2 x) (= x 2))
(define (!=2 x) (!= x 2))

(define (thread-handler o on-exit)
  (lambda (. rest)
    (stderr "~a: Error in thread\n" o)
    (force-output (current-error-port))
    (apply %thread-handler rest)
    (on-exit o)))

(define (empty-is-#f o) (if (string-empty? o) #f o))
(define (none-is-#f o) (if (or (eq? o 'none) (eq? o '#{}#)) #f o))
(define (null-is-#f o) (if (null? o) #f o))

(define (caddddr lst) (cadr (cdddr lst)))

(define-method (object->number (o <top>))
  (string->number (object->string o)))

(define-method (object->number (o <number>)) o)

(define-method (object->number (o <string>))
  (string->number o))

(define-method (object->symbol (o <top>))
  (string->symbol (object->string o)))

(define-method (object->symbol (o <string>))
  (string->symbol o))

(define (toplevel-define! name val)
  (module-define! (current-module) name val)
  (export name))

(define (lisp->string lisp)
  (let* ((str (format #f "~s" lisp))
	 (null-quoted (string-regexp-substitute str " [(][)]" "'()")))
    null-quoted))

(define (get-host-name ip)
  ;; (set! ip 1049396315) ;; this vodafone host has no DNS
  (catch #t
	 (lambda ()
	   (vector-ref (gethostbyaddr ip) 0))
	 (lambda (key . args)
	   (case key
	     ((host-not-found)
	      (let ((dq (inet-ntoa ip)))
		(stderr "~a: host not found: ~a\n" dq (cadr args))
		dq))
	     (else (apply throw key args))))))
