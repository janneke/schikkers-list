;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(read-set! keywords 'prefix)
(define-module (schikkers generics)
  ;; base
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  ;; gtk
  :use-module (gnome canvas)
  :use-module (gnome gtk)
;;;  :use-module (gnome pango)
  ;;
  :export (add-rhythmic-event
	   draw
	   engrave
	   engrave-system
	   get-paper
	   lisp
	   lisp-value
	   move-viewport
	   polygon
	   rect
	   script
	   set-adjustments
	   set-duration
	   set-paper
	   update-adjustments
	   .model
	   .music)
  :re-export (activate
	      add
	      connect
	      freeze-updates
	      get-buffer
	      invalidate
	      offset
	      path
	      render
	      scale
	      set-cursor
	      set-key
	      set-scroll-region
	      set-model
	      show
	      thaw-updates)
  :replace (find
	    remove
	    remove!))

;; base generics
(define (find . rest) (apply (@(srfi srfi-1) find) rest))
(define-generic find)

(define (remove . rest) (apply (@(srfi srfi-1) remove) rest))
(define-generic remove)

(define (remove! . rest) (apply (@(srfi srfi-1) remove!) rest))
(define-generic remove!)

(define (pure . rest)
  (throw 'assert (string-join (cons* "pure virtual function called" rest))))

;; gtk generics
(define-method (activate (x <boolean>)))
(define-method (add (x <boolean>)))
(define-method (get-buffer (x <boolean>)))
(define-method (freeze-updates (x <boolean>)))
(define-method (invalidate (x <boolean>)))
(define-method (offset (x <boolean>)))
(define-method (render (x <boolean>)))
(define-method (scale (x <boolean>)))
(define-method (set-cursor (x <boolean>)) (pure "set cursor"))
(define-method (set-key (x <boolean>)))
(define-method (set-model (x <boolean>)))
(define-method (show (x <boolean>)))
(define-method (thaw-updates (x <boolean>)))

;; schikker generics
(define-method (add-rhythmic-event (x <boolean>)))
(define-method (connect (x <boolean>)))
(define-method (draw (x <boolean>)))
(define-method (engrave (x <boolean>)))
(define-method (engrave-system (x <boolean>)))
(define-method (get-paper (x <boolean>)))
(define-method (lisp (x <boolean>)))
(define-method (lisp-value (x <boolean>)))
(define-method (move-viewport (x <boolean>)))
(define-method (polygon (x <boolean>)))
(define-method (path (x <boolean>)))
(define-method (rect (x <boolean>)))
(define-method (script (x <boolean>)))
(define-method (set-adjustments (x <boolean>)))
(define-method (set-duration (x <boolean>)))
(define-method (set-paper (x <boolean>)))
(define-method (set-scroll-region (x <boolean>)))
(define-method (update-adjustments (x <boolean>)))
(define-class <.accessors> ()
  (model :accessor .model)
  (music :accessor .music))
