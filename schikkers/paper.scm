;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2010--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers paper)
  ;; base
  :use-module (ice-9 and-let-star)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; user
  :use-module (schikkers misc)
  :export (<paper>
	   area
	   default-paper
	   line-width
	   margin
	   unit
	   .mm-to-unit
	   .landscape
	   .bottom-margin
	   .left-margin
	   .indent
	   .output-scale
	   .width
	   .height
	   .page-breaking
	   .right-margin
	   .size
	   .staff-space
	   .top-margin)
  :re-export (equal?
	      lisp-value))

(define-class <paper> ()

  (mm-to-unit :accessor .mm-to-unit :init-value (1/ 1.75729901757299))
  (output-scale :accessor .output-scale :init-value 1.75729901757299)
  (staff-space :accessor .staff-space :init-value 1)
  (page-breaking :accessor .page-breaking :init-value 'ly:minimal-breaking)

  ;; set up for a4 in mm
  (size :accessor .size :init-value #f :init-keyword :size)
  (indent :accessor .indent :init-value 15 :init-keyword :indent)
  (width :accessor .width :init-value 210 :init-keyword :width)
  (height :accessor .height :init-value 297 :init-keyword :height)
  (landscape :accessor .landscape :init-value #f :init-keyword :landscape)
  (left-margin :accessor .left-margin :init-value 10 :init-keyword :left-margin)
  (top-margin :accessor .top-margin :init-value 5 :init-keyword :top-margin)
  (right-margin :accessor .right-margin :init-value 10 :init-keyword :right-margin)
  (bottom-margin :accessor .bottom-margin :init-value 6 :init-keyword :bottom-margin))

(define-method (initialize (o <paper>) . initargs)
  (next-method)
  (set-size o (.size o))
  o)

(define-method (equal? (o <paper>) (paper <paper>))
  (and (eq? (.landscape o) (.landscape paper))
       (= (.mm-to-unit o) (.mm-to-unit paper))
       (= (.output-scale o) (.output-scale paper))
       (= (.indent o) (.indent paper))
       (= (.width o) (.width paper))
       (= (.height o) (.height paper))
       (= (.left-margin o) (.left-margin paper))
       (= (.right-margin o) (.right-margin paper))
       (= (.top-margin o) (.top-margin paper)))
       (= (.bottom-margin o) (.bottom-margin paper)))

(define (display-address o file)
  (display (number->string (object-address o) 16) file))

(define-method (display-slots (o <paper>) port)
  (display (.width o) port)
  (display #\space port)
  (display (.height o) port))

(define-method (write (o <paper>) port)
  (display "#<" port)
  (display (class-name (class-of o)) port)
  (display #\space port)
  (display-address o port)
  (display #\space port)
  (display-slots o port)
  (display #\space port)
  (display #\> port))

(define-method (area (o <paper>))
  (list (.width o) (.height o)))

(define-method (margin (o <paper>))
  (list (.left-margin o) (.top-margin o)))

(define-method (line-width (o <paper>))
  (- (.width o) (.left-margin o) (.right-margin o)))

(define-method (unit (o <paper>) value)
  (* (value o) (.mm-to-unit o)))

(define mm 1)
(define in 25.4)

(define paper-alist

  ;; don't use decimals.
  ;; ISO 216 has a tolerance of +- 2mm

  `((a11 . ,(cons (* 18.5 mm) (* 26 mm)))
    (a10 . ,(cons (* 26 mm) (* 37 mm)))
    (a9 . ,(cons (* 37 mm) (* 52 mm)))
    (a8 . ,(cons (* 52 mm) (* 74 mm)))
    (a7 . ,(cons (* 74 mm) (* 105 mm)))
    (a6 . ,(cons (* 105 mm) (* 148 mm)))
    (a5 . ,(cons (* 148 mm) (* 210 mm)))
    (a4 . ,(cons (* 210 mm) (* 297 mm)))
    (a3 . ,(cons (* 297 mm) (* 420 mm)))
    (a2 . ,(cons (* 420 mm) (* 594 mm)))
    (a1 . ,(cons (* 594 mm) (* 841 mm)))
    (a0 . ,(cons (* 841 mm) (* 1189 mm)))
    (legal . ,(cons (* 8.5 in) (* 14.0 in)))
    (letter . ,(cons (* 8.5 in) (* 11.0 in)))
    (line . ,(cons (* 840 mm) (* 30 mm)))))

(define-method (set-size (o <paper>) (size <boolean>))
  (set! (.width o) (inexact->exact (.width o)))
  (set! (.height o) (inexact->exact (.height o))))

(define-method (set-size (o <paper>) (size <symbol>))
  (let ((w-h (or (assoc-get paper-alist size)
		 (assoc-get paper-alist 'a8))))
    (set! (.size o) size)
    (set-size o (car w-h) (cdr w-h))
    (when (eq? size 'line)
	  (set! (.page-breaking o) 'ly:one-line-breaking)
	  (set! (.indent o) 0)
	  (set! (.left-margin o) (* 1 mm))
	  (set! (.top-margin o) (* 1 mm)))))

(define-method (set-size (o <paper>) (width <number>) (height <number>))
  (set! (.width o) (inexact->exact width))
  (set! (.height o) (inexact->exact height))
  (and-let* ((size (.size o))
	     (a4 (if (eq? size 'a4) o (make <paper> :size 'a4)))
	     (fh (/ (.width o) (.width a4)))
	     (fv (/ (.height o) (.height a4))))
	    (set! (.indent o) (* fh (.indent a4)))
	    (set! (.left-margin o) (* fh (.left-margin a4)))
	    (set! (.right-margin o) (* fh (.right-margin a4)))
	    (set! (.top-margin o) (* fv (.top-margin a4)))
	    (set! (.bottom-margin o) (* fv (.bottom-margin a4)))
	    (debugf 'paper "~a set-size (~a) left: ~a\n" (class-name (class-of o)) size (.left-margin o))
	    (debugf 'paper "~a set-size (~a) line-width: ~a\n" (class-name (class-of o)) size (line-width o))))

(define-method (lisp-value (o <paper>) . rest)
  (debugf 'paper "<paper>lisp: ~a\n" o)
  (let* ((start (if (null? rest) 0 (car rest)))
	 (last (if (null? rest) 0 (cadr rest)))
	 (ragged-right? #f)
	 (ragged-last? (= start last)))
    (debugf 'engraver "get-paper-settings last: ~a\n" last)
    (debugf 'engraver "get-paper-settings start: ~a\n" start)
    (debugf 'engraver "get-paper-settings ragged: ~a ~a\n" ragged-right? ragged-last?)
    (string-append
     (format #f "(ly:output-def-set-variable! $defaultpaper 'page-breaking ~a)\n" (.page-breaking o))
     (format #f "(ly:output-def-set-variable! $defaultpaper 'ragged-right ~a)\n" ragged-right?)
     (format #f "(ly:output-def-set-variable! $defaultpaper 'ragged-last ~a)\n" ragged-last?)
     (format #f "(ly:output-def-set-variable! $defaultpaper 'indent ~a)\n" (if (=0 start) (.indent o) 0))
     (format #f "(ly:output-def-set-variable! $defaultpaper 'line-width ~a)\n" (line-width o))
     
     (format #f "(ly:output-def-set-variable! $defaultpaper 'paper-width ~a)\n" (.width o))
     (format #f "(ly:output-def-set-variable! $defaultpaper 'paper-height ~a)\n" (.height o))
     (format #f "(ly:output-def-set-variable! $defaultpaper 'left-margin ~a)\n" (.left-margin o))
     (format #f "(ly:output-def-set-variable! $defaultpaper 'top-margin ~a)\n" (.top-margin o))
     (format #f "(ly:output-def-set-variable! $defaultpaper 'bottom-margin ~a)\n" (.bottom-margin o)))))

(define (default-paper)
  (let ((size (command-line-symbol-opt '--size "line")))
    (make <paper> :size size)))
