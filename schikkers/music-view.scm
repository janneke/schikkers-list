#! /bin/sh
# -*- scheme -*-
exec guile -L $(dirname $(dirname $0)) -e test -s $0 "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define (test . args)
  (eval '(test (command-line)) (resolve-module '(schikkers music-view))))

(define-module (schikkers music-view)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 threads)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gobject)
  :use-module (gnome glib)
  :use-module (gnome gw gdk)
  :use-module (gnome gtk gdk-event)
  :use-module (gnome gtk)
  :use-module (gnome gw gtk)
  ;; user
  :use-module (schikkers lilypond-socket)
  :use-module (schikkers music-canvas)
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers music-layout)
  :use-module (schikkers music-model)
  :use-module (schikkers notation-item)
  :export (<music-view>
	   button-press-handler
	   canvas-point
	   canvas-rect
	   configure-handler
	   ensure-cursor-visible
	   expose-handler
	   flush-validate
	   in-when-rect?
	   key-press-handler
	   move-cursor
	   mousings-handler
	   paint
	   save-ly
	   validate
	   .layout
	   .mousings
	   .size
	   .validate-thread)
  :re-export (get-cursor
	      invalidate
	      move-viewport
	      offset
	      refresh
	      render
	      scale
	      set-adjustments
	      set-model
	      set-paper
	      update-adjustments
	      .model))

(define-class <music-view> (<gobject>)
  (model :accessor .model :init-value #f :init-keyword :model)
  (layout :accessor .layout :init-value #f :init-keyword :layout)

  (mousings :accessor .mousings :init-form (list))
  (size :accessor .size :init-value #(0 0 0 0))
  (scroll-request :accessor .scroll-request :init-value #f)
  (validate-thread :accessor .validate-thread :init-value #f)
  (validated :accessor .validated :init-value #t)
  
  :gsignal (list 'default-duration #f <gint>)
  :gsignal (list 'default-rest #f <gboolean>)
  :gsignal (list 'move-cursor #f <gtk-movement-step> <gint>)
  :gsignal (list 'set-cursor #f <gmusic>))

(define-method (initialize (o <music-view>) . initargs)
  (next-method)
  (set-model o (.model o))
  (set-paper o (.paper (.model o)))
  (let ((canvas (.canvas (.layout o)))
	(bindings
	 `(((,gdk:Right ()) . (move-cursor visual-positions 1))
	   ((,gdk:Left ()) . (move-cursor visual-positions -1))
	   ;; FIXME: gtk-scrolledwindow handles these
	   ;;((,gdk:Page-Down ()) . (move-cursor pages 1))
	   ;;((,gdk:Page-Up ()) . (move-cursor pages -1))
	   ))
	(mousings
	 `(
	   (,in-when-rect? . (set-cursor))
	   )))
    (connect canvas 'key-press-event
	     (lambda (w e) (key-press-handler o bindings e)))
    (set! (.mousings o) (append mousings (.mousings o)))
    (connect canvas 'button-press-event
	     (lambda (w e) (button-press-handler o e)))
    (connect canvas 'expose-event (lambda (w e) (expose-handler o e))))
  
  ;; Key signals
  (connect o 'move-cursor (lambda (x s c) (move-cursor-handler o s c)))
  (connect (.model o) 'modified (lambda (x m c) (modified-handler o m c)))

  ;; Pointer signals
  (connect o 'set-cursor (lambda (m c) (set-cursor-handler o c)))
  o)


;;;; Interface
(define-method (move-viewport (o <music-view>) (step <symbol>) count) (pure "move viewport"))
(define-method (set-adjustments (o <music-view>) (offset <point>)) (pure "set-adjustments"))

(define-method (update-adjustments (o <music-view>)) (pure "update-adjustments"))

(define-method (set-model (o <music-view>) (model <music-model>))
  (set! (.model o) model)
  (set-model (.layout o) model)
  (invalidate o))

(define-method (get-cursor (o <music-view>)) (get-cursor (.model o)))

(define-method (set-paper (o <music-view>) (paper <paper>))
  (set-paper (.layout o) paper)
  (update-adjustments o))

(define-method (scale (o <music-view>) step)
  (debugf 'view "\n\nscale: ~a\n" (pixels-per-unit o))
  (scale (.canvas (.layout o)) step)
  (update-adjustments o))

(define-method (refresh (o <music-view>))
  (refresh (.layout o))
  (set! (.validated o) #f)
  (validate o))

(define-method (move-cursor (o <music-view>) (step <symbol>) count)
  (debugf 'view "~a move-cursor step: ~a\n" (class-name (class-of o)) step)
  (debugf 'view "~a move-cursor count: ~a\n" (class-name (class-of o)) count)
  (and-let* ((cursor (get-cursor o))
	     (music (case step
		      ((visual-positions) (get-neighbor-note-or-rest cursor count))
		      ((pages) (move-viewport o step count) #f) ;; FIXME: should return #t?
		      (else (debugf 'view "move-cursor or else\n") #f)))
	     ((not (eq? music cursor)))
	     ((debugf 'view "move-cursor: not note or rest: ~a(~a)\n" music (.start music)))
	     ((is-a-note-or-rest? music))) ;; FIXME: don't want to go to endbar 
	    (debugf 'view "moving cursor (~a): ~a\n" cursor music)
	    ;;(set-cursor o music)
	    (emit o 'set-cursor (g music))
	    music))

(define-method (set-cursor (o <music-view>) (cursor <music>))
  (debugf 'modify "set cursor: ~a (~a)~a\n" cursor (.start cursor) (ly-string cursor))
  (set-cursor (.layout o) cursor)
  (set-cursor (.model o) cursor)
  (if (is-a-note-or-rest? cursor)
      (let* ((log (.log (.duration cursor)))
	     (d (expt 2 log)))
	(emit o 'default-duration d)
	(emit o 'default-rest (is-a-rest? cursor))))
  (scroll-cursor-onscreen o))

(define-method (scroll-cursor-onscreen (o <music-view>))
  (and-let* ((cursor (get-cursor o))) (scroll o cursor)))

(define-method (update-adjustments (o <boolean>)) #f)

(define-method (scroll (o <music-view>) (music <music>))
  (debugf 'scroll "~a scroll to: ~a:\n" (class-name (class-of o)) music)
  (and-let* ((target (get-location (.layout o) music))
	     ((debugf 'scroll "~a scroll target: ~a:\n" (class-name (class-of o)) target))
	     (visible (visible-rect o))
	     ((debugf 'scroll "~a scroll visible: ~a:\n" (class-name (class-of o)) visible))
	     (offset (offset o))

	     (target-x-range (x-range target))
	     (visible-x-range (x-range visible))
	     (x-increment (cond
			   ((> (right target-x-range) (right visible-x-range))
			    (+ 5
			       (- (right target-x-range)
				  (right visible-x-range))))
			   ((< (left target-x-range) (left visible-x-range))
			    (- (- (left target-x-range)
				  (left visible-x-range))
			       5))
			   (else 0)))

	     (target-y-range (y-range target))
	     (visible-y-range (y-range visible))
	     (y-increment (cond
			   ((> (right target-y-range) (right visible-y-range))
			    (+ 5
			       (- (right target-y-range)
				  (right visible-y-range))))
			   ((< (left target-y-range) (left visible-y-range))
			    (- (- (left target-y-range)
				  (left visible-y-range))
			       5))
			   (else 0)))

	     (ppu (pixels-per-unit o))
	     (increment (* (list x-increment y-increment) ppu)))
	    (debugf 'scroll "~a scroll increment: ~a:\n" (class-name (class-of o)) increment)
	    (if (or (!=0 (x increment)) (!=0 (y increment)))
		(let ((target-offset (+ offset increment)))
		  (validate-y-range (.layout o) (y target-offset)
				    (+ (y target-offset) (height visible)))
		  (set-adjustments o target-offset)))))

(define-method (queue-scroll-request (o <music-view>) (music <music>))
  (set! (.scroll-request o) music))

(define-method (flush-scroll (o <music-view>))
  (debugf 'scroll "~a flush-scroll\n" (class-name (class-of o)))
  (and-let* ((music (.scroll-request o))
	     (height (visible-rect o)))
	    (set! (.scroll-request o) #f)
	    (validate-y-range (.layout o) 0 (2* height))
	    (update-adjustments o)
	    (scroll-to-music music)))

(define-method (scroll-to-music (o <music-view>) (music <music>))
  (queue-scroll-request o music)
  (if (is-valid? (.layout o))
      (flush-scroll o)))

(define-method (invalidate (o <music-view>))
  ;; FIXME: cannot use thread until layout queues engrave system requests
  (invalidate-synchronously o))

(define-method (invalidate-thread (o <music-view>))
  (debugf 'view "~a invalidate\n" (class-name (class-of o)))
  (set! (.validated o) #f)
  (debugf 'view "~a invalidate thread: ~a\n" (class-name (class-of o)) (.validate-thread o))
  (if (not (.validate-thread o))
      (set! (.validate-thread o)
	    ;; (gdk-threads-idle-add-full (2- gdk-priority:refresh
	    (call-with-new-thread
	     (lambda () (flush-validate o)
		     (thread-handler o (lambda (x) (emit x 'delete))))))))


(define-method (invalidate-synchronously (o <music-view>))
  (debugf 'view "~a invalidate-synchronous\n" (class-name (class-of o)))
  (set! (.validated o) #f)
  (debugf 'view "~a invalidate thread: ~a\n" (class-name (class-of o)) (.validate-thread o))
  (when (not (.validate-thread o))
    (set! (.validate-thread o) #t)
    (flush-validate o)
    (set! (.validate-thread o) #f)))

(define-method (flush-validate (o <music-view>))
  (debugf 'view "~a flush-validate\n" (class-name (class-of o)))
  ;;(and-let* ((id (.validate-thread o))) (g-source-remove id))
  (set! (.validate-thread o) #f)
  (if (or (flush-scroll o) (not (.validated o)))
      (validate o)))

(define-method (height (o <music-view>))
  (height (.canvas (.layout o))))
  
(define-method (width (o <music-view>))
  (width (.canvas (.layout o))))

(define-method (offset (o <music-view>)) (list 0 0))

(define-method (pixels-per-unit (o <music-view>))
  (pixels-per-unit (.canvas (.layout o))))

(define-method (canvas-rect (o <music-view>) (rect <rect>))
  (* rect (1/ (pixels-per-unit o))))

(define-method (canvas-point (o <music-view>) (point <point>))
  (* point (1/ (pixels-per-unit o))))

(define-method (visible-rect (o <music-view>))
 (canvas-rect o (+ (.size o) (offset o))))

(define-method (validate (o <music-view>))
  (debugf 'view "~a validate\n" (class-name (class-of o)))
  (and-let* ((rect (visible-rect o))
	     (top (y rect))
	     (bottom (bottom rect)))
	    (validate-y-range (.layout o) top bottom)
	    (set! (.validated o) (union rect (.validated o))))
  (set! (.validated o) (or (.validated o) #t)))

(define-method (paint (o <music-view>) rect)
  (debugf 'view "~a paint rect: ~a\n" (class-name (class-of o)) rect)
  (and-let* (((>0 (height rect))))
	    (debugf 'view "1paint rect: ~a\n" (.validated o))
	    (debugf 'view "2paint thread: ~a\n" (.validate-thread o))
	    (let ((old-validate (.validated o)))
	      (while (.validate-thread o) (flush-validate o))
	      (debugf 'view "3paint validated: ~a\n" (.validated o))
	      (debugf 'view "4paint my union rect: ~a\n" (union rect old-validate)))
	    ;;;(assert (.validated o))
	    (apply draw (list (.layout o) rect))
	    (set! (.validated o) (union rect (.validated o)))
	    (update-adjustments o)
	    (debugf 'view "~a 5paint rect: ~a\n" (class-name (class-of o)) (.validated o))))

(define-method (configure-handler (o <music-view>) event)
  (debugf 'view "~a configure-handler\n" (class-name (class-of o)))
  #f)

(define-method (update-size (o <music-view>) rect)
  (debugf 'view "~a update-size: ~a\n" (class-name (class-of o)) rect)
  (let ((size (.size o)))
    (when (or (not size)
	    (> (height rect) (height size))
	    (> (width rect) (width size)))
      (set! (.size o) rect)
      (update-adjustments o))))

(define-method (move-cursor-onscreen (o <music-view>))
  (debugf 'scroll "~a move-cursor-onscreen\n" (class-name (class-of o)))
  (and-let* ((visible (visible-rect o))
	     (model (.model o))
	     (cursor (get-cursor model))
	     (music (move-cursor-to-y-range
		     (.layout o) cursor (top visible) (bottom visible))))
	    (set-cursor model music)
	    music))

(define-method (ensure-cursor-visible (o <music-view>))
  (debugf 'scroll "~a ensure-cursor-visible\n" (class-name (class-of o)))
  (if (not (cursor-visible? o (get-cursor o)))
      (if (not (move-cursor-onscreen o))
	  (stderr "currently exposed: ~a\n" (visible-rect o)))))

(define-method (cursor-visible? (o <music-view>) (cursor <boolean>)) #f)
(define-method (cursor-visible? (o <music-view>) (cursor <music>))
  (let ((cursor-nitem (set-cursor (.layout o) cursor)))
    (if cursor-nitem
	(contains (visible-rect o) (.rect cursor-nitem))
	(and (stderr "no nitem for cursor: ~a\n" cursor) #f))))

(define-method (expose-handler (o <music-view>) (event <vector>))
  (debugf 'view "\n\n\n~a expose-handler event: ~a\n" (class-name (class-of o)) event)
  (let* ((area (if (is-a? (vector-ref event 0) <gdk-event-type>)
		   (gdk-event-expose:area event)
		   event))
	 (area-canvas (canvas-rect o area))
	 (expose area-canvas))
    (debugf 'view "~a expose-handler area: ~a\n" (class-name (class-of o)) area)
    (debugf 'view "~a expose-handler validated: ~a\n" (class-name (class-of o)) (.validated o))
    (update-size o area)
    (debugf 'view "~a expose-handler area canvas: ~a\n" (class-name (class-of o)) area-canvas)
    (if (contains (.validated o) expose)
	(debugf 'view "~a expose-handler valid: ~a\n" (class-name (class-of o)) expose)
	(and
	 (debugf 'view "~a expose-handler painting: ~a -> ~a\n" (class-name (class-of o)) area expose)
	 (paint o expose)
	 (ensure-cursor-visible o))))
  #f)


;;;; Signal handlers
(define-method (button-press-handler (o <music-view>) event)
  (and-let* ((canvas (.canvas (.layout o)))
	     ((not (.drag-origin canvas))))
	    (button-press-handler o (.mousings o) event)))

(define-method (button-press-handler (o <music-view>) (mousings <list>) event)
  (debugf 'mouse "~a button-press-handler \n" (class-name (class-of o)))
  (debugf 'mouse "~a mouse: ~a\n" (class-name (class-of o)) event)
  (and-let* ((point (map number->integer (list (gdk-event-button:x event)
					       (gdk-event-button:y event)))))
	    (mousings-handler o mousings point)))

(define-method (mousings-handler (o <music-view>) (mousings <list>) . cookie)
  (let loop ((tests mousings))
    (if (null? tests)
	#f
	(let ((match (apply (caar tests) (cons* o cookie))))
	  (if match
	      (begin
		(if (eq? #t match)
		    (emit o (cadar tests))
		    (apply emit (cons* o (cadar tests) match)))
		#t)
	      (loop (cdr tests)))))))

(define-method (key-press-handler (o <music-view>) bindings event)
  (debugf 'key "~a key-press-handler: ~a\n" (class-name (class-of o)) (gdk-event-key:keyval event))
  (debugf 'key "event: ~a\n" event)
  (and-let* ((keyval (gdk-event-key:keyval event))
	     (mods (gdk-event-key:modifiers event))
	     (fixed-mod2-mods (filter-out (lambda (x) (eq? x 'mod2-mask)) mods))
	     (key (cons keyval (list fixed-mod2-mods)))
	     (args (assoc-get-default bindings key #f)))
	    (apply emit (cons* o args))
	    #t)) ;; FIXME: should only return #t if key is handled


;;;; Key handlers
(define-method (move-cursor-handler (o <music-view>)
				    (step <gtk-movement-step>) count)
  (move-cursor o (genum->symbol step) count))

(define-method (modified-handler (o <music-view>)
				 (cursor <gmusic>) (modified <gmusic>))
  (debugf 'modify "modified-handler gc:~a --> gm: ~a\n" cursor modified)
  (modified-handler o (.music cursor) (.music modified)))

(define-method (modified-handler (o <music-view>)
				 (cursor <boolean>) (modified <music>))
  (debugf 'modify "modified-handler c:~a --> m: ~a\n" cursor modified)
  (modified-handler o modified modified))

(define-method (modified-handler (o <music-view>)
				 (cursor <music>) (modified <music>))
  (debugf 'modify "modified-handler c:~a --> m: ~a\n" cursor modified)
  (and-let* (((not (.parent cursor)))
	     (nitem (find (.notation (.layout o)) cursor)))
	    (set! (.music nitem) modified))
  (or
   (invalidate (.layout o) modified)
   (invalidate (.layout o) cursor)
   (stderr "programming error: cannot invalidate: ~a (~a) or ~a (~a)\n" cursor (.start cursor) modified (.start modified)))
  (invalidate o)
  (while (.validate-thread o) (flush-validate o))
  (set-cursor o modified))

(define-method (in-when-rect? (o <music-view>) (mouse <point>))
  (and-let* ((cursor (get-music (.layout o) (canvas-point o mouse))))
	    (list (g cursor))))

(define-method (set-cursor-handler (o <music-view>) (cursor <gmusic>))
  (set-cursor o (.music cursor)))


;;;; File stuff -- move to common app.scm?
(define (ly-touchup s)
  (string-regexp-substitute
   (string-regexp-substitute s " [\\]#f" " \\major")
   " = #f " " = \"foo\" "))

;; old crufty code FIXME
(define-method (save-lily-ly (o <music-view>) paper)
  (or (and-let* ((model (.model o))
		 (lisp (lisp-value model))
		 (music-string (lisp->string lisp))
		 (string (string-append "(display-ly " music-string ")"))
		 (result (ask-lilypond string))
		 (ly-lines (let loop ((result result) (ly '()))
			     (if (or (not (pair? result))
				     (string-prefix? "paper" (car result)))
				 ly
				 (let ((line (car result))
				       (hello "hello LilyPond "))
				   (if (string-prefix? hello line)
				       (loop (cdr result) (append ly (list (format #f "\\version ~S\n" (substring line (string-length hello) (1- (string-length line)))))))
				       (loop (cdr result) (append ly (list line))))))))
		 (lisp-paper (if paper (lisp-value paper)))
		 (ly-paper (if paper (string-append "#(begin\n" lisp-paper ")\n") ""))
		 (ly-string (string-append ly-paper (ly-touchup (apply string-append ly-lines))))
		 (file-name (.file-name model))
		 (base (basename (basename file-name ".ly") ".sy"))
		 (ly-name (string-append base ".gly")))
		(stderr "Writing: ~a\n" ly-name)
		(dump-file ly-name ly-string)
		ly-name)
      #f))

(define-method (save-ly (o <music-view>) paper)
  (or (and-let* ((model (.model o))
		 (ly (ly model))
		 (file-name (.file-name model))
		 (base (basename (basename file-name ".ly") ".sy"))
		 (ly-name (string-append base ".gly")))
		(stderr "Writing: ~a\n" ly-name)
		(dump-file ly-name ly)
		ly-name)
      #f))

(define-method (render (o <music-view>) (format <symbol>) paper)
  (and-let* ((ly-name (save-ly o paper))
	     (fmt (symbol->string format))
	     (file (string-append (basename ly-name ".gly") "." fmt))
	     (options (if (eq? format 'svg)
			  (string-append "-dbackend=" fmt)
			  (string-append "--format=" fmt)))
	     ((=0 (system (string-join (list LILYPOND-COMMAND options ly-name))))))
	    file))
