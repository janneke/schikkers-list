;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; it under the terms of the GNU Affero General Public License as

;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers music-edit)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 threads)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome glib)
  :use-module (gnome gobject)
  :use-module (gnome gtk)
  :use-module (gnome gtk gdk-event)
  :use-module (gnome gw gtk)
  ;; user
  :use-module (schikkers music-canvas)
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-modify)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers music)
  :use-module (schikkers music-layout)
  :use-module (schikkers music-model)
  :use-module (schikkers music-view)
  :use-module (schikkers notation-item)
  :use-module (schikkers system)
  :export (<music-edit>
	   alteration
	   articulation
	   clef
	   dot
	   duration-log
	   dynamic
	   key
	   octave
	   rest
	   span
	   time
	   .duration-input)
  :re-export (offset
	      refresh
	      set-model
	      slur
	      tie
	      .layout
	      .duration))

(define-class <music-edit> (<music-view>)
  (duration :accessor .duration :init-value 4)
  (duration-input :accessor .duration-input :init-value #f)
  (rest-input :accessor .rest-input :init-value #f)
  (slur-start :accessor .slur-start :init-value #f)
  (slur-stop :accessor .slur-stop :init-value #f)

  ;; Signals
  :gsignal (list 'articulation #f <garticulation-type>)
  :gsignal (list 'clef #f <gclef-type>)
  :gsignal (list 'drag #f <gmusic> <gdouble> <gdouble>)
  :gsignal (list 'dynamic #f <gdynamic-type>)
  :gsignal (list 'key #f <gkey-signature>)
  :gsignal (list 'slur #f <grect>)
  :gsignal (list 'span #f <gspan-type> <gint>)
  :gsignal (list 'time #f <gchararray>)

  ;; Key signals
  :gsignal '(space #f)
  :gsignal '(backspace #f)
  :gsignal '(cut #f)

  :gsignal (list 'alteration #f <gint>)
  :gsignal (list 'duration #f <gint>)
  :gsignal (list 'duration-input #f <gint>)
  :gsignal (list 'octave #f <gint>)
  :gsignal (list 'pitch #f <gchar>)
  :gsignal (list 'step #f <gint>)

  :gsignal (list 'rest #f <grest-type>)

  ;; Pointer signals
  :gsignal (list 'beyond-music-end #f <gpoint>)
  :gsignal (list 'in-cursor-when #f <gpoint>)

  ;; Scrolling signals
  :gsignal (list 'scroll-in-when #f <gdk-scroll-direction> <gpoint>)
  )

(define-method (initialize (o <music-edit>) . initargs)
  (next-method)
  (let ((canvas (.canvas (.layout o)))
	(bindings
	 `(
	   ((,gdk:Up ()) . (step 1))
	   ((,gdk:Down ()) . (step -1))

	   ((,gdk:parenleft ()) . (span ,(g 'slur) -1))
	   ((,gdk:parenright ()) . (span ,(g 'slur) 1))

	   ((,gdk:space ()) . (space))
	   ((,gdk:BackSpace ()) . (backspace))

	   ((,gdk:apostrophe ()) . (octave 1))
	   ((,gdk:comma ()) . (octave -1))

	   ((,gdk:1 ()) . (duration-input 1))
	   ((,gdk:2 ()) . (duration-input 2))
	   ((,gdk:3 ()) . (duration-input 3))
	   ((,gdk:4 ()) . (duration-input 4))
	   ((,gdk:6 ()) . (duration-input 6))
	   ((,gdk:8 ()) . (duration-input 8))

	   ((,gdk:a ()) . (pitch #\a))
	   ((,gdk:b ()) . (pitch #\b))
	   ((,gdk:c ()) . (pitch #\c))
	   ((,gdk:d ()) . (pitch #\d))
	   ((,gdk:e ()) . (pitch #\e))
	   ((,gdk:f ()) . (pitch #\f))
	   ((,gdk:g ()) . (pitch #\g))

	   ((,gdk:r ()) . (rest ,(g 'rest)))
	   ((,gdk:s ()) . (rest ,(g 'skip)))

	   ((,gdk:at ()) . (alteration -1))
	   ((,gdk:numbersign ()) . (alteration 1))
	   ((,gdk:at (shift-mask)) . (alteration -1))
	   ((,gdk:numbersign (shift-mask)) . (alteration 1))
	   ))
	(mousings
	 `(
	   (,beyond-music-end? . (beyond-music-end))
	   (,in-cursor-when? . (in-cursor-when))
	   ))
	(scrollings
	 `(
	   (,scroll-in-when-rect? . (scroll-in-when))
	   )))
    (connect canvas 'key-press-event
	     (lambda (w e) (key-press-handler o bindings e)))
    (connect canvas 'key-press-event
	     (lambda (w e) (duration-input-handler o e)))
    (set! (.mousings o) (append mousings (.mousings o)))
    (connect canvas 'expose-event (lambda (w e) (expose-handler o e)))
    (connect canvas 'scroll-event (lambda (w e) (scroll-handler o scrollings e))))
  
  (connect o 'articulation (lambda (w t) (articulation o t)))
  (connect o 'clef (lambda (w t) (clef o t)))
  (connect o 'drag (lambda (w m x y) (drag o m x y)))
  (connect o 'dynamic (lambda (w t) (dynamic o t)))
  (connect o 'key (lambda (w k) (key o k)))
  (connect o 'span (lambda (w s d) (span o s d)))
  (connect o 'slur (lambda (w r) (slur o r)))
  (connect o 'time (lambda (w t) (time o t)))

  ;; Key signals
  (connect o 'backspace (lambda (m) (backspace o)))
  (connect o 'cut (lambda (m) (cut o)))
  (connect o 'space (lambda (m) (space o)))
  
  (connect o 'alteration (lambda (m d) (alteration o d)))
  (connect o 'duration (lambda (m d) (duration o d)))
  (connect o 'duration-input (lambda (m d) (duration-input o d)))
  (connect o 'octave (lambda (m d) (octave o d)))
  (connect o 'pitch (lambda (m p) (pitch o p)))
  (connect o 'step (lambda (m d) (step o d)))
  (connect o 'rest (lambda (m r) (rest o r)))
  
  ;; Pointer signals
  (connect o 'beyond-music-end (lambda (m p) (beyond-music-end o p)))
  (connect o 'in-cursor-when (lambda (m p) (in-cursor-when o p)))

  ;; Scroll signals
  (connect o 'scroll-in-when (lambda (m d p) (scroll-in-when o d p)))

  ;; Other
  (connect o 'default-duration (lambda (w d) (default-duration-handler o d)))
  (connect o 'default-rest (lambda (w r) (default-rest-handler o r)))

  o)

(define-method (beyond-music-end? (o <music-edit>) (mouse <point>))
  (and-let* (((beyond-music-end? (.layout o) (canvas-point o mouse))))
	    (list (g mouse))))

(define-method (in-cursor-when? (o <music-view>) (mouse <point>))
  (and-let* ((nitems (get-nitems (.layout o) (canvas-point o mouse)))
	     (cursor (get-cursor o))
	     (cursor-nitem (find (music-predicate (lambda (x) (eq? x cursor)))
				 nitems)))
	    (list (g mouse))))


;;;; Signal handlers
(define-method (duration-input-handler (o <music-edit>)  event)
  (debugf 'key "~a duration-input-handler: ~a\n" (class-name (class-of o)) (gdk-event-key:keyval event))
  (unless (and-let* ((duration-input (.duration-input o))
		     (mods (gdk-event-key:modifiers event))
		     (fixed-mod2-mods (filter-out (lambda (x) (eq? x 'mod2-mask)) mods))
		     ((eq? fixed-mod2-mods '()))
		     (keyval (gdk-event-key:keyval event))
		     (int (- keyval gdk:0))
		     ((member int '(1 2 3 4 6 8))))
		    #t)
    (set-duration-input o (.duration-input o))
    (set! (.duration-input o) #f))
  #f)

(define-method (default-duration-handler (o <music-edit>) d)
  (debugf 'modify "default-duration-handler: default-duration: ~a\n" d)
  (set! (.duration o) d))

(define-method (default-rest-handler (o <music-edit>) rest?)
  (debugf 'modify "default-rest-handler: default-rest: ~a\n" rest?)
  (set! (.rest-input o) rest?))

(define-method (scroll-handler (o <music-edit>) (scrollings <list>) event)
  (debugf 'mouse "~a scroll-handler: ~a\n" (class-name (class-of o)) event)
  (and-let* ((mouse (map number->integer (list (gdk-event-scroll:x event)
					       (gdk-event-scroll:y event))))
	     (direction (gdk-event-scroll:direction event)))
	    (debugf 'mouse "direction: ~a\n" (gdk-event-scroll:direction event))
	    (mousings-handler o scrollings direction mouse)))

(define-method (mouse->notes (o <music-edit>) (mouse <point>))
  (and-let* ((point (canvas-point o mouse))
	     (nitems (get-nitems (.layout o) point))
	     (note-nitems (null-is-#f (filter (music-predicate is-a-note?) nitems))))
	    (map .music note-nitems)))
  
(define-method (scroll-in-when-rect? (o <music-edit>) (direction <gdk-scroll-direction>) (mouse <point>))
  (debugf 'mouse "scroll-in-when-rect?: ~a ~a\n" direction mouse)
  (and-let* ((notes (mouse->notes o mouse)))
	    (list direction (g mouse))))

(define-method (set-duration-input (o <music-edit>) duration)
  (debugf 'modify "set-duration-input: ~a\n" duration)
  (if duration
      (emit o 'default-duration duration)))


;;;; Key signals
(define-method (backspace (o <music-edit>))
  (backspace o (get-cursor o)))

(define-method (cut (o <music-edit>))
  ;; TODO: if selection, etc.
  (backspace o (get-cursor o)))

(define-method (alteration (o <music-edit>) (dir <integer>))
  (debugf 'key "alteration: ~a\n" dir)
  (delta-alteration o (get-cursor o) dir))

(define-method (articulation (o <music-edit>) (type <garticulation-type>))
  (debugf 'key "articulation: ~a\n" type)
  (articulation o (genum->symbol type)))

(define-method (articulation (o <music-edit>) (type <symbol>))
  (debugf 'key "articulation: ~a\n" type)
  (let ((t (string-drop-prefix (symbol->string type) "articulation."))
	(model (.model o))
	(cursor (get-cursor o)))
    (add-articulation model cursor (string->symbol t))))

(define-method (clef (o <music-edit>) (type <gclef-type>))
  (debugf 'key "clef: ~a\n" type)
  (clef o (genum->symbol type)))

(define-method (clef (o <music-edit>) (type <symbol>))
  (debugf 'key "clef: ~a\n" type)
  (let* ((t (string-drop-prefix (symbol->string type) "clefs."))
	 (details (assoc-get clef-alist (string->symbol t)))
	 (glyph (car details))
	 (pos (cadr details))
	 (middle-c (caddr details)))
  (clef o glyph pos middle-c)))

(define-method (clef (o <music-edit>) (glyph <symbol>)
		     (pos <integer>) (middle-c <integer>))
  (debugf 'key "clef: ~a\n" glyph)
  (let* ((model (.model o))
	 (music (find model (lambda (x) (is-a-clef-glyph? x)))))
    (set-clef model music glyph pos middle-c)))

(define-method (drag (o <music-edit>) (cursor <gmusic>) (x <number>) (y <number>))
  (drag o (.music cursor) (list x y)))

(define-method (drag (o <music-edit>) (cursor <music>) (point <point>))
  (drag (.model o) cursor point))

(define-method (dot (o <music-edit>))
  (let ((cursor (get-cursor o)))
    (set-dots o cursor (modulo (1+ (.dots (.duration cursor))) 3))))

(define-method (duration-log (o <music-edit>) (log <integer>))
  (debugf 'modify "duration-log: ~a\n" log)
  (let ((d (expt 2 log)))
    (duration o d)
    (set-duration-input o d)))

(define-method (duration (o <music-edit>) (d <integer>))
  (debugf 'modify "duration: ~a\n" d)
  (duration o (get-cursor o) d))

(define-method (duration-input (o <music-edit>) (d <integer>))
  (debugf 'key "duration-input: ~a\n" d)
  (let* ((duration (if (.duration-input o)
		       (+ (* (.duration-input o) 10) d)
		       d)))
    (set! (.duration-input o)
	  (and-let* (((member duration '(1 2 3 4 6 8 12 16 32 64 128))))
		    duration))))

(define-method (dynamic (o <music-edit>) (type <gdynamic-type>))
  (debugf 'key "dynamic: ~a\n" type)
  (dynamic o (genum->symbol type)))

(define-method (dynamic (o <music-edit>) (type <symbol>))
  (debugf 'key "dynamic: ~a\n" type)
  (let ((t (string-drop-prefix (symbol->string type) "dynamic."))
	(model (.model o))
	(cursor (get-cursor o)))
    (add-dynamic model cursor (string->symbol t))))

(define-method (key (o <music-edit>) (k <gkey-signature>))
  (debugf 'key "key: ~a\n" k)
  (key o (genum->symbol k)))

(define-method (key (o <music-edit>) (k <symbol>))
  (debugf 'key "key: ~a\n" k)
  (let* ((name (string->symbol (string-drop-prefix (symbol->string k) "key-")))
	 (model (.model o))
	 (music (find model (lambda (x) (is-a-key-change-event? x)))))
    (set-key model music name)))

(define-method (key (o <music-edit>) (tonic <integer>) (alteration <number>) (scale <list>))
  (debugf 'key "key: ~a ~a ~a\n" tonic alteration scale)
  (let* ((model (.model o))
	 (music (find model (lambda (x) (is-a-key-change-event? x)))))
    (set-key model music tonic alteration scale)))

(define-method (octave (o <music-edit>) (dir <integer>))
  (delta-octave o (get-cursor o) dir))

(define-method (pitch (o <music-edit>) (c <char>))
  (set-relative-pitch o (get-cursor o) c))

(define-method (rest (o <music-edit>) (rest? <boolean>))
  (debugf 'key "rest: ~a\n" rest?)
  (if rest?
      (rest o 'rest)
      (pitch o #\c)))

(define-method (rest (o <music-edit>) (r <grest-type>))
  (rest o (genum->symbol r)))
			      
(define-method (rest (o <music-edit>) (r <symbol>))
  (debugf 'key "rest: ~a\n" r)
  (set-rest o (get-cursor o) r))

(define-method (slur (o <music-edit>) (dir <integer>))
  (debugf 'slur "slur: ~a\n" dir)
  (and-let* ((cursor (get-cursor o))
	     ((is-a-note? cursor)))
	    (set! ((if (=-1 dir) .slur-start .slur-stop) o) cursor)
	    (when (is-a-music? (slur o (.slur-start o) (.slur-stop o)))
	      (debugf 'slur "reset slur start/stop\n")
	      (set! (.slur-start o) #f)
	      (set! (.slur-stop o) #f))))

(define-method (slur (o <music-edit>) (start <top>) (stop <top>)) #f)
(define-method (slur (o <music-edit>) (start <note-event>) (stop <note-event>))
  (if (> (.start stop) (.start start))
      (slur (.model o) start stop)))

(define-method (slur (o <music-edit>) (r <grect>))
  (debugf 'slur "slur: ~a\n" r)
  (slur o (.rect r)))

(define-method (slur (o <music-edit>) (r <rect>))
  (debugf 'slur "slur: ~a\n" r)
  (and-let* ((system (get-system o))
	     (paper (get-paper (.layout o)))
	     (start (find-nitem system (note-intersect paper r)))
	     (stop (last-nitem system (note-intersect paper r))))
	    (debugf 'slur "slur: ~a..~a\n" start stop)
	    (debugf 'slur "slur: ~a..~a\n" (.start (.music start)) (.start (.music stop)))
	    (slur (.model o) (.music start) (.music stop))))

(define-method (space (o <music-edit>))
  (space o (get-cursor o)))

(define-method (span (o <music-edit>) (s <gspan-type>) (d <integer>))
  (case (genum->symbol s)
    ((slur) (slur o d))
    (else "unknown span type: ~a\n" s)))

(define-method (step (o <music-edit>) (dir <integer>))
  (step o (get-cursor o) dir))
(define-method (step (o <music-edit>) (cursor <boolean>) (dir <integer>)) #f)
(define-method (step (o <music-edit>) (cursor <music>) (dir <integer>)) #f)
(define-method (step (o <music-edit>) (cursor <note-event>) (dir <integer>))
  (let ((step (.step (.pitch cursor))))
    (set-relative-pitch o cursor (modulo (+ step dir) 7))))

(define-method (get-system (o <music-edit>))
  (and-let* ((cursor (get-cursor o))
  	     (nitem (find (.notation (.layout o)) cursor))
	     (system (.system nitem))
	     (notation (.notation (.layout o))))
	    (get-system notation system)))

(define-method (intersect? (o <notation-item>) (paper <paper>) (r <rect>))
  (intersect? (paper-rect o paper) r))

(define (note-intersect paper rect)
  (lambda (n) (and-let* ((m (.music n)) ((is-a-note? m))) (intersect? n paper rect))))

(define-method (tie (o <music-edit>) (tie? <boolean>))
  (debugf 'key "tie: ~a\n" tie?)
  (tie o (get-cursor o) tie?))

(define-method (tie (o <music-edit>) (cursor <music>) (tie? <boolean>)) #f)

(define-method (tie (o <music-edit>) (cursor <note-event>) (tie? <boolean>))
  (debugf 'key "tie: ~a\n" tie?)
  (if (tie-possible? cursor)
      (tie (.model o) cursor tie?)))

(define-method (time (o <music-edit>) (t <string>))
  (debugf 'time "time: ~a\n" t)
  (time o (string->symbol t)))

(define-method (time (o <music-edit>) (t <symbol>))
  (debugf 'time "time: ~a\n" t)
  (let ((name (string->symbol (string-drop-prefix (symbol->string t) "time."))))
    (debugf 'time "name: ~a\n" name)
    (apply time (cons o (case name
			      ((C-) '(2 2 default))
			      ((C/) '(2 2 default))
			      ((C) '(4 4 default))
			      ((#{2/2}#) '(2 2 numeric))
			      ((#{4/4}#) '(4 4 numeric))
			      (else (map string->integer (string-split (symbol->string name) #\/))))))))

(define-method (time (o <music-edit>) (n <integer>) (d <integer>) . style)
  (debugf 'time "time: ~a/~a ~a\n" n d style)
  (let* ((model (.model o))
	 (music (find model (lambda (x) (is-a-time-signature? x)))))
  (apply set-time (cons* model music n d style))))


;;;; Pointer signals
(define-method (beyond-music-end (o <music-edit>) (mouse <gpoint>))
  (add-rhythmic-event o (.point mouse)))

(define-method (in-cursor-when (o <music-edit>) (mouse <gpoint>))
  (and-let* ((point (canvas-point o (.point mouse)))
	     (nitems (get-nitems (.layout o) point))
	     (cursor (get-cursor o))
	     (cursor-nitem (find (music-predicate (lambda (x) (eq? x cursor)))
				 nitems))
	     (pos (point->pos (.layout o) point))
	     (octave-step (pos->octave-step o pos)))
	    (apply select-or-repitch (cons* o (get-cursor o) octave-step))))

(define-method (gdk-scroll-direction->integer (o <gdk-scroll-direction>))
  (case (genum->symbol o) ((up) 1) ((down) -1) (else 0)))
	
(define-method (scroll-in-when (o <music-edit>) (direction <gdk-scroll-direction>) (mouse <gpoint>))
   (and-let* ((notes (mouse->notes o (.point mouse)))
	      (dir (gdk-scroll-direction->integer direction)))
	     (for-each (lambda (x) (step o x dir)) notes)))


;;;; Edit music
(define-method (pos->octave-step (o <music-edit>) (pos <integer>))
  (let* ((model (.model o))
	 (clef (find model (lambda (x) (is-a-clef-glyph? x))))
	 (clef-name (if clef (.value clef) "G"))
	 (clef-type (string->symbol
		     (string-drop-prefix clef-name "clefs.")))
	 (middle-c-property
	  (find model (lambda (x) (is-a-clef-middle-c? x))))
	 (clef-middle-c (if middle-c-property (.value middle-c-property) 0))
	 (staff-0-step (case clef-type
			 ((G) 2)
			 ((F) 4)
			 ((C) (case clef-middle-c ((0) 3) ((2) 1)))))
	 (staff-0-octave (case clef-type
			   ((G) 0)
			   ((F) -2)
			   ((C) -1)))
	 (staff-step (+ staff-0-step pos))
	 (step (modulo staff-step 7))
	 (octave (+ staff-0-octave (quotient staff-step 7)
		    (if (and (<0 staff-step) (>0 step)) -1 0))))
    (list octave step)))

(define-method (add-rhythmic-event (o <music-edit>) (mouse <point>))
  (let* ((pos (point->pos (.layout o) (canvas-point o mouse)))
	 (octave-step (pos->octave-step o pos))
	 (nitem (last-music-item (last-system (.layout o)) is-a-note-or-rest?))
	 (cursor (.music nitem)))
    (apply add-rhythmic-event (cons* o cursor octave-step))))

(define-method (add-rhythmic-event (o <music-edit>) (cursor <rhythmic-event>)
				   octave step)
  (debugf 'modify "add rhytmic event, duration-input: ~a\n" (.duration o))
  (if (append-allowed? o cursor)
      (let ((duration (or (.duration o) 4)))
	(if (.rest-input o)
	    (add-rest (.model o) cursor duration)
	    (add-note (.model o) cursor duration octave step)))))
      
(define-method (backspace (o <music-edit>) (cursor <rhythmic-event>))
  (debugf 'edit "backspace: is first?: ~a\n" (is-first-rhythmic-event? cursor))
  (debugf 'edit "backspace: cursor: ~a\n" cursor)
  (debugf 'edit "backspace: neigbor: ~a\n" (get-neighbor cursor -1))
  (if (is-first-rhythmic-event? cursor)
      (debugf 'edit "not removing first element\n")
      (if (and (is-a-rest? cursor) (not (is-last-rhythmic-event? cursor)))
	  (move-cursor o 'visual-positions -1)
	  (remove-or-replace-by-rest-and-backward o cursor))))

(define-method (backspace (o <music-edit>) (cursor <lyric-event>))
  (if (equal? (.text cursor) lyric-placeholder)
      (remove-lyric-and-backward o cursor)
      (remove-character (.model o) cursor)))

(define-method (delta-alteration (o <music-edit>) (cursor <rhythmic-event>)
			       (dir <integer>))
  (delta-alteration (.model o) cursor dir))

(define-method (delta-octave (o <music-edit>) (cursor <rhythmic-event>)
			   (dir <integer>))
  (delta-octave (.model o) cursor dir))

(define-method (duration (o <music-edit>) (cursor <rhythmic-event>)
			 (d <integer>))
  (eat-duration (.model o) cursor d))

(define-method (select-or-repitch (o <music-edit>) (cursor <note-event>)
				  (octave <integer>) (step <integer>))
  (debugf 'modify "select or repitch <note-event> --> repitching\n")
  ;; FIXME: select event-chord
  (repitch (.model o) cursor octave step))

(define-method (select-or-repitch (o <music-edit>) (cursor <rest-event>)
				  (octave <integer>) (step <integer>))
  ;; FIXME: select event-chord
  (debugf 'modify "select or repitch <rest-event> --> repitching\n")
  (repitch (.model o) cursor octave step))

(define-method (set-dots (o <music-edit>) (cursor <rhythmic-event>)
			 (dots <integer>))
  (eat-dots (.model o) cursor dots))

(define-method (set-relative-pitch (o <music-edit>) (cursor <note-event>)
				   (c <char>))
  (set-relative-pitch (.model o) cursor c))

(define-method (set-relative-pitch (o <music-edit>) (cursor <note-event>)
				   (step <integer>))
  (set-relative-pitch (.model o) cursor step))

(define-method (set-relative-pitch (o <music-edit>) (cursor <rest-event>)
				   (c <char>))
  (set-relative-pitch (.model o) cursor c))

(define-method (set-rest (o <music-edit>) (cursor <rhythmic-event>)
			 (r <symbol>))
  (set-rest (.model o) cursor r))

(define-method (remove-lyric-and-backward (o <music-edit>) (cursor <music>))
  (if (is-first-rhythmic-event? cursor)
      (debugf 'edit "not removing first element\n")
      (remove-rhythmic-event-and-backward (.model o) cursor)))

(define-method (remove-or-replace-by-rest-and-backward
		(o <music-edit>) (cursor <rhythmic-event>))
  (remove-or-replace-by-rest-and-backward (.model o) cursor))

;;(define-method (space (o <music-edit>) (cursor <music>))
;;  (set-cursor o (find (.model o) is-a-note-or-rest?)))

(define-method (space (o <music-edit>) (cursor <rhythmic-event>))
  (if (and (not (move-cursor o 'visual-positions 1))
	   (append-allowed? o cursor))
      (set-duration-add-rhythmic-event (.model o) cursor (.duration-input o))))

(define-method (space (o <music-edit>) (cursor <lyric-event>))
  (if (and (not (move-cursor o 'visual-positions 1))
	   (append-allowed? o cursor))
      (add-lyric (.model o) cursor)))

(define-method (append-allowed? (o <music-edit>) (cursor <rhythmic-event>))
  #t)

(define-method (urg-append-allowed? (o <music-edit>) (cursor <rhythmic-event>))
  (let* ((paper (get-paper (.layout o))))
    (debugf 'allow "size: ~a (~a)\n" (.size paper) (class-of (.size paper)))
    (debugf 'allow "eq?: ~a ~a\n" (equal? paper (make <paper> :size 'line)) (make <paper> :size 'line))
    (or (and (not (eq? (.size paper) 'line))
	     (not (equal? (.width paper) (.width (make <paper> :size 'line)))))
	(and-let* ((paper (get-paper (.layout o)))
		   (nitem (last-music-item (last-system (.layout o)) is-or-has-a-bar-line?))
		   (start (.start (.music nitem)))
		   (f 2)
		   (rect (* (paper-rect nitem paper) f))
		   (width (unit paper line-width))
		   (margin (unit paper .right-margin)))
		  (debugf 'allow "start: ~a\n" start)
		  (debugf 'allow "line-width: ~a\n" width)
		  (debugf 'allow "width: ~a\n" (unit paper .width))
		  (debugf 'allow "margin: ~a\n" margin)
		  (debugf 'allow "x: ~a\n" (x rect))
		  (and
		   (< start 8)
		   (> (- width margin) (x rect)))))))
