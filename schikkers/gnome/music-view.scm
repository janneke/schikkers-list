#! /bin/sh
# -*- scheme -*-
exec guile -L $(dirname $(dirname $(dirname $0))) -e test -s $0 "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define (test . args)
  (eval '(test (command-line)) (resolve-module '(schikkers gnome music-view))))

(define-module (schikkers gnome music-view)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gtk)
  :use-module (gnome gw gtk)
  ;; user
  :use-module (schikkers music-canvas)
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers music-layout)
  :use-module (schikkers music-model)
  :use-module (schikkers music-view)
  ;; user-gnome
  :use-module (schikkers gnome music-canvas)
  :export (<gnome-music-view>)
  :re-export (add
	      connect
	      pack-start
	      set-adjustments
	      show
	      show-all
	      update-adjustments))

(define-class <gnome-music-view> (<music-view>)
  (h-adjust-id :accessor .h-adjust-id :init-value #f)
  (v-adjust-id :accessor .v-adjust-id :init-value #f)
  (widget :accessor .widget :init-value (make <gtk-scrolled-window>)))

(define-method (initialize (o <gnome-music-view>) . initargs)
  (next-method)
  (set-policy o 'always 'always)
  (add o (.canvas (.layout o)))
  (connect-h-adjust o)
  (connect-v-adjust o)
  (show-all o)
  o)


;;; gtk-widget wrappers
(define-method (add (w <gtk-widget>) (o <gnome-music-view>)) (add w (.widget o)))
(define-method (remove (w <gtk-container>) (o <gnome-music-view>)) (remove w (.widget o)))
(define-method (pack-start (w <gtk-container>) (o <gnome-music-view>) . rest) (apply pack-start (cons* w (.widget o) rest)))

(define-method (add (o <gnome-music-view>) (c <gnome-music-canvas>)) (add (.widget o) c))
(define-method (show-all (o <gnome-music-view>)) (show-all (.widget o)))
(define-method (disconnect (o <gnome-music-view>) id) (disconnect (.widget o) id))
(define-method (get-hadjustment (o <gnome-music-view>)) (get-hadjustment (.widget o)))
(define-method (get-vadjustment (o <gnome-music-view>)) (get-vadjustment (.widget o)))
(define-method (grab-focus (o <gnome-music-view>)) (grab-focus (.widget o)))
(define-method (freeze-updates (o <gnome-music-view>)) (freeze-updates (get-window (.widget o))))
(define-method (height (o <gnome-music-view>)) (height (.widget o)))
(define-method (hide (o <gnome-music-view>)) (hide (.widget o)))
(define-method (set-policy (o <gnome-music-view>) . rest) (apply set-policy (cons* (.widget o) rest)))
(define-method (show (o <gnome-music-view>)) (show (.widget o)))
(define-method (thaw-updates (o <gnome-music-view>)) (thaw-updates (get-window (.widget o))))
(define-method (width (o <gnome-music-view>)) (width (.widget o)))


;;;; Scroll-adjustments
(define-method (connect-h-adjust (o <gnome-music-view>))
  (set! (.h-adjust-id o) (connect (get-hadjustment o) 'value-changed
				  (lambda (x) (value-changed o x)))))

(define-method (connect-v-adjust (o <gnome-music-view>))
  (set! (.v-adjust-id o) (connect (get-vadjustment o) 'value-changed
				  (lambda (x) (value-changed o x)))))

(define-method (slot-changed? (o <gtk-adjustment>) slot value)
  (let ((old (slot-ref o slot)))
    (or (and (=0 value) (!=0 old))
	(> (abs (/ (- old value) value)) 0.01))))

(define-method (set-if-changed (o <gtk-adjustment>) slot value)
  (if (slot-changed? o slot value)
      (slot-set! o slot value)))
		  
(define-method (set-adjustments (o <gnome-music-view>) (offset <point>))
  (and-let* ((h (get-hadjustment o))
	     (v (get-vadjustment o))
	     (size (.size o))
	     ((not (equal? size #(0 0 0 0))))
	     ((debugf 'scroll "scrolling to: ~a\n" (y offset)))
	     (y (y offset))
	     (x (x offset)))
	    (when (slot-changed? h 'value x)
	      (and-let* ((id (.h-adjust-id o))) (disconnect h id))
	      (slot-set! h 'value x)
	      (connect-h-adjust o))
	    (when (slot-changed? v 'value y)
	      (and-let* ((id (.v-adjust-id o))) (disconnect v id))
	      (slot-set! v 'value y)
	      (connect-v-adjust o))
	    (debugf 'scroll "offset now: ~a\n" (list (slot-ref (get-hadjustment o) 'value) (slot-ref (get-vadjustment o) 'value)))
	    (debugf 'scroll "describe:\n")
	    (if (member "--debug=scroll" (command-line))
		(describe v))))

(define-method (update-adjustments (o <gnome-music-view>))
  (and-let* ((h (get-hadjustment o))
	     (v (get-vadjustment o))
	     (paper (get-paper (.layout o)))
	     (ppu (pixels-per-unit o))
	     (mm-to-unit (.mm-to-unit paper))
	     (mm-to-pixel (* ppu mm-to-unit))
	     (size (.size o))
	     ((not (equal? size #(0 0 0 0))))
	     (page-count (max 1 (page-count (.layout o)))))
	    (debugf 'view "ppu: ~a\n" ppu)
	    (debugf 'view "mm-to-unit: ~a\n" mm-to-unit)
	    (debugf 'view "mm-to-pixel: ~a\n" mm-to-pixel)
	    (debugf 'view "pages: ~a\n" page-count)
	    (set-if-changed h 'page-size (width size))
	    (set-if-changed h 'page-increment (slot-ref h 'page-size))
	    (set-if-changed h 'upper (* mm-to-pixel (.width paper)))
	    (set-if-changed v 'page-size (height size))
	    (set-if-changed v 'page-increment (slot-ref v 'page-size))
	    (set-if-changed v 'upper (* page-count mm-to-pixel (.height paper)))
	    (debugf 'view "describe:\n")
	    (if (member "--debug=view" (command-line))
		(describe v))))

(define-method (offset (o <gnome-music-view>))
  (list (slot-ref (get-hadjustment o) 'value)
	(slot-ref (get-vadjustment o) 'value)))

(define-method (move-viewport (o <gnome-music-view>) (step <symbol>) count)
  (let* ((adjust (get-vadjustment o))
	 (inc (slot-ref adjust 'page-increment))
	 (val (slot-ref adjust 'value)))
    (debugf 'view "~a move-viewport inc: ~a\n" (class-name (class-of o)) inc)
    (debugf 'view "~a move-viewport val: ~a\n" (class-name (class-of o)) val)))

(define-method (value-changed (o <gnome-music-view>) adjust)
  (debugf 'scroll "~a value-changed val: ~a\n" (class-name (class-of o)) (slot-ref adjust 'value))
  (cond
   ((eq? adjust (get-hadjustment o)) (debugf 'scroll "h-adjust\n")
    (ensure-cursor-visible o))
   ((eq? adjust (get-vadjustment o)) (debugf 'scroll "v-adjust\n")
    (ensure-cursor-visible o))
   (else "debugf: else\n"))
  (validate o)
  (while (.validate-thread o) (flush-validate o))
  #f)


;;;; Test

(define (test . args)
  (let* ((main-window (make <gtk-window>
			:title "Music View Demo"
			:type 'toplevel))
	 (music (test-music))
	 (paper (make <paper> :size 'a8))
	 (model (make <music-model> :music music :paper paper))
	 (engraver (make (@ (schikkers test mock-engraver) <mock-engraver>)
		     :model model))
	 (canvas (make <gnome-music-canvas>))
	 (layout (make <music-layout> :canvas canvas :engraver engraver
		       :model model))
	 (view (make <gnome-music-view> :model model :layout layout))
	 ;;
	 (vbox (make <gtk-vbox>))
	 (actions (make <gtk-action-group> :name "Actions"))
	 (ui (make <gtk-ui-manager>)))
    (add main-window vbox)
    (add-actions actions
		 `(("ViewMenu"  #f "_View")
		   ("Zoom in" ,(gtk-stock-id 'zoom-in) "Zoom _in" "plus" "Zoom in" ,(lambda (x) (scale canvas 1)))
		   ("Zoom out" ,(gtk-stock-id 'zoom-out) "Zoom _out" "minus" "Zoom out" ,(lambda (x) (scale canvas -1)))))
    (insert-action-group ui actions 0)
    (add-ui-from-string ui "<ui><toolbar name='ToolBar'><toolitem action='Zoom in'/><toolitem action='Zoom out'/></toolbar></ui>")
    (pack-start vbox (get-widget ui "/ToolBar") #f #f 0)
    (add-accel-group main-window (get-accel-group ui))
    (pack-start vbox view #t #t 0)
    (set-default-size main-window 600 400)
    (connect main-window 'delete-event (lambda (w e) (gtk-main-quit) #f))
    (show-all main-window))
  (gtk-main))
