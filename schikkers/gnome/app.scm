;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2010--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers gnome app)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (os process)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome glib)
  :use-module (gnome gw glib)
  :use-module (gnome gobject)
  :use-module (gnome gtk gdk-event)
  :use-module (gnome gtk)
  :use-module (gnome-2)
  ;; user
  :use-module (schikkers engraver)
  :use-module (schikkers ly)
  :use-module (schikkers misc)
  :use-module (schikkers music-canvas)
  :use-module (schikkers music-modify)
  :use-module (schikkers music)
  :use-module (schikkers notation)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  ;; user-gtk
  :use-module (schikkers gnome ly-editor)
  :use-module (schikkers gnome music-canvas)
  :use-module (schikkers gmisc)
  :use-module (schikkers handler)
  :use-module (schikkers handler-edit)
  :use-module (schikkers ly-editor)
  :use-module (schikkers music-edit)
  :use-module (schikkers music-layout)
  :use-module (schikkers music-model)
  :use-module (schikkers music-tree-view)
  ;; user-gnome
  :use-module (schikkers gnome music-edit)
  :use-module (schikkers gnome music-edit-dialogs)
  :use-module (schikkers gnome music-tree-view)
  :export (main))

(define SLEEP 5000)

(define WIDTH 800)
(define HEIGHT 500)

(define (file-choose-dialog parent-window window-title mode default-file-name)
  (let ((dialog (make <gtk-file-chooser-dialog>
		  :action mode :title window-title :destroy-with-parent #t)))
    (if parent-window
	(set-transient-for dialog parent-window))
    (if default-file-name
        (set-filename dialog default-file-name))
    (for-each
     (lambda (id)
       (add-button dialog (gtk-stock-id id)
                   (genum->value (make <gtk-response-type> :value id))))
     '(cancel ok))
    (let* ((response (genum->symbol
		      (make <gtk-response-type> :value (gtk-dialog-run dialog))))
           (filename (get-filename dialog)))
      (destroy dialog)
      (case response
        ((ok) filename)
        ;;((cancel delete-event) (throw 'cancel))
        (else #f)))))

(define (message-dialog title text)
  (let* ((dialog (make <gtk-message-dialog>
		   :title title
		   :modal #t
		   :destroy-with-parent #t
		   :message-type 'info
		   :buttons 'close
		   )))
    (set-markup dialog text)
    (connect dialog 'response (lambda (d arg1) (gtk-widget-destroy dialog)))
    (gtk-dialog-run dialog)))

(define (help-key-bindings action)
  (message-dialog "Key bindings"
		  (string-join '(
				 "<big><b>Note mode</b></big><tt>"
				 "c d e f g b a          set pitch"
				 "' (high &lt;comma&gt;)       octave up"
				 ", (low &lt;comma&gt;)        octave down"
				 "#                      sharpen pitch"
				 "@                      flatten pitch"
				 "&lt;space&gt;                append new note"
				 "&lt;backspace&gt;            delete last note"
				 "1 2 4 8 16 32 64 128   set duration"
				 "&lt;                      open chord mode"
				 "&gt;                      close chord mode"
				 "&lt;up&gt;                   select higher chord note"
				 "&lt;down&gt;                 select lower chord note"
				 "C-k                    edit key signature"
				 "C-t                    edit time signature"
				 "C-i                    edit paper dimensions"
				 "</tt>"
				 "<big><b>Lyrics mode</b></big><tt>"
				 "A-Z, a-z, punctuation  enter lyric"
				 "&lt;space&gt;                append new lyric"
				 "&lt;backspace&gt;            delete lyric"
				 "</tt>"
				 "<big><b>General</b></big><tt>"
				 "&lt;left&gt;                 move cursor back"
				 "&lt;right&gt;                move cursor forward"
				 "C-&lt;minus&gt;              zoom out"
				 "C-&lt;plus&gt;/C-&lt;equals&gt;    zoom in"
				 "C-f                    add staff"
				 "C-l                    add lyrics"
				 "C-n                    new file"
				 "C-N                    new file from template"
				 "C-o                    open file"
				 "C-h                    show key bindings"
				 "C-r                    refresh"
				 "C-s                    save"
				 "C-S                    save as"
				 "C-p                    print"
				 "C-e                    export as ly"
				 "C-q                    quit"
				 "</tt>"
				 )
			       "\n")))

(define (help-about action)
  (let ((name (get-name action)))
    (message-dialog "About Schikkers-List" "Schikkers List Is Not Ikebana")))

(define (file-quit o)
  (save-sy (.handler o) #f)
  (format #t "killing lily: ~a\n" (.lily-pid o))
  (kill (.lily-pid o) SIGTERM)
  (format #t "killed with exit: ~a\n" (waitpid (.lily-pid o)))
  (gtk-main-quit)
  #t)

(define ui-info "
<ui>
  <menubar name='MenuBar'>
    <menu action='FileMenu'>
      <menuitem action='New'/>
      <menuitem action='New from template'/>
      <menuitem action='Open'/>
      <menuitem action='Save'/>
      <menuitem action='Save as'/>
      <menuitem action='Export ly'/>
      <menuitem action='Print'/>
      <separator/>
      <menuitem action='Quit'/>
    </menu>
    <menu action='EditMenu'>
      <menuitem action='Cut'/>
      <menu action='DurationMenu'>
	<menuitem action='Longa'/>
	<menuitem action='Breve'/>
	<menuitem action='Whole'/>
	<menuitem action='Half'/>
	<menuitem action='Quarter'/>
	<menuitem action='Eighth'/>
	<menuitem action='16th'/>
	<menuitem action='32nd'/>
	<menuitem action='64th'/>
	<menuitem action='128th'/>
      </menu>
      <menuitem action='Dot'/>
      <separator/>
      <menuitem action='Sharpen'/>
      <menuitem action='Flatten'/>
      <separator/>
      <menuitem action='8va'/>
      <menuitem action='8vb'/>
      <separator/>
      <menuitem action='Rest'/>
      <separator/>
      <menu action='Clef'>
	<menuitem action='clefs.G'/>
	<menuitem action='clefs.F'/>
	<menuitem action='clefs.C'/>
	<menuitem action='clefs.C.2'/>
	<menuitem action='Any-clef'/>
      </menu>
      <menu action='Key'>
	<menuitem action='key-C'/>
        <separator/>
        <menu action='Sharp'>
	  <menuitem action='key-G'/>
	  <menuitem action='key-D'/>
	  <menuitem action='key-A'/>
	  <menuitem action='key-E'/>
        </menu>
        <menu action='Flat'>
	  <menuitem action='key-F'/>
	  <menuitem action='key-BES'/>
	  <menuitem action='key-EES'/>
	  <menuitem action='key-AES'/>
        </menu>
	<menuitem action='Any-key'/>
      </menu>
      <menu action='Time'>
        <menu action='Time-1'>
	  <menuitem action='time.1-1'/>
	  <menuitem action='time.2-1'/>
	  <menuitem action='time.3-1'/>
	  <menuitem action='time.4-1'/>
	  <menuitem action='time.5-1'/>
        </menu>
        <menu action='Time-2'>
	  <menuitem action='time.C-'/>
	  <menuitem action='time.1-2'/>
	  <menuitem action='time.2-2'/>
	  <menuitem action='time.3-2'/>
	  <menuitem action='time.4-2'/>
	  <menuitem action='time.5-2'/>
	  <menuitem action='time.6-2'/>
        </menu>
        <menu action='Time-4'>
	  <menuitem action='time.C'/>
	  <menuitem action='time.1-4'/>
	  <menuitem action='time.2-4'/>
	  <menuitem action='time.3-4'/>
	  <menuitem action='time.4-4'/>
	  <menuitem action='time.5-4'/>
	  <menuitem action='time.6-4'/>
	  <menuitem action='time.7-4'/>
	  <menuitem action='time.8-4'/>
	  <menuitem action='time.9-4'/>
	  <menuitem action='time.12-4'/>
        </menu>
        <menu action='Time-8'>
	  <menuitem action='time.1-8'/>
	  <menuitem action='time.2-8'/>
	  <menuitem action='time.3-8'/>
	  <menuitem action='time.4-8'/>
	  <menuitem action='time.5-8'/>
	  <menuitem action='time.6-8'/>
	  <menuitem action='time.7-8'/>
	  <menuitem action='time.8-8'/>
	  <menuitem action='time.9-8'/>
	  <menuitem action='time.12-8'/>
        </menu>
        <menu action='Time-16'>
	  <menuitem action='time.1-16'/>
	  <menuitem action='time.2-16'/>
	  <menuitem action='time.3-16'/>
	  <menuitem action='time.4-16'/>
	  <menuitem action='time.5-16'/>
	  <menuitem action='time.6-16'/>
	  <menuitem action='time.7-16'/>
	  <menuitem action='time.8-16'/>
	  <menuitem action='time.9-16'/>
	  <menuitem action='time.12-16'/>
        </menu>
	<menuitem action='Any-time'/>
      </menu>
      <menu action='Articulation'>
	<menuitem action='articulation.clear'/>
        <menu action='Accent'>
	  <menuitem action='articulation.staccato'/>
	  <menuitem action='articulation.accent'/>
	  <menuitem action='articulation.tenuto'/>
	  <menuitem action='articulation.marcato'/>
	  <menuitem action='articulation.portato'/>
	  <menuitem action='articulation.espressivo'/>
	  <menuitem action='articulation.staccatissimo'/>
        </menu>
        <menu action='Finger'>
	  <menuitem action='articulation.thumb'/>
	  <menuitem action='articulation.digit-0'/>
	  <menuitem action='articulation.digit-1'/>
	  <menuitem action='articulation.digit-2'/>
	  <menuitem action='articulation.digit-3'/>
	  <menuitem action='articulation.digit-4'/>
	  <menuitem action='articulation.digit-5'/>
        </menu>
        <menu action='String'>
	  <menuitem action='articulation.downbow'/>
	  <menuitem action='articulation.upbow'/>
	  <menuitem action='articulation.flageolet'/>
	  <menuitem action='articulation.snappizzicato'/>
        </menu>
        <menu action='Wind'>
	  <menuitem action='articulation.open'/>
	  <menuitem action='articulation.halfopen'/>
	  <menuitem action='articulation.stopped'/>
        </menu>
        <menu action='Sign'>
	  <menuitem action='articulation.fermata'/>
	  <menuitem action='articulation.coda'/>
	  <menuitem action='articulation.segno'/>
	  <menuitem action='articulation.varcoda'/>
	  <menuitem action='articulation.shortfermata'/>
	  <menuitem action='articulation.longfermata'/>
	  <menuitem action='articulation.verylongfermata'/>
	  <menuitem action='articulation.signumcongruentiae'/>
        </menu>
        <menu action='Organ'>
	  <menuitem action='articulation.lheel'/>
	  <menuitem action='articulation.ltoe'/>
	  <menuitem action='articulation.rheel'/>
	  <menuitem action='articulation.rtoe'/>
        </menu>
        <menu action='Ornament'>
	  <menuitem action='articulation.trill'/>
	  <menuitem action='articulation.prall'/>
	  <menuitem action='articulation.turn'/>
	  <menuitem action='articulation.mordent'/>
	  <menuitem action='articulation.reverseturn'/>
	  <menuitem action='articulation.upprall'/>
	  <menuitem action='articulation.downprall'/>
	  <menuitem action='articulation.pralldown'/>
	  <menuitem action='articulation.prallup'/>
	  <menuitem action='articulation.upmordent'/>
	  <menuitem action='articulation.downmordent'/>
	  <menuitem action='articulation.prallmordent'/>
	  <menuitem action='articulation.prallprall'/>
	  <menuitem action='articulation.lineprall'/>
        </menu>
      </menu>
      <menu action='Dynamic'>
        <menuitem action='dynamic.clear'/>
        <menu action='Forte'>
	  <menuitem action='dynamic.mf'/>
	  <menuitem action='dynamic.f'/>
	  <menuitem action='dynamic.ff'/>
	  <menuitem action='dynamic.fff'/>
	  <menuitem action='dynamic.ffff'/>
	  <menuitem action='dynamic.fffff'/>
        </menu>
        <menu action='Piano'>
	  <menuitem action='dynamic.mp'/>
	  <menuitem action='dynamic.p'/>
	  <menuitem action='dynamic.pp'/>
	  <menuitem action='dynamic.ppp'/>
	  <menuitem action='dynamic.pppp'/>
	  <menuitem action='dynamic.ppppp'/>
        </menu>
        <menu action='FortePiano'>
	  <menuitem action='dynamic.sp'/>
	  <menuitem action='dynamic.spp'/>
	  <menuitem action='dynamic.sf'/>
	  <menuitem action='dynamic.sff'/>
	  <menuitem action='dynamic.fp'/>
	  <menuitem action='dynamic.sfz'/>
	  <menuitem action='dynamic.rfz'/>
        </menu>
      </menu>
      <separator/>
      <menuitem action='Tie'/>
      <separator/>
      <menuitem action='Add Lyrics'/>
      <menuitem action='Add Staff'/>
      <menuitem action='Add Voice'/>
      <separator/>
      <menuitem action='Paper Dimensions'/>
    </menu>
    <menu action='ViewMenu'>
      <menuitem action='Refresh'/>
      <menuitem action='Zoom in'/>
      <menuitem action='Zoom out'/>
    </menu>
    <menu action='HelpMenu'>
      <menuitem action='Key bindings'/>
      <menuitem action='About'/>
    </menu>
  </menubar>
  <toolbar name='ToolBar'>
    <toolitem action='New'/>
    <toolitem action='Open'/>
    <toolitem action='Save'/>
    <toolitem action='Save as'/>
    <toolitem action='Export ly'/>
    <toolitem action='Print'/>
    <separator action='Sep1'/>
    <toolitem action='Zoom in'/>
    <toolitem action='Zoom out'/>
    <separator action='Sep2'/>
    <toolitem action='Cut'/>
    <separator action='Sep3'/>
    <toolitem action='Refresh'/>
    <separator action='Sep4'/>
    <toolitem action='Quit'/>
    <separator action='Sep5'/>
  </toolbar>
  <toolbar name='NoteBar'>
    <placeholder name='Duration'>
      <toolitem action='Whole'/>
      <toolitem action='Half'/>
      <toolitem action='Quarter'/>
      <toolitem action='Eighth'/>
      <toolitem action='16th'/>
      <toolitem action='32nd'/>
      <toolitem action='64th'/>
      <toolitem action='128th'/>
    </placeholder>
    <separator action='Sep1'/>
    <toolitem action='Dot'/>
    <separator action='Sep2'/>
    <toolitem action='Sharpen'/>
    <toolitem action='Flatten'/>
    <separator action='Sep3'/>
    <toolitem action='8va'/>
    <toolitem action='8vb'/>
    <separator action='Sep4'/>
    <toolitem action='Rest'/>
    <separator action='Sep5'/>
    <toolitem action='Popup Clef'/>
    <toolitem action='Popup Key'/>
    <toolitem action='Popup Time'/>
    <toolitem action='Popup Articulation'/>
    <toolitem action='Popup Dynamic'/>
    <separator action='Sep6'/>
    <toolitem action='Tie'/>
  </toolbar>
  <popup name='ClefMenu'>
    <menuitem action='Popup clefs.G'/>
    <menuitem action='Popup clefs.F'/>
    <menuitem action='Popup clefs.C'/>
    <menuitem action='Popup clefs.C.2'/>
    <menuitem action='Popup Any-clef'/>
  </popup>
  <popup action='KeyMenu'>
    <menuitem action='Popup key-C'/>
    <menu action='Popup Sharp'>
      <menuitem action='Popup key-G'/>
      <menuitem action='Popup key-D'/>
      <menuitem action='Popup key-A'/>
      <menuitem action='Popup key-E'/>
    </menu>
    <menu action='Popup Flat'>
      <menuitem action='Popup key-F'/>
      <menuitem action='Popup key-BES'/>
      <menuitem action='Popup key-EES'/>
      <menuitem action='Popup key-AES'/>
    </menu>
    <menuitem action='Popup Any-key'/>
  </popup>
  <popup action='TimeMenu'>
    <menu action='Popup Time-1'>
      <menuitem action='Popup time.1-1'/>
      <menuitem action='Popup time.2-1'/>
      <menuitem action='Popup time.3-1'/>
      <menuitem action='Popup time.4-1'/>
      <menuitem action='Popup time.5-1'/>
    </menu>
    <menu action='Popup Time-2'>
      <menuitem action='Popup time.1-2'/>
      <menuitem action='Popup time.C-'/>
      <menuitem action='Popup time.2-2'/>
      <menuitem action='Popup time.3-2'/>
      <menuitem action='Popup time.4-2'/>
      <menuitem action='Popup time.5-2'/>
      <menuitem action='Popup time.6-2'/>
    </menu>
    <menu action='Popup Time-4'>
      <menuitem action='Popup time.1-4'/>
      <menuitem action='Popup time.2-4'/>
      <menuitem action='Popup time.3-4'/>
      <menuitem action='Popup time.C'/>
      <menuitem action='Popup time.4-4'/>
      <menuitem action='Popup time.5-4'/>
      <menuitem action='Popup time.6-4'/>
      <menuitem action='Popup time.7-4'/>
      <menuitem action='Popup time.8-4'/>
      <menuitem action='Popup time.9-4'/>
      <menuitem action='Popup time.12-4'/>
    </menu>
    <menu action='Popup Time-8'>
      <menuitem action='Popup time.1-8'/>
      <menuitem action='Popup time.2-8'/>
      <menuitem action='Popup time.3-8'/>
      <menuitem action='Popup time.4-8'/>
      <menuitem action='Popup time.5-8'/>
      <menuitem action='Popup time.6-8'/>
      <menuitem action='Popup time.7-8'/>
      <menuitem action='Popup time.8-8'/>
      <menuitem action='Popup time.9-8'/>
      <menuitem action='Popup time.12-8'/>
    </menu>
    <menu action='Popup Time-16'>
      <menuitem action='Popup time.1-16'/>
      <menuitem action='Popup time.2-16'/>
      <menuitem action='Popup time.3-16'/>
      <menuitem action='Popup time.4-16'/>
      <menuitem action='Popup time.5-16'/>
      <menuitem action='Popup time.6-16'/>
      <menuitem action='Popup time.7-16'/>
      <menuitem action='Popup time.8-16'/>
      <menuitem action='Popup time.9-16'/>
      <menuitem action='Popup time.12-16'/>
     </menu>
     <menuitem action='Popup Any-time'/>
  </popup>
  <popup action='ArticulationMenu'>
    <menuitem action='Popup articulation.clear'/>
    <menu action='Popup Accent'>
      <menuitem action='Popup articulation.staccato'/>
      <menuitem action='Popup articulation.accent'/>
      <menuitem action='Popup articulation.tenuto'/>
      <menuitem action='Popup articulation.marcato'/>
      <menuitem action='Popup articulation.portato'/>
      <menuitem action='Popup articulation.espressivo'/>
      <menuitem action='Popup articulation.staccatissimo'/>
    </menu>
    <menu action='Popup Finger'>
      <menuitem action='Popup articulation.thumb'/>
      <menuitem action='Popup articulation.digit-0'/>
      <menuitem action='Popup articulation.digit-1'/>
      <menuitem action='Popup articulation.digit-2'/>
      <menuitem action='Popup articulation.digit-3'/>
      <menuitem action='Popup articulation.digit-4'/>
      <menuitem action='Popup articulation.digit-5'/>
    </menu>
    <menu action='Popup String'>
      <menuitem action='Popup articulation.downbow'/>
      <menuitem action='Popup articulation.upbow'/>
      <menuitem action='Popup articulation.flageolet'/>
      <menuitem action='Popup articulation.snappizzicato'/>
    </menu>
    <menu action='Popup Wind'>
      <menuitem action='Popup articulation.open'/>
      <menuitem action='Popup articulation.halfopen'/>
      <menuitem action='Popup articulation.stopped'/>
    </menu>
    <menu action='Popup Sign'>
      <menuitem action='Popup articulation.fermata'/>
      <menuitem action='Popup articulation.coda'/>
      <menuitem action='Popup articulation.segno'/>
      <menuitem action='Popup articulation.varcoda'/>
      <menuitem action='Popup articulation.shortfermata'/>
      <menuitem action='Popup articulation.longfermata'/>
      <menuitem action='Popup articulation.verylongfermata'/>
      <menuitem action='Popup articulation.signumcongruentiae'/>
    </menu>
    <menu action='Popup Organ'>
      <menuitem action='Popup articulation.lheel'/>
      <menuitem action='Popup articulation.ltoe'/>
      <menuitem action='Popup articulation.rheel'/>
      <menuitem action='Popup articulation.rtoe'/>
    </menu>
    <menu action='Popup Ornament'>
      <menuitem action='Popup articulation.trill'/>
      <menuitem action='Popup articulation.prall'/>
      <menuitem action='Popup articulation.turn'/>
      <menuitem action='Popup articulation.mordent'/>
      <menuitem action='Popup articulation.reverseturn'/>
      <menuitem action='Popup articulation.upprall'/>
      <menuitem action='Popup articulation.downprall'/>
      <menuitem action='Popup articulation.pralldown'/>
      <menuitem action='Popup articulation.prallup'/>
      <menuitem action='Popup articulation.upmordent'/>
      <menuitem action='Popup articulation.downmordent'/>
      <menuitem action='Popup articulation.prallmordent'/>
      <menuitem action='Popup articulation.prallprall'/>
      <menuitem action='Popup articulation.lineprall'/>
    </menu>
  </popup>
  <popup action='DynamicMenu'>
    <menuitem action='Popup dynamic.clear'/>
    <menu action='Popup Forte'>
      <menuitem action='Popup dynamic.mf'/>
      <menuitem action='Popup dynamic.f'/>
      <menuitem action='Popup dynamic.ff'/>
      <menuitem action='Popup dynamic.fff'/>
      <menuitem action='Popup dynamic.ffff'/>
      <menuitem action='Popup dynamic.fffff'/>
    </menu>
    <menu action='Popup Piano'>
      <menuitem action='Popup dynamic.mp'/>
      <menuitem action='Popup dynamic.p'/>
      <menuitem action='Popup dynamic.pp'/>
      <menuitem action='Popup dynamic.ppp'/>
      <menuitem action='Popup dynamic.pppp'/>
      <menuitem action='Popup dynamic.ppppp'/>
    </menu>
    <menu action='Popup FortePiano'>
      <menuitem action='Popup dynamic.sp'/>
      <menuitem action='Popup dynamic.spp'/>
      <menuitem action='Popup dynamic.sf'/>
      <menuitem action='Popup dynamic.sff'/>
      <menuitem action='Popup dynamic.fp'/>
      <menuitem action='Popup dynamic.sfz'/>
      <menuitem action='Popup dynamic.rfz'/>
    </menu>
  </popup>
</ui>
")

(define-class <gnome-app> ()
  ;; JUNKME
  (handler-edit :accessor .handler-edit :init-value #f :init-keyword :handler-edit)
  (handler :accessor .handler :init-value #f :init-keyword :handler)

  (model :accessor .model :init-value #f :init-keyword :model)
  (edit :accessor .edit :init-value #f :init-keyword :edit)

  (duration-radio :accessor .duration-radio :init-value #f)
  (rest-checkbox :accessor .rest-checkbox :init-value #f)
  (tie-checkbox :accessor .tie-checkbox :init-value #f)
  (dotted-button :accessor .dotted-button :init-value #f)

  (edit-menu :accessor .edit-menu :init-value #f)
  (menu-bar :accessor .menu-bar :init-value #f)

  (articulation-menu :accessor .articulation-menu :init-value #f)
  (articulation-button :accessor .articulation-button :init-value #f)
  (edit-articulation-menu-item :accessor .edit-articulation-menu-item :init-value #f)
  (articulation-accent :accessor .articulation-accent :init-value #f)
  (articulation-finger :accessor .articulation-finger :init-value #f)
  (articulation-string :accessor .articulation-string :init-value #f)
  (articulation-wind :accessor .articulation-wind :init-value #f)
  (articulation-sign :accessor .articulation-sign :init-value #f)
  (articulation-organ :accessor .articulation-organ :init-value #f)
  (articulation-ornament :accessor .articulation-ornament :init-value #f)

  (clef-menu :accessor .clef-menu :init-value #f)
  (clef-button :accessor .clef-button :init-value #f)
  (edit-clef-menu-item :accessor .edit-clef-menu-item :init-value #f)

  (dynamic-menu :accessor .dynamic-menu :init-value #f)
  (dynamic-button :accessor .dynamic-button :init-value #f)
  (edit-dynamic-menu-item :accessor .edit-dynamic-menu-item :init-value #f)
  (dynamic-forte :accessor .dynamic-forte :init-value #f)
  (dynamic-paino :accessor .dynamic-piano :init-value #f)
  (dynamic-fortepiano :accessor .dynamic-fortepiano :init-value #f)

  (key-menu :accessor .key-menu :init-value #f)
  (key-button :accessor .key-button :init-value #f)
  (edit-key-menu-item :accessor .edit-key-menu-item :init-value #f)
  (key-flat :accessor .key-flat :init-value #f)
  (key-sharp :accessor .key-sharp :init-value #f)

  (time-menu :accessor .time-menu :init-value #f)
  (time-button :accessor .time-button :init-value #f)
  (edit-time-menu-item :accessor .edit-time-menu-item :init-value #f)
  (time-time-1 :accessor .time-time-1 :init-value #f)
  (time-time-2 :accessor .time-time-2 :init-value #f)
  (time-time-4 :accessor .time-time-4 :init-value #f)
  (time-time-8 :accessor .time-time-8 :init-value #f)
  (time-time-16 :accessor .time-time-16 :init-value #f)

  (editor :accessor .editor :init-value #f)
  (lily-pid :accessor .lily-pid :init-value #f :init-keyword :lily-pid)
  (lily-stdout :accessor .lily-stdout :init-value #f :init-keyword :lily-stdout)
  (main-window :accessor .main-window :init-value #f)
  (system-update :accessor .system-update :init-value #f)
  (tree-view :accessor .tree-view :init-value #f))

(define-method (initialize (o <gnome-app>) . initargs)
  (next-method)
  (let* ((canvas (get-canvas o))
	 (handler (.handler o)))
    (set! (.main-window o) (construct-main-window o))
    (set-title o)
    (set! (.edit-grob-callback canvas) (lambda (x) (edit-grob o x)))
    (set! (.editor o) (construct-editor-window o))
    (set! (.tree-view o) (construct-tree-view-window o))
    (connect (.editor o) 'insert-mark-changed
    	     (lambda (x m) (insert-mark-changed-handler o m)))
    (connect (.edit o) 'set-cursor
    	     (lambda (x m) (set-cursor-handler o m)))
    (connect (.tree-view o) 'selection-changed
    	     (lambda (x m) (selection-changed-handler o m)))
    (set-selection (.tree-view o) (get-cursor (.model o)))
    o))

(define-method (menu-actions (o <gnome-app>))
  (define (action action) (menu-action o action))
  (let ((handler (.handler o))
	(handler-edit (.handler-edit o))
	(model (.model o))
	(canvas (get-canvas o)))
    `(("FileMenu"  #f "_File")
      ;; (NAME STOCK-ID `label' `accelerator' `tooltip' `proc'?)
      ("New" ,(gtk-stock-id 'new) "_New" "<control>n" "Create a new file" ,(lambda (x) (file-new o)))
      ("New from template" ,(gtk-stock-id 'convert) "New from _template" "<control><shift>N" "Create a new file from template" ,(lambda (x) (file-new-from-template o)))
      ("Open" ,(gtk-stock-id 'open) "_Open" "<control>o" "Open a file" ,(lambda (x) (file-open o)))
      ("Save" ,(gtk-stock-id 'save) "_Save" "<control>s" "Save current file" ,(lambda (x) (save-sy handler #f)))
      ("Save as" ,(gtk-stock-id 'save-as) "Save _as" "<control><shift>s" "Save as" ,(lambda (x) (file-save-as o)))
      ("Export ly" ,(gtk-stock-id 'properties) "_Export ly" "<control>e" "Export current file as ly" ,(lambda (x) (save-ly handler)))
      ("Print" ,(gtk-stock-id 'print) "_Print" "<control>p" "Print current file" ,(lambda (x) (render-pdf handler-edit)))
      ("Quit" ,(gtk-stock-id 'quit) "_Quit" "<control>q" "Quit" ,(lambda (x) (file-quit o)))
      ("EditMenu"  #f "_Edit")
      ("Cut" ,(gtk-stock-id 'cut) "_Cut" #f "Cut" ,action)
      ("DurationMenu"  #f "Set _duration")
      ;; ("Longa" "note--2" "_Longa" #f "Longa" ,action)
      ;; ("Breve" "note--1" "_Breve" #f "Breve" ,action)
      ;; ("Whole" "note-0" "_Whole" "1" "Whole" ,action)
      ;; ("Half" "note-1" "_Half" "2" "Half" ,action)
      ;; ("Quarter" "note-2" "_Quarter" "4" "Quarter" ,action)
      ;; ("Eighth" "note-3" "_Eighth" "8" "Eighth" ,action)
      ;; ("16th" "note-4" "_16th" #f "16th" ,action)
      ;; ("32nd" "note-5" "_32nd" #f "32nd" ,action)
      ;; ("64th" "note-6" "_64th" #f "64th" ,action)
      ;; ("128th" "note-7" "128th" #f "128th" ,action)
      ("Dot" "dotted" "_dot" #f "Dot" ,action)
      ("Add Voice" #f "Add _Voice" "<control>v" "Add Voice" ,(lambda (x) (add-voice model)))
      ("Add Staff" #f "Add _Staff" "<control>f" "Add Staff" ,(lambda (x) (add-staff model)))
      ("Add Lyrics" #f "Add _Lyrics" "<control>l" "Add Lyrics" ,(lambda (x) (add-lyrics model)))
      ("Sharpen" "alteration-1" "_Sharpen" "numbersign" "Sharpen" ,action)
      ("Flatten" "alteration--1" "_Flatten" "at" "Flatten" ,action)
      ("8va" "ottava.1" "_' / 8va" "comma" "' / 8va" ,action)
      ("8vb" "ottava.-1" "_, / 8vb" "apostrophe" ", / 8vb" ,action)

      ("Clef" "clefs.G" "Set _clef" #f "clefs.G" #f)
      ("clefs.G" "clefs.G" "_G violin" #f "clefs.G" ,action)
      ("clefs.F" "clefs.F" "_F bass" #f "clefs.F" ,action)
      ("clefs.C" "clefs.C" "_C alto" #f "clefs.C" ,action)
      ("clefs.C.2" "clefs.C.2" "C_2 tenor" #f "clefs.C.2" ,action)
      ("Any-clef" "any-clef" "An_y clef" #f "Edit clef music" ,action)

      ("Popup Clef" "clefs.G" "Set _clef" #f "clefs.G" ,action)
      ("Popup clefs.G" "clefs.G" "_G violin" #f "clefs.G" ,action)
      ("Popup clefs.F" "clefs.F" "_F bass" #f "clefs.F" ,action)
      ("Popup clefs.C" "clefs.C" "_C alto" #f "clefs.C" ,action)
      ("Popup clefs.C.2" "clefs.C.2" "C_2 tenor" #f "clefs.C.2" ,action)
      ("Popup Any-clef" "any-clef" "An_y clef" #f "Edit clef music" ,action)

      ("Key" "key-A" "Set _Key Signature" #f "key A major" #f)
      ("key-C" "key-C" "Key _C" #f "key C" ,action)

      ("Sharp" "key-A" "_Sharp" #f #f ,action)
      ("key-G" "key-G" "_G" #f "key G" ,action)
      ("key-D" "key-D" "_D" #f "key D" ,action)
      ("key-A" "key-A" "_A" #f "key A" ,action)
      ("key-E" "key-E" "_E" #f "key E" ,action)

      ("Flat" "key-EES" "_Flat" #f #f ,action)
      ("key-F" "key-F" "_F" #f "key F" ,action)
      ("key-BES" "key-BES" "_BES" #f "key BES" ,action)
      ("key-EES" "key-EES" "_EES" #f "key EES" ,action)
      ("key-AES" "key-AES" "_AES" #f "key AES" ,action)
      ("Any-key" "any-key" "An_y key" #f "Edit key music" ,action)

      ("Popup Key" "key-A" #f #f #f ,action)
      ("KeyMenu" "key-A" #f #f #f ,action)
      ("Popup Sharp" "key-A" "_Sharp" #f #f ,action)
      ("Popup Flat" "key-EES" "_Flat" #f #f ,action)

      ("Popup key-C" "key-C" "C" #f "key C" ,action)

      ("Popup key-G" "key-G" "_G" #f "key G" ,action)
      ("Popup key-D" "key-D" "_D" #f "key D" ,action)
      ("Popup key-A" "key-A" "_A" #f "key A" ,action)
      ("Popup key-E" "key-E" "_E" #f "key E" ,action)

      ("Popup key-F" "key-F" "_F" #f "key F" ,action)
      ("Popup key-BES" "_BES" "BES" #f "key BES" ,action)
      ("Popup key-EES" "_EES" "EES" #f "key EES" ,action)
      ("Popup key-AES" "_AES" "AES" #f "key AES" ,action)
      ("Popup Any-key" "any-key" "An_y key" #f "Edit key music" ,action)

      ("Time" "time.3-4" "Set _Time Signature" #f "time.3/4" #f)
      ("Time-1" "time.1-1" "/_1" #f #f ,action)
      ("Time-2" "time.2-2" "/_2" #f #f ,action)
      ("Time-4" "time.4-4" "/_4" #f #f ,action)
      ("Time-8" "time.4-8" "/_8" #f #f ,action)
      ("Time-16" "time.4-16" "/1_6" #f #f ,action)

      ("time.1-1" "time.1-1" "_1-1" #f "time 1/1" ,action)
      ("time.2-1" "time.2-1" "_2-1" #f "time 2/1" ,action)
      ("time.3-1" "time.3-1" "_3-1" #f "time 3/1" ,action)
      ("time.4-1" "time.4-1" "_4-1" #f "time 4/1" ,action)
      ("time.5-1" "time.5-1" "_5-1" #f "time 5/1" ,action)

      ("time.C-" "time.C-" "_C-" #f "time &cent; (2/2)" ,action)
      ("time.1-2" "time.1-2" "_1-2" #f "time 1/2" ,action)
      ("time.2-2" "time.2-2" "_2-2" #f "time 2/2" ,action)
      ("time.3-2" "time.3-2" "_3-2" #f "time 3/2" ,action)
      ("time.4-2" "time.4-2" "_4-2" #f "time 4/2" ,action)
      ("time.5-2" "time.5-2" "_5-2" #f "time 5/2" ,action)
      ("time.6-2" "time.6-2" "_6-2" #f "time 6/2" ,action)

      ("time.C" "time.C" "_C" #f "time C (4/4)" ,action)
      ("time.1-4" "time.1-4" "_1-4" #f "time 1/4" ,action)
      ("time.2-4" "time.2-4" "_2-4" #f "time 2/4" ,action)
      ("time.3-4" "time.3-4" "_3-4" #f "time 3/4" ,action)
      ("time.4-4" "time.4-4" "_4-4" #f "time 4/4" ,action)
      ("time.5-4" "time.5-4" "_5-4" #f "time 5/4" ,action)
      ("time.6-4" "time.6-4" "_6-4" #f "time 6/4" ,action)
      ("time.7-4" "time.7-4" "_7-4" #f "time 7/4" ,action)
      ("time.8-4" "time.8-4" "_8-4" #f "time 8/4" ,action)
      ("time.9-4" "time.9-4" "_9-4" #f "time 9/4" ,action)
      ("time.12-4" "time.12-4" "12-4" #f "time 12/4" ,action)

      ("time.1-8" "time.1-8" "_1-8" #f "time 1/8" ,action)
      ("time.2-8" "time.2-8" "_2-8" #f "time 2/8" ,action)
      ("time.3-8" "time.3-8" "_3-8" #f "time 3/8" ,action)
      ("time.4-8" "time.4-8" "_4-8" #f "time 4/8" ,action)
      ("time.5-8" "time.5-8" "_5-8" #f "time 5/8" ,action)
      ("time.6-8" "time.6-8" "_6-8" #f "time 6/8" ,action)
      ("time.7-8" "time.7-8" "_7-8" #f "time 7/8" ,action)
      ("time.8-8" "time.8-8" "_8-8" #f "time 8/8" ,action)
      ("time.9-8" "time.9-8" "_9-8" #f "time 9/8" ,action)
      ("time.12-8" "time.12-8" "12-8" #f "time 12/8" ,action)

      ("time.1-16" "time.1-16" "_1-16" #f "time 1/16" ,action)
      ("time.2-16" "time.2-16" "_2-16" #f "time 2/16" ,action)
      ("time.3-16" "time.3-16" "_3-16" #f "time 3/16" ,action)
      ("time.4-16" "time.4-16" "_4-16" #f "time 4/16" ,action)
      ("time.5-16" "time.5-16" "_5-16" #f "time 5/16" ,action)
      ("time.6-16" "time.6-16" "_6-16" #f "time 6/16" ,action)
      ("time.7-16" "time.7-16" "_7-16" #f "time 7/16" ,action)
      ("time.8-16" "time.8-16" "_8-16" #f "time 8/16" ,action)
      ("time.9-16" "time.9-16" "_9-16" #f "time 9/16" ,action)
      ("time.12-16" "time.12-16" "12-16" #f "time 12/16" ,action)
      ("Any-time" "any-time" "An_y time" #f "Edit time music" ,action)

      ("Popup Time" "time.3-4" #f #f #f ,action)
      ("TimeMenu" "time.3-4" #f #f #f ,action)
      ("Popup Time-1" "time.1-1" "/_1" #f #f ,action)
      ("Popup Time-2" "time.2-2" "/_2" #f #f ,action)
      ("Popup Time-4" "time.4-4" "/_4" #f #f ,action)
      ("Popup Time-8" "time.4-8" "/_8" #f #f ,action)
      ("Popup Time-16" "time.4-16" "/1_6" #f #f ,action)

      ("Popup time.1-1" "time.1-1" #f #f "time 1/1" ,action)
      ("Popup time.2-1" "time.2-1" #f #f "time 2/1" ,action)
      ("Popup time.3-1" "time.3-1" #f #f "time 3/1" ,action)
      ("Popup time.4-1" "time.4-1" #f #f "time 4/1" ,action)
      ("Popup time.5-1" "time.5-1" #f #f "time 5/1" ,action)

      ("Popup time.C-" "time.C-" #f #f "time &cent; (2/2)" ,action)
      ("Popup time.1-2" "time.1-2" #f #f "time 1/2" ,action)
      ("Popup time.2-2" "time.2-2" #f #f "time 2/2" ,action)
      ("Popup time.3-2" "time.3-2" #f #f "time 3/2" ,action)
      ("Popup time.4-2" "time.4-2" #f #f "time 4/2" ,action)
      ("Popup time.5-2" "time.5-2" #f #f "time 5/2" ,action)
      ("Popup time.6-2" "time.6-2" #f #f "time 6/2" ,action)

      ("Popup time.C" "time.C" #f #f "time C (4/4)" ,action)
      ("Popup time.1-4" "time.1-4" #f #f "time 1/4" ,action)
      ("Popup time.2-4" "time.2-4" #f #f "time 2/4" ,action)
      ("Popup time.3-4" "time.3-4" #f #f "time 3/4" ,action)
      ("Popup time.4-4" "time.4-4" #f #f "time 4/4" ,action)
      ("Popup time.5-4" "time.5-4" #f #f "time 5/4" ,action)
      ("Popup time.6-4" "time.6-4" #f #f "time 6/4" ,action)
      ("Popup time.7-4" "time.7-4" #f #f "time 7/4" ,action)
      ("Popup time.8-4" "time.8-4" #f #f "time 8/4" ,action)
      ("Popup time.9-4" "time.9-4" #f #f "time 9/4" ,action)
      ("Popup time.12-4" "time.12-4" #f #f "time 12/4" ,action)

      ("Popup time.1-8" "time.1-8" #f #f "time 1/8" ,action)
      ("Popup time.2-8" "time.2-8" #f #f "time 2/8" ,action)
      ("Popup time.3-8" "time.3-8" #f #f "time 3/8" ,action)
      ("Popup time.4-8" "time.4-8" #f #f "time 4/8" ,action)
      ("Popup time.5-8" "time.5-8" #f #f "time 5/8" ,action)
      ("Popup time.6-8" "time.6-8" #f #f "time 6/8" ,action)
      ("Popup time.7-8" "time.7-8" #f #f "time 7/8" ,action)
      ("Popup time.8-8" "time.8-8" #f #f "time 8/8" ,action)
      ("Popup time.9-8" "time.9-8" #f #f "time 9/8" ,action)
      ("Popup time.12-8" "time.12-8" #f #f "time 12/8" ,action)

      ("Popup time.1-16" "time.1-16" #f #f "time 1/16" ,action)
      ("Popup time.2-16" "time.2-16" #f #f "time 2/16" ,action)
      ("Popup time.3-16" "time.3-16" #f #f "time 3/16" ,action)
      ("Popup time.4-16" "time.4-16" #f #f "time 4/16" ,action)
      ("Popup time.5-16" "time.5-16" #f #f "time 5/16" ,action)
      ("Popup time.6-16" "time.6-16" #f #f "time 6/16" ,action)
      ("Popup time.7-16" "time.7-16" #f #f "time 7/16" ,action)
      ("Popup time.8-16" "time.8-16" #f #f "time 8/16" ,action)
      ("Popup time.9-16" "time.9-16" #f #f "time 9/16" ,action)
      ("Popup time.12-16" "time.12-16" #f #f "time 12/16" ,action)
      ("Popup Any-time" "any-time" "An_y time" #f "Edit time music" ,action)

      ("Articulation" "articulation.button" "Set _Articulation" #f "Articulation" #f)
      ("Accent" "accent" "_Accent" #f #f ,action)
      ("Finger" "finger" "_Finger" #f #f ,action)
      ("String" "string" "_String" #f #f ,action)
      ("Wind" "wind" "_Wind" #f #f ,action)
      ("Sign" "sign" "_Sign" #f #f ,action)
      ("Organ" "organ" "_Organ" #f #f ,action)
      ("Ornament" "ornament" "O_rnament" #f #f ,action)

      ("articulation.clear" "articulation.clear" "_clear" #f "clear" ,action)
      ("articulation.staccato" "articulation.staccato" "_staccato" #f "staccato" ,action)
      ("articulation.accent" "articulation.accent" "_accent" #f "accent" ,action)
      ("articulation.tenuto" "articulation.tenuto" "_tenuto" #f "tenuto" ,action)
      ("articulation.marcato" "articulation.marcato" "_marcato" #f "marcato" ,action)
      ("articulation.portato" "articulation.portato" "_portato" #f "portato" ,action)
      ("articulation.espressivo" "articulation.espressivo" "_espressivo" #f "espressivo" ,action)
      ("articulation.staccatissimo" "articulation.staccatissimo" "_staccatissimo" #f "staccatissimo" ,action)

      ("articulation.thumb" "articulation.thumb" "_thumb" #f "thumb" ,action)
      ("articulation.digit-0" "articulationa.digit-0" "digit _0" #f "digit-0" ,action)
      ("articulation.digit-1" "articulation.digit-1" "digit _1" #f "digit-1" ,action)
      ("articulation.digit-2" "articulation.digit-2" "digit _2" #f "digit-2" ,action)
      ("articulation.digit-3" "articulation.digit-3" "digit _3" #f "digit-3" ,action)
      ("articulation.digit-4" "articulation.digit-4" "digit _4" #f "digit-4" ,action)
      ("articulation.digit-5" "articulation.digit-5" "digit _5" #f "digit-5" ,action)

      ("articulation.downbow" "articulation.downbow" "_downbow" #f "downbow" ,action)
      ("articulation.upbow" "articulation.upbow" "_upbow" #f "upbow" ,action)
      ("articulation.flageolet" "articulation.flageolet" "_flageolet" #f "flageolet" ,action)
      ("articulation.snappizzicato" "articulation.snappizzicato" "_snappizzicato" #f "snappizzicato" ,action)

      ("articulation.open" "articulation.open" "_open" #f "open" ,action)
      ("articulation.halfopen" "articulation.halfopen" "_halfopen" #f "halfopen" ,action)
      ("articulation.stopped" "articulation.stopped" "_stopped" #f "stopped" ,action)

      ("articulation.fermata" "articulation.fermata" "_fermata" #f "fermata" ,action)
      ("articulation.coda" "articulation.coda" "_coda" #f "coda" ,action)
      ("articulation.segno" "articulation.segno" "_segno" #f "segno" ,action)
      ("articulation.varcoda" "articulation.varcoda" "_varcoda" #f "varcoda" ,action)
      ("articulation.shortfermata" "articulation.shortfermata" "_shortfermata" #f "shortfermata" ,action)
      ("articulation.longfermata" "articulation.longfermata" "_longfermata" #f "longfermata" ,action)
      ("articulation.verylongfermata" "articulation.verylongfermata" "_verylongfermata" #f "verylongfermata" ,action)
      ("articulation.signumcongruentiae" "articulation.signumcongruentiae" "_signumcongruentiae" #f "signumcongruentiae" ,action)

      ("articulation.lheel" "articulation.lheel" "_lheel" #f "lheel" ,action)
      ("articulation.ltoe" "articulation.ltoe" "_ltoe" #f "ltoe" ,action)
      ("articulation.rheel" "articulation.rheel" "_rheel" #f "rheel" ,action)
      ("articulation.rtoe" "articulation.rtoe" "_rtoe" #f "rtoe" ,action)

      ("articulation.trill" "articulation.trill" "_trill" #f "trill" ,action)
      ("articulation.prall" "articulation.prall" "_prall" #f "prall" ,action)
      ("articulation.turn" "articulation.turn" "_turn" #f "turn" ,action)
      ("articulation.mordent" "articulation.mordent" "_mordent" #f "mordent" ,action)
      ("articulation.reverseturn" "articulation.reverseturn" "_reverseturn" #f "reverseturn" ,action)
      ("articulation.upprall" "articulation.upprall" "_upprall" #f "upprall" ,action)
      ("articulation.downprall" "articulation.downprall" "_downprall" #f "downprall" ,action)
      ("articulation.pralldown" "articulation.pralldown" "_pralldown" #f "pralldown" ,action)
      ("articulation.prallup" "articulation.prallup" "_prallup" #f "prallup" ,action)
      ("articulation.upmordent" "articulation.upmordent" "_upmordent" #f "upmordent" ,action)
      ("articulation.downmordent" "articulation.downmordent" "_downmordent" #f "downmordent" ,action)
      ("articulation.prallmordent" "articulation.prallmordent" "_prallmordent" #f "prallmordent" ,action)
      ("articulation.prallprall" "articulation.prallprall" "_prallprall" #f "prallprall" ,action)
      ("articulation.lineprall" "articulation.lineprall" "_lineprall" #f "lineprall" ,action)

      ("Popup Articulation" "articulation.button" "Set _Articulation" #f "Set Articulation" ,action)
      ("ArticulationMenu" "articulation.button" #f #f #f ,action)
      ("Popup Accent" "accent" "_Accent" #f #f ,action)
      ("Popup Finger" "finger" "_Finger" #f #f ,action)
      ("Popup String" "string" "_String" #f #f ,action)
      ("Popup Wind" "wind" "_Wind" #f #f ,action)
      ("Popup Sign" "sign" "_Sign" #f #f ,action)
      ("Popup Organ" "organ" "_Organ" #f #f ,action)
      ("Popup Ornament" "ornament" "O_rnament" #f #f ,action)

      ("Popup articulation.clear" "articulation.clear" "_clear" #f "clear" ,action)
      ("Popup articulation.staccato" "articulation.staccato" "_staccato" #f "staccato" ,action)
      ("Popup articulation.accent" "articulation.accent" "_accent" #f "accent" ,action)
      ("Popup articulation.tenuto" "articulation.tenuto" "_tenuto" #f "tenuto" ,action)
      ("Popup articulation.marcato" "articulation.marcato" "_marcato" #f "marcato" ,action)
      ("Popup articulation.portato" "articulation.portato" "_portato" #f "portato" ,action)
      ("Popup articulation.espressivo" "articulation.espressivo" "_espressivo" #f "espressivo" ,action)
      ("Popup articulation.staccatissimo" "articulation.staccatissimo" "_staccatissimo" #f "staccatissimo" ,action)

      ("Popup articulation.thumb" "articulation.thumb" "_thumb" #f "thumb" ,action)
      ("Popup articulation.digit-0" "articulation.digit-0" "digit _0" #f "digit-0" ,action)
      ("Popup articulation.digit-1" "articulation.digit-1" "digit _1" #f "digit-1" ,action)
      ("Popup articulation.digit-2" "articulation.digit-2" "digit _2" #f "digit-2" ,action)
      ("Popup articulation.digit-3" "articulation.digit-3" "digit _3" #f "digit-3" ,action)
      ("Popup articulation.digit-4" "articulation.digit-4" "digit _4" #f "digit-4" ,action)
      ("Popup articulation.digit-5" "articulation.digit-5" "digit _5" #f "digit-5" ,action)

      ("Popup articulation.downbow" "articulation.downbow" "_downbow" #f "downbow" ,action)
      ("Popup articulation.upbow" "articulation.upbow" "_upbow" #f "upbow" ,action)
      ("Popup articulation.flageolet" "articulation.flageolet" "_flageolet" #f "flageolet" ,action)
      ("Popup articulation.snappizzicato" "articulation.snappizzicato" "_snappizzicato" #f "snappizzicato" ,action)

      ("Popup articulation.open" "articulation.open" "_open" #f "open" ,action)
      ("Popup articulation.halfopen" "articulation.halfopen" "_halfopen" #f "halfopen" ,action)
      ("Popup articulation.stopped" "articulation.stopped" "_stopped" #f "stopped" ,action)

      ("Popup articulation.fermata" "articulation.fermata" "_fermata" #f "fermata" ,action)
      ("Popup articulation.coda" "articulation.coda" "_coda" #f "coda" ,action)
      ("Popup articulation.segno" "articulation.segno" "_segno" #f "segno" ,action)
      ("Popup articulation.varcoda" "articulation.varcoda" "_varcoda" #f "varcoda" ,action)
      ("Popup articulation.shortfermata" "articulation.shortfermata" "_shortfermata" #f "shortfermata" ,action)
      ("Popup articulation.longfermata" "articulation.longfermata" "_longfermata" #f "longfermata" ,action)
      ("Popup articulation.verylongfermata" "articulation.verylongfermata" "_verylongfermata" #f "verylongfermata" ,action)
      ("Popup articulation.signumcongruentiae" "articulation.signumcongruentiae" "_signumcongruentiae" #f "signumcongruentiae" ,action)

      ("Popup articulation.lheel" "articulation.lheel" "_lheel" #f "lheel" ,action)
      ("Popup articulation.ltoe" "articulation.ltoe" "_ltoe" #f "ltoe" ,action)
      ("Popup articulation.rheel" "articulation.rheel" "_rheel" #f "rheel" ,action)
      ("Popup articulation.rtoe" "articulation.rtoe" "_rtoe" #f "rtoe" ,action)

      ("Popup articulation.trill" "articulation.trill" "_trill" #f "trill" ,action)
      ("Popup articulation.prall" "articulation.prall" "_prall" #f "prall" ,action)
      ("Popup articulation.turn" "articulation.turn" "_turn" #f "turn" ,action)
      ("Popup articulation.mordent" "articulation.mordent" "_mordent" #f "mordent" ,action)
      ("Popup articulation.reverseturn" "articulation.reverseturn" "_reverseturn" #f "reverseturn" ,action)
      ("Popup articulation.upprall" "articulation.upprall" "_upprall" #f "upprall" ,action)
      ("Popup articulation.downprall" "articulation.downprall" "_downprall" #f "downprall" ,action)
      ("Popup articulation.pralldown" "articulation.pralldown" "_pralldown" #f "pralldown" ,action)
      ("Popup articulation.prallup" "articulation.prallup" "_prallup" #f "prallup" ,action)
      ("Popup articulation.upmordent" "articulation.upmordent" "_upmordent" #f "upmordent" ,action)
      ("Popup articulation.downmordent" "articulation.downmordent" "_downmordent" #f "downmordent" ,action)
      ("Popup articulation.prallmordent" "articulation.prallmordent" "_prallmordent" #f "prallmordent" ,action)
      ("Popup articulation.prallprall" "articulation.prallprall" "_prallprall" #f "prallprall" ,action)
      ("Popup articulation.lineprall" "articulation.lineprall" "_lineprall" #f "lineprall" ,action)

      ("Dynamic" "dynamic.button" "Set _Dynamic" #f "Dynamic" #f)
      ("Forte" "f" "_Forte" #f #f ,action)
      ("Piano" "p" "_Piano" #f #f ,action)
      ("FortePiano" "fp" "Fo_rtePiano" #f #f ,action)

      ("dynamic.mf" "dynamic.mf" "_mf" #f "mf" ,action)
      ("dynamic.f" "dynamic.f" "_f" #f "f" ,action)
      ("dynamic.ff" "dynamic.ff" "_ff" #f "ff" ,action)
      ("dynamic.fff" "dynamic.fff" "_fff" #f "fff" ,action)
      ("dynamic.ffff" "dynamic.ffff" "_ffff" #f "ffff" ,action)
      ("dynamic.fffff" "dynamic.fffff" "_fffff" #f "fffff" ,action)
      ("dynamic.mp" "dynamic.mp" "_mp" #f "mp" ,action)
      ("dynamic.p" "dynamic.p" "_p" #f "p" ,action)
      ("dynamic.pp" "dynamic.pp" "_pp" #f "pp" ,action)
      ("dynamic.ppp" "dynamic.ppp" "_ppp" #f "ppp" ,action)
      ("dynamic.pppp" "dynamic.pppp" "_pppp" #f "pppp" ,action)
      ("dynamic.ppppp" "dynamic.ppppp" "_ppppp" #f "ppppp" ,action)
      ("dynamic.clear" "dynamic.clear" "_clear" #f "clear" ,action)
      ("dynamic.sp" "dynamic.sp" "_sp" #f "sp" ,action)
      ("dynamic.spp" "dynamic.spp" "_spp" #f "spp" ,action)
      ("dynamic.sf" "dynamic.sf" "_sf" #f "sf" ,action)
      ("dynamic.sff" "dynamic.sff" "_sff" #f "sff" ,action)
      ("dynamic.fp" "dynamic.fp" "_fp" #f "fp" ,action)
      ("dynamic.sfz" "dynamic.sfz" "_sfz" #f "sfz" ,action)
      ("dynamic.rfz" "dynamic.rfz" "_rfz" #f "rfz" ,action)

      ("Popup Dynamic" "dynamic.button" "Set _Dynamic" #f "Set Dynamic" ,action)
      ("DynamicMenu" "dynamic.button" #f #f #f ,action)
      ("Popup Forte" "f" "_Forte" #f #f ,action)
      ("Popup Piano" "p" "_Piano" #f #f ,action)
      ("Popup FortePiano" "fp" "Fo_rtePiano" #f #f ,action)

      ("Popup dynamic.mf" "dynamic.mf" "_mf" #f "mf" ,action)
      ("Popup dynamic.f" "dynamic.f" "_f" #f "f" ,action)
      ("Popup dynamic.ff" "dynamic.ff" "_ff" #f "ff" ,action)
      ("Popup dynamic.fff" "dynamic.fff" "_fff" #f "fff" ,action)
      ("Popup dynamic.ffff" "dynamic.ffff" "_ffff" #f "ffff" ,action)
      ("Popup dynamic.fffff" "dynamic.fffff" "_fffff" #f "fffff" ,action)
      ("Popup dynamic.mp" "dynamic.mp" "_mp" #f "mp" ,action)
      ("Popup dynamic.p" "dynamic.p" "_p" #f "p" ,action)
      ("Popup dynamic.pp" "dynamic.pp" "_pp" #f "pp" ,action)
      ("Popup dynamic.ppp" "dynamic.ppp" "_ppp" #f "ppp" ,action)
      ("Popup dynamic.pppp" "dynamic.pppp" "_pppp" #f "pppp" ,action)
      ("Popup dynamic.ppppp" "dynamic.ppppp" "_ppppp" #f "ppppp" ,action)
      ("Popup dynamic.clear" "dynamic.clear" "_clear" #f "clear" ,action)
      ("Popup dynamic.sp" "dynamic.sp" "_sp" #f "sp" ,action)
      ("Popup dynamic.spp" "dynamic.spp" "_spp" #f "spp" ,action)
      ("Popup dynamic.sf" "dynamic.sf" "_sf" #f "sf" ,action)
      ("Popup dynamic.sff" "dynamic.sff" "_sff" #f "sff" ,action)
      ("Popup dynamic.fp" "dynamic.fp" "_fp" #f "fp" ,action)
      ("Popup dynamic.sfz" "dynamic.sfz" "_sfz" #f "sfz" ,action)
      ("Popup dynamic.rfz" "dynamic.rfz" "_rfz" #f "rfz" ,action)

      ("Paper Dimensions" #f "_Paper Dimensions" #f "Edit Paper Dimension" ,(lambda (x) (edit-paper handler-edit)))
      ("ViewMenu"  #f "_View")
      ("Refresh" ,(gtk-stock-id 'refresh) "_Refresh" "<control>r" "Refresh Music" ,(lambda (x) (refresh o)))
      ("Zoom in" ,(gtk-stock-id 'zoom-in) "Zoom _in" "<control>plus" "Zoom in" ,(lambda (x) (scale o 1)))
      ("Zoom out" ,(gtk-stock-id 'zoom-out) "Zoom _out" "<control>minus" "Zoom out" ,(lambda (x) (scale o -1)))
      ("HelpMenu" ,(gtk-stock-id 'help) "_Help")
      ("Key bindings" #f "_Key bindings" "<control>h" "Key bindings" ,help-key-bindings)
      ("About" ,(gtk-stock-id 'about') "_About" "<control>a" "About" ,help-about))))

(define-method (menu-action (o <gnome-app>) action)
  (debugf 'app "menu-action: ~a\n" (get-name action))
  (let* ((edit (.edit o))
	 (act (string-drop-prefix (get-name action) "Popup "))
	 (act-art (string-drop-prefix act "articulation."))
	 (act-art-dyn (string-drop-prefix act-art "dynamic."))
	 (symbol (string->symbol act-art-dyn)))
    (debugf 'app "menu-action: ~a\n" act-art-dyn)
    (case symbol
      ((Cut) (emit edit 'cut))
      ((Longa) (duration-log edit -2))
      ((Breve) (duration-log edit -1))
      ((Whole) (duration-log edit 0))
      ((Half) (duration-log edit 1))
      ((Quarter) (duration-log edit 2))
      ((Eighth) (duration-log edit 3))
      ((16th) (duration-log edit 4))
      ((32nd) (duration-log edit 5))
      ((64th) (duration-log edit 6))
      ((128th) (duration-log edit 7))
      ((Sharpen) (alteration edit 1))
      ((Flatten) (alteration edit -1))
      ((8va) (octave edit 1))
      ((8vb) (octave edit -1))
      ((Dot) (dot edit))
      ((Clef) (clef-menu o))
      ((clefs.G) (clef-edit o symbol))
      ((clefs.F) (clef-edit o symbol))
      ((clefs.C) (clef-edit o symbol))
      ((clefs.C.2) (clef-edit o symbol))
      ((Any-clef) (edit-clef o))
      ((Key) (key-menu o))
      ((Flat) (gtk-grab-remove (.key-flat o)))
      ((Sharp) (gtk-grab-remove (.key-sharp o)))
      ((key-C) (key-edit o symbol))
      ((key-G) (key-edit o symbol))
      ((key-D) (key-edit o symbol))
      ((key-A) (key-edit o symbol))
      ((key-E) (key-edit o symbol))
      ((key-B) (key-edit o symbol))
      ((key-FIS) (key-edit o symbol))
      ((key-F) (key-edit o symbol))
      ((key-BES) (key-edit o symbol))
      ((key-EES) (key-edit o symbol))
      ((key-AES) (key-edit o symbol))
      ((key-DES) (key-edit o symbol))
      ((key-GES) (key-edit o symbol))
      ((Any-key) (edit-key o))

      ((Time) (time-menu o))
      ((Time-1) (gtk-grab-remove (.time-time-1 o)))
      ((Time-2) (gtk-grab-remove (.time-time-2 o)))
      ((Time-4) (gtk-grab-remove (.time-time-4 o)))
      ((Time-8) (gtk-grab-remove (.time-time-8 o)))
      ((Time-16) (gtk-grab-remove (.time-time-16 o)))
      ((time.1-1) (time o symbol))
      ((time.2-1) (time o symbol))
      ((time.3-1) (time o symbol))
      ((time.4-1) (time o symbol))
      ((time.5-1) (time o symbol))
      ((time.1-2) (time o symbol))
      ((time.C-) (time o symbol))
      ((time.2-2) (time o symbol))
      ((time.3-2) (time o symbol))
      ((time.4-2) (time o symbol))
      ((time.5-2) (time o symbol))
      ((time.6-2) (time o symbol))
      ((time.1-4) (time o symbol))
      ((time.2-4) (time o symbol))
      ((time.3-4) (time o symbol))
      ((time.C) (time o symbol))
      ((time.4-4) (time o symbol))
      ((time.5-4) (time o symbol))
      ((time.6-4) (time o symbol))
      ((time.7-4) (time o symbol))
      ((time.8-4) (time o symbol))
      ((time.9-4) (time o symbol))
      ((time.12-4) (time o symbol))
      ((time.1-8) (time o symbol))
      ((time.2-8) (time o symbol))
      ((time.3-8) (time o symbol))
      ((time.4-8) (time o symbol))
      ((time.5-8) (time o symbol))
      ((time.6-8) (time o symbol))
      ((time.7-8) (time o symbol))
      ((time.8-8) (time o symbol))
      ((time.9-8) (time o symbol))
      ((time.12-8) (time o symbol))
      ((time.1-16) (time o symbol))
      ((time.2-16) (time o symbol))
      ((time.3-16) (time o symbol))
      ((time.4-16) (time o symbol))
      ((time.5-16) (time o symbol))
      ((time.6-16) (time o symbol))
      ((time.7-16) (time o symbol))
      ((time.8-16) (time o symbol))
      ((time.9-16) (time o symbol))
      ((time.12-16) (time o symbol))
      ((Any-time) (edit-time o))

      ((Articulation) (articulation-menu o))
      ((Accent) (gtk-grab-remove (.articulation-accent o)))
      ((Finger) (gtk-grab-remove (.articulation-finger o)))
      ((String) (gtk-grab-remove (.articulation-string o)))
      ((Wind) (gtk-grab-remove (.articulation-wind o)))
      ((Sign) (gtk-grab-remove (.articulation-sign o)))
      ((Organ) (gtk-grab-remove (.articulation-organ o)))
      ((Ornament) (gtk-grab-remove (.articulation-ornament o)))

      ((clear) (articulation o symbol))
      ((staccato) (articulation o symbol))
      ((accent) (articulation o symbol))
      ((tenuto) (articulation o symbol))
      ((marcato) (articulation o symbol))
      ((portato) (articulation o symbol))
      ((espressivo) (articulation o symbol))
      ((staccatissimo) (articulation o symbol))

      ((digit-0) (articulation o symbol))
      ((digit-1) (articulation o symbol))
      ((digit-2) (articulation o symbol))
      ((digit-3) (articulation o symbol))
      ((digit-4) (articulation o symbol))
      ((digit-5) (articulation o symbol))

      ((downbow) (articulation o symbol))
      ((upbow) (articulation o symbol))
      ((flageolet) (articulation o symbol))
      ((snappizzicato) (articulation o symbol))

      ((open) (articulation o symbol))
      ((halfopen) (articulation o symbol))
      ((stopped) (articulation o symbol))

      ((fermata) (articulation o symbol))
      ((coda) (articulation o symbol))
      ((segno) (articulation o symbol))
      ((varcoda) (articulation o symbol))
      ((shortfermata) (articulation o symbol))
      ((longfermata) (articulation o symbol))
      ((verylongfermata) (articulation o symbol))
      ((signumcongruentiae) (articulation o symbol))

      ((lheel) (articulation o symbol))
      ((ltoe) (articulation o symbol))
      ((rheel) (articulation o symbol))
      ((rtoe) (articulation o symbol))

      ((trill) (articulation o symbol))
      ((prall) (articulation o symbol))
      ((turn) (articulation o symbol))
      ((mordent) (articulation o symbol))
      ((reverseturn) (articulation o symbol))
      ((upprall) (articulation o symbol))
      ((downprall) (articulation o symbol))
      ((pralldown) (articulation o symbol))
      ((prallup) (articulation o symbol))
      ((upmordent) (articulation o symbol))
      ((downmordent) (articulation o symbol))
      ((prallmordent) (articulation o symbol))
      ((prallprall) (articulation o symbol))
      ((lineprall) (articulation o symbol))

      ((Dynamic) (dynamic-menu o))
      ((Forte) (gtk-grab-remove (.dynamic-forte o)))
      ((Piano) (gtk-grab-remove (.dynamic-piano o)))
      ((FortePiano) (gtk-grab-remove (.dynamic-fortepiano o)))

      ((mf) (dynamic o symbol))
      ((f) (dynamic o symbol))
      ((ff) (dynamic o symbol))
      ((fff) (dynamic o symbol))
      ((ffff) (dynamic o symbol))
      ((fffff) (dynamic o symbol))
      ((mp) (dynamic o symbol))
      ((p) (dynamic o symbol))
      ((pp) (dynamic o symbol))
      ((ppp) (dynamic o symbol))
      ((pppp) (dynamic o symbol))
      ((ppppp) (dynamic o symbol))
      ;;((clear) (dynamic o symbol))
      ((sp) (dynamic o symbol))
      ((spp) (dynamic o symbol))
      ((sf) (dynamic o symbol))
      ((sff) (dynamic o symbol))
      ((fp) (dynamic o symbol))
      ((sfz) (dynamic o symbol))
      ((rfz) (dynamic o symbol))))
  #t)

(define-method (toggle-action (o <gnome-app>) action)
  (let ((edit (.edit o))
	(active (get-active action)))
    (case (string->symbol (get-name action))
      ((Rest) (rest edit (eq? active #nil)))
      ((Tie) (tie edit (eq? active #nil))))))

(define-method (radio-action-duration (o <gnome-app>) action current)
  (debugf 'app "radio-duration: ~a, ~a\n" (get-current-value action) current)
  (and-let* ((log (get-current-value action)))
	    (duration-log (.edit o-o) log)))

(define o-o #f) ;; FIXME: o crashes in weird ways...closure o gets eaten?
(define (radio-action-duration action current)
  (debugf 'app "radio-duration: ~a, ~a\n" (get-current-value action) current)
  (and-let* ((log (get-current-value action)))
	    (duration-log (.edit o-o) log)))

(define-method (get-offset (o <boolean>)) '(0 0))
(define-method (get-offset (o <gtk-widget>))
  (+ (offset (allocation-rect o))
     (get-offset (get-parent o))))

(define-method (popup (o <gnome-app>) menu menu-item button)
  (debugf 'popup "POPUP: ~a\n" menu)
  (popup menu (.menu-bar o) menu-item
	 ;;(lambda (w) (append (get-offset button) '(#t)))
	 #f
	 1 (gtk-get-current-event-time))
  (gtk-grab-remove menu)
  #t)

(define-method (popdown (o <gnome-app>) menu)
  (debugf 'popup "POPDOWN: ~a\n" menu)
  (popdown menu)
  (gtk-grab-remove menu)
  (gdk-pointer-ungrab (gtk-get-current-event-time))
  (gdk-keyboard-ungrab (gtk-get-current-event-time))
  #t)

(define-method (clef-menu (o <gnome-app>))
  (if (get (.clef-menu o) 'has-focus)
      (debugf 'popup "WOULD POPDOWN\n") ;;popdown o (.clef-menu o))
      (popup o (.clef-menu o) (.edit-clef-menu-item o) (.clef-button o))))

(define-method (clef-edit (o <gnome-app>) symbol)
  (popdown o (.clef-menu o))
  (clef (.edit o) symbol))

(define-method (key-menu (o <gnome-app>))
  (if (get (.key-menu o) 'has-focus)
      (debugf 'popup "WOULD POPDOWN\n") ;;popdown o (.key-menu o))
      (popup o (.key-menu o) (.edit-key-menu-item o) (.key-button o))))

(define-method (key-edit (o <gnome-app>) symbol)
  (popdown o (.key-menu o))
  (key (.edit o) symbol))

(define-method (time-menu (o <gnome-app>))
  (if (get (.time-menu o) 'has-focus)
      (debugf 'popup "WOULD POPDOWN\n") ;;popdown o (.time-menu o))
      (popup o (.time-menu o) (.edit-time-menu-item o) (.time-button o))))

(define-method (time (o <gnome-app>) symbol)
  (debugf 'popup "time: ~a\n" symbol)
  (popdown o (.time-menu o))
  (time (.edit o) (string->symbol
		   (string-regexp-substitute
		    (symbol->string symbol) "-" "/"))))

(define-method (articulation-menu (o <gnome-app>))
  (if (get (.articulation-menu o) 'has-focus)
      (debugf 'popup "WOULD POPDOWN\n") ;;popdown o (.articulation-menu o))
      (popup o (.articulation-menu o) (.edit-articulation-menu-item o) (.articulation-button o))))

(define-method (articulation (o <gnome-app>) symbol)
  (debugf 'popup "articulation: ~a\n" symbol)
  (popdown o (.articulation-menu o))
  (articulation (.edit o) symbol))

(define-method (dynamic-menu (o <gnome-app>))
  (if (get (.dynamic-menu o) 'has-focus)
      (debugf 'popup "WOULD POPDOWN\n") ;;popdown o (.dynamic-menu o))
      (popup o (.dynamic-menu o) (.edit-dynamic-menu-item o) (.dynamic-button o))))

(define-method (dynamic (o <gnome-app>) symbol)
  (debugf 'popup "dynamic: ~a\n" symbol)
  (popdown o (.dynamic-menu o))
  (dynamic (.edit o) symbol))

(define-method (edit-grob (o <gnome-app>) (music <music>))
  (if (edit music)
      (notify (.model o) music music)))

(define-method (edit-clef (o <gnome-app>))
  (edit-grob o (find (.model o) (lambda (x) (is-a-clef-glyph? x)))))

(define-method (edit-key (o <gnome-app>))
  (let ((music (find (.model o) (lambda (x) (is-a? x <key-change-event>)))))
    (if (edit-grob o music)
	(begin
	  (set! (.key (.model o)) (.scale music))
	  #t)
	#f)))

(define-method (edit-time (o <gnome-app>))
  (edit-grob o (find (.model o) (lambda (x) (is-a? x <time-signature-music>)))))

(define-method (update-toolbar-buttons (o <gnome-app>) (cursor <music>))
  (debugf 'cursor "update-toolbar-buttons: cursor: ~a @~a\n" cursor (.start cursor))
  (when (is-a-note-or-rest? cursor)
	(let* ((radios (.duration-radio o))
	       (rest? (is-a-rest? cursor))
	       (stock (if rest? "rest-" "note-")))
	  (for-each (lambda (i)
		      (set-stock-id (list-ref radios i) (string-append stock (number->string i)))) (iota 8))
	  (for-each (lambda (x) (set-sensitive x #t)) radios)
	  (set-sensitive (.dotted-button o) #t)
	  (if (not rest?)
	      (let ((free-ahead (free-ahead (.model o)))
		    (n (duration->number (.duration cursor))))
		(let loop ((i (iota 8)))
		  (if (or (null? i)
			  (>= free-ahead (/ 1 (expt 2 (car i)))))
		      #t
		      (let ((button (list-ref radios (car i))))
			(set-sensitive button #f)
			(loop (cdr i)))))
		(case (.dots (.duration cursor))
		  ((0) (if (< free-ahead (* 1.5 n))
			   (set-sensitive (.dotted-button o) #f)))
		  ((1) (if (< free-ahead (* 1.75 n))
			   (set-sensitive (.dotted-button o) #f))))))
	  (set-stock-id (.dotted-button o) (if rest? "rest-dotted" "dotted"))
	  (set-stock-id (.rest-checkbox o) (if rest? "note-2" "rest-2"))
	  (set-active (.rest-checkbox o) rest?)
	  (set-sensitive (.tie-checkbox o) (tie-possible? cursor))
	  (set-active (.tie-checkbox o) (tie-active? cursor)))))

(define-method (insert-mark-changed-handler (o <gnome-app>) (cursor <gmusic>))
  (debugf 'cursor "~a: insert-mark-changed-handler: ~a\n" (class-name (class-of o)) cursor)
  (insert-mark-changed-handler o (.music cursor)))

(define-method (insert-mark-changed-handler (o <gnome-app>) (cursor <music>))
  (debugf 'cursor "~a: insert-mark-changed-handler: ~a\n" (class-name (class-of o)) cursor)
  (set-cursor (.edit o) cursor)
  (set-selection (.tree-view o) cursor))

(define-method (set-cursor-handler (o <gnome-app>) (cursor <gmusic>))
  (debugf 'cursor "~a: set-cursor-handler: ~a\n" (class-name (class-of o)) cursor)
  (set-cursor-handler o (.music cursor)))

(define-method (set-cursor-handler (o <gnome-app>) (cursor <music>))
  (debugf 'cursor "~a: set-cursor-handler: ~a\n" (class-name (class-of o)) cursor)
  (move-insert-mark (.editor o) cursor)
  (set-selection (.tree-view o) cursor)
  (update-toolbar-buttons o cursor))

(define-method (selection-changed-handler (o <gnome-app>) (cursor <gmusic>))
  (debugf 'cursor "~a: selection-changed-handler: ~a\n" (class-name (class-of o)) cursor)
  (selection-changed-handler o (.music cursor)))

(define-method (selection-changed-handler (o <gnome-app>) (cursor <music>))
  (debugf 'cursor "~a: selection-changed-handler: ~a\n" (class-name (class-of o)) cursor)
  (set-cursor (.edit o) cursor)
  (move-insert-mark (.editor o) cursor))

(define-method (get-canvas (o <gnome-app>))
  (.canvas (.handler-edit o)))

(define-method (scale (o <gnome-app>) dir)
  (debugf 'scale "~a: scale: ~a\n" (class-name (class-of o)) dir)
  (scale (.edit o) dir))

(define-method (refresh (o <gnome-app>))
  (refresh (.edit o)))

(define (icon name)
  (gdk-pixbuf-new-from-file
   (string-append
    (get-data-dir)
    "/images/icon-" (object->string name) ".svg")))

(define-method (add-icons (o <gnome-app>))
  (let ((factory (make <gtk-icon-factory>)))
    (for-each
     (lambda (name)
       (let ((icon-set (gtk-icon-set-new-from-pixbuf (icon name))))
	 (add factory (object->string name) icon-set)))
     '(
       articulation.button
       articulation.clear
       articulation.staccato
       articulation.accent
       articulation.tenuto
       articulation.marcato
       articulation.portato
       articulation.espressivo
       articulation.staccatissimo
       
       articulation.thumb
       articulation.digit-0
       articulation.digit-1
       articulation.digit-2
       articulation.digit-3
       articulation.digit-4
       articulation.digit-5
       
       articulation.downbow
       articulation.upbow
       articulation.flageolet
       articulation.snappizzicato
       
       articulation.open
       articulation.halfopen
       articulation.stopped
       
       articulation.fermata
       articulation.coda
       articulation.segno
       articulation.varcoda
       articulation.shortfermata
       articulation.longfermata
       articulation.verylongfermata
       articulation.signumcongruentiae
       
       articulation.lheel
       articulation.ltoe
       articulation.rheel
       articulation.rtoe
       
       articulation.trill
       articulation.prall
       articulation.turn
       articulation.mordent
       articulation.reverseturn
       articulation.upprall
       articulation.downprall
       articulation.pralldown
       articulation.prallup
       articulation.upmordent
       articulation.downmordent
       articulation.prallmordent
       articulation.prallprall
       articulation.lineprall
       
       alteration--2
       alteration--1 alteration-0 alteration-1 alteration-2
       ottava.-1 ottava.1
       clefs.G clefs.F clefs.C clefs.C.2
       dotted rest-dotted

       dynamic.button
       dynamic.mp dynamic.p dynamic.pp dynamic.ppp dynamic.pppp dynamic.ppppp
       dynamic.mf dynamic.f dynamic.ff dynamic.fff dynamic.ffff dynamic.fffff
       dynamic.clear
       dynamic.sp dynamic.spp dynamic.sf dynamic.sff
       dynamic.fp dynamic.sfz dynamic.rfz

       key-C key-G key-D key-A key-E key-F key-BES key-EES key-AES
       note--2 note--1 note-0
       note-1 note-2 note-3 note-4 note-5 note-6 note-7
       rest--2 rest--1 rest-0
       rest-1 rest-2 rest-3 rest-4 rest-5 rest-6 rest-7
       tie
       time.1-1 time.2-1 time.3-1 time.4-1 time.5-1
       time.C- time.1-2 time.2-2 time.3-2 time.4-2 time.5-2 time.6-2
       time.C time.1-4 time.2-4 time.3-4 time.4-4 time.5-4 time.6-4 time.7-4
       time.8-4 time.9-4 time.12-4
       time.1-8 time.2-8 time.3-8 time.4-8 time.5-8 time.6-8 time.7-8
       time.8-8 time.9-8 time.12-8
       time.1-16 time.2-16 time.3-16 time.4-16 time.5-16 time.6-16 time.7-16
       time.8-16 time.9-16 time.12-16))
    (add-default factory)))

(define-method (toggle-actions (o <gnome-app>))
  (define (action action) (toggle-action o action))
  `(("Rest" "rest-2" "_rest" #f "Rest" ,action)
    ("Tie" "tie" "_tie" #f "Tie" ,action)))

(define-method (duration-actions (o <gnome-app>))
  `(("Longa" "note--2" "_Longa" #f "Longa" -2)
    ("Breve" "note--1" "_Breve" #f "Breve" -1)
    ("Whole" "note-0" "_Whole" "1" "Whole" 0)
    ("Half" "note-1" "_Half" "2" "Half" 1)
    ("Quarter" "note-2" "_Quarter" "4" "Quarter" 2)
    ("Eighth" "note-3" "_Eighth" "8" "Eighth" 3)
    ("16th" "note-4" "_16th" #f "16th" 4)
    ("32nd" "note-5" "_32nd" #f "32nd" 5)
    ("64th" "note-6" "_64th" #f "64th" 6)
    ("128th" "note-7" #f #f "128th" 7)))

(define-method (construct-main-window (o <gnome-app>))
  (let ((window (make <gtk-window> 
		  :type 'toplevel :title "Schikkers-List" :border-width 0))
	(actions (make <gtk-action-group> :name "Actions"))
	(ui (make <gtk-ui-manager>))
	(vbox (make <gtk-vbox> :homogeneous #f :spacing 0))
	(edit (.edit o)))

    (define (menu-item- menu sub item)
      (let ((menu-item (get-widget ui (string-append menu sub item))))
	(if (not menu-item)
	    (debugf 'popup "BARF: ~a~a~a\n" menu sub item))
	(when menu-item
	      (set menu-item 'use-stock #t)
	      (set menu-item 'always-show-image #t))))
    (define (menu-item-popup menu sub item)
      (menu-item- menu (if (string-empty? sub) "" (string-append "Popup " sub))
		  (string-append "Popup " item)))
    (define (menu-item menu popup submenu item)
      (let ((str (symbol->string item)))
	(menu-item- (string-append "/MenuBar" menu) submenu str)
	(if (not (string-empty? str))
	    (menu-item-popup popup submenu str))))
    (define (articulation-item submenu item)
      (menu-item "/EditMenu/Articulation/" "/ArticulationMenu/" submenu item))
    (define (clef-item item)
      (menu-item "/EditMenu/Clef/" "/ClefMenu/" "" item))
    (define (dynamic-item submenu item)
      (menu-item "/EditMenu/Dynamic/" "/DynamicMenu/" submenu item))
    (define (key-item submenu item)
      (menu-item "/EditMenu/Key/" "/KeyMenu/" submenu item))
    (define (time-item submenu item)
      (menu-item "/EditMenu/Time/" "/TimeMenu/" submenu item))

    (debugf 'popup "settings: ~a\n" (gtk-settings-get-default))
    (if (member 'app (debug?))
	(describe (gtk-settings-get-default)))
    (set (gtk-settings-get-default) 'gtk-button-images #t)

    (add-icons o)
    (add-actions actions (menu-actions o))
    (add-toggle-actions actions (toggle-actions o))
    (set! o-o o)
    (add-radio-actions actions (duration-actions o) 2
		       (lambda (a c) (radio-action-duration a c)))
    (insert-action-group ui actions 0)
    (add-accel-group window (get-accel-group ui))

    (catch #t
	   (lambda ()
	     (add-ui-from-string ui ui-info))
	   (lambda (key . args)
	     (case key
	       ((g-error)
		(display (format #f "building menus failed: ~A\n"
				 (caddr args))))
               (else (apply throw key args)))))

    (add window vbox)
    (pack-start vbox (get-widget ui "/MenuBar") #f #f 0)
    (if (not (stealth?))
	(pack-start vbox (get-widget ui "/ToolBar") #f #f 0))
    (if (not (stealth?))
	(pack-start vbox (get-widget ui "/NoteBar") #f #f 0))

    (set! (.duration-radio o)
	  (list (get-widget ui "/NoteBar/Duration/Whole")
		(get-widget ui "/NoteBar/Duration/Half")
		(get-widget ui "/NoteBar/Duration/Quarter")
		(get-widget ui "/NoteBar/Duration/Eighth")
		(get-widget ui "/NoteBar/Duration/16th")
		(get-widget ui "/NoteBar/Duration/32nd")
		(get-widget ui "/NoteBar/Duration/64th")
		(get-widget ui "/NoteBar/Duration/128th")))
    (set! (.rest-checkbox o) (get-widget ui "/NoteBar/Rest"))
    (set! (.tie-checkbox o) (get-widget ui "/NoteBar/Tie"))
    (set! (.dotted-button o) (get-widget ui "/NoteBar/Dot"))

    (set! (.menu-bar o) (get-widget ui "/MenuBar"))
    (set! (.edit-menu o) (get-widget ui "/MenuBar/EditMenu"))

    (add-events (get-widget ui "/NoteBar") '(button-press-mask enter-notify-mask focus-change-mask proximity-in-mask proximity-out-mask))

    (set! (.edit-clef-menu-item o) (get-widget ui "/MenuBar/EditMenu/Clef"))
    (set! (.clef-button o) (get-widget ui "/NoteBar/Popup Clef"))
    (set! (.clef-menu o) (get-widget ui "/ClefMenu"))
    (clef-item (string->symbol ""))
    (for-each clef-item '(clefs.G clefs.F clefs.C clefs.C.2))

    (set! (.edit-key-menu-item o) (get-widget ui "/MenuBar/EditMenu/Key"))
    (set! (.key-button o) (get-widget ui "/NoteBar/Popup Key"))
    (set! (.key-menu o) (get-widget ui "/KeyMenu"))
    (key-item "" (string->symbol ""))
    (set! (.key-sharp o) (get-widget ui "/KeyMenu/Popup Sharp"))
    (set! (.key-flat o) (get-widget ui "/KeyMenu/Popup Flat"))
    (for-each (lambda (x) (key-item "" x)) '(key-C Sharp Flat))
    (for-each (lambda (x) (key-item "Sharp/" x)) '(key-G key-D key-A key-E))
    (for-each (lambda (x) (key-item "Flat/" x)) '(key-F key-BES key-EES key-AES))

    (set! (.edit-time-menu-item o) (get-widget ui "/MenuBar/EditMenu/Time"))
    (set! (.time-button o) (get-widget ui "/NoteBar/Popup Time"))
    (set! (.time-menu o) (get-widget ui "/TimeMenu"))
    (set! (.time-time-1 o) (get-widget ui "/TimeMenu/Popup Time-1"))
    (set! (.time-time-2 o) (get-widget ui "/TimeMenu/Popup Time-2"))
    (set! (.time-time-4 o) (get-widget ui "/TimeMenu/Popup Time-4"))
    (set! (.time-time-8 o) (get-widget ui "/TimeMenu/Popup Time-8"))
    (set! (.time-time-16 o) (get-widget ui "/TimeMenu/Popup Time-16"))
    (time-item "" (string->symbol ""))
    (for-each (lambda (x) (time-item "" x)) '(Time-1 Time-2 Time-4 Time-8 Time-16))
    (for-each (lambda (x) (time-item "Time-1/" x))
	      '(time.1-1 time.2-1 time.3-1 time.4-1 time.5-1))
    (for-each (lambda (x) (time-item "Time-2/" x))
	      '(time.C- time.1-2 time.2-2 time.3-2 time.4-2 time.5-2 time.6-2))
    (for-each (lambda (x) (time-item "Time-4/" x))
	      '(time.C time.1-4 time.2-4 time.3-4 time.4-4 time.5-4 time.6-4 time.7-4 time.8-4 time.9-4 time.12-4))
    (for-each (lambda (x) (time-item "Time-8/" x))
	      '(time.1-8 time.2-8 time.3-8 time.4-8 time.5-8 time.6-8 time.7-8 time.8-8 time.9-8 time.12-8))
    (for-each (lambda (x) (time-item "Time-16/" x))
	      '(time.1-16 time.2-16 time.3-16 time.4-16 time.5-16 time.6-16 time.7-16 time.8-16 time.9-16 time.12-16))

    (set! (.edit-articulation-menu-item o) (get-widget ui "/MenuBar/EditMenu/Articulation"))
    (set! (.articulation-button o) (get-widget ui "/NoteBar/Popup Articulation"))
    (set! (.articulation-menu o) (get-widget ui "/ArticulationMenu"))
    (set! (.articulation-accent o) (get-widget ui "/ArticulationMenu/Popup Accent"))
    (set! (.articulation-finger o) (get-widget ui "/ArticulationMenu/Popup Finger"))
    (set! (.articulation-string o) (get-widget ui "/ArticulationMenu/Popup String"))
    (set! (.articulation-wind o) (get-widget ui "/ArticulationMenu/Popup Wind"))
    (set! (.articulation-sign o) (get-widget ui "/ArticulationMenu/Popup Sign"))
    (articulation-item "" (string->symbol ""))
    (for-each (lambda (x) (articulation-item "" x)) '(Accent Finger String Wind Sign Organ Ornament))
    (for-each (lambda (x) (articulation-item "Accent/" (symbol-append 'articulation. x)))
	      '(clear staccato accent tenuto marcato portato espressivo staccatissimo))
    (for-each (lambda (x) (articulation-item "Finger/" (symbol-append 'articulation. x)))
	      '(thumb digit-0 digit-1 digit-2 digit-3 digit-4 digit-5))
    (for-each (lambda (x) (articulation-item "String/" (symbol-append 'articulation. x)))
	      '(downbow upbow flageolet snappizzicato))
    (for-each (lambda (x) (articulation-item "Wind/" (symbol-append 'articulation. x)))
	      '(open halfopen stopped))
    (for-each (lambda (x) (articulation-item "Sign/" (symbol-append 'articulation. x)))
	      '(fermata coda segno varcoda shortfermata longfermata verylongfermata signumcongruentiae))
    (for-each (lambda (x) (articulation-item "Organ/" (symbol-append 'articulation. x)))
	      '(lheel ltoe rheel rtoe))
    (for-each (lambda (x) (articulation-item "Ornament/" (symbol-append 'articulation. x)))
	      '(trill prall turn mordent reverseturn upprall downprall pralldown prallup upmordent downmordent prallmordent prallprall lineprall))

    (set! (.edit-dynamic-menu-item o) (get-widget ui "/MenuBar/EditMenu/Dynamic"))
    (set! (.dynamic-button o) (get-widget ui "/NoteBar/Popup Dynamic"))
    (set! (.dynamic-menu o) (get-widget ui "/DynamicMenu"))
    (set! (.dynamic-forte o) (get-widget ui "/DynamicMenu/Popup Forte"))
    (set! (.dynamic-piano o) (get-widget ui "/DynamicMenu/Popup Piano"))
    (set! (.dynamic-fortepiano o) (get-widget ui "/DynamicMenu/Popup FortePiano"))
    (dynamic-item "" (string->symbol ""))
    (for-each (lambda (x) (dynamic-item "" x)) '(Forte Piano FortePiano))
    (for-each (lambda (x) (dynamic-item "Forte/" (symbol-append 'dynamic. x)))
	      '(mf f ff fff ffff fffff))
    (for-each (lambda (x) (dynamic-item "Piano/" (symbol-append 'dynamic. x)))
	      '(mp p pp ppp pppp ppppp))
    (for-each (lambda (x) (dynamic-item "FortePiano/" (symbol-append 'dynamic. x)))
	      '(clear sp spp sf sff fp sfz rfz))
    (connect (get-canvas o) 'enter-notify-event
	     (lambda (w e)
	       (popdown o (.articulation-menu o))
	       (popdown o (.clef-menu o))
	       (popdown o (.dynamic-menu o))
	       (popdown o (.key-menu o))
	       (popdown o (.time-menu o))
	       #f))
    (connect (get-widget ui "/ToolBar") 'enter-notify-event
	     (lambda (w e)
	       (popdown o (.articulation-menu o))
	       (popdown o (.clef-menu o))
	       (popdown o (.dynamic-menu o))
	       (popdown o (.key-menu o))
	       (popdown o (.time-menu o))
	       #f))
    (connect (get-widget ui "/NoteBar") 'enter-notify-event
	     (lambda (w e)
	       (popdown o (.articulation-menu o))
	       (popdown o (.clef-menu o))
	       (popdown o (.dynamic-menu o))
	       (popdown o (.key-menu o))
	       (popdown o (.time-menu o))
	       #f))

    (pack-start vbox edit #t #t 0)
    (connect window 'delete-event (lambda (w e) (file-quit o)))
    (connect window 'key-press-event (lambda (w e) (key-press-handler o w e)))
    (connect window 'configure-event (lambda (w e) (configure-handler edit e)))
    (connect edit 'default-duration (lambda (w d) (default-duration-handler o d)))
    (connect edit 'default-rest (lambda (w r) (default-rest-handler o r)))
    (sigaction SIGHUP (lambda (x) (catch-sighup o)))

    (let ((screen (get-screen window)))
      (set-default-size window WIDTH (quotient (get-height screen) 1)))
    (grab-focus (get-canvas o))
    (show-all window)
    window))

(define-method (construct-tree-view-window (o <gnome-app>))
  (let* ((window (make <gtk-window> :type 'toplevel
		       :title "Schikkers List -- Music Tree"))
	 (model (.model o))
	 (handler-edit (.handler-edit o))
	 (tree-view (make <gnome-music-tree-view> :model model)))
    (connect window 'delete-event (lambda (w e) (file-quit o)))
    (add window tree-view)
    (let ((screen (get-screen window)))
      (set-default-size window WIDTH (quotient (get-height screen) 3)))
    (show-all window)
    tree-view))

(define-method (construct-editor-window (o <gnome-app>))
  (let* ((window (make <gtk-window> :type 'toplevel
		       :title "Schikkers-List -- Editor"))
	 (model (.model o))
	 (handler-edit (.handler-edit o))
	 (view (make <gtk-text-view>))
	 (editor (make <gnome-ly-editor> :view view :model model)))
    (connect window 'delete-event (lambda (w e) (file-quit o)))
    (add window editor)
    (let ((screen (get-screen window)))
      (set-default-size window WIDTH (quotient (get-height screen) 3)))
    (show-all window)
    editor))

(define-method (load-file (o <gnome-app>) file-name . rest)
  (let ((name (if (pair? rest) (car rest) file-name)))
    (set-model (.edit o) (load-ly file-name))
    (set! (.file-name (.model o)) file-name))
  (set-title o))

(define-method (default-duration-handler (o <gnome-app>) d)
  (and-let* ((log (duration->log d)))
	    (set-active (list-ref (.duration-radio o) log) #t)))

(define-method (default-rest-handler (o <gnome-app>) rest?)
  (update-toolbar-buttons o-o (get-cursor (.model o))))

(define-method (expose-handler (o <gnome-app>) w e)
  (debugf 'app "~a: expose-handler: ~a\n" (class-name (class-of o)) e)
  #f)

(define-method (size-request-handler (o <gnome-app>) w e)
  (debugf 'app "~a: size-request: ~a\n" (class-name (class-of o)) e)
  ;;  (scale o 0)
  #f)

(define-method (app-idle (o <gnome-app>))
  (usleep SLEEP)
  #t)

(define-method (tree-selection-changed (o <gnome-app>) music)
  (debugf 'render "~a: tree-selection-changed:~a\n" (class-name (class-of o)) music)
  (and-let* ((handler-edit (.handler-edit o)))
	    (set-cursor handler-edit music)))

(define-method (insert-mark-changed (o <gnome-app>) music)
  (debugf 'render "~a: insert-mark-changed:~a\n" (class-name (class-of o)) music)
  (and-let* ((handler-edit (.handler-edit o)))
	    (set-cursor handler-edit music)))

(define-method (file-new (o <gnome-app>))
  (file-new (.handler o))
  (set-title o))

(define-method (file-new-from-template (o <gnome-app>))
  (let* ((main-window (.main-window o))
	 (current-name "FIXME")
	 (default-name (string-append (get-data-dir) "/ly/template/single-staff-one-voice.ly"))
	 (file-name (file-choose-dialog main-window "Choose Template" 'open default-name)))
    (if file-name
	(load-file o file-name "untitled.ly"))))

(define-method (file-open (o <gnome-app>))
  (let* ((main-window (.main-window o))
	 (default-name (string-append (get-data-dir) "/input/twinkle.ly"))
	 (file-name (file-choose-dialog main-window "Open File" 'open default-name)))
    (if file-name
	(load-file o file-name))))

(define-method (file-save-as (o <gnome-app>))
  (let* ((main-window (.main-window o))
	 (handler (.handler o))
	 (model (.model o))
	 (current-name "FIXME")
	 (default-name (string-append (basename (basename current-name ".ly") ".sy") ".sy"))
	 (file-name (file-choose-dialog main-window "Save as" 'save default-name)))
    (if file-name
	(let ((ly-name (string-append (basename (basename file-name ".ly") ".sy") ".ly")))
	  (set! (.file-name model) ly-name)
	  (set-title o)
	  (save-sy handler #t)
	  (save-sy handler #f)))))

;; ~/pkg/guile-config/share/guile-gnome-2/gnome/gtk/
;; ~/guile-2/share/guile-gnome-2/gnome/gtk/gdk-event.scm
(define (key-press-handler o widget event)
  (let* ((keyval (gdk-event-key:keyval event))
	 (mods (gdk-event-key:modifiers event)))
    ;; FIXME: mod2-mask
    (set! mods (filter (lambda (x) (not (equal? x 'mod2-mask))) mods))
    (debugf 'key "~a key-press-handler: ~a ~a\n" (class-name (class-of o)) mods keyval)
    #f))

(define (catch-sighup o)
  (sigaction SIGHUP SIG_DFL)
  (file-quit o))

(define-method (set-title (o <gnome-app>))
  (let* ((main-window (.main-window o))
	 (file-name (.file-name (.model o))))
    (set-title main-window (string-append "Schikkers-List -- " file-name))))

(define (main args)
  (debugf "ARGS: ~s\n" args)
  (let* ((fork (fork-lilypond-server))
	 (size (command-line-symbol-opt '--size "line"))
	 (paper (make <paper> :size size))
	 (model (get-music-model args :paper paper))
	 (handler (make <handler> :model model))
	 (canvas (make <gnome-music-canvas> :width WIDTH :height HEIGHT))
	 (engraver (make <engraver> :model model))
	 (layout (make <music-layout> :canvas canvas :engraver engraver))
	 (handler-edit (make <handler-edit> :canvas canvas :handler handler :model model))
	 (edit (make <gnome-music-edit> :model model :layout layout))
	 (app (make <gnome-app> :model model :edit edit
		    :lily-pid (car fork) :lily-stdout (cdr fork)
		    :handler-edit handler-edit :handler handler)))
    (g-idle-add (lambda () (app-idle app)))
    (gtk-main)))
