#! /bin/sh
# -*- scheme -*-
exec guile -L $(dirname $(dirname $(dirname $0))) -e test -s $0 "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2010--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define (test . args)
  (eval '(test (command-line)) (resolve-module '(schikkers gnome music-canvas))))

(define-module (schikkers gnome music-canvas)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 receive)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome canvas)
  :use-module (gnome gobject)
  :use-module (gnome gtk gdk-event)
  :use-module (gnome gtk)
  :use-module (gnome pango)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-canvas)
  :use-module (schikkers notation)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  :use-module (schikkers system)
  ;; user-gtk
  :use-module (schikkers gmisc)
  ;; user-gnome
  :use-module (schikkers gnome music-canvas-text)
  :export (<gnome-music-canvas>
	   .widget)
  :re-export (activate
	      add
	      connect
	      disconnect
	      freeze-updates
	      glyph
	      grab-focus
	      height
	      hide
	      line
	      path
	      polygon
	      rectangle
	      refresh
	      remove
	      scale
	      set-cursor
	      set-scroll-region
	      show
	      text
	      thaw-updates
	      translate
	      width))

(define-class <gnome-music-canvas> (<music-canvas>)

  (page-count :accessor .page-count :init-value 0)
  (scroll-pages :accessor .scroll-pages :init-value 0)
  (background :accessor .background :init-value #f)
  (widget :accessor .widget :init-value (make <gnome-canvas>))
  (resolution :accessor .resolution :init-value 96))

(define-method (initialize (o <gnome-music-canvas>) . initargs)
  (next-method)
  (let* ((widget (.widget o))
	 (screen (get-screen widget))
	 (resolution (get-resolution screen)))
    (debugf 'canvas "resolution:~a\n" resolution)
    (set! (.resolution o) (if (<0 resolution) 96 resolution))
    (set-center-scroll-region widget #f)
    (if (member "--evince" (command-line))
	(scale o 14.5139) ;; 25.5 ppu = 1600% evince scaling
	(scale o 12.25)
	;; (scale o 7)
	)
    (set! (.when-cursor o)
	  (apply create-cursor (cons* o (cursor-color 'when))))
    (set! (.nitem-cursor o)
	  (apply create-cursor (cons* o (cursor-color 'nitem))))
    (set! (.select-rect o)
	  (apply create-cursor (cons* o (cursor-color 'when))))
    (show-all widget)
    o))

(define (cursor-color cursor)
  (if (member "--stealth" (command-line))
      (list #xf0f0f000 0)
      (case cursor
	((when) (list #xebeef700 #x0036ff00))
	((nitem) (list #x0036ff00 #x00187200)))))

(define-method (create-cursor (o <gnome-music-canvas>) fill outline)
  (make <gnome-canvas-rect> :parent (root (.widget o))
	:width-units (1/ (pixels-per-unit o))
	:fill-color-rgba fill :outline-color-rgba outline))

(define-method (refresh (o <gnome-music-canvas>))
  (set! (.notation o) (make <notation>))
  (set! (.background o) #f)
  (set! (.pixels-per-unit o) 1)
  (set! (.widget o) (make <gnome-canvas>)))

(define-method (draw-paper-line (o <gnome-music-canvas>) x y w h)
  (make <gnome-canvas-rect> :parent (root (.widget o))
	:x1 x :y1 (+ y h) :x2 w :y2 (+ y h)
	:width-units (1/ (pixels-per-unit o))
	:fill-color "black" :outline-color "black"))

(define-method (draw-paper-full (o <gnome-music-canvas>) x y w h)
  (make <gnome-canvas-rect> :parent (root (.widget o))
	:x1 x :y1 y :x2 w :y2 (+ y h)
	:width-units (1/ (pixels-per-unit o))
	:fill-color-rgba (if (and (not (member "--debug" (command-line)))
				  (not (member "--stealth" (command-line))))
			     ;;#xfcf6e800 ;; middle
			     #xfcf7ee00  ;; whiter
			     #xffffff00)
	:outline-color "black"))

(define-method (draw-paper (o <gnome-music-canvas>) x y w h)
  (let ((background (draw-paper-full o x y w h)))
    ;;(connect background 'button-press-event (lambda (w e) (paper-button-press o background e)))
    ;;(connect background 'motion-notify-event (lambda (w e) (paper-motion-notify o background e)))
    ;;(connect background 'button-release-event (lambda (w e) (paper-button-release o background e)))
    (connect background 'event (lambda (w e) (paper-event o background e)))
    (lower-to-bottom background)
    background))

(define-method (set-scroll-region (o <gnome-music-canvas>) (paper <paper>) page-count)
  (if (!= page-count (.scroll-pages o))
      (let* ((ppu (pixels-per-unit o))
	     (w (* ppu (paper-width o)))
	     (h (* ppu (paper-height o)))
	     (widget (.widget o)))
	(set! (.scroll-pages o) page-count)
	(set-scroll-region widget 0 0 w h))))

(define-method (draw-background (o <gnome-music-canvas>))
  (let* ((w (paper-width o))
	 (h (paper-height o))
	 (page-count (page-count (.notation o)))
	 (canvas-height (* page-count h)))
    (set! (.background o)
	  (let loop ((y (* (.page-count o) h)))
	    (if (>= y canvas-height) (list (.background o))
		(cons (draw-paper o 0 y w h) (loop (+ y h))))))
    (set! (.page-count o) page-count)))

(define-method (set-paper (o <gnome-music-canvas>) (paper <paper>))
  (set-paper (.notation o) paper)
  (draw-background o))

(define-method (text-items (o <gnome-music-canvas>))
  (filter-map (lambda (x) (and-let* ((c (.canvas-item x)))
				    (and (is-a? c <gnome-music-canvas-text>) c)))
	  (.items (.notation o))))

(define-method (text-items (o <system>))
  (filter-map (lambda (x) (and-let* ((c (.canvas-item x)))
				    (and (is-a? c <gnome-music-canvas-text>) c)))
	      (.items o)))

(define-method (scale-text-items (o <gnome-music-canvas>))
  (let ((ppu (.pixels-per-unit o))
	(res (.resolution o)))
    (for-each (lambda (x) (scale x res ppu)) (text-items o))))

(define-method (expose-handler (o <gnome-music-canvas>) event)
  (debugf 'canvas "~a expose-handler event: ~a\n" (class-name (class-of o)) event)
  #f)


;;; gtk-widget wrappers

(define-method (add (w <gtk-widget>) (o <gnome-music-canvas>)) (add w (.widget o)))
(define-method (connect (o <gnome-music-canvas>) (signal <symbol>) (func <procedure>))
  (connect (.widget o) signal func))
(define-method (disconnect (o <gnome-music-canvas>) id) (disconnect (.widget o) id))
(define-method (grab-focus (o <gnome-music-canvas>)) (grab-focus (.widget o)))
(define-method (freeze-updates (o <gnome-music-canvas>)) (freeze-updates (get-window (.widget o))))
(define-method (height (o <gnome-music-canvas>)) (height (.widget o)))
(define-method (hide (o <gnome-music-canvas>)) (hide (.widget o)))
(define-method (remove (w <gtk-container>) (o <gnome-music-canvas>))
  (remove w (.widget o)))
(define-method (show (o <gnome-music-canvas>)) (show (.widget o)))
(define-method (thaw-updates (o <gnome-music-canvas>)) (thaw-updates (get-window (.widget o))))
(define-method (width (o <gnome-music-canvas>)) (width (.widget o)))


;;; FIXME: missing generics
(define-method (closepath (o <gnome-canvas-path-def>))
  (gnome-canvas-path-def-closepath o))
(define-method (curveto (o <gnome-canvas-path-def>) x1 y1 x2 y2 x3 y3)
  (gnome-canvas-path-def-curveto o x1 y1 x2 y2 x3 y3))
(define-method (lineto (o <gnome-canvas-path-def>) x y)
  (gnome-canvas-path-def-lineto o x y))
(define-method (moveto (o <gnome-canvas-path-def>) x y)
  (gnome-canvas-path-def-moveto o x y))
(define-method (reset (o <gnome-canvas-path-def>))
  (gnome-canvas-path-def-reset o))


;;; Stencil primitives
(define-method (line (o <notation-item-line>) (canvas <gnome-music-canvas>) color thick x1 y1 x2 y2)
  (debugf 'canvas "draw line: ~a ~a ~a ~a\n" x1 y1 x2 y2)
  ;; FIXME: missing generics
  ;;  (let* ((def (make <gnome-canvas-path-def> :value #f))
  (let* ((def (gnome-canvas-path-def-new))
	 (path (make <gnome-canvas-bpath>
		  :parent (root (.widget canvas))
		  :fill-color color
		  :outline-color color
		  :width-units thick)))
    (reset def)
    (moveto def x1 (- y1))
    (lineto def x2 (- y2))
    (set-path-def path def)
    path))

(define-method (path (o <notation-item-path>) (canvas <gnome-music-canvas>) color path thick filled? cap join)
  (let* ((def (gnome-canvas-path-def-new))
	 ;; FIXME: missing generics
	 ;;  (let* ((def (make <gnome-canvas-path-def>))
	 (bpath (make <gnome-canvas-bpath>
		  :parent (root (.widget canvas))
		  :fill-color (if filled? color "white")
		  :outline-color color
		  :join-style join
		  :cap-style cap
		  :width-units thick))
	 (command-alist `((curveto . ,curveto)
			  (moveto . ,moveto)
			  (lineto . ,lineto)
			  (closepath . ,closepath)))
	 (relative-alist `((rmoveto . ,moveto)
			  (rcurveto . ,curveto)
			  (rlineto . ,lineto))))
    (reset def)
    (let loop ((path path) (point '(0 0)))
      (if (null? path)
	  '()
	  (let* ((head (car path))
		 (rest (cdr path))
		 (arity (cond ((memq head '(rmoveto rlineto lineto moveto)) 2)
			      ((memq head '(rcurveto curveto)) 6)
			      ((eq? head 'closepath) 0)
			      (else 1)))
		 (args (take rest arity))
		 (points (map flip-y (list->points args)))
		 (command (assoc-get-default command-alist head #f))
		 (relative (assoc-get-default relative-alist head #f))
		 (canvas-head (or command relative)))
	    (if relative (apply moveto (cons* def point)))
	    (apply canvas-head (cons* def (points->list points)))
	    (loop (drop rest arity) (if (=0 arity) point (car (last-pair points)))))))
    (set-path-def bpath def)
    bpath))

(define-method (polygon (o <notation-item-polygon>) (canvas <gnome-music-canvas>) color coords blot-diameter filled?)
  ;; FIXME: missing generics
  ;;  (let* ((def (make <gnome-canvas-path-def>))
  (let* ((def (gnome-canvas-path-def-new))
	 (path (make <gnome-canvas-bpath>
		  :parent (root (.widget canvas))
		  :fill-color (if filled? color "white")
		  :outline-color color
		  :join-style 'round
		  :width-units blot-diameter))
	 (points (map flip-y (list->points coords)))
	 (last-point (car (last-pair points))))
    (reset def)
    (apply moveto (cons* def last-point))
    (for-each (lambda (x) (apply lineto (cons* def x))) points)
    (closepath def)
    (set-path-def path def)
    path))

(define-method (rectangle (o <notation-item-rectangle>) (canvas <gnome-music-canvas>) color breapth width depth height blot-diameter)
  (let ((b (/ blot-diameter 2)))
    (make <gnome-canvas-rect>
      :parent (root (.widget canvas))
      :x1 (- b breapth) :y1 (- depth b) :x2 (- width b) :y2 (- b height)
      :fill-color color
      :outline-color color
      :width-units blot-diameter
      :join-style 'round)))

(define-method (text (o <notation-item>) (canvas <gnome-music-canvas>) color font size text)
  (scale
   (make <gnome-music-canvas-text> :parent (root (.widget canvas)) :canvas canvas
	 :font font :text text :fill-color color)
   (.resolution canvas) (.pixels-per-unit canvas)))


;;; Drawing

(define-method (clear (o <gnome-music-canvas>) (system <system>))
  (debugf 'canvas "~a: clear system:~a\n" (class-name (class-of o)) system)
  (for-each (lambda (x) (scale x 1 0)) (text-items system))
  (for-each destroy-canvas-item (.items system)))

(define-method (scale (o <gnome-music-canvas>) step)
  (let* ((factor (expt 1.25 step))
	 (ppu (* (.pixels-per-unit o) factor))
	 (res (.resolution o)))
    (debugf 'canvas "~a: scale step: ~a->~a \n" (class-name (class-of o)) step ppu)
    (set! (.pixels-per-unit o) ppu)
    (set-pixels-per-unit (.widget o) ppu)
    (scale-text-items o)))

(define-method (draw-staff (o <gnome-music-canvas>) (system <system>))
;;  (and-let* ((rect (paper-staff-rect system (.paper (.notation o))))
  (and-let* ((rect (paper-system-rect system (.paper (.notation o))))
	     (citem (make <gnome-canvas-rect> :parent (root (.widget o))
			  :width-units (1/ (pixels-per-unit o))
			  :fill-color-rgba #xfff0f000 :outline-color-rgba #xff000000)))
	    (debugf 'canvas "draw staff: ~a\n" rect)
	    (set! (.canvas-item system) citem)
	    (set-cursor citem rect)))
     
(define-method (set-cursor (o <gnome-music-canvas>) (nitem <notation-item>))
  (let* ((notation (.notation o))
	 (paper (.paper notation))
	 (system (get-system notation (.system nitem))))
    (set-cursor (.when-cursor o) (paper-when-rect system nitem paper))
    (set-cursor (.nitem-cursor o) (paper-rect nitem paper))))

(define-method (set-cursor (o <gnome-canvas-rect>) (cursor <rect>))
  (debugf 'translate "set-cursor \n")
  (pair-for-each (lambda (s c) (set o (car s) (car c)))
		 '(x1 y1 x2 y2) (rect->bbox cursor)))

(define-method (activate (o <gnome-music-canvas>) (nitem <notation-item>))
  (connect (.canvas-item nitem) 'event (lambda (w e) (item-event o nitem w e)))
  nitem)

(define-method (translate (o <gnome-music-canvas>) (citem <gnome-canvas-item>)
			  origin offset)
  (apply move (cons* citem (+ origin (flip-y offset))))
  citem)


;;; Events

(define (paper-event o paper event)
  (case (gdk-event:type event)
    ((button-press) (paper-button-press o paper event))
    ((button-release) (paper-button-release o paper event))
    ((motion-notify) (paper-motion-notify o paper event))
    (else #f)))

(define (paper-button-press o paper event)
  (and-let* ((button (gdk-event-button:button event))
	     ((=1 button))
	     ;;(mods (gdk-event-button:modifiers event))
	     ;;((null? (filter (lambda (x) (not (equal? x 'mod2-mask))) mods)))
	     ;; (point (list (gdk-event-button:x event) (gdk-event-button:y event)))
	     ;;(x (x point))
	     (x (gdk-event-button:x event))
	     (y (get (.when-cursor o) 'y1))
	     (h (- (get (.when-cursor o) 'y2) y))
	     (point (list x y)))
	    (set! (.drag-origin o) point)
	    (set-cursor (.select-rect o) (list->vector (list x y 0 h)))
	    #t))

(define (paper-motion-notify o paper event)
  (and-let* ((origin (.drag-origin o))
	     (x1 (get (.select-rect o) 'x1))
	     (x (gdk-event-motion:x event))
	     (y1 (get (.select-rect o) 'y1))
	     (h (- (get (.select-rect o) 'y2) y1))
	     (w (- x x1)))
	    (set-cursor (.select-rect o) (list->vector (list x1 y1 w h)))
	    #t))

(define (paper-button-release o paper event)
  (and-let* ((origin (.drag-origin o))
	     (button (gdk-event-button:button event))
	     ((=1 button))
	     ;;(mods (gdk-event-button:modifiers event))
	     ;;((null? (filter (lambda (x) (not (equal? x 'mod2-mask))) mods)))
	     (point (list (gdk-event-button:x event) (gdk-event-button:y event))))
	    (debugf 'canvas "paper drag end: ~a\n" (- point (.drag-origin o)))
	    (set-cursor (.select-rect o) #(0 0 0 0))
	    (set! (.drag-origin o) #f)
	    #t))

(define (item-event o nitem item event)
  (debugf 'canvas "~a item-event event ~a\n" (class-name (class-of o)) (gdk-event:type event))
  (case (gdk-event:type event)
    ((enter-notify) (set item 'fill-color (notify-color)))
    ((leave-notify)
     (set item 'fill-color
	   (if (is-a? item <gnome-music-canvas-text>) (text-color) (color))))
    ((motion-notify)
     (debugf 'canvas "~a item-event dragging? ~a\n" (class-name (class-of o)) (gdk-event:type event)))
    ((button-release)
     (debugf 'canvas "~a item-event end dragging? ~a\n" (class-name (class-of o)) (gdk-event:type event)))
    ((button-press)
     (let ((button (gdk-event-button:button event)))
       (debugf 'canvas "~a item-event button: ~a\n" (class-name (class-of o)) button)
       (cond
	((= button 1)
	 (if (or (null? (gdk-event-button:modifiers event))
		 ;; FIXME: MOD2
		 (equal? (gdk-event-button:modifiers event) '(mod2-mask)))
	     (debugf 'canvas "~a item-event start dragging? ~a\n" (class-name (class-of o)) (gdk-event:type event))
	     (debugf 'canvas "~a item-event modifiers: ~a\n" (class-name (class-of o)) (gdk-event-button:modifiers event)))))))
    ((2button-press)
     (debugf 'canvas "~a item-event 2button-pressmodifiers: ~a\n" (class-name (class-of o)) (gdk-event-button:modifiers event))
     (set item 'fill-color "green")
     (and-let* ((n nitem)
		(music (.music nitem)))
	       (debugf 'canvas "MUSIC: ~S\n" (lisp-value music))
	       ((.edit-grob-callback o) music)))
    ((key-press)
     ;;(debugf 'canvas "~a item-event tweak: ~a\n" (class-name (class-of o)) (gdk-event-key-press:modifiers event))))
     (debugf 'canvas "~a item-event tweak: ~a\n" (class-name (class-of o)) event)))
  #f)


;;; Test

(define (test . args)
  (let* ((main-window (make <gtk-window>
			:title "Music Canvas Demo"
			:type 'toplevel))
	 (music (test-music))
	 (canvas (make <gnome-music-canvas>))
	 (scrolled (make <gtk-scrolled-window>))
	 (system ((@ (schikkers test mock-system) mock-system)))
	 (vbox (make <gtk-vbox>))
	 (actions (make <gtk-action-group> :name "Actions"))
	 (ui (make <gtk-ui-manager>)))
    (set-paper canvas (make <paper>))
    (add main-window vbox)

    (add-actions actions
		 `(("ViewMenu"  #f "_View")
		   ("Zoom in" ,(gtk-stock-id 'zoom-in) "Zoom _in" "plus" "Zoom in" ,(lambda (x) (scale canvas 1)))
		   ("Zoom out" ,(gtk-stock-id 'zoom-out) "Zoom _out" "minus" "Zoom out" ,(lambda (x) (scale canvas -1)))))
    (insert-action-group ui actions 0)
    (add-ui-from-string ui "<ui><toolbar name='ToolBar'><toolitem action='Zoom in'/><toolitem action='Zoom out'/></toolbar></ui>")
    (pack-start vbox (get-widget ui "/ToolBar") #f #f 0)
    (add-accel-group main-window (get-accel-group ui))

    (add scrolled canvas)
    (pack-start vbox scrolled #t #t 0)
    (set-default-size main-window 600 400)
    (connect main-window 'delete-event (lambda (w e) (gtk-main-quit) #f))
    (draw canvas system)
    (show-all main-window))
  (gtk-main))
