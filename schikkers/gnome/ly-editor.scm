#! /bin/sh
# -*- scheme -*-
exec guile -L $(dirname $(dirname $(dirname $0))) -e test -s $0 "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define (test . args)
  (eval '(test (command-line)) (resolve-module '(schikkers gnome ly-editor))))

(define-module (schikkers gnome ly-editor)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gtk)
  :use-module (gnome gw gtk)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers paper)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers ly-editor)
  :use-module (schikkers music-model)
  ;; user-gnome
  :export (<gnome-ly-editor>)
  :re-export (add
	      connect
	      pack-start
	      show
	      show-all))

(define-class <gnome-ly-editor> (<ly-editor>)
  (widget :accessor .widget :init-value (make <gtk-scrolled-window>)))

(define-method (initialize (o <gnome-ly-editor>) . initargs)
  (next-method)
  (set-policy o 'always 'always)

  (let ((view (.view o)))
    (set view 'editable #t)
    (set view 'cursor-visible #t)
    (set view 'wrap-mode 'word)
    (set view 'pixels-above-lines 1)
    (set view 'pixels-below-lines 1)
    (add o view))
  
  (show-all o)
  o)


;;; Gtk-widget wrappers
(define-method (add (w <gtk-widget>) (o <gnome-ly-editor>)) (add w (.widget o)))
(define-method (remove (w <gtk-container>) (o <gnome-ly-editor>)) (remove w (.widget o)))
(define-method (pack-start (w <gtk-container>) (o <gnome-ly-editor>) . rest) (apply pack-start (cons* w (.widget o) rest)))

(define-method (add (o <gnome-ly-editor>) (c <gtk-text-view>)) (add (.widget o) c))
(define-method (show-all (o <gnome-ly-editor>)) (show-all (.widget o)))
(define-method (disconnect (o <gnome-ly-editor>) id) (disconnect (.widget o) id))
(define-method (get-hadjustment (o <gnome-ly-editor>)) (get-hadjustment (.widget o)))
(define-method (get-vadjustment (o <gnome-ly-editor>)) (get-vadjustment (.widget o)))
(define-method (grab-focus (o <gnome-ly-editor>)) (grab-focus (.widget o)))
(define-method (freeze-updates (o <gnome-ly-editor>)) (freeze-updates (get-window (.widget o))))
(define-method (height (o <gnome-ly-editor>)) (height (.widget o)))
(define-method (hide (o <gnome-ly-editor>)) (hide (.widget o)))
(define-method (set-policy (o <gnome-ly-editor>) . rest) (apply set-policy (cons* (.widget o) rest)))
(define-method (show (o <gnome-ly-editor>)) (show (.widget o)))
(define-method (thaw-updates (o <gnome-ly-editor>)) (thaw-updates (get-window (.widget o))))
(define-method (width (o <gnome-ly-editor>)) (width (.widget o)))


;;;; Test
(define (test . args)
  (let* ((main-window (make <gtk-window>
			:title "Ly Editor Demo"
			:type 'toplevel))
	 (model (get-music-model args))
	 (view (make <gtk-text-view>))
	 (editor (make <gnome-ly-editor> :view view :model model)))
    (add main-window editor)
    (set-default-size main-window 600 400)
    (connect main-window 'delete-event (lambda (w e) (gtk-main-quit) #f))
    (show-all main-window))
  (gtk-main))
