#! /bin/sh
# -*- scheme -*-
exec guile -L $(dirname $(dirname $(dirname $0))) -e test -s $0 "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; it under the terms of the GNU Affero General Public License as

;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define (test . args)
  (eval '(test (command-line)) (resolve-module '(schikkers gnome music-tree-view))))

(define-module (schikkers gnome music-tree-view)
  ;; base
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gobject)
  :use-module (gnome glib)
  :use-module (gnome gtk)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-model)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :use-module (schikkers music-tree-view)
  :export (<gnome-music-tree-view>))

(define-class <gnome-music-tree-view> (<music-tree-view>)
  (widget :accessor .widget :init-value (make <gtk-scrolled-window>)))

(define-method (initialize (o <gnome-music-tree-view>) . initargs)
  (apply next-method (list o (cons* :view (make <gtk-tree-view>) (car initargs))))
  (let* ((view (.view o))
	 (cell (make <gtk-cell-renderer-text>))
	 (column-type (make <gtk-tree-view-column> :title "Type"))
	 (column-contents (make <gtk-tree-view-column> :title "Ly")))
      (pack-start column-type cell #t)
      (add-attribute column-type cell "text" 0)
      (append-column view column-type)
      (pack-start column-contents cell #f)
      (add-attribute column-contents cell "text" 1)
      (append-column view column-contents)
      (add (.widget o) view))
  o)


;;;; Gtk-widget wrappers
(define-method (add (w <gtk-widget>) (o <gnome-music-tree-view>)) (add w (.widget o)))


;;;; Test

(define (test . args)
  (let* ((main-window (make <gtk-window>
			:title "Music Tree Demo"
			:type 'toplevel))
	 (model (get-music-model args))
	 (view (make <gnome-music-tree-view> :model model)))
    (add main-window view)
    (set-default-size main-window 600 400)
    (connect main-window 'delete-event (lambda (w e) (gtk-main-quit) #f))
    (show-all main-window))
  (gtk-main))
