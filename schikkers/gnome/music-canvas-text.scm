;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers gnome music-canvas-text)
  ;; base
  ;; oop goops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome canvas)
  :use-module (gnome gobject)
  :use-module (gnome gtk)
  ;; user
  :use-module (schikkers font)
  :use-module (schikkers misc)
  ;; user-gtk
  :export (<gnome-music-canvas-text>)
  :re-export (destroy
	      scale))

(define-class <gnome-music-canvas-text> (<gnome-canvas-text>)
  (canvas :accessor .canvas :init-keyword :canvas :init-value #f)
  (design-size :accessor .design-size :init-value 1)
  (emmentaler? :accessor .emmentaler? :init-value #f))

(define-method (initialize (o <gnome-music-canvas-text>) . initargs)
  (debugf 'canvas "~a: create: ~a\n" (class-name (class-of o)) o)
  (next-method)
  (let ((font (slot-ref o 'font)))
    (debugf 'canvas-text "initialize setting design-size: ~a\n" (design-size font))
    (set! (.design-size o) (design-size font))
    (set! (.emmentaler? o) (emmentaler? font))
    (slot-set! o 'y-offset (if (.emmentaler? o) (/ 28 75) (/ -23 40)))
    (slot-set! o 'x-offset 0.007)
    (slot-set! o 'anchor 'west)
    (slot-set! o 'size-set #t)))

(define-method (destroy (o <boolean>)))

(define-method (destroy (o <gnome-canvas-item>))
  (debugf 'canvas "~a: destroy: ~a\n" (class-name (class-of o)) o)
  (gtype-instance-destroy! o))

(define-method (destroy (o <gnome-music-canvas-text>))
  (debugf 'canvas "~a: destroy: ~a\n" (class-name (class-of o)) o)
  (next-method)
  (gtype-instance-destroy! o))

(define-method (scale (o <gnome-music-canvas-text>) resolution pixels-per-unit)
  (debugf 'canvas-text "~a: scale ~a: ~a\n" (class-name (class-of o)) o pixels-per-unit)
  (let* ((magic (* (if (.emmentaler? o) (/ 43 3) (/ 94 9)) (/ 1 resolution)))
	 (size-points (* pixels-per-unit magic (.design-size o))))
    (slot-set! o 'size-points 0)
    (slot-set! o 'size-points size-points))
  o)
