#! /bin/sh
# -*- scheme -*-
exec guile-gnome-2 -l $0 -e test "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2010--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define (test . args)
  (set! %load-path (cons "." %load-path))
  (format #t "test-music:~a\n" (eval '(lisp-value (test-music)) (resolve-module '(schikkers gnome music-edit-dialogs)))))

(define-module (schikkers gnome music-edit-dialogs)
  ;; base
  :use-module (ice-9 receive)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gobject)
  :use-module (gnome gtk)
  ;; user
  :use-module (schikkers music)
  :use-module (schikkers music-modify)
  :use-module (schikkers paper)
  :use-module (schikkers misc)
  :export (edit
	   edit-clef))

(define-method (edit (o <music>))
  (stderr "sorry, cannot edit ~a yet\n" o)
  #f)

(define-method (edit (o <property-set>))
  (if (is-a-clef-glyph? o)
      (let* ((music (.parent (.parent o)))
	     (position (find music (lambda (x) (is-a-clef-position? x))))
	     (middle-c (find music (lambda (x) (is-a-clef-middle-c? x)))))
	(edit-clef o position middle-c))))

(define-method (edit-clef (o <property-set>) op om)
  (let ((glyph (.value o))
	(position (if op (.value op) #f))
	(middle-c (if om (.value om) #f))
	(changed #f)
	(dialog (make <gtk-dialog>
		  :title "Edit Clef" 
		  :modal #t :destroy-with-parent #t))
	(hbox   (make <gtk-hbox> :homogeneous #f :spacing 8))
	(stock  (make <gtk-image> 
		  :stock (gtk-stock-id 'dialog-question)
		  :icon-size (genum->value 
			       (make <gtk-icon-size> :value 'dialog))))
	(table  (make <gtk-table> :n-columns 2 :n-rows 2 :homogeneous #f))
	(label1 (make <gtk-label> :label "_Glyph" :use-underline #t))
	(entry1 (make <gtk-entry>))
	(label2 (make <gtk-label> :label "_Position" :use-underline #t))
	(entry2 (make <gtk-entry>))
	(label3 (make <gtk-label> :label "_Middle C" :use-underline #t))
	(entry3 (make <gtk-entry>)))
    (add-button dialog 
		(gtk-stock-id 'cancel)
		(genum->value 
		 (make <gtk-response-type> :value 'cancel)))
    (add-button dialog 
		(gtk-stock-id 'ok)
		(genum->value 
		 (make <gtk-response-type> :value 'ok)))
		      
    (set-border-width hbox 8)
    (pack-start (get-vbox dialog) hbox #f #f 0)

    (pack-start hbox stock #f #f 0)

    (set-row-spacings table 4)
    (set-col-spacings table 4)
    (pack-start hbox table #t #t 0)

    (attach-defaults table label1 0 1 0 1)
    (set-text entry1 glyph)
    (attach-defaults table entry1 1 2 0 1)
    (set-mnemonic-widget label1 entry1)

    (attach-defaults table label2 0 1 1 2)
    (set-text entry2 (if position (number->string position) ""))
    (attach-defaults table entry2 1 2 1 2)
    (set-mnemonic-widget label2 entry2)

    (attach-defaults table label3 0 1 2 3)
    (set-text entry3 (if middle-c (number->string middle-c) ""))
    (attach-defaults table entry3 1 2 2 3)
    (set-mnemonic-widget label3 entry3)

    (show-all hbox)

    (if (eq? 'ok (genum->symbol (make <gtk-response-type> 
				  :value (run dialog))))
	(let ((g (get-text entry1))
	      (p (string->number (get-text entry2)))
	      (m (string->number (get-text entry3))))
	  (stderr "OK clicked\n")
	  (when (set-clef o g p m)
	    (format #t "Clef: ~a ~a ~a\n~" g p m)
	    (set! changed #t))))
    (destroy dialog)
    changed))

(define-method (edit (o <time-signature-music>))
  (let ((numerator (.numerator o))
	(denominator (.denominator o))
	(style (get-style o))
	(changed #f)
	(dialog (make <gtk-dialog>
		  :title "Edit TimeSignature" 
		  :modal #t :destroy-with-parent #t))
	(hbox   (make <gtk-hbox> :homogeneous #f :spacing 8))
	(stock  (make <gtk-image> 
		  :stock (gtk-stock-id 'dialog-question)
		  :icon-size (genum->value 
			       (make <gtk-icon-size> :value 'dialog))))
	(table  (make <gtk-table> :n-columns 2 :n-rows 2 :homogeneous #f))
	(label1 (make <gtk-label> :label "_Numerator" :use-underline #t))
	(entry1 (make <gtk-entry>))
	(label2 (make <gtk-label> :label "_Denominator" :use-underline #t))
	(entry2 (make <gtk-entry>))
	(label3 (make <gtk-label> :label "_Style" :use-underline #t))
	(entry3 (make <gtk-entry>)))
    (add-button dialog 
		(gtk-stock-id 'cancel)
		(genum->value 
		 (make <gtk-response-type> :value 'cancel)))
    (add-button dialog 
		(gtk-stock-id 'ok)
		(genum->value 
		 (make <gtk-response-type> :value 'ok)))
		      
    (set-border-width hbox 8)
    (pack-start (get-vbox dialog) hbox #f #f 0)

    (pack-start hbox stock #f #f 0)

    (set-row-spacings table 4)
    (set-col-spacings table 4)
    (pack-start hbox table #t #t 0)

    (attach-defaults table label1 0 1 0 1)
    (set-text entry1 (number->string numerator))
    (attach-defaults table entry1 1 2 0 1)
    (set-mnemonic-widget label1 entry1)

    (attach-defaults table label2 0 1 1 2)
    (set-text entry2 (number->string denominator))
    (attach-defaults table entry2 1 2 1 2)
    (set-mnemonic-widget label2 entry2)

    (attach-defaults table label3 0 1 2 3)
    (set-text entry3 (symbol->string style))
    (attach-defaults table entry3 1 2 2 3)
    (set-mnemonic-widget label3 entry3)

    (show-all hbox)

    (if (eq? 'ok (genum->symbol (make <gtk-response-type> 
				  :value (run dialog))))
	(let ((n (string->number (get-text entry1)))
	      (d (string->number (get-text entry2)))
	      (s (string->symbol (get-text entry3))))
	  (stderr "OK clicked\n")
	  (when (set-time o n d (eq? s 'numeric))
	    (format #t "Time: ~a/~a (~a)\n~" n d s)
	    (set! changed #t))))
    (destroy dialog)
    changed))

(define-method (edit (o <key-change-event>))
  (let ((scale (.scale o))
	(tonic (.step (.tonic o)))
	(alteration (.alteration (.tonic o)))
	(changed #f)
	(dialog (make <gtk-dialog>
		  :title "Edit KeySignature" 
		  :modal #t :destroy-with-parent #t))
	(hbox   (make <gtk-hbox> :homogeneous #f :spacing 8))
	(stock  (make <gtk-image>
		  :stock (gtk-stock-id 'dialog-question)
		  :icon-size (genum->value 
			       (make <gtk-icon-size> :value 'dialog))))
	(table  (make <gtk-table> :n-columns 2 :n-rows 2 :homogeneous #f))
	(label1 (make <gtk-label> :label "_Tonic" :use-underline #t))
	(entry1 (make <gtk-entry>))
	(label2 (make <gtk-label> :label "_Alteration" :use-underline #t))
	(entry2 (make <gtk-entry>))
	(label3 (make <gtk-label> :label "_Scale" :use-underline #t))
	(entry3 (make <gtk-entry>)))
    (add-button dialog 
		(gtk-stock-id 'cancel)
		(genum->value 
		 (make <gtk-response-type> :value 'cancel)))
    (add-button dialog 
		(gtk-stock-id 'ok)
		(genum->value 
		 (make <gtk-response-type> :value 'ok)))
		      
    (set-border-width hbox 8)
    (pack-start (get-vbox dialog) hbox #f #f 0)

    (pack-start hbox stock #f #f 0)

    (set-row-spacings table 4)
    (set-col-spacings table 4)
    (pack-start hbox table #t #t 0)

    (attach-defaults table label1 0 1 0 1)
    (set-text entry1 (number->string tonic))
    (attach-defaults table entry1 1 2 0 1)
    (set-mnemonic-widget label1 entry1)

    (attach-defaults table label2 0 1 1 2)
    (set-text entry2 (number->string alteration))
    (attach-defaults table entry2 1 2 1 2)
    (set-mnemonic-widget label2 entry2)

    (attach-defaults table label3 0 1 2 3)
    (set-text entry3 (string-join (map number->string scale) " "))
    (attach-defaults table entry3 1 2 2 3)
    (set-mnemonic-widget label3 entry3)

    (show-all hbox)

    (if (eq? 'ok (genum->symbol (make <gtk-response-type> 
				  :value (run dialog))))
	(let ((t (string->number (get-text entry1)))
	      (a (string->number (get-text entry2)))
	      (s (map string->number (string-split (get-text entry3) #\ ))))
	  (stderr "OK clicked\n")
	  (when (or (and t (!= t tonic))
		    (and a (!= a alteration))
		    (and s (not (equal? s scale))))
	    (set! (.step (.tonic o)) t)
	    (set! (.alteration (.tonic o)) a)
	    (set! (.scale o) s)
	    (format #t "Key: ~a.~a: ~a\n~" t a s)
	    (set! changed #t))))
    (destroy dialog)
    changed))

(define mm 1)
(define in 25.4)

;; from lily -- partial list
(define-public paper-alist

  ;; don't use decimals.
  ;; ISO 216 has a tolerance of +- 2mm

  `(("a10" . ,(cons (* 26 mm) (* 37 mm)))
    ("a9" . ,(cons (* 37 mm) (* 52 mm)))
    ("a8" . ,(cons (* 52 mm) (* 74 mm)))
    ("a7" . ,(cons (* 74 mm) (* 105 mm)))
    ("a6" . ,(cons (* 105 mm) (* 148 mm)))
    ("a5" . ,(cons (* 148 mm) (* 210 mm)))
    ("a4" . ,(cons (* 210 mm) (* 297 mm)))
    ("a3" . ,(cons (* 297 mm) (* 420 mm)))
    ("a2" . ,(cons (* 420 mm) (* 594 mm)))
    ("a1" . ,(cons (* 594 mm) (* 841 mm)))
    ("a0" . ,(cons (* 841 mm) (* 1189 mm)))
    ("legal" . ,(cons (* 8.5 in) (* 14.0 in)))
    ("letter" . ,(cons (* 8.5 in) (* 11.0 in)))))

(define (paper-size-from-height height)
  (let loop ((alist paper-alist))
    (if (not (pair? alist))
	"a6"
	(if (= (cddar alist) height)
	      (caar alist)
	      (loop (cdr alist))))))
   
;; FIXME c&p time-signature-music
(define-method (edit (o <paper>))
  (let* ((indent (.indent o))
	 (line-width (line-width o))
	 (height (.height o))
	 (size (paper-size-from-height height))
	 (changed #f)
	 (dialog (make <gtk-dialog>
		  :title "Edit KeySignature" 
		  :modal #t :destroy-with-parent #t))
	 (hbox   (make <gtk-hbox> :homogeneous #f :spacing 8))
	 (stock  (make <gtk-image>
		  :stock (gtk-stock-id 'dialog-question)
		  :icon-size (genum->value 
			       (make <gtk-icon-size> :value 'dialog))))
	 (table  (make <gtk-table> :n-columns 2 :n-rows 2 :homogeneous #f))
	 (label1 (make <gtk-label> :label "_indent" :use-underline #t))
	 (entry1 (make <gtk-entry>))
	 (label2 (make <gtk-label> :label "_line-width" :use-underline #t))
	 (entry2 (make <gtk-entry>))
	 (label3 (make <gtk-label> :label "_paper size" :use-underline #t))
	(entry3 (make <gtk-entry>)))
    (add-button dialog 
		(gtk-stock-id 'cancel)
		(genum->value 
		 (make <gtk-response-type> :value 'cancel)))
    (add-button dialog 
		(gtk-stock-id 'ok)
		(genum->value 
		 (make <gtk-response-type> :value 'ok)))

    (set-border-width hbox 8)
    (pack-start (get-vbox dialog) hbox #f #f 0)

    (pack-start hbox stock #f #f 0)

    (set-row-spacings table 4)
    (set-col-spacings table 4)
    (pack-start hbox table #t #t 0)

    (attach-defaults table label1 0 1 0 1)
    (set-text entry1 (number->string indent))
    (attach-defaults table entry1 1 2 0 1)
    (set-mnemonic-widget label1 entry1)

    (attach-defaults table label2 0 1 1 2)
    (set-text entry2 (number->string line-width))
    (attach-defaults table entry2 1 2 1 2)
    (set-mnemonic-widget label2 entry2)

    (attach-defaults table label3 0 1 2 3)
    (set-text entry3 size)
    (attach-defaults table entry3 1 2 2 3)
    (set-mnemonic-widget label3 entry3)

    (show-all hbox)

    (if (eq? 'ok (genum->symbol (make <gtk-response-type> 
				  :value (run dialog))))
	(let ((i (string->number (get-text entry1)))
	      (w (string->number (get-text entry2)))
	      (s (get-text entry3)))
	  (if (or (and i (!= i indent))
		  (and w (!= w line-width))
		  (and s (not (equal? s size))))
	      (let* ((dimensions (assoc-get paper-alist s))
		     (width (car dimensions))
		     (height (cdr dimensions)))
		(set! (.indent o) i)
		(set! (line-width o) w)
		(set! (.width o) width)
		(set! (.height o) height)
		(set! (.left-margin o) (/ (- width w) 2))
		(format #t "Paper: ~a.~a: ~a\n~" i w s)
		(set! changed #t)))))
    (destroy dialog)
    changed))
