;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers system)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  :export (<system>
	   find-nitem
	   find-music
	   find-music-item
	   last-nitem
	   last-music
	   last-music-item
	   notify
	   paper-when-rect
	   paper-staff-rect
	   paper-system-rect
	   replace
	   set-offset
	   staff-position
	   .box
	   .invalid
	   .items
	   .number)
  :re-export (add
	      find
	      height
	      paper-rect
	      rect
	      .canvas-item
	      .offset))

(define-class <system> ()
  (box :accessor .box :init-value #f :init-keyword :box)
  (offset :accessor .offset :init-form (list 0 0))
  (items :accessor .items :init-form (list) :init-keyword :notation-items)
  (number :accessor .number :init-value 0 :init-keyword :number)
  (invalid :accessor .invalid :init-value #f)
  (staff-rect :accessor .staff-rect :init-value #f)
  (canvas-item :accessor .canvas-item :init-value #f))
  

(define-method (initialize (o <system>) . initargs)
  (next-method)
  (debugf 'system "~a initialze box: ~a\n" (class-name (class-of o)) (.box o))
  (let ((number (.number o)))
    (for-each (lambda (x) (set-system x number)) (.items o)))
  o)

(define (display-address o file)
  (display (number->string (object-address o) 16) file))

(define-method (display-slots (o <system>) port)
  (display (.number o) port)
  (display #\space port)
  (display (.offset o) port))

(define-method (write (o <system>) port)
  (display "#<" port)
  (display (class-name (class-of o)) port)
  (display #\space port)
  (display-address o port)
  (display #\space port)
  (display-slots o port)
  (display #\space port)
  (display #\> port))

;;(define-method (object->string (o <system>))
;;  (call-with-output-string (lambda (p) (write o p))))

(define-method (replace (o <system>) system)
  (set! (.offset o) (.offset system)))

(define-method (rect (o <system>))
  (let* ((box (.box o))
	 (offset (.offset o)))
    (list->vector (list (x offset) (y offset)
			(box-width box) (box-height box)))))

(define-method (paper-rect (o <system>) (paper <paper>))
  (+ (rect o) (unit paper margin)))

(define-method (height (o <system>)) (box-height (.box o)))

(define-method (add (o <system>) (item <notation-item>))
  (let ((item-system (.system item))
	(number (.number o)))
    (if (and (= number 0)
	     (> item-system 0))
	(set! (.number o) item-system)))
  (assert (= (.number o) (.system item)))
  (set! (.items o) (append (.items o) (list item))))

(define-method (is-staff-line? (o <notation-item>) line-width)
  (let* ((fields (map string->number (cdr (.fields o))))
	 (rect (points->rect (map flip-y (list->points (cdr fields)))))
	 (t (car fields))
	 (t-rect (+ rect (list t t)))
	 (width (width rect))
	 (height (abs (height rect)))
	 (epsilon 0.01))
    (and-let* (((and (> (/ width height) 15)
		     (< (/ line-width width) 20))))
	      (+ rect (flip-y (.offset o))))))

(define-method (notify (o <system>) (nitem <notation-item>) line-width)
  (and-let* ((rect (is-staff-line? nitem line-width)))
	    (if (.staff-rect o)
		(set! (.staff-rect o) (encompass (.staff-rect o) rect))
		(set! (.staff-rect o) rect))))

(define-method (find (o <system>) (predicate <procedure>))
  (find predicate (.items o)))

(define-method (rect (o <boolean>))
  #(0 0 0 0))

(define-method (paper-when-rect (o <system>) (nitem <notation-item>)
				(paper <paper>))
  (let* ((system-rect (paper-system-rect o paper))
	 (staff-space (.staff-space paper))
	 (paper-rect (paper-rect nitem paper))
	 (offset (offset paper-rect))
	 (width (width paper-rect))
	 (wider 0.2))
    (list->vector (list (- (x offset) (* wider width))
			(- (y system-rect) (* 0.5 staff-space))
			(* (1+ (2* wider)) width)
			(+ staff-space (height system-rect))))))

;;(define-method (- (o <boolean>) (point <point>))
;;  (- #(0 0 0 0) point))

(define-method (paper-staff-rect (o <system>) (paper <paper>))
  (+ (- (.staff-rect o) (flip-y (.offset o))) (unit paper margin)))

(define-method (paper-system-rect (o <system>) (paper <paper>))
  (+ (rect o) (unit paper margin)))

(define-method (item-when-at? (o <point>) (system <system>) (paper <paper>))
  (lambda (x) (intersect? (paper-when-rect system x paper)
			  (list->rect (append o '(0 0))))))

(define-method (find (o <system>) (p <point>) (paper <paper>))
  (filter (item-when-at? p o paper) (.items o)))

(define-method (unordered-find-music-item (o <system>) (predicate <procedure>))
  (find o (music-predicate predicate)))

(define-method (unordered-find-music (o <system>) (predicate <procedure>))
  (and-let* ((nitem (find o (music-predicate predicate)))) (.music nitem)))

(define-method (find-music (o <system>) (predicate <procedure>))
  (and-let* ((nitem (find-music-item o predicate))) (.music nitem)))

(define-method (find-nitem (o <system>) (predicate <procedure>))
  (let* ((first #f)
	 (start (inf)))
    (for-each (lambda (n)
		(and-let* ((m (.music n))
			   ((predicate n))
			   (t (.start m)))
			  (when (< t start)
				(set! first n)
				(set! start t))))
	      (.items o))
    first))

(define-method (find-music-item (o <system>) (predicate <procedure>))
  (find-nitem o (lambda (n) (and-let* ((m (.music n))) (predicate m)))))

(define-method (last-music (o <system>) (predicate <procedure>))
  (and-let* ((nitem (last-music-item o predicate))) (.music nitem)))

(define-method (last-nitem (o <system>) (predicate <procedure>))
  (let* ((last #f)
	 (start -1))
    (for-each (lambda (n)
		(and-let* ((m (.music n))
			   ((predicate n))
			   (t (.start m)))
			  (when (> t start)
				(set! last n)
				(set! start t))))
	      (.items o))
    last))

(define-method (last-music-item (o <system>) (predicate <procedure>))
  (last-nitem o (lambda (n) (and-let* ((m (.music n))) (predicate m)))))

(define-method (staff-position (o <system>) (paper <paper>) (c-y <number>))
  (let* ((rect (paper-staff-rect o paper))
	 (staff-height (height rect))
	 (line-count 5)
	 (staff-space (/ (abs staff-height) (- line-count 1)))
	 (staff-position (/ staff-space 2))
	 (staff-offset (- (bottom rect) c-y))
	 (staff-position-offset (/ staff-offset staff-position)))
    (number->integer staff-position-offset)))
