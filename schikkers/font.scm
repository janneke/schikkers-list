;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers font)
  ;; base
  ;; oop goops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; user
  :use-module (schikkers misc)
  ;; user-gtk
  :export (music-font?
	   number-font?
	   emmentaler?
	   design-size))

(define (music-font? font)
  (and (not (equal? font "Emmentaler-Brace"))
       (string-prefix? "Emmentaler" font)))

(define (number-font? font)
  (string-prefix? "emmentaler" font))

(define (emmentaler? font)
  (debugf 'canvas-text "emmentaler? ~a: ~a\n" font (or (music-font? font) (number-font? font)))
  (or (music-font? font) (number-font? font)))

(define (design-size font)
  (debugf 'canvas-text "design-size font: ~a\n" font)
  (debugf 'canvas-text "design-size split: ~a\n" (if (emmentaler? font)
						     (cadr (string-split font #\-))
						     "nop"))
  (debugf 'canvas-text "design-size font: ~a\n" font)
  (if (emmentaler? font) (string->number (cadr (string-split font #\-))) 12))
