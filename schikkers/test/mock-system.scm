;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers test mock-system)
  ;; base
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  ;; user
  :use-module (schikkers notation-item)
  :use-module (schikkers system)
  :export (mock-system))

;; run ./schikkers.scm --debug=mock input/test-music.ly
(define (test-music-notation-items)
  (map list->notation-item
       '(
	 ((8.53582677165354 -6.31330236220472) (drawline 0.1 0.0500000000000007 0.0 26.238254143895 0.0))
	 ((8.53582677165354 -5.31330236220472) (drawline 0.1 0.0500000000000007 0.0 26.238254143895 0.0))
	 ((8.53582677165354 -4.31330236220472) (drawline 0.1 0.0500000000000007 0.0 26.238254143895 0.0))
	 ((8.53582677165354 -3.31330236220472) (drawline 0.1 0.0500000000000007 0.0 26.238254143895 0.0))
	 ((8.53582677165354 -2.31330236220472) (drawline 0.1 0.0500000000000007 0.0 26.238254143895 0.0))
	 ((0.0 -7.31330236220472) (draw_round_box -20.0215597047244 21.9958267047244 0.1 0.1 0.2))
	 ((29.629857916414 -4.31330236220472) (draw_round_box -0.0 0.19 2.0 2.0 0.0))
	 ((34.2240809155486 -4.31330236220472) (draw_round_box -0.0 0.6 2.0 2.0 0.0))
	 ((33.7340809155486 -4.31330236220472) (draw_round_box -0.0 0.19 2.0 2.0 0.0))
	 ((28.7688453179888 -1.26330236220472) (utf-8 "Century\040Schoolbook\040L\0403.0673828125" "2"))
	 ((31.971035916414 -4.31330236220472) (draw_round_box 0.065 0.065 2.312186 1.0 0.08))
	 ((30.719857916414 -6.81330236220472) (glyphshow 59796 "Emmentaler-20" 4.0 "noteheads.s2"))
	 ((20.3506042047244 -7.31330236220472) (glyphshow 59796 "Emmentaler-20" 4.0 "noteheads.s2"))
	 ((9.33582677165354 -5.31330236220472) (glyphshow 59763 "Emmentaler-20" 4.0 "clefs.G"))
	 ((12.9558367716535 -4.31330236220472) (glyphshow 59675 "Emmentaler-20" 4.0 "accidentals.flat"))
	 ((13.8758507716535 -2.81330236220472) (glyphshow 59675 "Emmentaler-20" 4.0 "accidentals.flat"))
	 ((14.7958647716535 -4.81330236220472) (glyphshow 59675 "Emmentaler-20" 4.0 "accidentals.flat"))
	 ((16.7458687716535 -6.32775748031496) (utf-8 "emmentaler-20\0407.029296875" "4"))
	 ((16.882442 -4.31330236220472) (utf-8 "emmentaler-20\0407.029296875" "3"))
	 ((21.6017822047244 -4.31330236220472) (draw_round_box 0.065 0.065 2.812186 0.5 0.08))
	 ((27.6302282029936 -4.31330236220472) (draw_round_box 0.065 0.065 1.312186 2.0 0.08))
	 ((23.364827203859 -6.31330236220472) (glyphshow 59796 "Emmentaler-20" 4.0 "noteheads.s2"))
	 ((24.616005203859 -4.31330236220472) (draw_round_box 0.065 0.065 1.812186 1.5 0.08))
	 ((22.330027203859 -6.31330236220472) (glyphshow 59671 "Emmentaler-20" 4.0 "accidentals.natural"))
	 ((26.3790502029936 -5.81330236220472) (glyphshow 59796 "Emmentaler-20" 4.0 "noteheads.s2"))
	 )))

(define (mock-system)
  (make <system> :notation-items (test-music-notation-items)
	:box '((0 0) (10 100))))
