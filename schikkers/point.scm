;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers point)
  ;; base
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;;
  :export (<box> ;; deprecated
	   <interval>
	   <point>
	   <rect>
	   area
	   bottom
	   bottom-right
	   bbox->rect
	   box-height
	   box->list
	   box->rect
	   box-width
	   contains
	   encompass
	   flip-y
	   height
	   intersect?
	   intersect-x?
	   intersect-y?
	   left
	   list->box
	   list->points
	   list->rect
	   origin?
	   pair->point
	   point->pair
	   points->list
	   rect->bbox
	   rect->box
	   rect->list
	   rect->points
	   right
	   points->rect
	   top
	   top-left
	   union
	   width
	   x
	   x-range
	   y
	   y-range)
  :re-export (offset))

(define <point> <list>)
;; DEPRECATE BOX
(define <box> <list>)
(define-method (x (o <list>)) (car o))
(define-method (y (o <list>)) (cadr o))
(define-method (top-left (o <list>)) (car o))
(define-method (bottom-right (o <list>)) (cadr o))
(define-method (+ (o <list>) . rest) (apply map + (cons* o rest)))
(define-method (- (o <list>) . rest) (apply map - (cons* o rest)))
(define-method (* (o <list>) (factor <number>))
  (map * (make-list (length o) factor) o))
(define-method (* (o <list>) . rest) (apply map * (cons* o rest)))
(define-method (flip-y (o <list>)) (* '(1 -1) o))
(define-method (list->points (o <list>))
  (if (null? o)
      '()
      (cons (list (car o) (cadr o))
	    (list->points (cddr o)))))
(define-method (origin? (o <list>)) (= 0 (x o) (y o)))

(define-method (point->pair (o <list>)) (cons (car o) (cadr o)))
(define-method (pair->point (o <pair>)) (list (car o) (cdr o)))

(define-method (rect->box (o <top>)) '((0 0) (0 0)))
(define-method (rect->box (o <vector>)) (list->points (vector->list o)))
(define-method (rect->box (o <list>)) (list->points o))
(define-method (list->box (o <list>)) (list->points o))
(define-method (points->list (o <list>)) (apply append o))
(define-method (box->list (o <list>)) (points->list o))

(define-method (box-height (o <list>))
  (max 0 (- (y (bottom-right o)) (y (top-left o)))))
(define-method (box-width (o <list>))
  (max 0 (- (x (bottom-right o)) (x (top-left o)))))
(define-method (box->rect (o <box>))
  ;;(list->vector (list (x o) (y o) (box-width o) (box-height o))))
  (list->vector (list (car o) (cadr o) (box-width o) (box-height o))))

(define <rect> <vector>)
(define-method (contains (o <boolean>) (rect <rect>)) #f)
(define-method (contains (o <rect>) (rect <boolean>)) #f)
(define-method (contains (o <rect>) (rect <rect>))
  (and (<= (x o) (x rect))
       (<= (y o) (y rect))
       (>= (right o) (right rect))
       (>= (bottom o) (bottom rect))))

(define-method (x (o <rect>)) (vector-ref o 0))
(define-method (left (o <rect>)) (x o))
(define-method (y (o <rect>)) (vector-ref o 1))
(define-method (top (o <rect>)) (y o))
(define-method (width (o <rect>)) (vector-ref o 2))
(define-method (height (o <rect>)) (vector-ref o 3))
;;(define-method (bottom (o <rect>)) (+ (y o) (height o)))
(define-method (bottom (o <rect>)) (+ (y o) (abs (height o)))) ;; FIXME flipped
(define-method (right (o <rect>)) (+ (x o) (width o)))
(define-method (bottom (o <rect>)) (+ (y o) (height o)))
(define-method (offset (o <rect>)) (list (x o) (y o)))
(define-method (x-range (o <rect>)) (list (x o) (right o)))
(define-method (y-range (o <rect>)) (list (y o) (bottom o)))
(define-method (area (o <rect>)) (list (width o) (height o)))
(define-method (rect->list (o <rect>)) (vector->list o))
(define-method (bbox->rect (o <list>))
   (let* ((x1 (list-ref o 0)) (y1 (list-ref o 1))
	  (x2 (list-ref o 2)) (y2 (list-ref o 3)))
     (list->rect (list x1 y1 (- x2 x1) (- y2 y1)))))
(define-method (points->rect (o <list>)) (bbox->rect (points->list o)))
(define-method (rect->bbox (o <rect>))
  (let ((x (x o)) (y (y o)))
  (list x y (+ x (width o)) (+ y (height o)))))
(define-method (list->rect (o <list>)) (list->vector o))
(define-method (* (o <rect>) (factor <number>))
  (list->rect (* (rect->list o) factor)))
(define-method (* (o <rect>) . rest)
  (list->rect (apply * (map rect->list (cons* o rest)))))
(define-method (+ (o <rect>) . rest)
  (list->rect (points->list (list (apply map + (cons* (offset o) rest))
				  (area o)))))
(define-method (- (o <rect>) . rest)
  (list->rect (points->list (list (apply map - (cons* (offset o) rest))
				  (area o)))))

;;
;;  o
;;
;;
(define-method (intersect-x? (o <rect>) (rect <rect>))
  (or (and (>= (right o) (x rect))
	   (<= (x o) (right rect)))
      (and (>= (right rect) (x o))
	   (<= (x rect) (right o)))))

(define-method (intersect-y? (o <rect>) (rect <rect>))
  (or (and (>= (bottom o) (y rect))
	   (<= (y o) (bottom rect)))
      (and (>= (bottom rect) (y o))
	   (<= (y rect) (bottom o)))))

(define-method (intersect? (o <rect>) (rect <rect>))
  (and (intersect-x? o rect) (intersect-y? o rect)))

(define-method (encompass (o <rect>) <boolean>) o)
(define-method (encompass (o <rect>) (rect <rect>))
  (let* ((x (min (x o) (x rect)))
	 (y (min (y o) (y rect)))
	 (r (max (right o) (right rect)))
	 (b (max (bottom o) (bottom rect))))
    (list->vector (list x y (- r x) (- b y)))))

(define-method (union (o <rect>) <boolean>) o)
(define-method (union (o <rect>) (rect <rect>))
  (if (intersect? o rect)
      (encompass o rect)
      o))

(define <interval> <list>)
(define-method (left (o <interval>)) (car o))
(define-method (right (o <interval>)) (cadr o))

(define-method (intersect? (o <interval>) (interval <interval>))
  (or (and (>= (right o) (left interval))
	   (<= (left o) (right interval)))
      (and (>= (right interval) (left o))
	   (<= (left interval) (right o)))))

(define-method (union (o <interval>) (interval <interval>))
  (if (intersect? o interval)
      (list (min (left o) (left interval)) (max (right o) (right interval)))
      o))
