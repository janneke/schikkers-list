#! /bin/sh
# -*- scheme -*-
exec guile -L $(dirname $(dirname $0)) -e test -s $0 "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2010--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define (test . args)
  (eval '(test (command-line)) (resolve-module '(schikkers ly))))

(define-module (schikkers ly)
  ;; base
  :use-module (ice-9 regex)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;;
  :use-module (schikkers paper)
  :use-module (schikkers music-canvas)
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-model)
  :export (load-ly))

(define (ly:make-duration log dots factor-or-num . den)
  (if (null? den)
      (make <duration> :log log :dots dots :factor factor-or-num)
      (make <duration> :log log :dots dots :factor (rationalize (/ factor-or-num (car den)) 1/1000))))

(define (ly:make-pitch octave step alteration)
  (make <pitch> :octave octave :step step :alteration alteration))

(define (ly:make-moment n d gn gd)
  (make <moment> :main (rationalize (/ n d) 1/1000) :grace (rationalize (/ gn gd) 1/1000)))

(define ly:set-middle-C! 'ly:set-middle-C!)

(defmacro markup (. rest)
  (make <markup> :args (primitive-eval (list 'quasiquote rest))))

(define (camel->dashed camel)
  (string->symbol (substring (regexp-substitute/global #f "([A-Z])" (symbol->string camel) 'pre (lambda (match) (string-append "-" (string-downcase (match:substring match)))) 'post) 1)))

(define (music-keyword-maker class)
  (lambda (. rest)
    (let* ((s (split rest))
	   (keys (car s))
	   (keywords (map symbol->keyword keys))
	   (values (cdr s))
	   (symbol-values (map quote-symbol-cons values))
	   (symbol-values-null (map quote-symbol-null values))
	   (init (fold-merge keywords symbol-values))
	   (init-null (fold-merge keywords symbol-values)))
      ;;(format #t "make ~a ~a\n" class init)
      (apply make (append (list class) init-null)))))

(define make-absolute-dynamic-event (music-keyword-maker <absolute-dynamic-event>))
(define make-apply-context (music-keyword-maker <apply-context>))
(define make-articulation-event (music-keyword-maker <articulation-event>))
(define make-fingering-event (music-keyword-maker <fingering-event>))
(define make-bar-check (music-keyword-maker <bar-check>))
(define make-beam-event (music-keyword-maker <beam-event>))
(define make-skip-event (music-keyword-maker <skip-event>))
(define make-skip-music (music-keyword-maker <skip-music>))
(define make-completize-extender-event (music-keyword-maker <completize-extender-event>))
(define make-crescendo-event (music-keyword-maker <crescendo-event>))
(define make-decrescendo-event (music-keyword-maker <decrescendo-event>))
(define make-context-specced-music (music-keyword-maker <context-specced-music>))
(define make-event-chord (music-keyword-maker <event-chord>))
(define make-extender-event (music-keyword-maker <extender-event>))
(define make-grace-music (music-keyword-maker <grace-music>))
(define make-hyphen-event (music-keyword-maker <hyphen-event>))
(define make-key-change-event (music-keyword-maker <key-change-event>))
(define make-line-break-event (music-keyword-maker <line-break-event>))
(define make-lyric-combine-music (music-keyword-maker <lyric-combine-music>))
(define make-lyric-event (music-keyword-maker <lyric-event>))
(define make-mark-event (music-keyword-maker <mark-event>))
(define make-multi-measure-rest-music (music-keyword-maker <multi-measure-rest-music>))
(define make-note-event (music-keyword-maker <note-event>))
(define make-override-property (music-keyword-maker <override-property>))
(define make-property-set (music-keyword-maker <property-set>))
(define make-property-unset (music-keyword-maker <property-unset>))
(define make-partial-set (music-keyword-maker <partial-set>))
(define make-phrasing-slur-event (music-keyword-maker <phrasing-slur-event>))
(define make-relative-octave-music (music-keyword-maker <relative-octave-music>))
(define make-relative-octave-music (music-keyword-maker <relative-octave-music>))
(define make-rest-event (music-keyword-maker <rest-event>))
(define make-revert-property (music-keyword-maker <revert-property>))
(define make-sequential-music (music-keyword-maker <sequential-music>))
(define make-simultaneous-music (music-keyword-maker <simultaneous-music>))
(define make-slur-event (music-keyword-maker <slur-event>))
(define make-text-script-event (music-keyword-maker <text-script-event>))
(define make-tie-event (music-keyword-maker <tie-event>))
(define make-time-scaled-music (music-keyword-maker <time-scaled-music>))
(define make-time-signature-music (music-keyword-maker <time-signature-music>))
(define make-transposed-music (music-keyword-maker <transposed-music>))
(define make-unrelativable-music (music-keyword-maker <unrelativable-music>))
(define make-unfolded-repeated-music (music-keyword-maker <unfolded-repeated-music>))
(define make-volta-repeated-music (music-keyword-maker <volta-repeated-music>))
(define make-voice-separator (music-keyword-maker <voice-separator>))

(define (make-music music . rest)
  (let* ((dashed (camel->dashed music))
	 (make-music-func (eval-string (string-append "make-" (symbol->string dashed)))))
    (eval (apply make-music-func rest) (resolve-module '(schikkers ly)))))

(define display-header
  (gulp-text-file (string-append (get-data-dir) "/ly/display-as-scheme.ly")))

(define display-footer
  (gulp-text-file (string-append (get-data-dir) "/ly/display-paper-defaults.ly")))

(define paper #f)

;;(define book-paper #f)
;;(define (ly-book-paper) (set! book-paper #t))

(define ly:minimal-breaking 'ly:minimal-breaking)
(define ly:one-line-breaking 'ly:one-line-breaking)
(define ly:optimal-breaking 'ly:optimal-breaking)
(define (ly-pt x) #f)
(define (ly-mm x) #f)
(define (ly-mm-to-unit x) (set! (.mm-to-unit paper) x))
(define (ly-staff-space x) (set! (.staff-space paper) x))
(define (ly-page-breaking x) (set! (.page-breaking paper) x))
(define (ly-output-scale x)
  (set! (.output-scale paper) x)
  (set! (.mm-to-unit paper) (/ (.mm-to-unit paper) x)))
(define (ly-landscape x) (set! (.landscape paper) x))
(define (ly-paper-width x) (if (.landscape paper) (set! (.height paper) x) (set! (.width paper) x)))
(define (ly-paper-height x) (if (.landscape paper) (set! (.width paper) x) (set!(.height paper) x)))
(define (ly-indent x) (set! (.indent paper) x))
(define (ly-left-margin-default-scaled x) (set! (.left-margin paper) x))
(define (ly-right-margin-default-scaled x) (set! (.right-margin paper) x))
(define (ly-left-margin x) (set! (.left-margin paper) x))
(define (ly-right-margin x) (set! (.right-margin paper) x))
(define (ly-line-width x) (set! (.right-margin paper) (- (.width paper) (.left-margin paper) x)))
(define (ly-top-margin x) (set! (.top-margin paper) x))
(define (ly-bottom-margin x) (set! (.bottom-margin paper) x))

(define-generic load-ly)
(define-method (load-ly (o <top>)) #f)
(define-method (load-ly (file-name <string>))
  (stderr "Loading ly file: ~a\n" file-name)
  (let* ((base (basename (basename file-name ".ly") ".sy"))
	 (ly-name (string-append base "-display.ly"))
	 (sy-name (string-append base ".sy"))
	 (dir (dirname file-name))
	 (sy-name-at-ly (string-append dir "/" sy-name)))
    (if (and (not (equal? dir "."))
	     (not (first-is-newer? file-name sy-name-at-ly))
	     (file-exists? sy-name)
	     (not (first-is-newer? sy-name sy-name-at-ly)))
	(set! sy-name sy-name-at-ly))
    (if (or #t (first-is-newer? file-name sy-name))
	(let ((ly-str (string-append display-header (gulp-text-file file-name))))
	  (dump-file ly-name ly-str)
	  (system (format #f "lilypond ~a > ~a" ly-name sy-name))
	  (delete-file ly-name))
	(stderr "Reading cached sy file: ~a\n" sy-name))
    (set! paper (make <paper>))
    (let* ((music (eval-string (gulp-text-file sy-name) (resolve-module '(schikkers ly))))
	   (model (make <music-model> :file-name file-name :music music :paper paper)))
      model)))

(define (test . args)
  (format #t "parse:~a\n" (load-ly (cadar args))))
