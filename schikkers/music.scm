#! /bin/sh
# -*- scheme -*-
exec guile -L $(dirname $(dirname $0)) -e test -s $0 "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2010--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define (test . args)
  (eval '(test (command-line)) (resolve-module '(schikkers music))))

(define-module (schikkers music)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 receive)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;;
  :use-module (schikkers misc)
  :use-module (schikkers paper)
  :export (<absolute-dynamic-event>
	   <apply-context>
	   <articulation-event>
	   <bar-check>
	   <beam-event>
	   <completize-extender-event>
	   <context-specced-music>
	   <crescendo-event>
	   <decrescendo-event>
	   <duration>
	   <fingering-event>
	   <event-chord>
	   <extender-event>
	   <grace-music>
	   <hyphen-event>
	   <key-change-event>
	   <line-break-event>
	   <lyric-combine-music>
	   <lyric-event>
	   <mark-event>
	   <moment>
	   <multi-measure-rest-music>
	   <music>
	   <nested-music>
	   <note-event>
	   <override-property>
	   <pitch>
	   <partial-set>
	   <phrasing-slur-event>
	   <property-set>
	   <property-unset>
	   <relative-octave-music>
	   <rest-event>
	   <revert-property>
	   <rhythmic-event>
	   <sequential-music>
	   <simultaneous-music>
	   <skip-event>
	   <skip-music>
	   <slur-event>
	   <text-script-event>
	   <tie-event>
	   <time-scaled-music>
	   <time-signature-music>
	   <transposed-music>
	   <unrelativable-music>
	   <unfolded-repeated-music>
	   <volta-repeated-music>
	   <voice-separator>
	   <markup>
	   append-element!
	   articulation-alist
	   clef-alist
	   duration->number
	   duration->log
	   start-music
	   insert-element!
	   get-elements
	   get-neighbor
	   get-neighbor-note-or-rest
	   get-style
	   find-first
	   find-last
	   index
	   is-a-bar-line?
	   is-or-has-a-bar-line?
	   is-a-clef-glyph?
	   is-a-clef-position?
	   is-a-clef-middle-c?
	   is-a-context-lyrics?
	   is-a-context-staff?
	   is-a-context-voice?
	   is-a-key-change-event?
	   is-a-lyric-or-hyphen-or-extender?
	   is-a-music?
	   is-a-note?
	   is-a-note-or-rest?
	   is-a-note-or-skip?
	   is-a-relative-octave-music?
	   is-a-rest?
	   is-a-rest-or-skip?
	   is-a-skip?
	   is-a-slur?
	   is-a-slur-or-tie?
	   is-a-simultaneous?
	   is-a-tie?
	   is-a-time-signature?
	   is-a-voice?
	   is-first-rhythmic-event?
	   is-last-rhythmic-event?
	   key-alist
	   ly-dir
	   ly-string
	   make-articulation
	   make-clef
	   make-clef-glyph
	   make-clef-position
	   make-clef-middle-c
	   make-dynamic
	   make-slur
	   make-tie
	   name
	   re-tag!
	   replace-element!
	   tie-active?
	   tie-possible?
	   scm-name
	   set-bar-number-visibility
	   set-bar-type
	   set-measure-number
	   set-staff-symbol-width
	   set-start
	   set-style
	   set-time-signature-stencil
	   skip-typesetting
	   test-music
	   walk
	   .aesees
	   .alteration
	   .articulations
	   .denominator
	   .dots
	   .duration
	   .element
	   .explicit
	   .factor
	   .input-tag
	   .log
	   .numerator
	   .octave
	   .parent
	   .pitch
	   .scale
	   .start
	   .step
	   .style
	   .text
	   .tonic
	   .tweaks
	   .value)
  :re-export (find
	      lisp-value
	      remove!))

(define (duration->log duration)
  (and duration
       (or (and (member duration '(-2 -1 0)) duration)
	   (and (member duration '(1 2 4 8 16 32 64 128))
		(inexact->exact (log2 duration))))))

(define-class <markup> ()
  (args :accessor .args :init-value '() :init-keyword :args))

(define-method (lisp-value (o <markup>) predicate)
  `(markup ,(.args o)))

(define-class <duration> ()
  (log :accessor .log :init-value 2 :init-keyword :log)
  (dots :accessor .dots :init-value 0 :init-keyword :dots)
  (factor :accessor .factor :init-value 1/1 :init-keyword :factor)
  (explicit :accessor .explicit :init-value #f :init-keyword :explicit))

(define-method (equal? (o <duration>) (duration <duration>))
  (and (= (.log o) (.log duration))
       (= (.dots o) (.dots duration))
       (= (.factor o) (.factor duration))))

(define-method (ly (o <duration>))
  (string-append
   (format #f "~d~a" (ash 1 (.log o)) (make-string (.dots o) #\.))
   (if (equal? (.factor o) 1)
       ""
       (format #f "*~a" (.factor o)))))

(define-method (ly (o <duration>) state)
  (let ((duration (car state)))
    (if (and (equal? o duration)
	     (not (.explicit o)))
      ""
      (begin
	(set! (.log duration) (.log o))
	(set! (.dots duration) (.dots o))
	(ly o)))))

(define-method (duration->number (o <duration>))
  (let* ((dots (.dots o))
	 (dots-factor (/ (1- (ash 1 (1+ dots))) (ash 1 dots)))
	 (log (abs (.log o)))
	 (dur (ash 1 log))
	 (base (if (< (.log o) 0) dur (/ 1 dur))))
    (* base dots-factor (.factor o))))

(define-method (lisp-value (o <duration>) predicate)
  `(ly:make-duration ,(.log o) ,(.dots o) ,(numerator (.factor o)) ,(denominator (.factor o))))

(define-class <moment> ()
  (main :accessor .main :init-value 0 :init-keyword :main)
  (grace :accessor .grace :init-value 0 :init-keyword :grace))

(define-method (lisp-value (o <moment>) predicate)
  `(ly:make-moment ,(numerator (.main o)) ,(denominator (.main o))
		   ,(numerator (.grace o)) ,(denominator (.grace o))))

(define-class <pitch> ()
  (alteration :accessor .alteration :init-value 0 :init-keyword :alteration)
  (step :accessor .step :init-value 0 :init-keyword :step)
  (octave :accessor .octave :init-value 0 :init-keyword :octave)
  (aesees :accessor .aesees :init-value #f :init-keyword :aesees))

(define-method (equal? (o <pitch>) (pitch <pitch>))
  (and (= (.alteration o) (.alteration pitch))
       (= (.step o) (.step pitch))
       (= (.octave o) (.octave pitch))))

(define-method (lisp-value (o <pitch>) predicate)
  `(ly:make-pitch ,(.octave o) ,(.step o) ,(.alteration o)))

(define-method (ly (o <pitch>))
  (let* ((alteration (.alteration o))
	 (step (.step o))
	 (octave (.octave o))
	 (ae (if (or (and (!= step 2) (!= step 5)) (.aesees o)) 0 1)))
    (string-append
     (string-copy "cdefgab" (.step o) (1+ (.step o)))
     (cond ((> alteration 0) (string-copy "isis" 0 (* alteration 4)))
	   ((< alteration 0) (string-copy "eses" ae (* alteration -4)))
	   (else ""))
     (cond ((>0 octave) (make-string octave #\'))
	   ((<0 octave) (make-string (abs octave) #\,))
	   (else "")))))

(define-method (absolute-step (o <pitch>))
  (+ (* (.octave o) 7) (.step o) (.alteration o)))
  
(define-method (relative-pitch (o <pitch>) pitch)
  (let ((delta (- (absolute-step o) (absolute-step pitch))))
    (make <pitch>
      :alteration (.alteration o)
      :step (.step o)
      :octave (round (/ delta 7))
      :aesees (.aesees o))))

(define-method (ly (o <pitch>) state)
  (or (and-let* ((pitch (cdr state))
		 (ly (ly (relative-pitch o pitch))))
		(set! (.alteration pitch) (.alteration o))
		(set! (.step pitch) (.step o))
		(set! (.octave pitch) (.octave o))
	  ly)
      (ly o)))

(define-class <music> ()
  (properties :accessor .properties :init-value '(input-tag tweaks))
  (input-tag :accessor .input-tag :init-value -1)
  (tweaks :accessor .tweaks :init-form (list) :init-keyword :tweaks)
  (parent :accessor .parent :init-value #f)
  (start :accessor .start :init-value 0/1 :init-keyword :start))

(define-method (get-elements (o <music>))
  '())

(define-method (ly (o <music>))
  "")

(define-method (ly (o <music>) state)
  (ly o))

(define-method (scm-name (o <music>))
  (class-name (class-of o)))

(define-method (name (o <music>))
  (apply string-append
	 (map string-capitalize
	      (string-tokenize (symbol->string (scm-name o)) char-set:lower-case))))

(define-method (lisp-properties (o <music>) predicate)
  (apply append (map (lambda (name)
		       (let ((v (slot-ref o name)))
			 (if (and (pair? v)
				  (list? v))
			     (if (eq? (car v) 'quote)
				 (if (member (cadr v) lily-variables)
				     (list `',name (cadr v))
				     (list `',name v))
				 (if (or (list? (car v))
					 (member name lily-alists))
				     (list `',name `',v)
				     (append `(',name) (list (append '(list) (lisp-value
									      (if (eq? name 'elements)
										  (filter predicate v)
										  v)
									      predicate))))))
			     (append `(',name) (list (lisp-value v predicate))))))
		     (.properties o))))

(define-method (lisp-value (o <top>) predicate)
  o)

(define lily-alists '(
		      tweaks
		      ))

(define lily-variables '(
			 ly:minimal-breaking
			 ly:one-line-breaking
			 ly:optimal-breaking
			 ly:set-middle-C!
			 ))

(define lily-symbols '(
		       BarNumber
		       Lyrics
		       Score
		       Staff
		       StaffSymbol
		       Stem
		       Timing
		       TimeSignature
		       Voice
		       break-visibility
		       clefGlyph
		       clefOctavation
		       clefPosition
		       currentBarNumber
		       direction
		       font-shape
		       font-size
		       height-limit
		       middleCPosition
		       middleCClefPosition
		       numbered
		       padding
		       skipTypesetting
		       stencil
		       stroke-style
		       style
		       transparent
		       width
		       whichBar
		       ))

(define-method (lisp-value (o <symbol>) predicate)
  (if (member o lily-symbols)
      `',o
      o))

(define-method (lisp-value (o <list>) predicate)
  (map (lambda (x) (lisp-value x predicate)) o))

(define-method (lisp-value (o <music>))
  (lisp-value o (lambda (x) #t)))

(define-method (lisp-value (o <music>) predicate)
  (append `(make-music (quote ,(string->symbol (name o))))
	  (lisp-properties o predicate)))

(define-method (duration->number (o <music>))
  0/1)

(define-method (index (o <music>))
  (let ((parent (.parent o)))
    (if parent
	(or (list-index (lambda (x) (eq? x o)) (get-elements parent)) 0)
	0)))

(define-method (re-tag! (o <music>) tag tag-table)
  (debugf 're-tag "~a: ~a\n" o tag)
  (set! (.input-tag o) tag)
  (hashq-set! tag-table tag o)
  (+ tag 1))

(define-method (set-start (o <music>) start)
  (set! (.start o) start))

(define (is-a-bar-line? x)
  (and (is-a? x <property-set>)
       (or (equal? (.symbol x) 'whichBar)
	   (equal? (.symbol x) '(quote whichBar)))))

(define (is-or-has-a-bar-line? x)
  (or (is-a-bar-line? x)
      (and (is-a? x <context-specced-music>)
	   (is-a-bar-line? (.element x)))))

(define (is-a-clef-glyph? x)
  (and (is-a? x <property-set>)
       (eq? (.symbol x) 'clefGlyph)))

(define (is-a-clef-position? x)
  (and (is-a? x <property-set>)
       (eq? (.symbol x) 'clefPosition)))

(define (is-a-clef-middle-c? x)
  (and (is-a? x <property-set>)
       (eq? (.symbol x) 'middleCPosition)))

(define (context-type x)
  (and-let* ((context (is-a? x <context-specced-music>)))
	    (.context-type x)))

(define (is-a-context? x type)
  (and-let* ((symbol (context-type x)))
	    (or (equal? symbol `(quote ,type))
		(eq? symbol type))))

(define (is-a-context-lyrics? x) (is-a-context? x 'Lyrics))
(define (is-a-context-staff? x) (is-a-context? x 'Staff))
(define (is-a-context-voice? x) (is-a-context? x 'Voice))

(define (is-a-key-change-event? x) (is-a? x <key-change-event>))

(define (is-a-lyric-or-hyphen-or-extender? x)
  (or (is-a? x <lyric-event>)
      (is-a? x <hyphen-event>)
      (is-a? x <extender-event>)))

(define (is-a-music? x) (is-a? x <music>))
(define (is-a-note? x) (is-a? x <note-event>))
(define (is-a-rest? x) (is-a? x <rest-event>))
(define (is-a-skip? x) (is-a? x <skip-event>))
(define (is-a-rest-or-skip? x) (or (is-a-rest? x) (is-a-skip? x)))
(define (is-a-note-or-rest? x) (or (is-a-note? x) (is-a-rest? x)))
(define (is-a-note-or-skip? x) (or (is-a-note? x) (is-a-skip? x)))
(define (is-a-relative-octave-music? x) (is-a? x <relative-octave-music>))
(define (is-a-slur? x) (is-a? x <slur-event>))
(define (is-a-slur-or-tie? x) (or (is-a-slur? x) (is-a-tie? x)))
(define (is-a-tie? x) (is-a? x <tie-event>))
(define (is-a-time-signature? x) (is-a? x <time-signature-music>))
(define (is-a-simultaneous? x) (is-a? x <simultaneous-music>))

(define-method (is-first-rhythmic-event? (o <music>))
  (let ((prev (get-neighbor o -1)))
    (or (eq? o prev)
	(not (is-a-note-or-rest? prev)))))

(define-method (is-last-rhythmic-event? (o <music>))
  (let ((next (get-neighbor o 1)))
    (or (eq? o next)
	(is-a-skip? next)
	(is-or-has-a-bar-line? next))))

(define-method (get-neighbor (o <boolean>) (music <music>) dir) music)

(define-method (get-neighbor (o <music>) dir)
  (debugf 'neighbor "<music> get-neighbor: ~a\n" dir)
  (get-neighbor (.parent o) o dir))

(define-method (get-neighbor-note-or-rest (o <music>) count) ;; FIXME
  (let ((neighbor (get-neighbor o count))
	(cursor o))
    (while (and (not (eq? neighbor cursor)) (not (is-a-note-or-rest? neighbor)))
	    (set! cursor neighbor)
	    (set! neighbor (get-neighbor cursor (sign count))))
    (if (is-a-note-or-rest? neighbor) neighbor o)))

(define-method (find-first (o <music>) predicate)
  (if (predicate o)
      o
      #f))

(define-method (find-last (o <music>) predicate)
  (find-last o predicate (.start o)))

(define-method (find-last (o <music>) predicate start)
  (let loop ((found #f) (next (find-first o predicate)))
    (if (or (not next)
	    (>= (.start next) start)
	    (eq? next (get-neighbor next 1)))
	found
	(loop next (find-first (get-neighbor next 1) predicate)))))

(define-method (append-element! (o <music>) elt)
  (let ((parent (.parent o)))
    (if (is-a? parent <event-chord>)
	(append-element! (.parent parent) parent elt)
	(append-element! (.parent o) o elt))))

(define-method (insert-element! (o <music>) elt)
  (let ((parent (.parent o)))
    (if (is-a? parent <event-chord>)
	(insert-element! (.parent parent) parent elt)
	(insert-element! (.parent o) o elt))))

(define-method (remove! (o <music>))
  (let* ((parent (.parent o))
	 (elements (.elements parent)))
    (set! (.elements parent) (remove (lambda (x) (eq? x o)) elements))
    (set! (.parent o) #f)
    o))

(define-method (replace-element! (o <music>) elt)
  (append-element! o elt)
  (remove! o))

(define-class <nested-music> (<music>)
  (properties :accessor .properties :init-value '(input-tag elements))
  (elements :accessor .elements :init-value (list) :init-keyword :elements))

(define-method (initialize (obj <nested-music>) . initargs)
  (let* ((o (next-method)))
    (for-each (lambda (x) (if (is-a? x <music>) (set! (.parent x) o))) (get-elements o))
    o))

(define-method (get-elements (o <nested-music>))
  (.elements o))

(define-method (ly (o <nested-music>))
  '("" . ""))

(define-method (append-elements! (o <nested-music>) elts)
  (set! (.elements o) (append (.elements o) elts))
  (for-each (lambda (x) (if (is-a? x <music>) (set! (.parent x) o))) elts)
  o)

(define-method (append-element! (o <nested-music>) elt)
  (append-elements! o (list elt)))

(define-method (append-element! (o <nested-music>) after elt)
  (let* ((elements (.elements o))
	 (i (list-index (lambda (x) (eq? x after)) elements)))
    (receive (left right) (split-at elements (1+ i))
      (set! (.elements o) (append left (list elt) right)))
    (set! (.parent elt) o))
  o)

(define-method (insert-element! (o <nested-music>) before elt)
  (let* ((elements (.elements o))
	 (i (list-index (lambda (x) (eq? x before)) elements)))
    (receive (left right) (split-at elements i)
      (set! (.elements o) (append left (list elt) right)))
    (set! (.parent elt) o))
  o)

(define-method (re-tag! (o <nested-music>) tag tag-table)
  (debugf 're-tag "~a: ~a --> ~a\n" o tag (get-elements o))
  (set! tag (next-method))
  (for-each (lambda (x) (set! tag (re-tag! x tag tag-table))) (get-elements o))
  tag)

(define-method (get-neighbor (o <nested-music>) (music <music>) dir)
  (let* ((elements (get-elements o))
	 (i (list-index (lambda (x) (eq? x music)) elements))
	 (len (length elements))
	 (j (max (min (+ i dir) (- len 1)) 0)))
    (list-ref elements j)))

(define-method (set-start-update (o <nested-music>) start element)
  start)

(define-method (set-start (o <top>) start)
  #f)

(define-method (set-start (o <nested-music>) start)
  (set! (.start o) start)
  (for-each (lambda (x) (set-start x start) (set! start (set-start-update o start x))) (get-elements o)))

(define-method (find-first (o <nested-music>) predicate)
  (let ((r (next-method)))
    (if r
	r
	(let loop ((elts (get-elements o)))
	  (if (not (pair? elts))
	      #f
	      (let* ((e (car elts))
		     (r (find-first e predicate)))
		(if r
		    r
		    (loop (cdr elts)))))))))

(define-class <sequential-music> (<nested-music>)
  (properties :accessor .properties :init-value '(input-tag elements)))

(define-method (ly (o <sequential-music>))
  '("{" . "}"))

(define-method (ly (o <sequential-music>) state)
  (ly o))

(define-method (set-start-update (o <sequential-music>) start (element <music>))
  (+ start (duration->number element)))

(define-class <event-chord> (<nested-music>)
  (properties :accessor .properties :init-value '(input-tag elements)))

(define-method (duration->number (o <event-chord>))
  (let ((d 0))
    (for-each (lambda (x) (set! d (max d (duration->number x)))) (.elements o))
    d))

(define-method (ly (o <event-chord>) state)
  (if (> (length (.elements o)) 1)
      (cons "<" (string-append ">" (ly (.duration (car (.elements o))) state)))
      '("" . "")))

(define-class <simultaneous-music> (<nested-music>)
  (properties :accessor .properties :init-value '(input-tag elements)))

(define-method (ly (o <simultaneous-music>))
  '("<<" . ">>"))

(define-class <event> (<music>)
  (properties :accessor .properties :init-value '(input-tag)))

(define-class <rhythmic-event> (<event>)
  (properties :accessor .properties :init-value '(input-tag duration articulations tweaks))
  (duration :accessor .duration :init-form (make <duration>) :init-keyword :duration)
  (articulations :accessor .articulations :init-value '() :init-keyword :articulations))

(define-method (re-tag! (o <rhythmic-event>) tag tag-table)
  (debugf 're-tag "~a: ~a --> ~a\n" o tag (get-elements o))
  (set! tag (next-method))
  (for-each (lambda (x) (set! tag (re-tag! x tag tag-table))) (.articulations o))
  tag)

(define-method (get-neighbor (o <rhythmic-event>) dir)
  (debugf 'neighbor "<rhythmic-event> get-neighbor: ~a\n" dir)
  (let ((parent (.parent o)))
    (if (is-a? parent <event-chord>)
	(let ((neighbor (get-neighbor (.parent parent) parent dir)))
	  (find-first neighbor is-a-note-or-rest?))
	(get-neighbor parent o dir))))

(define-method (duration->number (o <rhythmic-event>))
  (duration->number (.duration o)))

(define-class <rest-event> (<rhythmic-event>)
  (properties :accessor .properties :init-value '(input-tag duration articulations tweaks)))

(define-method (ly (o <rest-event>) state)
  (string-append (tweaks->ly o) "r" (ly (.duration o) state)))

(define-class <multi-measure-rest-music> (<rhythmic-event>)
  (properties :accessor .properties :init-value '(input-tag duration articulations)))

(define-class <skip-event> (<rhythmic-event>)
  (properties :accessor .properties :init-value '(input-tag duration)))

(define-class <skip-music> (<rhythmic-event>)
  (properties :accessor .properties :init-value '(input-tag duration)))

(define-method (ly (o <skip-music>)) "")
(define-method (ly (o <skip-event>)) "")

(define-class <note-event> (<rhythmic-event>)
  (properties :accessor .properties :init-value '(input-tag duration pitch articulations tweaks))
  (pitch :accessor .pitch :init-form (make <pitch>) :init-keyword :pitch))

(define-method (tweaks->ly- (o <music>))
  (or (and-let* ((tweaks (null-is-#f (.tweaks o)))
		 (key 'extra-offset)
		 (value (assoc-get tweaks key))
		 ((not (equal? value '(0 . 0)))))
		(format #f "\\tweak #'~a #'~a" key value))
      ""))

(define-method (tweaks->ly (o <music>))
  (or (and-let* ((ly (empty-is-#f (tweaks->ly- o))))
		(string-append ly " "))
      ""))

(define-method (tweaks->postfix-ly (o <music>))
  (or (and-let* ((ly (empty-is-#f (tweaks->ly- o))))
		(string-append "-" ly))
      ""))

(define-method (ly (o <note-event>) state)
  (string-append (tweaks->ly o)
		 (ly (.pitch o) state)
		 (if (is-a? (.parent o) <event-chord>)
		     ""
		     (ly (.duration o) state))
		 (if (is-a? (.parent o) <event-chord>)
		     ""
		     (apply string-append (map ly (.articulations o))))))

(define-class <lyric-event> (<rhythmic-event>)
  (properties :accessor .properties :init-value '(input-tag duration text))
  (text :accessor .text :init-value "" :init-keyword :text))

(define-method (ly (o <lyric-event>))
  (let ((text (.text o)))
    (string-append
     (if (string-every char-alphabetic? text) text (object->string text))
     (ly (.duration o)))))

(define-class <hyphen-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag)))

(define-class <extender-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag)))

(define-class <line-break-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag break-permission))
  (break-permission :accessor .break-permission :init-value 'force :init-keyword :break-permission))

(define (scale->pitch-alist scale)
  (let loop ((i 0) (pitches scale) (alist '()))
    (if (not (pair? pitches))
	alist
	(loop (+ i 1) (cdr pitches) (append alist (list (cons i (car pitches))))))))

(define (pitch-alist->scale pitch-alist)
  (let loop ((i 0) (scale '()))
    (if (> i 6) scale
	(loop (1+ i) (append scale (list (assoc-get pitch-alist i)))))))

(define key-alist
  '((C . (0 0 (0 0 0 0 0 0 0)))

    (G . (4 0 (0 0 0 1/2 0 0 0)))
    (D . (1 0 (1/2 0 0 1/2 0 0 0)))
    (A . (5 0 (1/2 0 0 1/2 1/2 0 0)))
    (E . (2 0 (1/2 1/2 0 1/2 1/2 0 0)))
    (B . (6 0 (1/2 1/2 0 1/2 1/2 1/2 0)))
    (FIS . (3 1/2 (1/2 1/2 1/2 1/2 1/2 1/2 0)))
    (CIS . (3 1/2 (1/2 1/2 1/2 1/2 1/2 1/2 1/2)))

    (F . (3 0 (0 0 0 0 0 0 -1/2)))
    (BES . (6 -1/2 (0 0 -1/2 0 0 0 -1/2)))
    (EES . (2 -1/2 (0 0 -1/2 0 0 -1/2 -1/2)))
    (AES . (5 -1/2 (0 -1/2 -1/2 0 0 -1/2 -1/2)))
    (DES . (1 -1/2 (0 -1/2 -1/2 0 -1/2 -1/2 -1/2)))
    (GES . (4 -1/2 (-1/2 -1/2 -1/2 0 -1/2 -1/2 -1/2)))
    (CES . (0 -1/2 (-1/2 -1/2 -1/2 -1/2 -1/2 -1/2 -1/2)))

    (a . (5 0 (0 0 0 0 0 0 0)))

    (e . (2 0 (0 0 0 1/2 0 0 0)))
    (b . (6 0 (1/2 0 0 1/2 0 0 0)))
    (fis . (3 1/2 (1/2 0 0 1/2 1/2 0 0)))
    (cis . (0 1/2 (1/2 1/2 0 1/2 1/2 0 0)))
    (gis . (4 1/2 (1/2 1/2 0 1/2 1/2 1/2 0)))
    (dis . (1 1/2 (1/2 1/2 1/2 1/2 1/2 1/2 0)))
    (ais . (5 1/2 (1/2 1/2 1/2 1/2 1/2 1/2 1/2)))

    (d . (1 0 (0 0 0 0 0 0 -1/2)))
    (g . (4 0 (0 0 -1/2 0 0 0 -1/2)))
    (c . (0 0 (0 0 -1/2 0 0 -1/2 -1/2)))
    (f . (3 0 (0 -1/2 -1/2 0 0 -1/2 -1/2)))
    (bes . (6 -1/2 (0 -1/2 -1/2 0 -1/2 -1/2 -1/2)))
    (ees . (2 -1/2 (-1/2 -1/2 -1/2 0 -1/2 -1/2 -1/2)))
    (aes . (5 -1/2 (-1/2 -1/2 -1/2 -1/2 -1/2 -1/2 -1/2)))))

(define-class <key-change-event> (<event>)
  (properties :accessor .properties :init-value '(input-tag tonic))
  (tonic :accessor .tonic :init-form (make <pitch>) :init-keyword :tonic)
  (scale :accessor .scale :accessor .scale :init-value '(0 0 0 0 0 0 0) :init-keyword :scale)
  (pitch-alist :accessor .pitch-alist :accessor .pitch-alist :init-value #f :init-keyword :pitch-alist))

(define-method (initialize (obj <key-change-event>) . initargs)
  (let* ((o (next-method))
	 (pitch-alist (.pitch-alist o)))
    (if pitch-alist
	(set! (.scale o) (pitch-alist->scale pitch-alist)))
    (debugf 'key-alist "scale (~a . (~a ~a ~a))\n" (ly (.tonic o)) (.step (.tonic o)) (.alteration (.tonic o)) (.scale o))
    o))

(define-method (equal? (o <key-change-event>) (k <key-change-event>))
  (and (equal? (.tonic o) (.tonic k))
       (equal? (.scale o) (.scale k))))

(define-method (ly- (o <key-change-event>))
  (format #f "\\key ~a \\major" (ly (.tonic o) (cons (.tonic o) #f))))

(define-method (ly (o <key-change-event>))
  (ly-default o (make <key-change-event>)))

(define-method (pitch-alist (o <key-change-event>))
  (let loop ((i 0) (pitches (.scale o)) (alist '()))
    (if (not (pair? pitches))
	alist
	(loop (+ i 1) (cdr pitches) (append alist (list (cons i (car pitches))))))))

(define-method (lisp-value (o <key-change-event>) predicate)
  (append (next-method) `('pitch-alist ',(pitch-alist o))))

(define-class <music-wrapper> (<nested-music>)
  (properties :accessor .properties :init-value '(input-tag element))
  (element :accessor .element :init-value #f :init-keyword :element))

(define-method (get-elements (o <music-wrapper>))
  (list (.element o)))

(define-method (append-element! (o <music-wrapper>) elt)
  (set! (.element o) elt)
  (set! (.parent elt) o)
  o)

(define-class <context-specced-music> (<music-wrapper>)
  (properties :accessor .properties :init-value '(input-tag property-operations context-id context-type element))
  (property-operations :accessor .property-operations :init-value '() :init-keyword :property-operations)
  (context-id :accessor .context-id :init-value #f :init-keyword :context-id)
  (context-type :accessor .context-type :init-value #f :init-keyword :context-type))

(define-method (ly- (o <context-specced-music>))
  (let ((type (member-get (.context-type o) '(Lyrics Staff Voice)))
	(id (.context-id o)))
    (cons
     (if (or (and type id)
	     (eq? type 'Lyrics))
	 (string-append "\\context " (symbol->string type)
			(if id (format #f " = ~S" id) "")
			(if (eq? type 'Lyrics) "\\lyrics" ""))
	 "")
     "")))

(define-method (equal? (o <context-specced-music>) (t <context-specced-music>))
  (and (eq? (.context-type o) (.context-type t))
       (equal? (.context-id o) (.context-id t))))

(define-method (ly (o <context-specced-music>))
  (let ((default (or (and-let* ((parent (.parent o))
				(simultaneous? (find parent is-a-simultaneous?)))
			       (make <context-specced-music>))
		     (make <context-specced-music>
		       :context-type 'Voice
		       :context-id "one"))))
    (ly-default o default)))

(define-class <property-set> (<music>)
  (properties :accessor .properties :init-value '(input-tag value symbol))
  (value :accessor .value :init-value #f :init-keyword :value)
  (symbol :accessor .symbol :init-value #f :init-keyword :symbol))

(define-method (ly-clef- (o <property-set>))
  (format #f "\\clef ~S" (string-drop (.value o) 6)))

(define-method (equal? (o <property-set>) (t <property-set>))
  (and (is-a-clef-glyph? o)
       (is-a-clef-glyph? t)
       (equal? (.value o) (.value t))))
       
(define-method (ly- (o <property-set>))
  (let ((default-clef (make <property-set> :value "G" :symbol 'clefGlyph)))
    (if (equal? o default-clef) "" (ly-clef- o))))

(define-method (ly-default (o <music>) (default <music>))
  (if (equal? o default) "" (ly- o)))

(define-method (ly-default (o <nested-music>) (default <nested-music>))
  (if (equal? o default) '("" . "") (ly- o)))

(define-method (ly (o <property-set>))
  (cond
   ((is-a-clef-glyph? o)
    (ly-default o (make <property-set> :value "clefs.G" :symbol 'clefGlyph)))
   ((is-a-bar-line? o)
    (format #f "\\bar ~S" (.value o)))
   (else "")))

(define-class <property-unset> (<music>)
  (properties :accessor .properties :init-value '(input-tag symbol))
  (symbol :accessor .symbol :init-value #f :init-keyword :symbol))

(define-class <override-property> (<music>)
  (properties :accessor .properties :init-value '(input-tag grob-property-path grob-value pop-first symbol))
  (grob-property-path :accessor .grob-property-path :init-value '() :init-keyword :grob-property-path)
  (grob-value :accessor .grob-value :init-value #f :init-keyword :grob-value)
  (pop-first :accessor .pop-first :init-value #t :init-keyword :pop-first)
  (symbol :accessor .symbol :init-value #f :init-keyword :symbol))

(define-class <revert-property> (<music>)
  (properties :accessor .properties :init-value '(input-tag grob-property-path symbol))
  (grob-property-path :accessor .grob-property-path :init-value '() :init-keyword :grob-property-path)
  (symbol :accessor .symbol :init-value #f :init-keyword :symbol))

(define-class <partial-set> (<music>)
  (properties :accessor .properties :init-value '(input-tag partial-duration))
  (partial-duration :accessor .partial-duration :init-value 0 :init-keyword :partial-duration))

(define-class <time-signature-music> (<music>)
  (properties :accessor .properties :init-value '(input-tag beat-structure denominator numerator))
  (beat-structure :accessor .beat-structure :init-value '() :init-keyword :beat-structure)
  (denominator :accessor .denominator :init-value 4 :init-keyword :denominator)
  (numerator :accessor .numerator :init-value 4 :init-keyword :numerator)
  (style :accessor .style :init-value #f))

(define-method (get-style (o <time-signature-music>))
  (or (and-let* ((context (.style o))
		 (prop (.element context))
		 ((equal? (.grob-property-path prop) '(style))))
		(.grob-value prop))
      'default))

(define-method (set-style (o <time-signature-music>) style)
  (let* ((context (.style o)))
    (if context
	(if (eq? style 'default)
	    (begin
	      (set! (.style o) #f)
	      (remove! context))
	    (set! (.grob-value (.element context)) style))
	(if (not (eq? style 'default))
	    (let ((context (make-time-signature-style style)))
	      (set! (.style o) context)
	      (insert-element! (.parent o) o context))))))

(define-method (prefix-ly (o <time-signature-music>))
  (let ((style (get-style o)))
    (if (eq? style 'default)
	""
	(string-append "\\override Staff.TimeSignature.style = #'" (symbol->string style)))))
  
(define-method (ly- (o <time-signature-music>))
  (format #f "\\time ~d/~d" (.numerator o) (.denominator o)))

(define-method (equal? (o <time-signature-music>) (t <time-signature-music>))
  (and (= (.numerator o) (.numerator t))
       (= (.denominator o) (.denominator t))))
;;       (equal? (get-style o) (get-style t))))

(define-method (ly (o <time-signature-music>))
  (string-append
   (prefix-ly o)
   (ly-default o (make <time-signature-music>))))

(define-class <time-scaled-music> (<music-wrapper>)
  (properties :accessor .properties :init-value '(input-tag denominator numerator element))
  (denominator :accessor .denominator :init-value 4 :init-keyword :denominator)
  (numerator :accessor .numerator :init-value 4 :init-keyword :numerator))

(define-class <relative-octave-music> (<music-wrapper>)
  (properties :accessor .properties :init-value '(input-tag element))
  (pitch :accessor .pitch :init-value #f))

(define-method (ly (o <relative-octave-music>))
  '("\\relative" . ""))

(define-method (ly-dir (o <relative-octave-music>) dir state)
  (if (<0 dir)
      (let ((pitch (cdr state)))
	(if pitch
	    (set! (.pitch o) pitch)
	    (set-cdr! state (make <pitch> :octave -1 :step 3))))
      (if (not (.pitch o))
	  (set-cdr! state #f)
	  (set! (.pitch o) #f)))
  (next-method))

(define-class <grace-music> (<music-wrapper>)
  (properties :accessor .properties :init-value '(input-tag element)))

(define-class <transposed-music> (<music-wrapper>)
  (properties :accessor .properties :init-value '(input-tag element)))

(define-class <unrelativable-music> (<music-wrapper>)
  (properties :accessor .properties :init-value '(input-tag element)))

(define-class <unfolded-repeated-music> (<music-wrapper>)
  (properties :accessor .properties :init-value '(input-tag element repeat-count))
  (repeat-count :accessor .repeat-count :init-value 0 :init-keyword :repeat-count))

(define-class <volta-repeated-music> (<music-wrapper>)
  (properties :accessor .properties :init-value '(input-tag element repeat-count))
  (repeat-count :accessor .repeat-count :init-value 0 :init-keyword :repeat-count))

(define-class <voice-separator> (<music>)
  (properties :accessor .properties :init-value '(input-tag)))

(define-class <lyric-combine-music> (<music-wrapper>)
  (properties :accessor .properties :init-value '(input-tag element associated-context))
  (associated-context :accessor .associated-context :init-value #f :init-keyword :associated-context))

(define-class <bar-check> (<music>)
  (properties :accessor .properties :init-value '(input-tag)))

(define-class <tie-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag)))

(define-method (ly (o <tie-event>))
  "~")

(define (make-tie)
  (make <tie-event>))

(define-method (tie-active? (o <top>)) #f)
(define-method (tie-active? (o <note-event>))
  (and-let* (((is-a-note? o)) ((find (lambda (x) (is-a-tie? x)) (.articulations o)))) #t))

(define-method (tie-possible? (o <top>)) #f)
(define-method (tie-possible? (o <note-event>))
  (and-let* ((next (get-neighbor-note-or-rest o 1))
	      ((is-a-note? next))
	      ((not (eq? o next))))
	    (equal? (.pitch o) (.pitch next))))

(define-class <beam-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag span-direction))
  (span-direction :accessor .span-direction :init-value 0 :init-keyword :span-direction))

(define-class <decrescendo-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag span-direction))
  (span-direction :accessor .span-direction :init-value 0 :init-keyword :span-direction))

(define-class <completize-extender-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag)))

(define-class <crescendo-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag span-direction))
  (span-direction :accessor .span-direction :init-value 0 :init-keyword :span-direction))

(define-class <phrasing-slur-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag span-direction))
  (span-direction :accessor .span-direction :init-value 0 :init-keyword :span-direction))

(define-class <slur-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag span-direction))
  (span-direction :accessor .span-direction :init-value 0 :init-keyword :span-direction))

(define-method (ly (o <slur-event>))
  (if (=-1 (.span-direction o)) "(" ")"))

(define (make-slur dir)
  (make <slur-event> :span-direction dir))

(define-class <text-script-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag direction text))
  (direction :accessor .direction :init-value 0 :init-keyword :direction)
  (text :accessor .text :init-value 0 :init-keyword :text))

(define-class <mark-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag label))
  (label :accessor .label :init-value 0 :init-keyword :label))

(define articulation-alist
  '(
    (#\+ . stopped)
    (#\- . tenuto)
    (#\.  . staccato)
    (#\0 . #{0}#)
    (#\1 . #{1}#)
    (#\2 . #{2}#)
    (#\3 . #{3}#)
    (#\4 . #{4}#)
    (#\5 . #{5}#)
    (#\>  . accent)
    (#\^ . marcato)
    (#\_ . portato)
    (#\| . staccatissimo)
    ))

(define-class <articulation-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag articulation-type tweaks))
  (articulation-type :accessor .articulation-type :init-value 0 :init-keyword :articulation-type))

(define-method (ly (o <articulation-event>))
  (let ((type (.articulation-type o)))
    (string-append (tweaks->postfix-ly o)
		   "-" (or (and-let* ((shortcut (assoc-get (alist-swap articulation-alist)
							   (string->symbol type))))
				     (char->string shortcut))
			   (string-append "\\" type)))))

(define-class <fingering-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag digit tweaks))
  (digit :accessor .digit :init-value 0 :init-keyword :digit))

(define-method (ly (o <fingering-event>))
  (string-append (tweaks->postfix-ly o) "-" (number->string (.digit o))))

(define (make-articulation articulation)
  (let ((a (object->string articulation)))
    (or (and-let* ((digit (string->number (string-drop-prefix a "digit-"))))
		  (make <fingering-event> :digit digit))
	(make <articulation-event> :articulation-type a))))
  
(define-class <absolute-dynamic-event> (<music>)
  (properties :accessor .properties :init-value '(input-tag text))
  (text :accessor .text :init-value 0 :init-keyword :text))

(define-method (ly (o <absolute-dynamic-event>))
  (let ((type (.text o)))
    (string-append (tweaks->postfix-ly o) "-" "\\" type)))

(define (make-dynamic dynamic)
  (let ((t (object->string dynamic)))
    (make <absolute-dynamic-event> :text t)))

(define clef-alist
  '((G  . (clefs.G -2 -6))
    (C . (clefs.C 0 0))
    (C.2 . (clefs.C 2 2))
    (F . (clefs.F 2 6))))

(define (make-clef-glyph glyph)
  (let ((glyph-c (make <context-specced-music> :context-type 'Staff))
	(glyph-p (make <property-set>
		   :value glyph 
		   :symbol 'clefGlyph)))
    (append-element! glyph-c glyph-p)))

(define (make-clef-position position)
  (let ((pos-c (make <context-specced-music> :context-type 'Staff))
	(pos-p (make <property-set>
		 :value position
		 :symbol 'clefPosition)))
    (append-element! pos-c pos-p)))

(define (make-clef-middle-c position)
  (let ((middle-c (make <context-specced-music> :context-type 'Staff))
	(middle-p (make <property-set>
		    :value position
		    :symbol 'middleCPosition)))
    (append-element! middle-c middle-p)))

(define (make-clef m type)
  (let ((glyph (symbol->string (car (assoc-get clef-alist type))))
	(position (cadr (assoc-get clef-alist type)))
	(middle-c-position (caddr (assoc-get clef-alist type))))
    (append-element! m (make-clef-glyph glyph))
    (append-element! m (make-clef-position position))
    (append-element! m (make-clef-middle-c middle-c-position))
    m))

(define (simple-make-clef type)
  (let ((glyph (symbol->string (car (assoc-get clef-alist type)))))
    (make-clef-glyph glyph)))

(define (start-music)
  (let* ((r (make <relative-octave-music>))
	 (v (make <context-specced-music> :context-type 'Voice
		  :context-id "one"))
	 (m (make <sequential-music>))
	 (k (make <key-change-event>))
	 (t (make <time-signature-music>)))
    (append-element! r v)
    (append-element! v m)
    ;;(make-clef m 'G)
    (append-element! m (simple-make-clef 'G)) ;; FIXME: cannot change/set clef without this
    (append-element! m k) ;; FIXME: cannot change/set key without this
    (append-element! m t) ;; FIXME: cannot change/set time without this
    (append-element! m (make <note-event>))
    (append-element! m (make <skip-event>
			 :duration (make <duration> :factor 4)))
    (append-element! m (set-bar-type "|."))
    r))

(define (test-music)
  (let* ((r (make <relative-octave-music>))
	 (v (make <context-specced-music> :context-type 'Voice
		  :context-id "one"))
	 (m (make <sequential-music>))
	 (k (make <key-change-event>
	      :tonic (make <pitch> :step 2 :alteration -1/2)
	      :scale '(0 0 -1/2 0 0 -1/2 -1/2)))
	 (t (make <time-signature-music> :numerator 3 :denominator 4)))
    (append-element! r v)
    (append-element! v m)
    ;;(make-clef m 'G)
    (append-element! m (simple-make-clef 'G))
    (append-element! m k)
    (append-element! m t)
    (append-element! m (make <rest-event>))
    (append-element! m (make <note-event> :pitch (make <pitch> :step 2)))
    (append-element! m (make <note-event> :pitch (make <pitch> :step 3)))
    (append-element! m (make <note-event> :pitch (make <pitch> :step 1)))
    (append-element! m (make <skip-event>
			 :duration (make <duration> :factor 2)))
    (append-element! m (set-bar-type "|."))
    r))

(define (slur-test-music)
  (let* ((r (make <relative-octave-music>))
	 (v (make <context-specced-music> :context-type 'Voice
		  :context-id "one"))
	 (m (make <sequential-music>))
	 (k (make <key-change-event>
	      :tonic (make <pitch> :step 2 :alteration -1/2)
	      :scale '(0 0 -1/2 0 0 -1/2 -1/2)))
	 (t (make <time-signature-music> :numerator 3 :denominator 4))
	 (n1 (make <note-event> :pitch (make <pitch> :step 2)))
	 (n3 (make <note-event> :pitch (make <pitch> :step 1))))
    (append-element! r v)
    (append-element! v m)
    ;;(make-clef m 'G)
    (append-element! m (simple-make-clef 'G))
    (append-element! m k)
    (append-element! m t)
    (append-element! m (make <rest-event>))
    (append-element! m n1)
    (append-element! m (make <note-event> :pitch (make <pitch> :step 3)))
    (append-element! m n3)
    (set! (.articulations n1) (list (make-slur -1)))
    (set! (.articulations n3) (list (make-slur 1)))
    (append-element! m (make <skip-event>
			 :duration (make <duration> :factor 2)))
    (append-element! m (set-bar-type "|."))
    r))

;(set! start-music slur-test-music)

(define-class <apply-context> (<music>)
  (properties :accessor .properties :init-value '(input-tag procedure))
  (procedure :accessor .procedure :init-value #f :init-keyword :procedure))

(define (skip-typesetting value)
  (let ((c (make <context-specced-music> :context-type 'Score))
	(p (make <property-set> :symbol 'skipTypesetting :value value)))
    (append-element! c p)
    c))

(define (set-measure-number value)
  (let ((c (make <context-specced-music> :context-type 'Score))
	(p (make <property-set> :symbol 'currentBarNumber :value value)))
    (append-element! c p)
    c))

(define (set-time-signature-stencil value)
  (let ((c (make <context-specced-music> :context-type 'Staff))
	(p (make <override-property> :symbol 'TimeSignature :grob-property-path (list 'stencil) :grob-value value)))
    (append-element! c p)
    c))

(define (make-time-signature-style style)
  (let ((c (make <context-specced-music> :context-type 'Staff))
	(p (make <override-property> :symbol 'TimeSignature :grob-property-path (list 'style) :grob-value style)))
    (append-element! c p)
    c))

(define (set-bar-type type)
  (let ((c (make <context-specced-music> :context-type 'Timing))
	(p (make <property-set> :symbol 'whichBar :value type)))
    (append-element! c p)
    c))

(define (set-bar-number-visibility value)
  (let ((c (make <context-specced-music> :context-type 'Score))
	(p (make <override-property> :symbol 'BarNumber :grob-property-path (list 'break-visibility) :grob-value value)))
    (append-element! c p)
    c))

(define (set-staff-symbol-width width)
  (let ((c (make <context-specced-music> :context-type 'Score))
	(p (make <override-property> :symbol 'StaffSymbol :grob-property-path (list 'width) :grob-value width)))
    (append-element! c p)
    c))

(define (collect-string-join start children end)
  (smart-string-join (list start (smart-string-join children) end)))

(define-method (walk (o <music>) func state collect)
  (func o 0 state))

(define-method (walk (o <nested-music>) func state collect)
  (collect (func o -1 state)
	   (let loop ((elements (get-elements o)))
	     (if (null? elements)
		 '()
		 (cons (walk (car elements) func state collect)
		       (loop (cdr elements)))))
	   (func o 1 state)))

(define-method (walk (o <music>) func collect)
  (walk o func (cons (make <duration>) #f) collect))

(define-method (find (o <music>) (predicate <procedure>))
  (find-first o predicate))

(define-method (find (o <music>) (music <music>))
  (find-first o (lambda (x) (eq? music x))))

(define-method (ly-dir (o <music>) dir state)
  (ly o state))

(define-method (ly-dir (o <nested-music>) dir state)
  ((if (< dir 0) car cdr) (ly o state)))

(define-method (ly-string (o <music>))
  (walk o ly-dir collect-string-join))

(define (test . args)
  (format #t "test-music:~a\n" (lisp-value (test-music)))
  (format #t "walk-ly: ~a\n" (ly-string (test-music))))
