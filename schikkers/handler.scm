;; JUNKME

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2010--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers handler)
  ;; base
  :use-module (srfi srfi-1)
  :use-module (ice-9 and-let-star)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gtk gdk-event)
  ;; user
  :use-module (schikkers lilypond-socket)
  :use-module (schikkers ly)
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-model)
  :use-module (schikkers paper)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :export (<handler>
	   file-new
	   force-update
	   save-ly
	   save-sy))

(define-class <handler> ()
  (model :accessor .model :init-value #f :init-keyword :model)
  (paper :accessor .paper :init-form (make <paper> :line-width 105 :page-height 148)))

(define-method (force-update (o <handler>))
  #f)

(define-method (set-paper-settings (o <handler>))
  (let ((paper (.paper o)))
    (if paper
	(string-append
	 (format #f "(ly-output-scale ~a)\n" (.output-scale paper))
	 (format #f "(ly-paper-width ~a)\n" (.width paper))
	 (format #f "(ly-paper-height ~a)\n" (.height paper))
	 (format #f "(ly-indent ~a)\n" (.indent paper))
	 (format #f "(ly-left-margin ~a)\n" (.left-margin paper))
	 (format #f "(ly-right-margin ~a)\n" (.right-margin paper))
	 (format #f "(ly-top-margin ~a)\n" (.top-margin paper))
	 (format #f "(ly-bottom-margin ~a)\n" (.bottom-margin paper)))
	"")))

(define-method (copy (o <object>) (source <object>))
  (for-each (lambda (slot)
	      (let ((name (car slot)))
		(slot-set! o name (slot-ref source name))))
	    (class-direct-slots (class-of o))))

(define-method (file-new (o <handler>))
  #f)

(define-method (xxsave-sy (o <handler>) temporary?)
  (stderr "NOT Writing: ~a\n" (.file-name (.model o)))
  #f)

(define-method (save-sy (o <handler>) temporary?)
  (let* ((file-name (.file-name (.model o)))
	 (base (basename (basename file-name ".ly") ".sy"))
	 (sy-name (string-append base ".sy"))
	 (temp-name (string-append "#" sy-name "#")))
    (if temporary?
	(and-let* ((model (.model o))
		   (touched (dirty? (.model o)))
		   (lisp (lisp-value model))
		   (paper-string (set-paper-settings o))
		   (music-string (lisp->string lisp))
		   (string (string-append paper-string music-string)))
		  (dump-file temp-name string))
	(if (file-exists? temp-name)
	    (begin
	      (stderr "Writing: ~a\n" sy-name)
	      (rename-file temp-name sy-name))))))

(define LILYPOND-COMMAND "lilypond")
(define VIEW-COMMAND (if (eq? PLATFORM 'windows) "acrord32" "evince"))
(define-method (render-pdf (o <handler>))
  (and-let* ((ly-name (save-ly o))
	     (pdf-name (string-append (basename ly-name ".gly") ".pdf"))
	     (result (= (system (string-append LILYPOND-COMMAND " " ly-name)) 0)))
	    (if (eq? PLATFORM 'windows)
		(run-with-pipe "r" VIEW-COMMAND pdf-name)
		(system (string-append "ld_library_path= " VIEW-COMMAND " " pdf-name "&")))))
