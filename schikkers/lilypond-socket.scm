;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers lilypond-socket)
  ;; base
  :use-module (ice-9 rdelim)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; user
  :use-module (schikkers misc)
  :export (ask-lilypond))

(define (ask-lilypond expression . count)
  "Send a LISP expression to LilyPond, wait for return value."
  (let* ((sock (socket PF_INET SOCK_STREAM 0))
	 (port 2904)
	 (server (inet-pton AF_INET "127.0.0.1")))
    (catch #t
	   (lambda ()
	     (connect sock AF_INET server port)
	     (send sock expression)
	     (let loop ((line (read-line sock 'concat)) (return '()))
	       (if (or (eof-object? line)
		       (equal? line "<EOF>\n"))
		   (reverse return)
		   (loop (read-line sock 'concat) (cons line return)))))
	   (lambda (key . args)
	     (case key
	       ((system-error)
		(debugf "LilyPond not there yet:~a:~a...\n" key (caddr args))
		(let ((count (if (null? count) 0 (car count))))
		  (if (> count 100)
		      '()
		      (begin
			(usleep (number->integer (* 1000 (expt 1.1 count))))
			(ask-lilypond expression (1+ count))))))
               (else (apply throw key args)))))))
