;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2010--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers music-canvas)
  ;; base
  :use-module (ice-9 receive)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers notation)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  :use-module (schikkers system)
  ;; user-gtk
  :use-module (schikkers gmisc) ;; utf-8
  ;;
  :export (<music-canvas>
	   clear
	   draw-
	   draw-staff
	   draw-background
	   glyph
	   line
	   paper-height
	   paper-margin
	   paper-unit
	   paper-width
	   pixels-per-unit
	   rectangle
	   refresh
	   text
	   translate
	   .drag-origin
	   .edit-grob-callback
	   .nitem-cursor
	   .notation
	   .page-count
	   .pixels-per-unit
	   .select-rect
	   .when-cursor)
  :re-export (activate
	      draw
	      freeze-updates
	      get-paper
	      height
	      path
	      polygon
	      rect
	      scale
	      set-cursor
	      set-paper
	      set-scroll-region
	      thaw-updates
	      width))

(define-class <music-canvas> ()
  (edit-grob-callback :accessor .edit-grob-callback :init-value (lambda (x) #f))
  (page-count :accessor .page-count :init-value 1 :init-keyword :page-count)
  (notation :accessor .notation :init-form (make <notation>))
  (when-cursor :accessor .when-cursor :init-value #f)
  (nitem-cursor :accessor .nitem-cursor :init-value #f)
  (offset :accessor .offset :init-form (list 0 0))
  (pixels-per-unit :accessor .pixels-per-unit :init-value 1)
  
  (drag-origin :accessor .drag-origin :init-value #f)
  (select-rect :accessor .select-rect :init-value #f))



;;; Interface
(define-method (activate (o <music-canvas>)) (pure))
(define-method (clear (o <music-canvas>) (system <system>)) (pure))
(define-method (draw (o <music-canvas>) (nitem <notation-item>)) (pure))
(define-method (draw-staff (o <music-canvas>) (nitem <notation-item>)) (pure))
(define-method (draw-background (o <music-canvas>)) (pure))
(define-method (freeze-updates (o <music-canvas>)) (pure))
(define-method (height (o <music-canvas>)) (pure))
(define-method (refresh (o <music-canvas>)) (pure))
(define-method (translate (o <music-canvas>)) (pure))
(define-method (set-cursor (o <music-canvas>) (nitem <notation-item>)) (pure))
(define-method (set-scroll-region (o <music-canvas>) (paper <paper>) page-count) (pure))
(define-method (thaw-updates (o <music-canvas>)) (pure))
(define-method (width (o <music-canvas>)) (pure))

;;; Stencil primitives
(define-method (line (o <music-canvas>) color thick x1 y1 x2 y2) (pure))
(define-method (path (o <music-canvas>) color path thick filled? cap join) (pure))
(define-method (polygon (o <music-canvas>) color coords blot-diameter filled?) (pure))
(define-method (rectangle (o <notation-item-rectangle>) (canvas <music-canvas>) color breapth width depth height blot-diameter))
(define-method (text (o <music-canvas>) color font size text) (pure))

;;; Generic implementations
(define-method (pixels-per-unit (o <music-canvas>)) (.pixels-per-unit o))

(define-method (paper-unit (o <music-canvas>) value)
  (unit (.paper (.notation o)) value))

(define-method (paper-margin (o <music-canvas>)) (paper-unit o margin))
(define-method (paper-width (o <music-canvas>)) (paper-unit o .width))
(define-method (paper-height (o <music-canvas>)) (paper-unit o .height))

(define-method (scale (o <music-canvas>) step)
  (let* ((factor (expt 1.25 step))
	 (ppu (* (.pixels-per-unit o) factor)))
    (set! (.pixels-per-unit o) ppu)
    ppu))

(define-method (draw (o <music-canvas>) (system <system>))
  (debugf 'canvas "redrawing system: ~a\n" system)
  (let* ((notation (.notation o))
	 (old (get-system notation (.number system))))
    (when (not (eq? old system))
      (if old 
	  (clear o old))
      (add notation system)
      (if (< (.page-count o) (page-count notation))
	  (draw-background o))
      (draw- o system))))

(define-method (draw- (o <music-canvas>) (system <system>))
  (set! (.offset o) (+ (.offset system) (paper-margin o)))
  (if (member 'canvas (debug?)) (draw-staff o system))
  (for-each (lambda (nitem) (draw o nitem)) (.items system)))

(define-method (get-paper (o <music-canvas>))
  (.paper (.notation o)))

(define-method (set-paper (o <music-canvas>) (paper <paper>))
  (set-paper (.notation o) paper))

(define-method (draw (o <notation-item>) (canvas <music-canvas>))
  (set! (.canvas-item o)
	(apply (draw-method o) (cons* o canvas (color o) (draw-args o))))
  (.canvas-item o))

(define-method (draw (o <music-canvas>) (nitem <notation-item>))
  (translate o (draw nitem o) (.offset o) (.offset nitem))
  (activate o nitem))


;;; <notation-item> Stencil primitives
(define-method (draw-method (o <notation-item-glyph>)) glyph)
(define-method (draw-args (o <notation-item-glyph>))
  (cdr (.fields o)))

(define-method (draw-method (o <notation-item-line>)) line)
(define-method (draw-args (o <notation-item-line>))
  (map string->number (cdr (.fields o))))

(define (param->path-element x)
  (or (string->number x) (string->symbol x)))
  
(define-method (draw-method (o <notation-item-path>)) path)
(define-method (draw-args (o <notation-item-path>))
  (let* ((params (cdr (.fields o)))
	 (thick (string->number (car params)))
	 (filled? (equal? (cadr params) "True"))
	 (cap (string->symbol (caddr params)))
	 (join (string->symbol (cadddr params)))
	 (path- (map param->path-element (cddddr params))))
    (list path- thick filled? cap join)))

(define-method (draw-method (o <notation-item-polygon>)) polygon)
(define-method (draw-args (o <notation-item-polygon>))
  (let* ((params (cdr (.fields o)))
	 (blot (string->number (car params)))
	 (filled? (equal? (cadr params) "True"))
	 (coords (map string->number (cddr params))))
    (list coords blot filled?)))

(define-method (draw-method (o <notation-item-rectangle>)) rectangle)
(define-method (draw-args (o <notation-item-rectangle>))
  (map string->number (cdr (.fields o))))

(define-method (draw-method (o <notation-item-text>)) text-)
(define-method (draw-args (o <notation-item-text>))
  (let* ((params (cdr (.fields o)))
	 (text (string-unquote (cadr params)))
	 (font-size-lst (string-split (string-regexp-substitute (string-unquote (car params)) "\\\\040" " ") #\ )))
    (receive (font-lst size-lst) (split-at font-size-lst (1- (length font-size-lst)))
      (let ((font (string-join font-lst " "))
	    (size (string->number (car size-lst))))
	(list font size text)))))

(define-method (glyph (o <notation-item-glyph>) (canvas <music-canvas>) color charcode font-name size name)
  (if (stealth?)
      (text o canvas color "sans" (string->number size) (integer->utf-8-string (char->integer #\x)))
      (text o canvas color (string-unquote font-name) (string->number size) (integer->utf-8-string (string->number charcode)))))

(define-method (text- (o <notation-item>) (canvas <music-canvas>) color font size (char <integer>))
  (text o canvas color font size (integer->utf-8-string char)))

(define-method (text- (o <notation-item>) (canvas <music-canvas>) color font size (str <string>))
  (text o canvas color font size (string->utf-8-string str)))
