;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; it under the terms of the GNU Affero General Public License as

;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers music-tree-model)
  ;; base
  :use-module (ice-9 and-let-star)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gobject)
  :use-module (gnome glib)
  :use-module (gnome gtk)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-model)
  :export (<music-tree-model>))

(define-class <music-tree-model> (<guile-gtk-tree-model> )
  (model :accessor .model :init-form #f :init-keyword :model))

(define-method (on-get-n-columns (o <music-tree-model>))
  2)

(define-method (on-get-column-type (o <music-tree-model>) index)
  <gchararray>)

(define-method (on-get-iter (o <music-tree-model>) path)
  (let loop ((iter (get-music (.model o))) (path (cdr path)))
    (if (null? path)
	iter
	(loop (list-ref (get-elements iter) (car path)) (cdr path)))))

(define-method (on-get-path (o <music-tree-model>) iter)
  (if (.parent iter)
      (append (on-get-path o (.parent iter))
	      (list (index iter)))
      '(0)))

(define-method (on-get-value (o <music-tree-model>) iter n)
  (case n ((0) (name iter)) ((1) (ly-string iter))))

(define-method (on-iter-next (o <music-tree-model>) iter)
  (and-let* ((parent (.parent iter))
	     (next (get-neighbor parent iter 1)))
	    (if (eq? next iter) #f next)))
	
(define-method (on-iter-children (o <music-tree-model>) iter)
  (and-let* ((elements (get-elements iter)))
	    (if (null? elements)
		#f
		(car elements))))

(define-method (on-iter-has-child (o <music-tree-model>) iter)
  (and-let* ((elements (get-elements iter)))
	    (not (null? elements))))

(define-method (on-iter-n-children (o <music-tree-model>) iter)
  (length (get-elements iter)))
	    
(define-method (on-iter-nth-child (o <music-tree-model>) iter n)
  (if iter
      (list-ref (get-elements iter) n)
      (get-music (.model o))))

(define-method (on-iter-parent (o <music-tree-model>) iter)
  (.parent iter))
