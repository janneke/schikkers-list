;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers lilypond-socket-parser)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 receive)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;;
  :use-module (schikkers misc)
  :use-module (schikkers notation)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  :use-module (schikkers system)
  :export (<lilypond-socket-parser>
	    parse))

(define-class <lilypond-socket-parser> ()
  (notation :accessor .notation :init-form (make <notation>) :init-keyword :notation)
  (done :accessor .done :init-value #f)
  (hello :accessor .hello :init-value #f)
  (.system-number :accessor .system-number :init-value 0 :init-keyword :system-number)
  (system :accessor .system :init-value #f)
  (cause :accessor .cause :init-value #f)
  (rect :accessor .rect :init-value #f))

(define-method (make-notation-item (o <lilypond-socket-parser>) offset fields)
  (let* ((command (string->symbol (car fields)))
	 (cause (.cause o))
	 (system-offset (.offset (.system o)))
	 (rect (+ (.rect o) system-offset))
	 (system (.number (.system o))))
    (debugf 'parser "~a make-notation-item fields: ~a ~a\n" (class-name (class-of o)) offset fields)
    (debugf 'mock "(~a ~a)\n" offset fields)
    (case command
      ((draw_round_box) (make <notation-item-rectangle> :offset offset :fields fields :cause cause :rect rect :system system))
      ((drawline)
       (let* ((paper (.paper (.notation o)))
	      (width (unit paper line-width))
	      (nitem (make <notation-item-line> :offset offset :fields fields :cause cause :rect rect :system system)))
	 (notify (.system o) nitem width) ;; FIXME: should pick-up cause: (-1 "StaffSymbol" 
	 nitem))
      ((glyphshow) (make <notation-item-glyph> :offset offset :fields fields :cause cause :rect rect :system system))
      ((path) (make <notation-item-path> :offset offset :fields fields :cause cause :rect rect :system system))
      ((polygon) (make <notation-item-polygon> :offset offset :fields fields :cause cause :rect rect :system system))
      ((utf-8) (make <notation-item-text> :offset offset :fields fields :cause cause :rect rect :system system))
      (else (begin (stderr "NOT HANDLED:~a\n" command) #f)))))

(define-method (handle-hello (o <lilypond-socket-parser>) args)
  (set! (.hello o) args)
  (assert (not (.system o)))
  (debugf "found server: ~a\n" args))

(define-method (handle-paper (o <lilypond-socket-parser>) args)
  (debugf 'parser "found paper: ~a\n" args)
  (debugf 'parser "SYSTEM: ~a\n" (.system-number o))
  
  (if (not (.paper (.notation o)))
      (let* ((values (map string->number args))
	     (init-keywords '(:width :height :left-margin :top-margin :indent :line-width)))
	(set-paper
	 (.notation o)
	 (apply make (cons* <paper>
			    (apply append (zip init-keywords values))))))))

(define-method (handle-system (o <lilypond-socket-parser>) args)
  (debugf 'parser "~a handle-system args: ~a\n" (class-name (class-of o)) args)
  (debugf 'mock ")\n(")
  (let ((notation (.notation o)))
    (debugf 'parser "handle-system system: ~a\n" (.system-number o))
    (let ((system (make <system> :number (.system-number o)
			:box (list->box (map string->number args)))))
      (set! (.system-number o) (1+ (.system-number o)))
      (add (.notation o) system)
      (set! (.system o) system))))

(define-method (handle-cause (o <lilypond-socket-parser>) args)
  (debugf 'cause "cause: ~a\n" args)
  (set! (.cause o) (cons (string->number (car args))
			 (string->symbol (cadr args))))
  (set! (.rect o) (bbox->rect (points->list
			       (map flip-y
				    (list->points
				     (map string->number (cddr args)))))))
  #t)

(define-method (handle-nocause (o <lilypond-socket-parser>) args)
  (set! (.cause o) #f)
  (set! (.rect o) #f)
  #t)

(define-method (handle-at (o <lilypond-socket-parser>) args)
  (receive (offset fields) (split-at args 2)
    (and-let* (((null-is-#f fields))
	       (offset-point (map string->number offset))
	       (item (make-notation-item o offset-point fields)))
	      (add (.system o) item))))

(define-method (handle-default (o <lilypond-socket-parser>) command-name)
  (stderr "not implemented: ~a\n" command-name))

(define lily-command-alist
  `((hello . ,handle-hello)
    (system . ,handle-system)
    (cause . ,handle-cause)
    (nocause . ,handle-nocause)
    (paper . ,handle-paper)
    (at . ,handle-at)))

(define-method (parse-line (o <lilypond-socket-parser>) line)
  (let* ((tokens (string-tokenize line))
	 (command-name (string->symbol (car tokens)))
	 (args (cdr tokens))
	 (command (assoc-get-default lily-command-alist command-name #f)))
    (if command
	(command o args)
	(handle-default o command-name))))

(define-method (parse (o <lilypond-socket-parser>) socket-lines)
  (for-each (lambda (x) (parse-line o x)) socket-lines)
  (.notation o))
