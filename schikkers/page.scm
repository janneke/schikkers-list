;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers page)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  :use-module (schikkers generics)
  ;;
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  :use-module (schikkers system)
  :export (<page>
	   get-system
	   set
	   system-count
	   .paper
	   .systems)
  :re-export (height
	      remove!
	      .items
	      .offset
	      .start))

(define-class <page> ()
  (number :accessor .number :init-value 0 :init-keyword :number)
  (start :accessor .start :init-value 0 :init-keyword :start)
  (offset :accessor .offset :init-form (list 0 0))
  (paper :accessor .paper :init-value #f :init-keyword :paper)
  (systems :accessor .systems :init-form (list)))

(define-method (set (o <page>) (system <system>))
  "Replace or add SYSTEM to page O."
  (remove-placeholder o system)
  (or (replace o system)
      (add o system)))

(define-method (replace (o <page>) (system <system>))
  (and-let* ((index (- (.number system) (.start o)))
	     ((< index (system-count o)))
	     (old (get-system o (.number system))))
	    (replace system old)
	    (list-set! (.systems o) index system)))

(define-method (remove-placeholder (o <page>) (system <system>))
  (and-let* ((index (- (.number system) (.start o)))
	     ((= index (1- (system-count o))))
	     (=0 (height (get-system o (.number system)))))
	    (set! (.systems o) (take (.systems o) index))))

(define-method (add (o <page>) (system <system>))
  "If SYSTEM fits on page O: add it."
  (and-let* ((offset-y (system-offset-y o system)))
	    (set! (.offset system) (+ (.offset o) (list 0.0 offset-y)))
	    (set! (.systems o) (append (.systems o) (list system)))))

(define-method (system-offset-y (o <page>) (system <system>))
  "If SYSTEM fits on page O: return its y-offset, else return #f."
  (and-let* ((paper (.paper o)))
	    (if (=0 (system-count o))
		(unit paper .top-margin)
		(and-let* ((bottom (text-bottom paper))
			   (offset-y (next-system-y o))
			   ((< (+ offset-y (height system)) bottom)))
			  offset-y))))

(define-method (next-system-y (o <page>))
  (and-let* ((paper (.paper o))
	     (last (car (last-pair (.systems o))))
	     (system-space (unit paper .staff-space)))
	    (+ (y (- (.offset last) (.offset o)))
	       (height last)
	       system-space)))

(define-method (text-bottom (o <boolean>)) #f)
(define-method (text-bottom (o <paper>))
  (- (unit o .height) (unit o .bottom-margin)))

(define-method (remove! (o <page>) (system <system>))
  (if (member system (.systems o))
      (set! (.systems o) (filter-out (lambda (x) (eq? x system)) (.systems o)))))

(define-method (height (o <page>))
  (let ((paper (.paper o)))
    (* (.mm-to-unit paper) (.height paper))))

(define-method (system-count (o <page>))
  (length (.systems o)))

(define-method (.items (o <page>))
  (apply append (map .items (.systems o))))

(define-method (get-system (o <page>) (number <integer>))
  (and-let* ((start (.start o))
	     ((>= number start))
	     ((< number (+ start (system-count o)))))
	    ;;(find (lambda (x) (= (.number x) number)) (.systems o))
	    (list-ref (.systems o) (- number start))))

(define-method (get-systems (o <page>))
  (.systems o))
