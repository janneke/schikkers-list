;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2012--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers notation)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;;
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers notation-item)
  :use-module (schikkers page)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  :use-module (schikkers system)
  :export (<notation>
	   find-above
	   find-below
	   find-system
	   find-system-above
	   find-system-below
	   get-system
	   get-systems
	   page-count
	   update
	   .pages
	   .paper)
  :re-export (find
	      find-music
	      remove!
	      set-paper
	      system-count
	      .items))

(define-class <notation> ()
  (paper :accessor .paper :init-value #f :init-keyword :paper)

  (page :accessor .page :init-value #f)
  (start-page :accessor .start-page :init-value 0)
  (pages :accessor .pages :init-form (list)))

(define-method (add-page (o <notation>))
  (debugf 'notation "~a add <page>\n" (class-name (class-of o)))
  (assert (.paper o))
  (let ((page (make <page> :number (page-count o) :start (system-count o) :paper (.paper o))))
    (add- o page)
    (set! (.page o) page)
    page))

(define-method (remove! (o <notation>) (system <system>))
  (for-each (lambda (page) (if (member system (.systems page))
			       (remove! page system)))
	    (.pages o)))

(define-method (set (o <boolean>) (system <system>)) #f)

(define-method (add (o <notation>) (system <system>))
  (debugf 'notation "~a add <system>: ~a\n" (class-name (class-of o)) system)
  (let* ((number (.number system))
	 (page (if (< number (system-count o))
		   (or (get-page system (.pages o)) (.page o))
		   (.page o))))
    (or (set page system) (and (add-page o) (set (.page o) system))))
  (debugf 'notation "~a add <system> count: ~a\n" (class-name (class-of o)) (system-count o)))

(define-method (get-page (o <system>) pages)
  (let loop ((pages pages))
    (if (null? pages)
	#f
	(let* ((page (car pages))
	       (start (.start page))
	       (end (+ start (system-count page))))
	  (if (and (>= (.number o) start)
		   (< (.number o) end))
	      page
	      (loop (cdr pages)))))))

(define-method (add- (o <notation>) (page <page>))
  (debugf 'notation "~a add page no: ~a\n" (class-name (class-of o)) (.number page))
  (if (= (page-count o) 0)
      (set! (.offset page) '(0 0))
      (let* ((last (car (last-pair (.pages o))))
	     (last-offset (.offset last))
	     (last-height (height last))
	     (offset (+ last-offset (list 0 last-height))))
	(debugf 'notation "~a add- last offset: ~a\n" (class-name (class-of o)) last-offset)
	(debugf 'notation "~a add- last height: ~a\n" (class-name (class-of o)) last-height)
	(debugf 'notation "~a add- offset: ~a\n" (class-name (class-of o)) offset)
	(set! (.offset page) offset)))
  (set! (.pages o) (append (.pages o) (list page))))

(define-method (page-count (o <notation>))
  (length (.pages o)))

(define-method (set-paper (o <notation>) (paper <paper>))
  (set! (.paper o) paper))

(define-method (.items (o <notation>))
  (apply append (map .items (.pages o))))

(define-method (find (o <notation>) (music <music>))
  (find o (lambda (x) (eq? (.music x) music))))

(define-method (find (o <boolean>) (predicate <procedure>)) #f)
(define-method (find (o <notation>) (predicate <procedure>))
  (find predicate (.items o)))

(define-method (find-music (o <notation>) (music <music>))
  (and-let* ((nitem (find o music))) (.music nitem)))

(define-method (find-music (o <boolean>) (predicate <procedure>)) #f)
(define-method (find-music (o <notation>) (predicate <procedure>))
  (and-let* ((nitem (find o predicate))) (.music nitem)))

(define-method (item-below? (o <number>))
  (lambda (x) (and-let* (((is-a-note-or-rest? (.music x)))
			 (rect (.rect x)))
			(> (top rect) o))))

(define-method (item-above? (o <number>))
  (lambda (x) (and-let* (((is-a-note-or-rest? (.music x)))
			 (rect (.rect x)))
			(< (bottom rect) o))))

(define-method (find-above (o <notation>) (y <number>))
  (find o (item-above? y)))

(define-method (find-below (o <notation>) (y <number>))
  (find o (item-below? y)))

(define-method (system-above? (o <number>) (paper <paper>))
  (lambda (x) (let ((rect (paper-system-rect x paper)))
		(< (bottom rect) o))))
  
(define-method (system-below? (o <number>) (paper <paper>))
  (lambda (x) (let ((rect (paper-system-rect x paper)))
		(> (top rect) o))))

(define-method (find-system-above (o <notation>) (y <number>))
  (find (system-above? y (.paper o)) (reverse (get-systems o))))

(define-method (find-system-below (o <notation>) (y <number>))
  (find (system-below? y (.paper o)) (get-systems o)))

(define-method (find-system (o <notation>) (p <point>))
  (find (lambda (x) (and (not ((system-above? (y p) (.paper o)) x))
			 (not ((system-below? (y p) (.paper o)) x))))
	(get-systems o)))

(define-method (find (o <notation>) (p <point>))
  (and-let* ((system (find-system o p)))
	    (find system p (.paper o))))

(define-method (get-page (o <notation>) (number <integer>))
  (and-let* (((< number (page-count o))))
	    (list-ref (.pages o) number)))

(define-method (page-count (o <notation>))
  (length (.pages o)))

(define-method (get-systems (o <notation>))
  (apply append (map .systems (.pages o))))

(define-method (system-count (o <notation>))
  (length (get-systems o)))

(define-method (get-system (o <notation>) (number <integer>))
  (and-let* ((systems (null-is-#f (get-systems o)))
	     ((< number (length systems))))
	    (list-ref systems number)))

(define-method (update (o <notation>) (new <notation>))
  (debugf 'notation "~a update: (~a)..(~a)\n" (class-name (class-of o)) (page-count o) (page-count new))
  (debugf 'notation "~a update new paper: ~a\n" (class-name (class-of o)) (.paper new))
  (debugf 'notation "~a update o paper: ~a\n" (class-name (class-of o)) (.paper o))
  (set-paper o (.paper new))
  (for-each (lambda (x) (add o x)) (get-systems new))
  o)
