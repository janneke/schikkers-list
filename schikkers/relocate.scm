;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2011--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers relocate)
  :use-module (ice-9 regex)
  :use-module (schikkers misc)
  :export (relocate))

(define-public (stderr string . rest)
  (apply format (cons (current-error-port) (cons string rest)))
  (force-output (current-error-port)))

(define (debugf string . rest)
  (if (member "--debug" (command-line))
      (apply stderr (cons string rest))))

(define (relocate prefix)
  (define (safe-getenv key default)
    (let ((val (getenv key)))
      (if val val default)))
  (define (reloc-file f)
    (for-each (lambda (line)
		(let ((match (string-match "^([^ ]+) ([^=]+)=(.*?) *$" line)))
		  (if match
		      (let* ((command (match:substring match 1))
			     (var (match:substring match 2))
			     (value (string-regexp-substitute (match:substring match 3)
							      "[$]INSTALLER_PREFIX" prefix)))
			(cond
			 ((or (equal? command "set")
			      (and (equal? command "setfile")
				   (is-file? value))
			      (and (equal? command "setdir")
				   (is-dir? value)))
			  (debugf "setting: ~a -> ~a\n" var value)
			  (setenv var value))
			 ((equal? command "prependdir")
			  (debugf "prepending: ~a -> ~a\n" var value)
			  (setenv var (string-append value PATHSEP (safe-getenv var "-")))))))))
		(read-lines f)))
		    
  (let* ((dir (string-append prefix "/etc/relocate")))
    (if (and (file-exists? dir)
	     (eq? 'directory (stat:type (stat dir))))
	(let ((files (map (lambda (x) (string-join (list dir x) "/"))
			  (directory-files dir))))
	  (for-each reloc-file files))
	(debugf "skipping relocation: no such directory: ~a\n" dir))))
