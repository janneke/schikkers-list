;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers ly-parser)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome glib)
  :use-module (gnome gobject)
  ;; user
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-model)
  ;; user-gtk
  :export (<ly-parser>
	   handle
	   parse))

(define-class <ly-parser> (<gobject>)
  (model :accessor .model :init-value #f :init-keyword :model))

(define-method (handle (o <ly-parser>) (model <music-model>) (cursor <music>) (key <boolean>) (value <string>))
  key)

(define-method (handle (o <ly-parser>) (model <music-model>) (cursor <music>) (key <symbol>) (value <string>))

  (define (integer->note step)
    (let* ((duration (if (is-a? cursor <rhythmic-event>)
			 (expt 2 (.log (.duration cursor)))
			 4)))
      (add-note model cursor duration step '(0 0 0 0 0 0 0 0))))

  (define (char->note c)
    (integer->note (modulo (- (char->integer c) (char->integer #\c)) 7)))

  (define (string->duration s)
    (let* ((d (.duration cursor))
	   (duration (string->number s))
	   (log (duration->log duration)))
      (set-duration model cursor duration (= log (.log d)))))

  (debugf 'parse "handle: ~a, ~a\n" key value)

  (cond ((symbol? key)
	 (case key
	   ((duration) (string->duration value))
	   ((duration-space) (string->duration (substring value 0 1)))
	   ((sharpen) (delta-alteration model cursor 1))
	   ((space) 'space)
	   ((flatten) (delta-alteration model cursor -1 value))
	   ((lower) (delta-octave model cursor -1))
	   ((higher) (delta-octave model cursor 1))
	   ((articulation) (add-articulation
			    model cursor
			    (assoc-get articulation-alist (last (string->list value)))))
	   ((note) (char->note (car (string->list value))))
	   (else (stderr "programming error") #f)))
	((pair? key) key)
	(else (stderr "programming error") #f)))

(define-method (parse (o <ly-parser>) (model <music-model>) (cursor <music>) space? iter text)

  (define parse-tree
    (list
     (list
      (lambda (text)
	(debugf 'parse "space?: ~a\n" space?)
	space?)
      '(("a" "b" "c" "d" "e" "f" "g") . note))
     (list
      (lambda (text)
	(debugf 'parse "not space?: ~a\n" (not space?))
	(not space?))
      (list
       (lambda (text)
	 (is-a-note? cursor))
       '("'" . higher)
       '("," . lower)
       ;;(list ;;;FIXME: looking-at "a..geses"
       '(("1 " "2 " "8 ") . duration-space)
       '(("4" "8" "16" "32" "64" "128") . duration)
       '(("1" "12" "3" "6") . #t)
       (cons
	(lambda (text)
	  (and (=0 (.log (.duration cursor)))
	       (member text '("2" "6") equal?)))
	'(eat . 1))
       (list
	(lambda (text)
	  (let ((alteration (.alteration (.pitch cursor))))
	    (and (>=0 alteration)
		 (< alteration 3/2))))
	'("i" . #t)
	'("is" . sharpen))
       (list
	(lambda (text)
	  (let ((alteration (.alteration (.pitch cursor))))
	    (and (<=0 alteration)
		 (> alteration -3/2))))
	'("e" . #t)
	'("es" . flatten)
	(cons (lambda (text)
		(and (let ((step (.step (.pitch cursor))))
		       (or (= step 2) (= step 5)))
		     (equal? text "s"))) 'flatten)
	'("-" . #t)
	'((
	   "-+"
	   "--"
	   "-."
	   "-0"
	   "-1"
	   "-2"
	   "-3"
	   "-4"
	   "-6"
	   "-<"
	   "-^"
	   "-_"
	   "-|"
	   ) . articulation)
	'(" " . space))))))

  (and-let* ((m (let loop ((tree parse-tree) (path '()))
		  (if (null? tree)
		      (if (null? path) #f
			  (loop (cons (car path) tree) (cdr path)))
		      (let* ((sub-tree (car tree))
			     (head (car sub-tree))
			     (tail (cdr tree)))
			(or (and (cond ((and (char? head)
					     (=1 (string-length text))
					     (eq? (car (string->list text)) head)))
				       ((string? head) (equal? text head))
				       ((list? head) (member text head equal?))
				       ((procedure? head) (head text)))
				 (if (list? sub-tree)
				     (loop (cdr sub-tree) (append tail path))
				     (cdr sub-tree)))
			    (loop tail path)))))))
	    (handle o model cursor (if (procedure? m) (m text) m) text)))
