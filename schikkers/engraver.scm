;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers engraver)
  ;; base
  :use-module (srfi srfi-1)
  :use-module (ice-9 and-let-star)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  ;; user
  :use-module (schikkers music-canvas)
  :use-module (schikkers lilypond-socket)
  :use-module (schikkers lilypond-socket-parser)
  :use-module (schikkers ly)
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-model)
  :use-module (schikkers music-modify)
  :use-module (schikkers notation)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  :use-module (schikkers system)
  :export (<engraver>)
  :re-export (engrave
	      engrave-system
	      get-system))

(define-class <engraver> ()
  (model :accessor .model :init-value #f :init-keyword :model)
  (paper :accessor .paper :init-form #f)

  (notation :accessor .notation :init-form (make <notation>))

  (set-bar-music :accessor .set-bar-music :init-value #f)
  (set-key-music :accessor .set-key-music :init-value #f)
  (set-no-time-signature-music :accessor .set-no-time-signature-music :init-value #f)
  (set-staff-symbol-width-music :accessor .set-staff-symbol-width-music :init-value #f) 
  (set-bar-number-visibility-music :accessor .set-bar-number-visibility-music :init-value #f)
  (set-empty-bar-music :accessor .set-empty-bar-music :init-value #f))

(define-method (initialize (o <engraver>) . initargs)
  (next-method)
  (set! (.paper o) (.paper (.model o))))

(define-method (get-system (o <engraver>) (number <integer>))
  (debugf 'engraver "get-system number: ~a\n" number)
  (let* ((notation (.notation o))
	 (system (get-system notation number)))
    (if system system
	(engrave-system o number))))

(define-method (engrave-system (o <engraver>) (number <integer>))
  (debugf 'engraver "engrave-system\n")
  ;; FIXME  (save-sy o #t)
  (and-let* ((request (system-lily-request o number))
	     ((debugf 'engraver "~a 1engrave-system no: ~a\n" (class-name (class-of o)) number))
	     ((debugf 'engraver-all "~a 2engrave-system request: ~a\n" (class-name (class-of o)) request))
	     (result (null-is-#f (ask-lilypond request)))
	     ((debugf 'engraver-all "~a 3engrave-system result: ~a\n" (class-name (class-of o)) result))
	     ((unsmurf-music o))
	     (model (.model o))
	     ((debugf 'engraver "~a 4engrave-system\n" (class-name (class-of o))))
	     (notation (parse (make <lilypond-socket-parser> :notation (.notation o) :system-number number) result))
	     ((debugf 'engraver "~a 5engrave-system\n" (class-name (class-of o))))
	     (systems (null-is-#f (get-systems notation)))
	     ((debugf 'engraver "~a 6engrave-system systems: ~a\n" (class-name (class-of o)) (length systems)))
	     (items (.items notation)))
	    (for-each (lambda (x) (if (not (.music x)) (set-music x (get-music model x)))) items)
	    notation))

(define-method (engrave (o <engraver>))
  (debugf 'engraver "engrave\n")
  ;; FIXME  (save-sy o #t)
  (and-let* ((request (lily-request o))
	     (result (null-is-#f (ask-lilypond request)))
	     (notation (parse (make <lilypond-socket-parser>) result))
	     (model (.model o))
	     (items (.items notation)))
	    (map (lambda (x) (set-music x (get-music model x))) items)
	    (set! (.notation o) notation)
	    notation))

(define-method (lily-request (o <engraver>))
  (let* ((lisp (lisp-value o))
	 (str (lisp->string lisp))
	 (paper-settings (lisp-value (.paper o)))
	 (paper-with-music (string-append "(begin " paper-settings str ")")))
    (debugf 'music "lily-request lisp: \n~a\n" paper-with-music)
    paper-with-music))

(define-method (measure-number (o <engraver>) (music <music>))
  (let* ((time (find (.model o) is-a-time-signature?))
	 (measure-length (if time (/ (.numerator time) (.denominator time)) 1))
	 (when (.start music)))
    (debugf 'engraver "measure-number ~a --> ~a\n" (/ when measure-length) (floor (/ when measure-length)))
  (inexact->exact (floor (/ when measure-length)))))

(define-method (smurf-music (o <engraver>) (first <boolean>)) #f)
(define-method (smurf-music (o <engraver>) first)
  (debugf 'engraver "smurf-music: ~a (~a)\n" first (.start first))
  (and-let* ((start (.start first))
	     (bar-vis (set-bar-number-visibility 'all-visible))

	     (paper (.paper (.notation o)))
	     (mm-to-unit (.mm-to-unit paper))
	     (indent (.indent paper))
	     (line-width (* mm-to-unit (- (line-width paper) indent)))
	     (system-width (set-staff-symbol-width line-width)))

	    (set! (.start bar-vis) start)
	    (insert-element! first bar-vis)
	    (set! (.set-bar-number-visibility-music o) bar-vis))

;;; nice idea, looks terrible
;;; using ragged-right, the system extends right of the last bar-line
;;; without ragged-right, the last note pushes the bar-line away from
;;; the system
;;;	    (set! (.start system-width) start)
;;;	    (insert-element! first system-width)
;;;	    (set! (.set-staff-symbol-width-music o) system-width)
  
  (and-let* ((start (.start first))
	     ((> start 0))
	     (measure-number (measure-number o first))
	     (empty-bar (set-bar-type ""))
	     (set-bar (set-measure-number (1+ measure-number)))
	     (no-time (set-time-signature-stencil #f)))
	     
	    (set! (.start empty-bar) start)
	    (insert-element! first empty-bar)
	    (set! (.set-empty-bar-music o) empty-bar)

	    (set! (.start set-bar) start)
	    (insert-element! first set-bar)
	    (set! (.set-bar-music o) set-bar)

	    (set! (.start no-time) start)
	    (insert-element! first no-time)
	    (set! (.set-no-time-signature-music o) no-time)))

(define-method (unsmurf-music (o <engraver>))
  (debugf 'engraver "unsmurf-music\n")
  (let ((model (.model o)))
    (and-let* ((set-bar-music (.set-bar-music o))
	       (set-bar (find model (lambda (x) (eq? x set-bar-music)))))
	      (remove! set-bar))
    (and-let* ((set-key-music (.set-key-music o))
	       (set-key (find model (lambda (x) (eq? x set-key-music)))))
	      (remove! set-key))
    (and-let* ((set-no-time-signature-music (.set-no-time-signature-music o))
	       (set-no-time-signature
		(find model (lambda (x) (eq? x set-no-time-signature-music)))))
	      (remove! set-no-time-signature))
    (and-let* ((set-staff-symbol-width-music (.set-staff-symbol-width-music o))
	       (set-staff-symbol-width
		(find model (lambda (x) (eq? x set-staff-symbol-width-music)))))
	      (remove! set-staff-symbol-width))
    (and-let* ((set-bar-number-visibility-music (.set-bar-number-visibility-music o))
	       (set-bar-number-visibility
		(find model (lambda (x) (eq? x set-bar-number-visibility-music)))))
	      (remove! set-bar-number-visibility))
    (and-let* ((set-empty-bar-music (.set-empty-bar-music o))
	       (set-empty-bar
		(find model (lambda (x) (eq? x set-empty-bar-music)))))
	      (remove! set-empty-bar)))
  #t)

(define-method (system-lily-request (o <engraver>) number)
  (debugf 'engraver "system-lily-request: ~a\n" number)
  (and-let* ((notation (.notation o))
	     (last (1- (max (system-count notation) 1)))
	     (lisp (system-lisp-value o number))
	     (str (lisp->string lisp))
	     (paper-settings (lisp-value (.paper o) number last))
	     (paper-with-music
	      (string-append "(begin " paper-settings str ")")))
    (debugf 'music "system-lily-request lisp: \n~a\n" paper-with-music)
    paper-with-music))

(define-method (system-lisp-value (o <engraver>) number)
  (debugf 'engraver "system-lisp-value: ~a\n" number)
  (debugf 'engraver "calling lisp-value: ~a ~a\n"
	   (system-first-element o number)
	   (system-first-element o (1+ number)))
  (lisp-value o (system-first-element o number)
	      (system-first-element o (1+ number))))

(define-method (find-music (o <boolean>) (predicate <procedure>)) #f)

(define-method (system-first-element (o <engraver>) (number <integer>))
  (find-music (get-system (.notation o) number) is-a-note-or-rest?))

(define-method (lisp-value (o <engraver>))
  (smurf-music o (find (.model o) is-a-note-or-rest?))
  (lisp-value (.model o)))

(define-method (lisp-value (o <engraver>) (first <boolean>) next) #f)

(define-method (lisp-value (o <engraver>) (first <music>) (next <boolean>))
  (lisp-value o first (inf)))

(define-method (lisp-value (o <engraver>) (first <music>) (next <music>))
  (lisp-value o first (.start next)))

(define-method (lisp-value (o <engraver>) (first <music>) (next <number>))
  (smurf-music o first)
  (lisp-value (.model o) (.start first) next))

