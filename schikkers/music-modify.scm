;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers music-modify)
  ;; base
  :use-module (ice-9 and-let-star)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  :use-module (schikkers generics)
  ;;
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers point)
  :export (add-articulation
	   add-character
	   add-dynamic
	   add-lyric
	   add-lyrics
	   add-note
	   add-pitch
	   add-rest
	   add-staff
	   add-voice
	   delta-alteration
	   delta-octave
	   drag
	   eat-notes-and-rests
	   fill-rests
	   lyric-placeholder
	   remove-character
	   remove-rhythmic-event
	   remove-rhythmic-event-and-backward
	   replace-by-rest
	   set-articulation
	   set-clef
	   set-dots
	   set-dynamic
	   set-octave
	   set-pitch
	   set-relative-pitch
	   set-rest
	   set-slur
	   set-tie
	   set-time)
  :re-export (add-rhythmic-event
	      set-duration
	      set-key))

(define (create-note- log dots factor octave step key)
  (make <note-event>
    :duration (make <duration>
		 :log log :dots dots :factor factor)
    :pitch (make <pitch>
	     :step step
	     :alteration (list-ref key step)
	     :octave octave)))

(define-method (create-note (o <note-event>) log dots factor step key)
  (let* ((pitch (.pitch o))
	 (delta (- step (.step pitch)))
	 (octave (if (> (abs delta) 3) (- (sign delta)) 0)))
    (create-note- log dots factor (+ octave (.octave pitch)) step key)))

(define-method (create-note (o <note-event>) step key)
  (let ((d (.duration o)))
    (create-note- o (.log d) (.dots d) (.factor d) step key)))

(define (dunno object str)
  (stderr "Don't know how to ~s: ~a\n" str object)
  #f)

(define-method (add-articulation (o <music>) articulation)
  (dunno o "add-articulation"))

(define-method (add-articulation (o <rhythmic-event>) articulation)
  (if (eq? articulation 'clear)
      (set-articulation o articulation)
      (set! (.articulations o)
	    (append (.articulations o) (list (make-articulation articulation)))))
  o)

(define-method (add-character (o <music>) c)
  (dunno o "add-character"))

(define-method (add-character (o <lyric-event>) c)
  (let ((text (.text o))
	(s (integer->string c)))
    (set! (.text o) (if (eq? text lyric-placeholder)
			      s
			      (string-append text s)))
    o))

(define-method (add-dynamic (o <music>) dynamic)
  (dunno o "add-dynamic"))

(define-method (add-dynamic (o <rhythmic-event>) dynamic)
  (if (eq? dynamic 'clear)
      (set-dynamic o dynamic)
      (set! (.articulations o)
	    (append (.articulations o) (list (make-dynamic dynamic)))))
  o)

(define-method (add-context (o <rhythmic-event>) context-type music)
  (let* ((e (.parent o))
	 (m (if (is-a? e <event-chord>) (.parent e) e))
	 (p (.parent m))
	 (x (let loop ((m m) (p (.parent m)))
	      (if (or (not p)
		      (is-a? p <simultaneous-music>)
		      (is-a? p <sequential-music>))
		  m
		  (loop p (.parent p)))))
	 (v (make <simultaneous-music>))
	 (c (make <context-specced-music>
	      :context-type context-type
	      :context-id (format #f "~a" v))))
    (debugf 'add "adding ~a to parent: ~a\n" context-type p)
    (if p
	(if (and (or (is-a? m <simultaneous-music>)
		     (is-a? m <sequential-music>))
		 (not (is-a? p <simultaneous-music>))
		 (not (is-a? p <sequential-music>)))
	    (let ((d (remove! m)))
	      (append-element! p v)
	      (append-element! v d))
	    (insert-element! m v))
	  (append-element! v m))
    (debugf 'modify "append elt, parent: ~a\n" p)
    (append-element! v (append-element! c music))
    v))

(define-method (add-lyrics (o <music>))
  (dunno o "add-lyrics"))

(define-method (add-lyrics (o <rhythmic-event>))
  (let* ((l (make <lyric-combine-music> :associated-context "one"))
	 (s (make <sequential-music>))
	 (r (make <lyric-event> :text lyric-placeholder))
	 (m (append-element! l (append-element! s r))))
    (cons (add-context o 'Lyrics m) r)))

(define-method (add-staff (o <music>))
  (dunno o "add-staff"))

(define-method (add-staff (o <rhythmic-event>))
  (let* ((s (make <sequential-music>))
	 (r (make <rest-event>))
	 (m (append-element! s r)))
    (cons (add-context o 'Staff m) r)))

(define-method (add-voice (o <music>))
  (dunno o "add-voice"))

(define-method (add-voice (o <rhythmic-event>))
  (let* ((s (make <sequential-music>))
	 (r (make <rest-event>))
	 (m (append-element! s r)))
    (cons (add-context o 'Voice m) r)))

;;(define lyric-placeholder (integer->utf-8-string #x2610))
(define lyric-placeholder (char->string #\.))

(define-method (add-lyric (o <music>))
  (dunno o "add-lyric"))

(define-method (add-lyric (o <lyric-event>))
  (let ((t (make <lyric-event> :text lyric-placeholder))
	(m (.parent o)))
    (if (is-a? m <event-chord>)
	(set! m (.parent m)))
    (debugf 'modify "\n")
    (debugf 'modify "add-lyric o: ~a\n" o)
    (debugf 'modify "add-lyric o' parent: ~a\n" (.parent o))
    (append-element! m t)
    t))

(define-method (add-note (o <music>) duration octave step key)
  (debugf 'modify "<music> add-note: ~a ~a ~a\n" duration octave step)
  (let ((m (.parent o))
	(n (create-note- (duration->log duration) 0 1 octave step key)))
    (if (is-a? m <event-chord>)
	(set! m (.parent m)))
    (append-element! m o n)
    n))

(define-method (add-note (o <music>) duration step key)
  (debugf 'modify "<music> add-relative-note: ~a ~a\n" duration step)
  (let ((m (.parent o))
	(n (create-note o (duration->log duration) 0 1 step key)))
    (if (is-a? m <event-chord>)
	(set! m (.parent m)))
    (append-element! m o n)
    n))

(define-method (add-pitch (o <music>) step key)
  (dunno o "add-pitch"))

(define-method (add-pitch (o <music>) step key)
  (debugf 'modify "<music> add-pitch: ~a\n" step)
  (add-pitch (.parent o) (create-note o step key) o))

(define-method (add-pitch (o <event-chord>) (note <note-event>) . rest)
  (debugf 'modify "<event-chord> add-pitch: ~a\n" note)
  (append-element! o note)
  note)

(define-method (add-pitch (o <simultaneous-music>) (note <note-event>) .rest)
  (debugf 'modify "<simultaneous-music> add-pitch: ~a\n" note)
  (append-element! o note)
  note)

(define-method (add-pitch (o <sequential-music>) (note <note-event>) (to <music>))
  (debugf 'modify "<sequential-music> add-pitch: ~a to: ~a\n" note to)
  (let* ((e (make <event-chord>))
	 (xx (debugf 'modify "replacing...\n"))
	 (r (replace-element! to e)))
    (debugf 'modify "replacing...\n")
    (append-element! e to)
    (append-element! e note)
    note))

(define-method (add-rest (o <music>) duration)
  (let ((m (.parent o))
	;;;(r (make <rest-event> :duration (.duration o))))
	(r (make <rest-event> :duration (make <duration>
					  :log (duration->log duration)))))
    (if (is-a? m <event-chord>)
	(set! m (.parent m)))
    (append-element! m o r)
    r))

(define-method (add-rhythmic-event (o <music>) duration)
  (dunno o "add-rhythmic-event"))

(define-method (add-rhythmic-event (o <rhythmic-event>) duration)
  (let ((s (shallow-clone o))
	(m (.parent o)))
    (if (is-a? m <event-chord>)
	(set! m (.parent m)))
    (set! (.parent s) #f)
    (debugf 'modify "\n")
    (debugf 'modify "add-rhythmic-event o: ~a\n" o)
    (debugf 'modify "add-rhythmic-event o' parent: ~a\n" (.parent o))
    (let ((n (deep-clone s)))
      ;; what about dots, articulation, fingering?
      (set! (.tweaks n) '())
      (set! (.articulations n) (filter (negate is-a-slur-or-tie?) (.articulations n)))
      (append-element! o n)
      (set-duration n duration)
      n)))

(define-method (delta-alteration (o <music>) (dir <integer>) . rest)
  (dunno o "delta-alteration"))

(define-method (delta-alteration (o <note-event>) (dir <integer>))
  (let* ((pitch (.pitch o))
	 (alteration (.alteration pitch)))
    (if (or (< (abs alteration) 1)
	    (and (= (abs alteration) 1)
		 (!= dir (sign alteration))))
	(begin
	  (set! (.alteration pitch) (+ (* dir 1/2) alteration))
	  (if (and (!=-1 (.alteration pitch))
		   (not (and (= (.alteration pitch) -1/2)
			     (=1 dir))))
	      (set! (.aesees pitch) #f))
	  o)
	#f)))

(define-method (delta-alteration (o <note-event>) (dir <integer>) (spelling <string>))
  (let* ((pitch (.pitch o))
	 (ae (.aesees pitch))
	 (e (string-prefix? "e" spelling))
	 (r (delta-alteration o dir)))
    (if (and (= (.alteration pitch) -1/2)
	     (=-1 dir))
	(set! (.aesees pitch) e))
    (if (!= ae (.aesees pitch))
	o
	r)))

(define-method (delta-octave (o <music>) (dir <integer>))
  (dunno o "delta-octave"))

(define-method (delta-octave (o <note-event>) (dir <integer>))
  (set-octave o (+ (.octave (.pitch o)) dir)))

(define-method (drag (o <music>) (point <point>))
  (dunno o "drag"))

(define-method (drag (o <articulation-event>) (point <point>))
  (drag- o point))

(define-method (drag (o <fingering-event>) (point <point>))
  (drag- o point))

(define-method (drag (o <rhythmic-event>) (point <point>))
  (drag- o point))

(define-method (drag- (o <music>) (point <point>))
  (debugf 'modify "<music> drag-: ~a\n" point)
  (let* ((origin (flip-y (pair->point (assoc-get-default (.tweaks o) 'extra-offset '(0 . 0)))))
	 (offset (flip-y (+ origin point))))
    (if (origin? offset)
	(set! (.tweaks o) '())
	(set! (.tweaks o) (assoc-set! (.tweaks o) 'extra-offset (point->pair offset)))))
  o)

(define-method (eat-notes-and-rests (o <music>))
  (dunno o "eat-notes-and-rests"))

(define-method (eat-notes-and-rests (o <rhythmic-event>) (d <number>))
  (debugf 'eat "must eat: ~a\n" d)
  (let loop ((d d) (cursor o))
    (debugf 'eat "loop cursor: ~a ~a\n" cursor (.duration cursor))
    (debugf 'eat "loop cursor: remaining: ~a\n" d)
    (let ((next (get-neighbor-note-or-rest cursor 1)))
      (debugf 'eat "found NEXT: ~a\n" next)
      (if (=0 d)
	  #t
	  (let ((n (duration->number cursor)))
	    (debugf 'eat "cursor n:~a\n" (duration->number cursor))
	    (if (<= n d)
		(let ((rest (- d n)))
		  (when (not (eq? o cursor))
			(debugf 'eat "removing cursor:\n")
			(remove! cursor))
		  (if (eq? cursor next)
		      #t
		      (loop rest next)))
		(set-duration-from-number cursor (- n d)))))))
  o)

(define-method (fill-rests (o <music>))
  (dunno o "fill-rests"))

(define-method (fill-rests (o <rhythmic-event>) (d <number>))
  (debugf 'eat "fill-rests: ~a\n" d)
  (let ((next (get-neighbor-note-or-rest o 1)))
    (if (or (= 0 d)
	    (eq? next o))
	#f
	(let ((r (add-rest o 4)))
	  (set-duration-from-number r d)
	  r))))

(define-method (remove-character (o <music>))
  (dunno o "remove-character"))

(define-method (remove-character (o <lyric-event>))
  (let* ((text (.text o))
	 (length (string-length text)))
    (set! (.text o)
	       (if (= length 1)
		   lyric-placeholder
		   (substring text 0 (1- length))))
    o))

(define-method (remove-rhythmic-event (o <music>))
  (dunno o "remove-rhythmic-event"))

(define-method (remove-rhythmic-event (o <rhythmic-event>))
  (debugf 'modify "removing-rythmic-event <rhythmic-event>\n")
  (let ((m (.parent o))
	(p (get-neighbor o -1)))
    (if (eq? p o)
	(begin
	  (stderr "remove-rhythmic-event: no prev: ~a\n" o)
	  o)
	(if (is-a? m <event-chord>)
	    (remove! m)
	    (remove! o)))))

(define-method (remove-rhythmic-event-and-backward (o <rhythmic-event>))
  (debugf 'modify "remove-rhythmic-event-and-backward <rhythmic-event>\n")
  (let ((prev (get-neighbor o -1)))
    (remove-rhythmic-event o)
    prev))

(define-method (replace-by-rest (o <music>))
  (dunno o "replace-by-rest"))

(define-method (replace-by-rest (o <note-event>))
  (debugf 'modify "replace-by-rest <note-event>\n")
  (let ((r (make <rest-event> :start (.start o) :duration (.duration o))))
    (replace-element! o r)
    r))

(define-method (set-articulation (o <music>) articulation)
  (dunno o "set-articulation"))

(define-method (set-articulation (o <rhythmic-event>) articulation)
  (set! (.articulations o)
	(if (eq? articulation 'clear)
	    '()
	    (list (make-articulation articulation))))
  o)

(define-method (set-clef (o <music>) glyph pos middle-c)
  (dunno o "set-clef"))

(define-method (set-clef (o <property-set>) glyph pos middle-c)
  (if (is-a-clef-glyph? o)
      (let* ((context (.parent o))
	     (music (.parent context))
	     (pos-property
	      (find music (lambda (x) (is-a-clef-position? x))))
	     (middle-c-property
	      (find music (lambda (x) (is-a-clef-middle-c? x))))
	     (glyph-changed?
	      (and glyph (not (equal? glyph (.value o)))))
	     (pos-changed?
	      (and pos
		   (or (not pos-property)
		       (not (equal? pos (.value pos-property))))))
	     (middle-c-changed?
	      (and middle-c
		   (or (not middle-c-property)
		       (not (equal?
			     middle-c
			     (.value middle-c-property)))))))
	(if glyph-changed?
	    (set! (.value o) (object->string glyph)))
	(if pos-changed?
	    (if pos-property
		(set! (.value pos-property) pos)
		(append-element! music context
				 (make-clef-position pos))))
	(if middle-c-changed?
	    (if middle-c-property
		(set! (.value middle-c-property) middle-c)
		(append-element!
		 music
		 (or (and pos-property (.parent pos-property))
		     context)
		 (make-clef-middle-c middle-c))))
	(and (or glyph-changed? pos-changed? middle-c-changed?) o))
      #f))


(define-method (set-dynamic (o <music>) dynamic)
  (dunno o "set-dynamic"))

(define-method (set-dynamic (o <rhythmic-event>) dynamic)
  (set! (.articulations o)
	(if (eq? dynamic 'clear)
	    '()
	    (list (make-dynamic dynamic))))
  o)

(define-method (set-key (o <music>) name)
  (dunno o "set-key"))

(define-method (set-key (o <key-change-event>) name)
  (debugf 'modify "<key-change-event> set-key: ~a\n" name)
  (describe name)
  (debugf 'modify "details: ~a\n" (assoc-get key-alist name))
  (and-let* ((details (assoc-get key-alist name))
	     (tonic (car details))
	     (alteration (cadr details))
	     (scale (caddr details))
	     ((or (!= tonic (.step (.tonic o)))
		  (!= alteration (.alteration (.tonic o)))
		  (not (equal? scale (.scale o))))))
	    (debugf 'modify "key: ~a ~a ~a\n" tonic alteration scale)
	    (set! (.step (.tonic o)) tonic)
	    (set! (.alteration (.tonic o)) alteration)
	    (set! (.scale o) scale)
	    o))

(define-method (set-dots (o <music>) dots)
  (dunno o "set-dots"))

(define-method (set-dots (o <rhythmic-event>) dots)
  (debugf 'modify "<rhythmic-event> set-dots: ~a\n" dots)
  (and-let* ((duration (.duration o))
	     ((!= dots (.dots duration))))
	    (set! (.dots duration) dots)
	    o))

(define-method (set-duration (o <music>) (duration <integer>) . rest)
  (dunno o "set-duration"))

(define-method (set-duration (o <rhythmic-event>) (duration <boolean>)) #f)

(define-method (set-duration (o <rhythmic-event>) (duration <integer>))
  (debugf 'modify "<rhythmic-event> set-duration: ~a\n" duration)
  (and-let* ((log (duration->log duration))
	     (duration (.duration o))
	     ((!= log (.log duration))))
	    (set! (.log duration) log)
	    (set! (.factor duration) 1)
	    (set! (.dots duration) 0)
	    (set! (.explicit duration) #f)
	    o))

(define-method (set-duration (o <rhythmic-event>) (duration <integer>) (explicit <boolean>))
  (and-let* ((d (.duration o))
	     ((or (set-duration o duration) (!= explicit (.explicit d)))))
	    (set! (.explicit d) explicit)
	    o))

(define-method (set-duration-from-number (o <music>) (n <number>))
  (dunno o "set-duration-from-number"))

(define-method (set-duration-from-number (o <rhythmic-event>) (n <number>))
  (debugf 'eat "set-duration-from-number: ~a ~a\n" o n)
  (let* ((ldf (let loop ((log 0))
		(let* ((d (expt 2 log))
		       (dn (/ 1 d)))
		  (debugf 'eat "log, dn: ~a ~a\n" log dn)
		  (if (> log 7)
		      (list log 0 (/ n dn))
		      (cond
		       ((= n (* dn 1.75))
			(debugf 'eat "n = *1.75: ~a\n"  (* dn 1.75))
			(list log 2 1))
		       ((= n (* dn 1.5))
			(debugf 'eat "n = *1.5: ~a\n"  (* dn 1.5))
			(list log 1 1))
		       ((> n dn)
			(debugf 'eat "> n ~a\n"  dn)
			 (list log 0 (/ n dn)))
		       ((= n dn)
			(debugf 'eat "n = dn: ~a\n"  dn)
			(list log 0 1))
		       (else (loop (+ log 1))))))))
	 (d (.duration o)))
    (debugf 'eat "set-duration-from-number --> ~a\n" ldf)
    (set! (.log d) (car ldf))
    (set! (.dots d) (cadr ldf))
    (set! (.factor d) (caddr ldf)))
  o)

(define-method (set-octave (o <music>) (octave <integer>))
  (dunno o "set-octave"))

(define-method (set-octave (o <note-event>) (octave <integer>))
  (debugf 'modify "<note-event> set-octave\n")
  (debugf 'modify "<note-event> set-octave parent: ~a\n" (.parent o))
  (and-let* ((pitch (.pitch o))
	     ((!= octave (.octave pitch))))
	    (set! (.octave pitch) octave)
	    o))

(define-method (set-octave (o <rest-event>) (octave <integer>))
  (debugf 'modify "<rest-event> set-octave\n")
  (debugf 'modify "<rest-event> set-octave parent: ~a\n" (.parent o))
  (let ((n (set-pitch o 0 (.scale (make <key-change-event>)))))
    (set-octave n octave)
    n))

(define-method (set-pitch (o <music>) (step <integer>) key)
  (dunno o "set-pitch"))

(define-method (set-pitch (o <note-event>) (step <integer>) key)
  (debugf 'modify "<note-event> set-pitch\n")
  (debugf 'modify "<note-event> set-pitch parent: ~a\n" (.parent o))
  (let* ((pitch (.pitch o))
	 (c-step (.step pitch)))
    (if (!= step c-step)
	(begin
	  (set! (.step pitch) step)
	  (set! (.alteration pitch) (list-ref key step))
	  o)
	#f)))

(define-method (set-pitch (o <rest-event>) (step <integer>) key)
  (debugf 'modify "<rest-event> set-pitch\n")
  (debugf 'modify "<rest-event> set-pitch parent: ~a\n" (.parent o))
  (let ((m (.parent o))
	(n (make <note-event>
	     :start (.start o)
	     :duration (.duration o)
	     :pitch (make <pitch> :step step :alteration (list-ref key step)))))
    (append-element! m o n)
    (remove! o)
    n))

(define-method (set-relative-pitch (o <music>) (step <integer>) key)
  (dunno o "set-relative-pitch"))

(define-method (set-relative-pitch (o <note-event>) (step <integer>) key)
  (debugf 'modify "<note-event> set-relative-pitch\n")
  (and-let* ((pitch (.pitch o))
	     (delta (- step (.step pitch)))
	     (octave (.octave pitch))
	     ((!=0 delta)))
	    (set! (.octave pitch) (+ octave (if (> (abs delta) 3) (- (sign delta)) 0)))
	    (set! (.step pitch) step)
	    (set! (.alteration pitch) (list-ref key step))
	    o))

(define-method (set-relative-pitch (o <rest-event>) (step <integer>) key)
  (debugf 'modify "<rest-event> set-relative-pitch\n")
  (set-pitch o step key))

(define-method (set-rest (o <music>) (r <symbol>))
  (dunno o "set-rest"))

(define-method (set-rest (o <note-event>) (r <symbol>))
  (let* ((r (case r
	      ((rest) (make <rest-event>
			:start (.start o)
			:duration (.duration o)))
	      ((skip) (make <skip-event>
			:start (.start o)
			:duration (.duration o))))))
    (replace-element! o r)
    r))

(define-method (set-rest (o <event-chord>) (r <symbol>))
  (let* ((r (case r
	      ((rest) (make <rest-event>
			:start (.start o)
			:duration (.duration o)))
	      ((skip) (make <skip-event>
			:start (.start o)
			:duration (.duration o))))))
    (replace-element! o r)
    r))

(define-method (set-slur (o <music>) (dir <integer>))
  (dunno o "slur"))

(define-method (set-slur (o <note-event>) (dir <integer>))
  (set! (.articulations o)
	(append (.articulations o) (list (make-slur dir))))
  o)

(define-method (set-tie (o <music>) (tie? <boolean>))
  (dunno o "tie"))

(define-method (set-tie (o <note-event>) (tie? <boolean>))
  (if tie?
      (set! (.articulations o)
	    (append (.articulations o) (list (make-tie))))
      (set! (.articulations o) (filter (negate is-a-tie?) (.articulations o))))
  o)

(define-method (set-time (o <music>) (n <integer>) (d <integer>) numeric?)
  (dunno o "set-time"))

(define-method (set-time (o <time-signature-music>) (n <integer>) (d <integer>) numeric?)
  (debugf 'modify "<time-signature-music> set-time: ~a/~a (~a)\n" n d numeric?)
  (debugf 'time "<time-signature-music> style: ~a" (get-style o))
  (and-let* ((style (if numeric? 'numbered 'default))
	     ((or (!= n (.numerator o))
		  (!= d (.denominator o))
		  (not (eq? style (get-style o))))))
	    (debugf 'modify "time: ~a ~a ~a\n" n d style)
	    (set! (.numerator o) n)
	    (set! (.denominator o) d)
	    (if (not (eq? style (get-style o)))
		(set-style o style))
	    o))

