;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers music-layout)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 threads)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (oop goops describe)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome gw gdk)
  ;; user
  :use-module (schikkers music-canvas)
  :use-module (schikkers engraver)
  :use-module (schikkers misc)
  :use-module (schikkers music-model)
  :use-module (schikkers music)
  :use-module (schikkers notation)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  :use-module (schikkers system)
  ;;
  :export (<music-layout>
	   beyond-music-end?
	   get-location
	   get-nitems
	   is-valid?
	   last-rect
	   last-system
	   move-cursor-to-y-range
	   point->pos
	   validate-y-range
	   .canvas)
  :re-export (draw
	      get-paper
	      get-system
	      invalidate
	      page-count
	      refresh
	      set-cursor
	      set-model
	      set-paper))

(define-class <music-layout> ()
  (model :accessor .model :init-value #f :init-keyword :model)
  (canvas :accessor .canvas :init-value #f :init-keyword :canvas)
  (engraver :accessor .engraver :init-value #f :init-keyword :engraver)

  (get-systems-request :accessor .get-systems-request :init-value #f)
  (notation :accessor .notation :init-value #f))

(define-method (initialize (o <music-layout>) . initargs)
  (next-method)
  (set-model o (.model o))
  o)

(define-method (clear (o <system>))
  (set! (.box o) '((0 0) (0 0)))
  (set! (.items o) '())
  (set! (.invalid o) #f)
  (debugf 'layout "height now: ~a\n" (height o)))

(define-method (invalidate (o <music-layout>) (music <music>))
  (debugf 'layout "invalidate music: ~a (~a)\n" music (.start music))
  (if (or (is-a-clef-glyph? music)
	  (is-a-key-change-event? music)
	  (is-a-time-signature? music))
      (set! (.notation o) #f))
  (and-let* ((notation (.notation o))
	     (nitem (find notation music))
	     (number (.system nitem))
	     (system (get-system notation number)))
	    (debugf 'layout "invalidate system: ~a\n" system)
	    (set! (.invalid system) #t)
	    #t))

(define-method (is-valid? (o <music-layout>))
  (not (dirty? (.model o))))

(define-method (page-count (o <boolean>)) 0)

(define-method (page-count (o <music-layout>))
  (page-count (.notation o)))

(define-method (get-paper (o <music-layout>))
  (get-paper (.canvas o)))

(define-method (refresh (o <music-layout>))
;;  (refresh (.canvas o))
;;  (set-paper (.canvas o) (.paper (.model o))))
;;  (set! (.notation o) #f)
  (for-each (lambda (x) (set! (.invalid x) #t))
	    (get-systems (.notation o))))

(define-method (set-paper (o <boolean>) (paper <paper>)))

(define-method (set-paper (o <music-layout>) (paper <paper>))
  (set-paper (.notation o) paper)
  (set-paper (.canvas o) paper))

(define-method (set-model (o <music-layout>) (model <boolean>)))
(define-method (set-model (o <music-layout>) (model <music-model>))
  (when (not (eq? (.model o) model))
    (set! (.model o) model)
    (set! (.engraver o) (make <engraver> :model model :paper (.paper model)))))

(define-method (draw (o <music-layout>) (top <number>) (bottom <number>))
  (debugf 'layout "~a draw top bottom ~a..~a\n" (class-name (class-of o)) top bottom)
  (and-let* (((>0 (- bottom top)))
	     (systems (get-systems o top bottom)))
	    (debugf 'layout "draw found systems: ~a\n" systems)
	    (debugf 'layout "height of last: ~a\n" (if (null? systems) "none" (height (car (last-pair systems)))))
	    (freeze-updates (.canvas o))
	    (for-each (lambda (x) (draw (.canvas o) x)) systems)
	    (if (not (null? systems))
		(let ((last (last systems)))
		  (if (=0 (height last)) (remove! (.notation o) last))))
	    (thaw-updates (.canvas o))))

(define-method (draw (o <music-layout>) (rect <rect>))
  (draw o (y rect) (bottom rect)))

(define-method (set-cursor (o <music-layout>) (cursor <music>))
  (debugf 'layout "~a set-cursor: systems: ~a\n" (class-name (class-of o)) (get-systems (.notation o)))
  (and-let* ((notation (.notation o))
	     (nitem (find notation cursor))
	     ((debugf 'layout "set-cursor: ~a --> ~a\n" cursor (find (.notation o) cursor)))
	     (notation (.notation (.canvas o)))
	     (system (get-system notation (.system nitem))))
	    (set-cursor (.canvas o) nitem)
	    nitem))

(define-method (move-cursor-to-y-range (o <music-layout>) (cursor <music>)
				       (top <number>) (bot <number>))
  (debugf 'cursor "~a move-cursor-to-y-range: ~a..~a\n" (class-name (class-of o)) top bot)
  (and-let* ((rect (or (get-location o cursor) #(-1 -1 -1 -1)))
	     ((debugf 'cursor "1move-cursor-to-y-range: rect: ~a\n" rect))
	     (notation (.notation o))
	     (system (cond
		     ((< (y rect) top)
		      (debugf 'cursor "1b find below top (~a): system: ~a\n" top (find-system-below notation top))
		      (find-system-below notation top))
		     ((> (bottom rect) bot)
		      (debugf 'cursor "1c find above bot (~a): system: ~a\n" bot (find-system-above notation bot))
		       (find-system-above notation bot))
		     (else 
		      (stderr "no system found in range: ~a..~a\n" top bot)
		      (stderr "already in range?: ~a\n" rect)
		      (set-cursor o cursor)
		      #f)))
	     ((debugf 'cursor "1dmove-cursor-to-y-range: system: ~a\n" system))
	     (nitem (find-music-item system is-a-note-or-rest?))
	     ((debugf 'cursor "2move-cursor-to-y-range: nitem: ~a\n" nitem))
	     (music (.music nitem)))
	    (debugf 'cursor "3move-cursor-to-y-range: music: ~a\n" music)
	    (set-cursor (.canvas o) nitem)
	    music))

(define-method (validate-y-range (o <music-layout>) (rect <rect>))
  (validate-y-range o (top rect) (+ (top rect) (height rect))))

(define-method (validate-y-range (o <music-layout>)
				 (top <number>) (bottom <number>))
  (debugf 'layout "~a validate-y-range: ~a..~a\n" (class-name (class-of o)) top bottom)
  (draw o top bottom))

(define-method (has-a-note-or-rest? (o <notation-item>))
  (and-let* ((music (.music o))) (is-a-note-or-rest? music)))

(define-method (find (o <boolean>) (p <point>)) #f)

(define-method (get-nitems (o <music-layout>) (p <point>))
  (and-let* ((nitems (null-is-#f (find (.notation o) p))))
	    (null-is-#f (filter has-a-note-or-rest? nitems))))

(define-method (get-music (o <music-layout>) (p <point>))
  (and-let* ((nitems (get-nitems o p))
	     (cursor (car nitems)))
	    (.music cursor)))

(define-method (get-location (o <music-layout>) (music <music>))
  (and-let* ((notation (.notation o))
	     (notation-item (find notation music)))
	    (.rect notation-item)))

(define-method (get-system (o <music-layout>) (point <point>))
  (find-system (.notation o) point))

(define-method (get-systems (o <music-layout>))
  (debugf 'layout "~a get-systems\n" (class-name (class-of o)))
  (get-systems (.notation o)))

(define (in-y-range? top bottom)
  (lambda (o) (let* ((y1 (y (.offset o)))
		     (y2 (+ y1 (box-height (.box o)))))
		(and (>= y1 top) (<= y2 bottom)))))

(define-method (urg-while-get-systems (o <music-layout>)
			    (top <number>) (bottom <number>))
  (while (.get-systems-request o) (yield))
  (set! (.get-systems-request o) #t)
  (let ((systems (get-systems- o top bottom)))
    (set! (.get-systems-request o) #f)
    systems))

(define get-systems-mutex (make-mutex))
(define-method (get-systems (o <music-layout>)
			    (top <number>) (bottom <number>))
  ;; FIXME: queue engrave system requests?
  (with-mutex get-systems-mutex (get-systems- o top bottom)))

(define-method (get-systems- (o <music-layout>)
			     (top <number>) (bottom <number>))
  (debugf 'layout "~a get-systems ~a..~a\n" (class-name (class-of o)) top bottom)
  (let* ((notation (or (.notation o) (engrave o)))
	 (systems (or (and-let* (((> (- bottom top) 0)))
				(filter (in-y-range? top bottom)
					(get-systems notation)))
		      '()))
	 (invalid (filter .invalid systems)))
    (debugf 'layout "invalid systems: ~a\n" invalid)
    (if (null? invalid)
	systems
	(begin
	  (engrave-system o (.number (car invalid)))
	  (get-systems- o top bottom)))))

(define-method (get-systems (o <boolean>)) '())

(define-method (engrave (o <music-layout>))
  (let* ((notation (engrave (.engraver o)))
	 (paper (.paper notation)))
    (set! (.notation o) notation)
    (set-paper (.canvas o) paper)
    (set-scroll-region (.canvas o) paper (page-count notation))
    notation))

(define-method (engrave-system (o <music-layout>) (number <integer>))
  (if (not (engrave-system (.engraver o) number))
      (add (.notation o) (make <system> :number number :box '((0 0) (0 0))))))

(define-method (last-rect (o <music-layout>))
  (and-let* ((system (last-system o))
	     (music (last-music-item system is-a-note-or-rest?)))
	    (paper-when-rect system music (.paper (.notation o)))))

(define-method (last-system (o <music-layout>))
  (and-let* ((systems (null-is-#f (get-systems (.notation o)))))
	    (last systems)))

(define-method (beyond-music-end? (o <music-layout>) (mouse <point>))
  (and-let* ((last-rect (last-rect o))
	     (system (last-system o))
	     (paper (.paper (.model o)))
	     (system-rect (paper-rect system paper))
	     ((intersect? system-rect (list->rect (append mouse '(0 0))))))
	    (> (x mouse) (+ (right last-rect) (/ (width last-rect) 5)))))

(define-method (point->pos (o <music-layout>) (mouse <point>))
  (and-let* ((paper (.paper (.model o)))
	     (system (get-system o mouse)))
	    (staff-position system paper (y mouse))))
