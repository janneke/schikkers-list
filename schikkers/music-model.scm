;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers music-model)
  ;; base
  :use-module (ice-9 and-let-star)
  :use-module (srfi srfi-1)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;; gtk
  :use-module (gnome glib)
  :use-module (gnome gobject)
  ;; user
  :use-module (schikkers ly)
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers music-modify)
  :use-module (schikkers notation-item)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  ;; user-gtk
  :use-module (schikkers gmisc)
  :export (<music-model>
	   dirty?
	   eat-dots
	   eat-duration
	   free-ahead
	   get-cursor
	   get-music
	   get-music-model
	   ly
	   notify
	   remove-or-replace-by-rest-and-backward
	   repitch
	   set-duration-add-rhythmic-event
	   slur
	   tie
	   .file-name
	   .key
	   .paper)
  :re-export (add-articulation
	      add-dynamic
	      add-lyrics
	      add-note
	      add-rest
	      add-rhythmic-event
	      add-staff
	      add-voice
	      delta-alteration
	      delta-octave
	      drag
	      find
	      lisp-value
	      re-tag!
	      set-articulation
	      set-clef
	      set-cursor
	      set-dots
	      set-duration
	      set-dynamic
	      set-key
	      set-time
	      walk))

(define-class <music-model> (<gobject>)
  (file-name :accessor .file-name :init-value "untitled.ly" :init-keyword :file-name)
  (music :accessor .music :init-value #f :init-keyword :music)
  (paper :accessor .paper :init-value #f :init-keyword :paper)

  (cursor :accessor .cursor :init-value #f)
  (key :accessor .key :accessor .key :init-value #f)
  (tag-table :accessor .tag-table :init-form (make-hash-table 0))

  :gsignal (list 'modified #f <gmusic> <gmusic>))
  
(define-method (initialize (o <music-model>) . initargs)
  (next-method)
  (re-tag! o)
  (init-cursor o)
  (init-key o)
  o)

(define-method (init-cursor (o <music-model>))
  (set! (.cursor o) (find-first (.music o) is-a-note-or-rest?)))

(define-method (init-key (o <music-model>))
  (set! (.key o) (.scale (or (find (.music o) is-a-key-change-event?)
			     (make <key-change-event>)))))

(define* (get-music-model args :key
			  (paper (default-paper))
			  (music (if (member "--test" (command-line))
				     (test-music) (start-music))))
  (let ((files (filter (lambda (x) (not (equal? (substring x 0 1) "-")))
		       (cdr args))))
    (if (> (length files) 0)
	(load-ly (car files))
	(make <music-model> :music music :paper paper))))

(define-method (dirty? (o <music-model>))
  #f)

(define-method (get-cursor (o <music-model>))
  (.cursor o))

(define-method (set-cursor (o <music-model>) (x <boolean>)))

(define-method (set-cursor (o <music-model>) (music <music>))
  (set! (.cursor o) music))

(define-method (free-ahead (o <music-model>))
  (free-ahead o (get-cursor o)))

(define-method (free-ahead (o <music-model>) (music <music>))
  (let loop ((music music) (duration 0))
    (let ((next (get-neighbor-note-or-rest music 1)))
      (if (eq? next music)
	  (if (or (=0 duration)
		  (is-a-rest? next))
	      (inf)
	      duration)
	  (if (and (>0 duration) (not (is-a-rest? music)))
	      duration
	      (loop next (+ (duration->number music) duration)))))))


;;;; Music interface
(define-method (re-tag! (o <music-model>))
  (let ((music (.music o)))
    (set! (.tag-table o) (make-hash-table 0))
    (re-tag! music 0 (.tag-table o))
    (set-start music 0))
  o)

(define-method (find (o <music-model>) (predicate <procedure>))
  (find (.music o) predicate))

(define-method (lisp-value (o <music-model>))
  (lisp-value (.music o)))

(define-method (lisp-value (o <music-model>) (predicate <procedure>))
  (lisp-value (.music o)) predicate)

(define-method (lisp-value (o <music-model>) (start <number>) (next <number>))
  (and-let* ((music (.music (re-tag! o))))
	    (lisp-value
	     music (lambda (x)
		     (and (< (.start x) next)
			  (or (>= (.start x) start)
			      (is-a? x <nested-music>)
			      (is-a? x <key-change-event>)
			      (is-a? x <time-signature-music>)))))))

(define-method (walk (o <music-model>) func collect)
  (walk (.music o) func collect))

(define-method (ly (o <music-model>))
  (ly-string (.music o)))

(define-method (get-music (o <music-model>))
  (.music o))

(define-method (get-music (o <music-model>) (tag <integer>))
  (hashq-ref (.tag-table o) tag))

(define-method (get-music (o <music-model>) (item <notation-item>))
  (let* ((cause (.cause item))
	 (name (cdr cause))
	 (input-tag (car cause))
	 (music (get-music o input-tag)))
    (if music music
	(and-let* ((symbol (string->symbol (string-unquote (symbol->string name))))
		   (is-a?-alist `((TimeSignature . ,is-a-time-signature?)
				  (Clef . ,is-a-clef-glyph?)
				  (ClefPosition . ,is-a-clef-position?)
				  (ClefMiddleC . ,is-a-clef-middle-c?)
				  (BarLine . ,is-a-bar-line?)))
		   (predicate? (assoc-get is-a?-alist symbol)))
		  (find o predicate?)))))


;;;; Modify music
(define-method (modify (o <music-model>) (music <boolean>) method . args) #f)

(define-method (notify (o <music-model>) (cursor <boolean>) (modified <music>))
  (emit o 'modified (make <gmusic> :music #f) (g modified))
  modified)

(define-method (notify (o <music-model>) (cursor <music>) (modified <music>))
  (emit o 'modified (g cursor) (g modified))
  modified)
      
(define-method (modify (o <music-model>) (cursor <music>) method . args)
  (and-let* ((modified (apply method (cons cursor args))))
	    (notify o cursor modified)))

(define-method (modify-at (o <music-model>) (cursor <music>) method . args)
  (apply modify (cons* o cursor method args)))

(define-method (modify-at-cursor (o <music-model>) . rest)
  (apply modify-at (cons* o (get-cursor o) rest)))

(define-method (add-lyric (o <music-model>) (cursor <lyric-event>))
  (modify-at o cursor add-lyric))

(define-method (add-rhythmic-event (o <music-model>) (cursor <music>) duration)
  (modify-at o cursor add-rhythmic-event duration))

(define-method (add-note (o <music-model>) (cursor <music>) (duration <integer>)
			 (octave <integer>) (step <integer>) (key <list>))
  (modify-at o cursor add-note duration octave step key))

(define-method (add-note (o <music-model>) (cursor <music>) (duration <integer>)
			 (octave <integer>) (step <integer>))
  (add-note o cursor duration octave step (.key o)))

(define-method (add-note (o <music-model>) (cursor <music>) (duration <integer>)
			 (step <integer>) (key <list>))
  (modify-at o cursor add-note duration step key))

(define-method (add-note (o <music-model>) (cursor <music>) (duration <integer>)
			 (step <integer>))
  (add-note o cursor duration step (.key o)))

(define-method (add-rest (o <music-model>) (cursor <music>) duration)
  (modify-at o cursor add-rest duration))

(define-method (remove-character (o <music-model>) (cursor <music>))
  (modify-at o cursor remove-character))

(define-method (remove-or-replace-by-rest-and-backward (o <music-model>)
						       (cursor <music>))
  (modify-at o cursor (if (is-last-rhythmic-event? cursor)
			  remove-rhythmic-event-and-backward
			  replace-by-rest)))

(define-method (remove-rhythmic-event-and-backward (o <music-model>)
						   (cursor <music>))
  (modify-at o cursor remove-rhythmic-event-and-backward))

(define-method (set-duration-add-rhythmic-event (o <music-model>)
						(cursor <music>)
						(duration <boolean>))
  (add-rhythmic-event o cursor duration))

(define-method (set-duration-add-rhythmic-event (o <music-model>)
						(cursor <music>)
						(duration <integer>))
  (set-duration cursor duration)
  (add-rhythmic-event o cursor duration))

(define-method (add-context (o <music-model>) (cursor <music>)
			    (procedure <generic>))
  (let* ((parent-modified (procedure cursor))
	 (parent (car parent-modified))
	 (pp (.parent parent))
	 (modified (cdr parent-modified)))
    (if (not pp)
	(set! (.music o) parent))
    (emit o 'modified (g cursor) (g modified))))

(define-method (add-articulation (o <music-model>) (cursor <music>)
				 (articulation <symbol>))
  (modify-at o cursor add-articulation articulation))

(define-method (add-dynamic (o <music-model>) (cursor <music>)
				 (dynamic <symbol>))
  (modify-at o cursor add-dynamic dynamic))

(define-method (add-lyrics (o <music-model>) (cursor <music>))
  (add-context o cursor add-lyrics))

(define-method (add-staff (o <music-model>) (cursor <music>))
  (add-context o cursor add-staff))

(define-method (add-voice (o <music-model>) (cursor <music>))
  (add-context o cursor add-voice))

(define-method (add-lyrics (o <music-model>)) (add-lyrics o (get-cursor o)))
(define-method (add-staff (o <music-model>)) (add-staff o (get-cursor o)))
(define-method (add-voice (o <music-model>)) (add-voice o (get-cursor o)))

(define-method (drag (o <music-model>) (cursor <music>) (point <point>))
  (modify-at o cursor drag point))

(define-method (delta-alteration (o <music-model>) (cursor <music>) dir . rest)
  (apply modify-at (cons* o cursor delta-alteration dir rest)))

(define-method (delta-octave (o <music-model>) (cursor <music>) dir)
  (modify-at o cursor delta-octave dir))

(define-method (repitch (o <music-model>) (cursor <music>)
			(octave <integer>) (step <integer>))
  (set-octave cursor octave)
  (set-pitch cursor step (.key o))
  (notify o cursor cursor))

(define-method (set-articulation (o <music-model>) (cursor <music>)
				 (articulation <symbol>))
  (modify-at o cursor set-articulation articulation))

(define-method (set-clef (o <music-model>) (cursor <music>)
			 (glyph <symbol>)
			 (pos <integer>) (middle-c <integer>))
  (modify-at o cursor set-clef glyph pos middle-c))

(define-method (set-dots (o <music-model>) (cursor <music>) (dots <integer>))
  (modify-at o cursor set-dots dots))

(define-method (eat-dots (o <music-model>) (cursor <music>) (dots <integer>)) #f)

(define-method (eat-dots (o <music-model>) (cursor <note-event>) (dots <integer>))
  (let* ((log (.log (.duration cursor)))
	 (d (expt 2 log))
	 (n (/ 1 d)))
    (if (= 0 dots)
	(begin
	  (fill-rests cursor (* 0.75 n))
	  (modify-at o cursor set-dots dots))
	(let ((free-ahead (free-ahead o cursor))
	      (dn (* (if (= dots 1) 1.5 1.75) n)))
	  (when (>= free-ahead dn)
		(eat-notes-and-rests cursor dn)
		(modify-at o cursor set-dots dots))))))

(define-method (eat-dots (o <music-model>) (cursor <rest-event>) (dots <integer>))
  (let* ((log (.log (.duration cursor)))
	 (d (expt 2 log))
	 (n (/ 1 d)))
    (if (= 0 dots)
	(begin
	  (fill-rests cursor (* 0.75 n))
	  (modify-at o cursor set-dots dots))
	(let ((free-ahead (free-ahead o cursor))
	      (dn (* (if (= dots 1) 1.5 1.75) n)))
	  (eat-notes-and-rests cursor dn)
	  (modify-at o cursor set-dots dots)))))

(define-method (eat-duration (o <music-model>) (cursor <music>) (d <integer>) . rest)
  #f)

(define-method (eat-duration (o <music-model>) (cursor <note-event>) (d <integer>) . rest)
  (let* ((n (/ 1 d))
	 (duration (.duration cursor)))
    (if (< d (expt 2 (.log duration)))
	(let ((free-ahead (free-ahead o cursor)))
	  (when (>= free-ahead n)
		(eat-notes-and-rests cursor n)
		(apply set-duration (cons* o cursor d rest))))
	(let ((n (/ 1 d))
	      (cn (/ 1 (expt 2 (.log duration)))))
	  (fill-rests cursor (- cn n))
	  (apply set-duration (cons* o cursor d rest))))))

(define-method (eat-duration (o <music-model>) (cursor <rest-event>) (d <integer>) . rest)
  (eat-notes-and-rests cursor (/ 1 d))
  (apply set-duration (cons* o cursor d rest)))

(define-method (set-duration (o <music-model>) (cursor <music>) (d <integer>) . rest)
  (apply modify-at (cons* o cursor set-duration d rest)))

(define-method (set-dynamic (o <music-model>) (cursor <music>)
				 (dynamic <symbol>))
  (modify-at o cursor set-dynamic dynamic))

(define-method (set-key (o <music-model>) (cursor <music>) (name <symbol>))
  (modify-at o cursor set-key name)
  (set! (.key o) (.scale cursor)))

(define-method (set-key (o <music-model>) (cursor <music>)
			(tonic <integer>) (alteration <number>) (scale <list>))
  (set! (.step (.tonic cursor)) tonic)
  (set! (.alteration (.tonic cursor)) alteration)
  (set! (.scale cursor) scale)
  (notify o cursor cursor))

(define-method (set-octave (o <music-model>) (cursor <music>)
			   (octave <integer>))
  (modify-at o cursor set-octave octave))

(define-method (set-pitch (o <music-model>) (cursor <music>) (step <integer>))
  (modify-at o cursor set-pitch step (.key o)))

(define-method (- (c <char>) . rest)
  (apply - (map char->integer (cons* c rest))))

(define-method (set-relative-pitch (o <music-model>) (cursor <music>)
				   (c <char>))
  (set-relative-pitch o cursor (modulo (+ 7 (- c #\c)) 7)))

(define-method (set-relative-pitch (o <music-model>) (cursor <music>)
				   (step <integer>))
  (modify-at o cursor set-relative-pitch step (.key o)))

(define-method (set-rest (o <music-model>) (cursor <music>) (r <symbol>))
  (modify-at o cursor set-rest r))

(define-method (set-time (o <music-model>) (cursor <music>) (n <integer>) (d <integer>) . style)
  (modify-at o cursor set-time n d (and (not (null? style))
					(eq? (car style) 'numeric))))

(define-method (slur (o <music-model>) (start <music>) (stop <music>))
  (set-slur start -1)
  (modify-at o stop set-slur 1))

(define-method (tie (o <music-model>) (cursor <music>) (tie? <boolean>))
  (modify-at o cursor set-tie tie?))
