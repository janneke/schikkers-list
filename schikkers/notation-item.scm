;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define-module (schikkers notation-item)
  ;; base
  :use-module (ice-9 and-let-star)
  ;; oops
  :duplicates (merge-generics last)
  :use-module (oop goops)
  :use-module (schikkers generics)
  ;;
  :use-module (schikkers gnome music-canvas-text)
  :use-module (schikkers misc)
  :use-module (schikkers music)
  :use-module (schikkers paper)
  :use-module (schikkers point)
  :export (<notation-item>
	   <notation-item-glyph>
	   <notation-item-line>
	   <notation-item-path>
	   <notation-item-polygon>
	   <notation-item-rectangle>
	   <notation-item-text>
	   color
	   destroy-canvas-item
	   list->notation-item
	   music-predicate
	   notify-color
	   paper-rect
	   set-music
	   set-system
	   text-color
	   .bbox
	   .canvas-item
	   .cause
	   .fields
	   .offset
	   .rect
	   .staff-offset
	   .system)
  :re-export (.music))

(define-class <notation-item> ()
  (offset :accessor .offset :init-value #f :init-keyword :offset)
  (fields :accessor .fields :init-form '() :init-keyword :fields)

  (bbox :accessor .bbox :init-value #f :init-keyword :bbox)
  (rect :accessor .rect :init-value #f :init-keyword :rect)

  (cause :accessor .cause :init-value #f :init-keyword :cause)
  (system :accessor .system :init-value -1 :init-keyword :system)

  (canvas-item :accessor .canvas-item :init-value #f)
  (music :accessor .music :init-value #f))

(define (display-address o file)
  (display (number->string (object-address o) 16) file))

(define-method (display-slots (o <notation-item>) port)
  (display (.offset o) port)
  (display #\space port)
  (display (.fields o) port))

(define-method (write (o <notation-item>) port)
  (display "#<" port)
  (display (class-name (class-of o)) port)
  (display #\space port)
  (display-address o port)
  (display #\space port)
  (display-slots o port)
  (display #\space port)
  (display #\> port))

(define-class <notation-item-glyph> (<notation-item>))
(define-class <notation-item-line> (<notation-item>))
(define-class <notation-item-path> (<notation-item>))
(define-class <notation-item-polygon> (<notation-item>))
(define-class <notation-item-rectangle> (<notation-item>))
(define-class <notation-item-text> (<notation-item>))

(define-method (list->notation-item (o <list>))
  (make
      (case (caadr o)
	((drawline) <notation-item-line>)
	((utf-8) <notation-item-text>)
	((draw_round_box) <notation-item-rectangle>)
	((glyphshow) <notation-item-glyph>))
    :offset (car o)
    :fields (map object->string (cadr o))))
  
(define-method (destroy-canvas-item (o <notation-item>))
  (destroy (.canvas-item o))
  (set! (.canvas-item o) #f))

(define-method (set-music (o <notation-item>) (music <boolean>)))

(define-method (set-music (o <notation-item>) (music <music>))
  (set! (.music o) music))

(define-method (set-system (o <notation-item>) number)
  (set! (.system o) number))

(define (music-predicate predicate)
  (lambda (x) (and-let* ((music (.music x))) (predicate music))))

(define-method (paper-rect (o <notation-item>) (paper <paper>))
  (+ (.rect o) (unit paper margin)))

(define (color)
  (if (stealth?) "white" "black"))

(define-generic color)

(define (notify-color)
  (if (stealth?) (color) "red"))

(define (text-color)
  (if (stealth?) "lightgray" "black"))
   
(define-method (color (o <notation-item>))
  (if (stealth?) "lightgray" "black"))

(define-method (color (o <notation-item-line>))
  (if (stealth?) "white" (next-method)))

(define-method (color (o <notation-item-rectangle>))
  (if (stealth?) "white" (next-method)))

(define-method (color (o <notation-item-text>))
  (if (and (null-is-#f (debug?)) (not stealth?)) "darkgray") (next-method))
