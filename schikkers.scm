#! /bin/sh
# -*- scheme -*-
PATH=$(dirname $0):$PATH exec guile-gnome-2 --debug -L $(dirname $0) -s $0 "$@"
!#

;; This file is part of Schikkers List, a GUI platform for LilyPond.
;;
;; Copyright (C) 2010--2013  Jan Nieuwenhuizen <janneke@gnu.org>
;;
;; Schikkers List is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Schikkers List is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Schikkers List.  If not, see <http://www.gnu.org/licenses/>.

(define (platform)
  (string->symbol
   (string-downcase
    (car (string-tokenize (vector-ref (uname) 0) char-set:letter)))))

(define (must-relocate? prefix)
  (and (not (equal? prefix ".")) (not (string-prefix? "/usr" prefix))))

(define (guile-gnome-setup prefix)
  (for-each (lambda (x) (set! %load-path (append
					  (list (string-append prefix x))
					  %load-path)))
	    (if (equal? prefix ".")
		'("/" "/schikkers/guile-gnome-2")
		`("/share/schikkers/guile-gnome-2"
		  "/share/guile/site"
		  "/share/guile-gnome-2"
		  ,(string-append "/share/guile/" (effective-version)))))
  (if (eq? (platform) 'windows)
      (setenv "PATH" (string-append
		      (getenv "PATH") ";"
		      (string-append prefix "/lib/guile-gnome-2")))))

(define-public (get-prefix-dir)
  (let* ((exe (car (command-line)))
	 (bin-dir (dirname exe))
	 (prefix (dirname bin-dir)))
    prefix))

(define (handle-relocate)
  (let ((prefix (get-prefix-dir)))
    (guile-gnome-setup prefix)
    (if (must-relocate? prefix)
	((@ (schikkers relocate) relocate) prefix))))

(define (main args)
  (handle-relocate)
  (let* ((gui (sloppy-assoc '--gui ((@ (schikkers misc) command-line-alist))))
	 (web (sloppy-assoc '--web ((@ (schikkers misc) command-line-alist)))))
    (if web
	((@ (schikkers web app-server) main) args)
	((@ (schikkers gnome app) main) args)))
  (exit 0))

(main (command-line))
