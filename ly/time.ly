#(set-default-paper-size "a5")

\header {
  tagline = ""
}

\layout {
  \context {
    \Score
    \override BarNumber.break-visibility = #all-invisible
  }
  \context {
    \Voice
    \remove "Stem_engraver"
  }
  \context {
    \Staff
    \override TimeSignature.break-visibility = #end-of-line-invisible
    \remove "Clef_engraver"
    \remove "Bar_engraver"
    \remove "Bar_number_engraver"
  }
}

\paper {
  indent = 0
}

\relative c'
{
  \cadenzaOn
  \time 1/1 s8

  \time 2/1 s8
  \time 3/1 s8
  \time 4/1 s8
  \time 5/1 s8

  \break

  \time 2/2 s8
  \time 1/2 s8
  \override Staff.TimeSignature.style = #'numbered
  \time 2/2 s8
  \revert Staff.TimeSignature.style
  \time 3/2 s8
  \time 4/2 s8
  \time 5/2 s8
  \time 6/2 s8

  \break

  \time 4/4 s8
  \time 1/4 s8
  \time 2/4 s8
  \time 3/4 s8
  \override Staff.TimeSignature.style = #'numbered
  \time 4/4 s8
  \revert Staff.TimeSignature.style
  \time 5/4 s8
  \time 6/4 s8
  \time 7/4 s8
  \time 8/4 s8
  \time 9/4 s8
  \time 12/4 s8

  \break

  \time 1/8 s8
  \time 2/8 s8
  \time 3/8 s8
  \time 4/8 s8
  \time 5/8 s8
  \time 6/8 s8
  \time 7/8 s8
  \time 8/8 s8
  \time 9/8 s8
  \time 12/8 s8

  \break
  \time 1/16 s8
  \time 2/16 s8
  \time 3/16 s8
  \time 4/16 s8
  \time 5/16 s8
  \time 6/16 s8
  \time 7/16 s8
  \time 8/16 s8
  \time 9/16 s8
  \time 12/16 s8
}
