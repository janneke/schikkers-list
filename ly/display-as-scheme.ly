\include "ly/display-paper-settings.ly"

#(define-public (music-display-as-scheme parser music)
  (display-paper-settings $defaultpaper)
  (display-scheme-music music))

#(define toplevel-music-handler music-display-as-scheme)

#(define (score-display-as-scheme parser score)
  (display-paper-settings $defaultpaper)
  (display-scheme-music (ly:score-music score)))

#(define toplevel-score-handler score-display-as-scheme)

#(define (book-part-display-as-scheme parser book-part)
 #t
 ;; (format (current-output-port) "book-part: ~a\n" book-part)
 ;; (format (current-output-port) "book-parts: ~a\n" (ly:book-book-parts book))
 ;; (format (current-output-port) "book-scores: ~a\n" (ly:book-scores book)))
 ;; (display-scheme-music (ly:score-music (car (ly:book-scores book-part)))))
)

#(define toplevel-bookpart-handler book-part-display-as-scheme)

#(define (book-display-as-scheme parser book)
  (display-paper-settings (ly:book-paper book))
  (display-scheme-music (ly:score-music (car (ly:book-scores book)))))

#(define toplevel-book-handler book-display-as-scheme)
%#(define toplevel-book-handler (lambda (x y) (current-output-port)))
