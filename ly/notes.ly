\header {
  tagline = ""
}

\layout {
  \context {
    \Staff
    \remove "Staff_symbol_engraver"
    \remove "Key_engraver"
    \remove "Clef_engraver"
    \remove "Bar_engraver"
    \remove "Time_signature_engraver"
  }
}

\paper {
  page-breaking = #ly:one-line-breaking
  ragged-last = ##t
  ragged-right = ##t
}
\relative {
  \cadenzaOn
  \stemUp
  c'\longa c\breve c1 c2 c4 c4. c8 c16 c32 c64 c128
  r\longa r\breve r1 r2 r4 r4. r8 r16 r32 r64 r128
}
