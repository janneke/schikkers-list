#(use-modules (guile-user))

\score {
  {
    \set Staff.midiInstrument = #patch
    
    #(make-music
      'NoteEvent
      'duration (ly:make-duration log 0 1 1)
      'pitch
      (ly:make-pitch oct step alt))
  }
  \midi {
    \tempo 4 = 120
  }
}
