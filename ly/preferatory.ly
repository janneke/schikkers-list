\header {
  tagline = ""
}

\layout {
  \context {
    \Voice
%    \remove "Note_heads_engraver"
    \remove "Stem_engraver"
  }
  \context {
    \Staff
    \remove "Staff_symbol_engraver"
    \remove "Bar_engraver"
  }
}

\paper {
  page-breaking = #ly:one-line-breaking
  ragged-last = ##t
  ragged-right = ##t
}
\relative {
  \time 3/4
  \clef "G"
  s1
  \clef "C"
  s1
  \clef "F"
  s1
  cis,
  c!
  cisis
  c
  ces
  ceses
  \key a \major
  s1
  \key c \major
  s1
  \key es \major
  s1
  \bar ":|"
}
