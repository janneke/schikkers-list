#(set-default-paper-size "a5")

\header {
  tagline = ""
}

\layout {
  \context {
    \Score
    \override BarNumber.break-visibility = #all-invisible
  }
  \context {
    \Voice
    \remove "Note_heads_engraver"
    \remove "Stem_engraver"
  }
  \context {
    \Staff
    \remove "Bar_engraver"
    \remove "Bar_number_engraver"
    \remove "Clef_engraver"
    \remove "Key_engraver"
    \remove "Staff_symbol_engraver"
    \remove "Time_signature_engraver"
  }
}

\paper {
  indent = 0
}

{
  s^0
  s^1
  s^2
  s^3
  s^4
  s^5
}