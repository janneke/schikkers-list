#(set-default-paper-size "a6")

\paper
{
  ragged-right = ##f
  ragged-last = ##t

  indent = 0 \mm
  tagline = ""

  between-system-spacing = #'((space . 10) (minimum-distance . 6) (padding . 0))
}


\layout {
  \context {
    \Score
    \override BarNumber  #'break-visibility = #all-visible
  }
  \context {
    \Voice
    \remove "Note_heads_engraver"
    \consists "Completion_heads_engraver"
  }
}

\relative c'
{
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 

  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 

  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 

  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
  f f f f f f f f f f f f f f f f f f f f 
}