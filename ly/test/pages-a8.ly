#(set-default-paper-size "a8")

\paper
{
  left-margin = 4\mm
  tagline = ##f
  % http://lists.gnu.org/archive/html/bug-lilypond/2013-03/msg00123.html
  ragged-last-bottom = ##f
}

\layout {
%  system-system-space = #'((space . 10) (minimum-distance . 6) (padding . 0))
  \context {
    \Score
    \override BarNumber  #'break-visibility = #all-visible
  }
  \context {
    \Voice
    \remove "Note_heads_engraver"
    \consists "Completion_heads_engraver"
  }
}

\relative c'
{
  e1       f2 g4 a
  d, e f g a b c d
  d, e f g a b c d
  d, e f g a b c d

  d, e f g a b c d
  d, e f g a b c d
  d, e f g a b c d
  d, e f g a b c d

  d, e f g a b c d
  d, e f g a b c d
  d, e f g a b c d
  d, e f2   g1
  \bar "|."
}
