\include "ly/display-paper-settings.ly"

#(define (display-layout-settings layout)
  (for-each (lambda (symbol)
	     (let ((value (ly:output-def-lookup layout symbol)))
	      (if (not (null? value))
	       (format #t "(ly-~a ~S)\n" symbol value))))
   '(system-system-space)))

#(display-layout-settings $defaultlayout)
#(display-paper-settings $defaultpaper)
