\layout {
  \context {
    \Score
    \override BarNumber.break-visibility = #all-visible
  }
  \context {
    \Staff
%    \override StaffSymbol.width = #(- (ly:output-def-lookup $defaultpaper 'paper-width) (ly:output-def-lookup $defaultpaper 'left-margin-default-scaled) (ly:output-def-lookup $defaultpaper 'right-margin-default-scaled))
  }
  \context {
    \Voice
    \remove "Note_heads_engraver"
    \consists "Completion_heads_engraver"
  }
}

#(use-modules (lily))
#(use-modules (srfi srfi-39))
#(define *indent* (make-parameter 0))
#(define *force-duration* (make-parameter #f))
#(define-public *previous-duration* (make-parameter (ly:make-duration 2)))

#(define clef-name-alist #f)
#(define (memoize-clef-names clefs)
  "Initialize `clef-name-alist', if not already set."
  (if (not clef-name-alist)
      (set! clef-name-alist
            (map (lambda (name+vals)
                   (cons (cdr name+vals)
                         (car name+vals)))
                 clefs))))

#(define-public display-ly #f)
displayLy = #(define-music-function (parser location music) (ly:music?)
  (set! display-ly (lambda (x)
		    (let ((out (current-output-port)))
		     (set-current-output-port client-port)
		     (memoize-clef-names supported-clefs)
		     (parameterize ((*indent* 0)
				    (*previous-duration* (ly:make-duration 2))
				    (*force-duration* #f))
		      (display-lily-music x parser))
		     (set-current-output-port out)
		     music)))
  music)

\displayLy {c}

#(define (render-socket-music music socket)
  (let* ((score (ly:make-score music)) 
	 (book (ly:make-book-part (list score))))
   (ly:book-process-to-systems book $defaultpaper $defaultlayout socket)))

#(define client-port (current-output-port))

#(if #t
  (let ((s (socket PF_INET SOCK_STREAM 0)))
   (setsockopt s SOL_SOCKET SO_REUSEADDR 1)
       ;; Specific address?
       ;; (bind s AF_INET (inet-pton AF_INET "127.0.0.1") 2904)
       (bind s AF_INET INADDR_ANY 2904)
       (listen s 5)

       (simple-format #t "Listening for clients in pid: ~S" (getpid))
       (newline)

       (while #t
              (let* ((client-connection (accept s))
		     (start-time (get-internal-real-time))
                     (client-details (cdr client-connection))
                     (client (car client-connection)))
	        (set! client-port client)
	        (simple-format #t "Got new client connection: ~S"
                               client-details)
                (newline)
                (simple-format #t "Client address: ~S"
                               (gethostbyaddr
                                (sockaddr:addr client-details)))
                (newline)
                ;; Send back the greeting to the client port
                (display (format
			  "hello LilyPond ~a\n"
			  (lilypond-version))
			  client)

	       (let* ((question (read client))
		      ;; (x01 (simple-format (current-error-port) "QUESTION: ~S\n" question))
		      (music (eval question (current-module))))

		;;(simple-format (current-error-port) "QUESTION:>>>~a<<<<\n" question)
		;;(format #t "MUSIC:>>>~a<<<<\n" music)
		(render-socket-music music client)
		(display "<EOF>\n" client)
;;                (close client)
		(display (format "Finished. Time elapsed: ~a\n"
			  (/ (- (get-internal-real-time) start-time) (* 1.0 internal-time-units-per-second))
			  
			))
		(gc) ; do GC while app is refreshing the screen.
	      )))))


#(define test-exp (make-music
  'SequentialMusic
  'elements
  (list (make-music
          'EventChord
          'elements
          (list (make-music
                  'NoteEvent
		  'input-tag 42
                  'duration
                  (ly:make-duration 2 0 1 1)
                  'pitch
                  (ly:make-pitch -1 0 0))))))
)

#(render-socket-music test-exp "test")
