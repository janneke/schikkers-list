#(set-default-paper-size "a5")

\header {
  tagline = ""
}

\layout {
  \context {
    \Score
    \override BarNumber.break-visibility = #all-invisible
  }
  \context {
    \Voice
    \remove "Stem_engraver"
  }
  \context {
    \Staff
    \remove "Time_signature_engraver"
    \remove "Clef_engraver"
    \remove "Bar_engraver"
    \remove "Bar_number_engraver"
  }
}

\paper {
  indent = 0
}

\relative c'
{
  \key c \major s8

  \key g \major s8
  \key d \major s8
  \key a \major s8
  \key e \major s8
  \key b \major s8
  \key fis \major s8
  \key cis \major s8

  \break

  \key c \major s8

  \key f \major s8
  \key bes \major s8
  \key ees \major s8
  \key aes \major s8
  \key des \major s8
  \key ges \major s8
  \key ces \major s8

  \break

  \key a \minor s8

  \key e \minor s8
  \key b \minor s8
  \key fis \minor s8
  \key cis \minor s8
  \key gis \minor s8
  \key dis \minor s8
  \key ais \minor s8

  \break

  \key a \minor s8

  \key d \minor s8
  \key g \minor s8
  \key c \minor s8
  \key f \minor s8
  \key bes \minor s8
  \key ees \minor s8
  \key aes \minor s8
}
