\relative c \context Staff <<
  %%\context Voice = one { \voiceOne a'' b c }
  %%\context Voice = two { \voiceTwo d, e f }
  \context Voice = one { \stemUp a'' b c }
  \context Voice = two { \stemDown d, e f }
>>
