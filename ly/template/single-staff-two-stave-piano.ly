\relative c <<
  \context Staff = melody { c'' }
  \context PianoStaff <<
    \context Staff = one { c, }
    \context Staff = two { \clef bass c, }
  >>
>>
