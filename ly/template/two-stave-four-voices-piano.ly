\relative c \context PianoStaff <<
  \context Staff = one <<
    %%\context Voice = one { \voiceOne c'' }
    %%\context Voice = two { \voiceTwo a }
    \context Voice = one { \stemUp c'' }
    \context Voice = two { \stemDown a }
  >>
  \context Staff = two <<
    \clef bass
    %%\context Voice = three { \voiceOne c,, }
    %%\context Voice = four { \voiceTwo a }
    \context Voice = three { \stemUp c,, }
    \context Voice = four { \stemDown a }
  >>
>>
