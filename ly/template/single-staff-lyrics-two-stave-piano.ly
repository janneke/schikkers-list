\relative c <<
  \context Staff = melody { c'' }
  \addlyrics { See }
  \context PianoStaff <<
    \context Staff = one { c, }
    \context Staff = two { \clef bass c, }
  >>
>>
