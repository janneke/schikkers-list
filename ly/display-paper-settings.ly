#(define (display-paper-settings paper)
  (format #t "(ly-mm-to-unit ~S)\n" (/ 1 lily-unit->mm-factor))
  (for-each (lambda (symbol)
	     (let ((value (ly:output-def-lookup paper symbol)))
	      (if (not (null? value))
	       (format #t "(ly-~a ~S)\n" symbol (if (procedure? value) (procedure-name value) value)))))
   '(pt
     output-scale
     page-breaking
     mm
     staff-size
     staff-space
     landscape
     paper-width
     paper-height
     indent
     left-margin-default-scaled
     right-margin-default-scaled
     left-margin
     right-margin
     line-width
     top-margin
     bottom-margin
     unit-length)))
