#(set-default-paper-size "a5")

\header {
  tagline = ""
}

\layout {
  \context {
    \Score
    \override BarNumber.break-visibility = #all-invisible
  }
  \context {
    \Voice
    \remove "Stem_engraver"
    %\remove "Note_heads_engraver"
  }
  \context {
    \Staff
    \remove "Bar_engraver"
    \remove "Bar_number_engraver"
    \remove "Clef_engraver"
    \remove "Key_engraver"
    \remove "Staff_symbol_engraver"
    \remove "Time_signature_engraver"
  }
}

\paper {
  indent = 0
}

{
  \ottava 1
  c'32
  \ottava 0
  s4
  \ottava -1
  c,32
  \ottava 0
  s4
}
