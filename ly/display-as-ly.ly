#(use-modules (scm display-lily))

#(define-public (music-display parser music)
  "Top-level music handler"
  (display (music->lily-string music parser)))

#(define toplevel-music-handler music-display)

{ a b c }
