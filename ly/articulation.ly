#(set-default-paper-size "a5")

\header {
  tagline = ""
}

\layout {
  \context {
    \Score
    \override BarNumber.break-visibility = #all-invisible
  }
  \context {
    \Voice
    \remove "Note_heads_engraver"
    \remove "Stem_engraver"
  }
  \context {
    \Staff
    \remove "Bar_engraver"
    \remove "Bar_number_engraver"
    \remove "Clef_engraver"
    \remove "Key_engraver"
    \remove "Staff_symbol_engraver"
    \remove "Time_signature_engraver"
  }
}

\paper {
  indent = 0
}

{
s^\accent
s^\coda
s^\downbow
s^\downmordent
s^\downprall
s^\espressivo
s^\fermata
s^\flageolet
s^\halfopen
s^\lheel
s^\lineprall
s^\longfermata
s^\ltoe
s^\marcato
s^\mordent
s^\open
s^\portato
s^\prall
s^\pralldown
s^\prallmordent
s^\prallprall
s^\prallup
s^\reverseturn
s^\rheel
s^\rtoe
s^\segno
s^\shortfermata
s^\signumcongruentiae
s^\snappizzicato
s^\staccatissimo
s^\staccato
s^\stopped
s^\tenuto
s^\thumb
s^\trill
s^\turn
s^\upbow
s^\upmordent
s^\upprall
s^\varcoda
s^\verylongfermata
}