#(set-default-paper-size "a5")

\header {
  tagline = ""
}

\layout {
  \context {
    \Score
    \override BarNumber.break-visibility = #all-invisible
  }
  \context {
    \Voice
    \remove "Note_heads_engraver"
    \remove "Stem_engraver"
  }
  \context {
    \Staff
    \remove "Bar_engraver"
    \remove "Bar_number_engraver"
    \remove "Clef_engraver"
    \remove "Key_engraver"
    \remove "Staff_symbol_engraver"
    \remove "Time_signature_engraver"
  }
}

\paper {
  indent = 0
}

{
  s-\mp
  s-\p
  s-\pp
  s-\ppp
  s-\pppp
  s-\ppppp
  \break
  s-\mf
  s-\f
  s-\ff
  s-\fff
  s-\ffff
  s-\fffff
  \break
  s-\sp
  s-\spp
  s-\sf
  s-\sff
  s-\fp
  s-\sfz
  s-\rfz
}