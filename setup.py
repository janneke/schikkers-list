#! /usr/bin/python

import getopt
import os
import re
import sys

def system (command):
    result = os.system (command)
    if result:
        raise Exception ('''failed (%(result)s): %(command)s''' % locals ())

def main ():
    prefix = os.environ['HOME']
    srcdir = os.path.dirname (__file__)
    (options, files) = getopt.getopt (sys.argv[1:], '',
                                      ['prefix=', 'root='])
    command = None
    for (o, a) in options:
        if o == '--prefix':
            prefix = a
    for f in files:
        if f.startswith ('--prefix='):
            prefix = f.split ('=')[1]
        if f.startswith ('--prefix '):
            prefix = f.split (' ')[1]
        if f in ('dist', 'install', 'tags', 'web'):
            command = f

    if command == 'tags':
        system ('etags $(git ls-files "*.scm" "*.js" "*.html")')

    if command == 'install':
        system ('mkdir -p %(prefix)s/bin' % locals ())
        system ('cp -pv %(srcdir)s/schikkers.scm %(prefix)s/bin/schikkers' % locals ())
        system ('cp -pv %(srcdir)s/schikkers.scm %(prefix)s/bin/schikkers-list' % locals ())
        ver = '.'.join (map (str, sys.version_info[:2]))
        data = '%(prefix)s/share/schikkers' % locals ()
        site = '%(prefix)s/share/guile/site' % locals ()
        system ('mkdir -p %(site)s' % locals ())
        system ('mkdir -p %(data)s' % locals ())
        system ('tar -C %(srcdir)s -cf- schikkers | tar -C %(site)s/ -xf-' % locals ())
        system ('tar -C %(srcdir)s -cf- images input ly | tar -C %(data)s/ -xf-' % locals ())
        #system ('tar -C %(srcdir)s -cf- locale | tar -C %(prefix)s/ -xf-' % locals ())
    if command == 'dist':
        news = open ('NEWS').read ()
        m = re.match ('^UNRELEASED v(([0-9]+[.]){2}[0-9]+)', news)
        version = m.group (1)
        news = news.replace ('UNRELEASED', 'RELEASE')
        open ('NEWS', 'w').write (news)
        system ('PATH=$(pwd)/scripts:$PATH; git-chl > ChangeLog')
        system ('git add ChangeLog NEWS')
        system ('git commit -m "Release v%(version)s"' % locals ())
        system ('git tag release/%(version)s' % locals ())
        system ('git archive --format=tar --prefix=schikkers-list-%(version)s/ release/%(version)s | gzip > schikkers-list-%(version)s.tar.gz' % locals ())
    if command == 'web':
        system ('w3m -dump web/README.html > README')
        system ('scp -p web/*.html lilypond.org:/var/www/lilypond/schikkers-list')
        system ('scp -p web/css/*.css lilypond.org:/var/www/lilypond/schikkers-list/css')
        system ('scp -p web/images/*.png web/images/*.ico lilypond.org:/var/www/lilypond/schikkers-list/images')

if __name__ == '__main__':
    main ()
